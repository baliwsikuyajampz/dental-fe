(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-ranks-ranks-module"],{

/***/ "./src/app/pages/admin/ranks/ranks.component.html":
/*!********************************************************!*\
  !*** ./src/app/pages/admin/ranks/ranks.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n\n</header>\n\n<div class=\"card\">\n  <div class=\"toolbar toolbar--inner\">\n    <div class=\"toolbar__label\">Ranks List</div>\n\n    <div class=\"actions\">\n        <!-- <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\"></i> -->\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getRanks()\" tooltip=\"Refresh list\"></a>\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addRank()\" tooltip=\"New\"></a>\n    </div>\n\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getDoctorsList()\" tooltip=\"Close search box\"></i>\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getDoctorsList()\" tooltip=\"Search\"></i>\n\n    </div>\n    \n  </div>\n  <div class=\"card-body\">\n    <div class=\"table-responsive \">\n      <table class=\"table table-striped\">\n        <thead>\n          <tr>\n            <th>Rank name</th>\n            <th>Special Commissions</th>\n            <th>Is Active</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor = \"let data of rank_list\" (click)=\"editRank(data)\" class=\"hover-edit\">\n            <td>{{ data.rank_name }}</td>\n            <td>\n              <span *ngFor=\"let sub of data.procedure\">\n                {{ sub.procedure_name }}(<span *ngIf=\"sub.commission_type=='fixed'\">₱</span>{{ sub.commission_rate }}<span *ngIf=\"sub.commission_type=='percentage'\">%</span>) <br>\n              </span>\n            </td>\n            <td>\n              <i class=\"zmdi zmdi-check c-success\" *ngIf=\"data.is_active\"></i>\n              <i class=\"zmdi zmdi-close c-danger\" *ngIf=\"!data.is_active\"></i>\n            </td>\n          </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td colspan=\"3\">\n              <pagination (pageChanged)=\"pageChange($event)\"\n                class=\"justify-content-center\"\n                [totalItems]=\"total\"\n                [maxSize]=\"10\"\n                [itemsPerPage]=\"rows\" >\n              </pagination>\n            </td>\n          </tr>\n        </tfoot>\n      </table>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalRanks=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-lg modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5>{{mode == 'add' ? 'Add Rank' : 'Edit Rank'}}</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n              <div class=\"form-group\">\n                  <label>Rank name<span style=\"color:red\">*</span></label>\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"rank_name\" placeholder=\"Rank name...\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n            <div class=\"form-group\">\n                <label>Procedure</label>\n                <ng-select\n                  [items]=\"procedures_list\"\n                  bindLabel=\"procedure_name\"\n                  placeholder=\"Select a Procedure\"\n                  groupBy=\"category\"\n                  [multiple]=\"true\"\n                  (change)=\"procedureChanged(procedure)\"\n                  [(ngModel)]=\"procedure\">\n                </ng-select>\n                <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"table-responsive \">\n            <table class=\"table table-striped table-sales\">\n              <thead>\n                <tr>\n                  <th>Procedure</th>\n                  <th colspan=\"2\">Rate</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngIf=\"procedure.length==0\">\n                  <td colspan=\"3\" align=\"center\">Select a Procedure First.</td>\n                </tr>\n                <tr *ngFor=\"let data_proc of procedure\">\n                  <td>\n                    {{ data_proc.procedure_name }}\n                  </td>\n                  <td>\n                    <div class=\"form-group\">\n                      <input type=\"number\" class=\"form-control\" min=\"1\" (click)=\"$event.target.select()\" [(ngModel)]=\"data_proc.commission_rate\" (ngModelChange)=\"data_proc.commission_rate = $event\" placeholder=\"0\">\n                      <i class=\"form-group__bar\"></i>\n                    </div>\n                  </td>\n                  <td>\n                    <div class=\"form-group\">\n                      <select class=\"form-control\" [(ngModel)]=\"data_proc.commission_type\" >\n                        <option value=\"percentage\" default>Percentage</option>\n                        <option value=\"fixed\">Fixed</option>\n                      </select>\n                      <i class=\"form-group__bar\"></i>\n                    </div>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n            <div class=\"checkbox checkbox--inline\">\n              <input type=\"checkbox\" id=\"customCheck4\" [(ngModel)]=\"isActive\">\n              <label class=\"checkbox__label\" for=\"customCheck4\">Is Active</label>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveRanks()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalRanks.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/ranks/ranks.component.scss":
/*!********************************************************!*\
  !*** ./src/app/pages/admin/ranks/ranks.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL3JhbmtzL3JhbmtzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/admin/ranks/ranks.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/admin/ranks/ranks.component.ts ***!
  \******************************************************/
/*! exports provided: RanksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RanksComponent", function() { return RanksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_5__);






var RanksComponent = /** @class */ (function () {
    function RanksComponent(API, growlService) {
        this.API = API;
        this.growlService = growlService;
        this.pageTitle = "Ranks";
        this.widgetSubTitle = "Manage Ranks for Dentists";
        this.key = null;
        this.rank_name = "";
        this.procedure = [];
        this.isActive = true;
        this.mode = "add";
        this.procedures_list = [];
        this.page = 1;
        this.rows = 10;
        this.rank_list = [];
        this.total = 0;
    }
    RanksComponent.prototype.ngOnInit = function () {
        this.getProcedures();
        this.getRanks();
    };
    RanksComponent.prototype.addRank = function () {
        this.clearFields();
        this.modalRanks.show();
    };
    RanksComponent.prototype.getProcedures = function () {
        var _this = this;
        this.API.post("lookups/get-procedures-ranks", {}).subscribe(function (response) {
            _this.procedures_list = response.devMessage;
        }, function (error) {
        });
    };
    RanksComponent.prototype.saveRanks = function () {
        var _this = this;
        if (this.rank_name == '') {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required field(s) Missing!", "warning");
        }
        else {
            this.API.post("ranks/save", {
                mode: this.mode,
                key: this.key,
                rank_name: this.rank_name,
                procedure: this.procedure,
                is_active: this.isActive
            }).subscribe(function (response) {
                if (response.devMessage) {
                    var msg = _this.mode == "add" ? "Record Saved." : "Record Updated.";
                    _this.growlService.addSuccess(msg);
                    _this.modalRanks.hide();
                    _this.clearFields();
                    _this.getRanks();
                }
            }, function (error) {
            });
        }
    };
    RanksComponent.prototype.clearFields = function () {
        this.mode = 'add';
        this.rank_name = "";
        this.procedure = [];
        this.isActive = true;
        this.key = null;
    };
    RanksComponent.prototype.getRanks = function () {
        var _this = this;
        this.API.post("ranks/get", {
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            _this.rank_list = response.devMessage;
            _this.total = response.total;
        }, function (error) {
        });
    };
    RanksComponent.prototype.editRank = function (data) {
        this.mode = 'edit';
        this.rank_name = data.rank_name;
        this.procedure = data.procedure;
        this.key = data.id;
        this.isActive = data.is_active;
        this.modalRanks.show();
    };
    RanksComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getRanks();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalRanks"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], RanksComponent.prototype, "modalRanks", void 0);
    RanksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ranks',
            template: __webpack_require__(/*! ./ranks.component.html */ "./src/app/pages/admin/ranks/ranks.component.html"),
            styles: [__webpack_require__(/*! ./ranks.component.scss */ "./src/app/pages/admin/ranks/ranks.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], ngx_growl__WEBPACK_IMPORTED_MODULE_5__["GrowlService"]])
    ], RanksComponent);
    return RanksComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/ranks/ranks.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/admin/ranks/ranks.module.ts ***!
  \***************************************************/
/*! exports provided: RanksModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RanksModule", function() { return RanksModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ranks_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ranks.component */ "./src/app/pages/admin/ranks/ranks.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-bootstrap/accordion */ "./node_modules/ngx-bootstrap/accordion/fesm5/ngx-bootstrap-accordion.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");















var ROUTE = [{ path: "", component: _ranks_component__WEBPACK_IMPORTED_MODULE_4__["RanksComponent"] }];
var RanksModule = /** @class */ (function () {
    function RanksModule() {
    }
    RanksModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _ranks_component__WEBPACK_IMPORTED_MODULE_4__["RanksComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"],
                ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_13__["AccordionModule"].forRoot(),
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_14__["BsDatepickerModule"].forRoot()
            ]
        })
    ], RanksModule);
    return RanksModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-ranks-ranks-module.js.map