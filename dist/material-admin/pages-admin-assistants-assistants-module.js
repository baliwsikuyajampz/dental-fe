(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-assistants-assistants-module"],{

/***/ "./src/app/pages/admin/assistants/assistants.component.html":
/*!******************************************************************!*\
  !*** ./src/app/pages/admin/assistants/assistants.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n</header>\r\n\r\n\r\n<div class=\"card\">\r\n  <div class=\"toolbar toolbar--inner\">\r\n    <div class=\"toolbar__label\">Assistants List</div>\r\n\r\n    <div class=\"actions\">\r\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\"></i>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getAssistants()\" tooltip=\"Refresh list\"></a>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addAssistant()\" tooltip=\"Add new assistant\"></a>\r\n    </div>\r\n\r\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\r\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \r\n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getAssistants()\" tooltip=\"Close search box\"></i>\r\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getAssistants()\" tooltip=\"Search\"></i>\r\n\r\n    </div>\r\n    \r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead>\r\n          <tr>\r\n            <th>Fullname</th>\r\n            <th>Branch</th>\r\n            <th>Is Active</th>\r\n            <th>Date Created</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let data of assistants_list\" class=\"hover-edit\" (click)=\"editAssistant(data)\">\r\n            <td>{{data.fullname}}</td>\r\n            <td>{{data.branch_name}}</td>\r\n            <td>\r\n              <i class=\"zmdi zmdi-check c-success\" *ngIf=\"data.is_active\"></i>\r\n              <i class=\"zmdi zmdi-close c-danger\" *ngIf=\"!data.is_active\"></i>\r\n            </td>\r\n            <td>\r\n              {{data.created_by}} <br>\r\n              {{data.date_created}}\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot>\r\n          <tr>\r\n            <td colspan=\"4\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                class=\"justify-content-center\"\r\n                [totalItems]=\"total\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<div class=\"modal fade\" bsModal #modalAssistants=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Assistant' : 'Edit Assistant'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"form-group\">\r\n              <label>Branch<span style=\"color:red\">*</span></label>\r\n              <ng-select\r\n                [items]=\"branch_list\"\r\n                bindLabel=\"branch_name\"\r\n                placeholder=\"Select a Branch\"\r\n                [(ngModel)]=\"branch\"\r\n                bindValue=\"id\"\r\n                >\r\n              </ng-select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n              <div class=\"form-group\">\r\n                  <label>First name<span style=\"color:red\">*</span></label>\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"fname\" placeholder=\"First name...\" id=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n              <div class=\"form-group\">\r\n                  <label>Middle name(Optional)</label>\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"mname\" placeholder=\"Middle name...\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n              <div class=\"form-group\">\r\n                  <label>Last name<span style=\"color:red\">*</span></label>\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"lname\" placeholder=\"Last name...\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\"></div>\r\n          <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"checkbox checkbox--inline\">\r\n              <input type=\"checkbox\" id=\"customCheck4\" [(ngModel)]=\"isActive\">\r\n              <label class=\"checkbox__label\" for=\"customCheck4\">Is Active</label>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveAssistant()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalAssistants.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/assistants/assistants.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/pages/admin/assistants/assistants.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL2Fzc2lzdGFudHMvYXNzaXN0YW50cy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/admin/assistants/assistants.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/admin/assistants/assistants.component.ts ***!
  \****************************************************************/
/*! exports provided: AssistantsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssistantsComponent", function() { return AssistantsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");








var AssistantsComponent = /** @class */ (function () {
    function AssistantsComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.branch_list = [];
        this.assistants_list = [];
        this.loggedUserInfo = null;
        this.total = 0;
        this.branch = null;
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.isActive = true;
        this.key = null;
        this.page = 1;
        this.rows = 10;
        this.searchKey = "";
        this.mode = "add";
        this.pageTitle = "Assistants";
        this.widgetSubTitle = "Manage Assistants";
        this.loggedUserInfo = this.common.getLoggedInUser();
    }
    AssistantsComponent.prototype.ngOnInit = function () {
        this.getBranches();
        this.getAssistants();
    };
    AssistantsComponent.prototype.addAssistant = function () {
        this.clearFields();
        this.modalAssistants.show();
    };
    AssistantsComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            _this.branch_list = response.devMessage;
        }, function (error) {
        });
    };
    AssistantsComponent.prototype.saveAssistant = function () {
        var _this = this;
        if (this.fname == "" || this.lname == "" || Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.branch)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required field(s) missing!", "warning");
        }
        else {
            this.API.post("assistants/save", {
                fname: this.fname,
                mname: this.mname,
                lname: this.lname,
                branch: this.branch,
                key: this.key,
                mode: this.mode,
                is_active: this.isActive,
                created_by: this.loggedUserInfo.id
            }).subscribe(function (response) {
                if (response.devMessage) {
                    var msg = _this.mode == "add" ? "Record Saved" : "Record Updated";
                    _this.growlService.addSuccess(msg);
                    _this.modalAssistants.hide();
                    _this.getAssistants();
                    _this.clearFields();
                }
            }, function (error) {
            });
        }
    };
    AssistantsComponent.prototype.clearFields = function () {
        this.mode = "add";
        this.key = null;
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.branch = null;
        this.isActive = true;
    };
    AssistantsComponent.prototype.getAssistants = function () {
        var _this = this;
        this.API.post("assistants/get", {
            search: this.searchKey,
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            _this.assistants_list = response.devMessage;
            _this.total = response.total;
        }, function (error) {
        });
    };
    AssistantsComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getAssistants();
    };
    AssistantsComponent.prototype.editAssistant = function (data) {
        this.mode = "edit";
        this.key = data.id;
        this.fname = data.fname;
        this.mname = data.mname;
        this.lname = data.lname;
        this.branch = data.branch_id;
        this.isActive = data.is_active;
        this.modalAssistants.show();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalAssistants"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], AssistantsComponent.prototype, "modalAssistants", void 0);
    AssistantsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-assistants',
            template: __webpack_require__(/*! ./assistants.component.html */ "./src/app/pages/admin/assistants/assistants.component.html"),
            styles: [__webpack_require__(/*! ./assistants.component.scss */ "./src/app/pages/admin/assistants/assistants.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_6__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
    ], AssistantsComponent);
    return AssistantsComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/assistants/assistants.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/admin/assistants/assistants.module.ts ***!
  \*************************************************************/
/*! exports provided: AssistantsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssistantsModule", function() { return AssistantsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _assistants_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assistants.component */ "./src/app/pages/admin/assistants/assistants.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");













var ROUTE = [
    {
        path: "",
        component: _assistants_component__WEBPACK_IMPORTED_MODULE_4__["AssistantsComponent"]
    },
];
var AssistantsModule = /** @class */ (function () {
    function AssistantsModule() {
    }
    AssistantsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _assistants_component__WEBPACK_IMPORTED_MODULE_4__["AssistantsComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], AssistantsModule);
    return AssistantsModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-assistants-assistants-module.js.map