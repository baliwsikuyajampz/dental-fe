(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-settings-sizes-sizes-module"],{

/***/ "./src/app/pages/admin/settings/sizes/sizes.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/settings/sizes/sizes.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n\r\n  <div class=\"actions actions\">\r\n    <div class=\"btn-demo\">\r\n      <button class=\"btn btn-primary btn--icon-text\" (click)=\"addSize()\"><i class=\"zmdi zmdi-plus\"></i> Add New</button>\r\n      <button class=\"btn btn-success btn--icon-text\"  (click)=\"getSizes()\"><i class=\"zmdi zmdi-refresh\"></i> Refresh</button>\r\n    </div>\r\n  </div>\r\n</header>\r\n\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead class=\"thead-dark\">\r\n        <tr>\r\n          <th width=\"110px\"></th>\r\n          <th>Size Code</th>\r\n          <th>Size Name</th>\r\n\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let item of sizes_list\">\r\n            <td>\r\n              <div class=\"btn-demo\">\r\n                <button type=\"button\" class=\"btn btn-success\"  (click)=\"editSize(item)\"><i class=\"zmdi zmdi-edit\"></i> Edit</button>\r\n              </div>\r\n            </td>\r\n            <td>{{ item.size_code }}</td>\r\n            <td>{{ item.size_name }}</td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"sizes_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"2\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                [totalItems]=\"sizes_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n            <td align=\"right\">\r\n              {{((page-1)*rows)+1 | number : '1.0-0'}} of {{sizes_count | number : '1.0-0'}}\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalSize=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog  modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Size' : 'Edit Size'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Code<span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"size_code\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Name<span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"size_name\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveSize()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalSize.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/settings/sizes/sizes.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/settings/sizes/sizes.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3Mvc2l6ZXMvQzpcXFVzZXJzXFxBRE1JTlxcRGVza3RvcFxcUHJvamVjdHNcXGRlbnRhbC1mZS9zcmNcXGFwcFxccGFnZXNcXGFkbWluXFxzZXR0aW5nc1xcc2l6ZXNcXHNpemVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUdNLG9CQUFvQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3Mvc2l6ZXMvc2l6ZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLWRlbW8ge1xyXG4gICAgJiA+IC5idG4sXHJcbiAgICAmID4gLmJ0bi1ncm91cCB7XHJcbiAgICAgIG1hcmdpbjogMCA1cHggMTBweCAwO1xyXG4gICAgfVxyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/admin/settings/sizes/sizes.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/admin/settings/sizes/sizes.component.ts ***!
  \***************************************************************/
/*! exports provided: SizesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SizesComponent", function() { return SizesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);





var SizesComponent = /** @class */ (function () {
    function SizesComponent(API) {
        this.API = API;
        this.pageTitle = "Sizes";
        this.widgetSubTitle = "Settings for Size Information";
        this.page = 1;
        this.rows = 10;
        this.sizes_list = [];
        this.sizes_count = 0;
        //form
        this.size_code = "";
        this.size_name = "";
        this.mode = "add";
    }
    SizesComponent.prototype.ngOnInit = function () {
        this.getSizes();
    };
    SizesComponent.prototype.getSizes = function () {
        var _this = this;
        this.API.post("settings/get-sizes", {
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                _this.sizes_list = response.devMessage;
                _this.sizes_count = response.total;
            }
        }, function (error) {
        });
    };
    SizesComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getSizes();
    };
    SizesComponent.prototype.addSize = function () {
        this.clearFields();
        this.modalSize.show();
    };
    SizesComponent.prototype.clearFields = function () {
        this.mode = "add";
        this.size_key = "";
        this.size_code = "";
        this.size_name = "";
    };
    SizesComponent.prototype.saveSize = function () {
        var _this = this;
        if (this.size_code == "" || this.size_name == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required Field(s) Missing", "warning");
        }
        else {
            this.API.post("settings/save-size", {
                size_code: this.size_code,
                size_name: this.size_name,
                mode: this.mode,
                key: this.size_key
            }).subscribe(function (response) {
                console.log(response);
                if (response.statusCode == 200) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Success", _this.mode == 'add' ? "Record has been Saved" : "Record has been Updated", "success");
                    _this.clearFields();
                    _this.getSizes();
                    _this.modalSize.hide();
                }
            }, function (error) {
            });
        }
    };
    SizesComponent.prototype.editSize = function (data) {
        this.size_key = data.id;
        this.size_code = data.size_code;
        this.size_name = data.size_name;
        this.mode = "edit";
        this.modalSize.show();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalSize"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], SizesComponent.prototype, "modalSize", void 0);
    SizesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sizes',
            template: __webpack_require__(/*! ./sizes.component.html */ "./src/app/pages/admin/settings/sizes/sizes.component.html"),
            styles: [__webpack_require__(/*! ./sizes.component.scss */ "./src/app/pages/admin/settings/sizes/sizes.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], SizesComponent);
    return SizesComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/settings/sizes/sizes.module.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/settings/sizes/sizes.module.ts ***!
  \************************************************************/
/*! exports provided: SizesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SizesModule", function() { return SizesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sizes_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sizes.component */ "./src/app/pages/admin/settings/sizes/sizes.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");













var ROUTE = [{ path: "", component: _sizes_component__WEBPACK_IMPORTED_MODULE_4__["SizesComponent"] }];
var SizesModule = /** @class */ (function () {
    function SizesModule() {
    }
    SizesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _sizes_component__WEBPACK_IMPORTED_MODULE_4__["SizesComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], SizesModule);
    return SizesModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-settings-sizes-sizes-module.js.map