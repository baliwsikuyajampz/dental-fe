(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-branch-head-patients-patients-module"],{

/***/ "./src/app/pages/branch-head/patients/list/list.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/branch-head/patients/list/list.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n\r\n</header>\r\n\r\n<div class=\"card\">\r\n  <div class=\"toolbar toolbar--inner\">\r\n    <div class=\"toolbar__label\">Patients List</div>\r\n\r\n    <div class=\"actions\">\r\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\"></i>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getPatientList()\" tooltip=\"Refresh list\"></a>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addPatient()\" tooltip=\"New Patient\"></a>\r\n    </div>\r\n\r\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\r\n        <input type=\"text\" placeholder=\"Search patient name (eg : Dela Cruz, Juan...)\" [(ngModel)]=\"searchKey\"> \r\n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getPatientList()\" tooltip=\"Close search box\"></i>\r\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getPatientList()\" tooltip=\"Search\"></i>\r\n\r\n    </div>\r\n    \r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead>\r\n        <tr>\r\n          <th width=\"200px\"></th>\r\n          <th>Branch</th>\r\n          <th>Fullname</th>\r\n          <th>Birthday</th>\r\n          <th>Address</th>\r\n          <th>Contact No.</th>\r\n          <th>Is Active</th>\r\n          <th>Date Created</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let data of patients_list\">\r\n            <td>\r\n              <div class=\"btn-demo\">\r\n                <button class=\"btn btn-primary\" [routerLink]=\"['/head/patient',data.id]\" ><i class=\"zmdi zmdi-eye\"></i></button>\r\n\r\n                <button class=\"btn btn-success\" (click)=\"editPatient(data)\"><i class=\"zmdi zmdi-edit\"></i></button>\r\n              </div>\r\n            </td>\r\n            <td>\r\n              {{ data.branch_name }}\r\n            </td>\r\n            <td>\r\n              {{ data.fullname }}\r\n            </td>\r\n            <td>\r\n              {{ data.birthday }}\r\n            </td>\r\n            <td>\r\n              {{ data.address }}\r\n            </td>\r\n            <td>\r\n              {{ data.contact_no }}\r\n            </td>\r\n            <td>\r\n              <i class=\"zmdi zmdi-check c-success\" *ngIf=\"data.is_active\"></i>\r\n              <i class=\"zmdi zmdi-close c-danger\" *ngIf=\"!data.is_active\"></i>\r\n            </td>\r\n            <td>\r\n              {{ data.date_created }}\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"patients_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"8\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                class=\"justify-content-center\"\r\n                [totalItems]=\"patient_total_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalPatient=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog modal-xl modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Patient' : 'Edit Patient'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>Branch<span style=\"color:red\">*</span></label>\r\n              <ng-select\r\n                  [items]=\"branches_list\"\r\n                  bindLabel=\"branch_name\"\r\n                  bindValue=\"id\"\r\n                  placeholder=\"Select Branch\"\r\n                  [(ngModel)]=\"branch\">\r\n                </ng-select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>First Name <span style=\"color:red\">*</span></label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: Juan\" [(ngModel)]=\"fname\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Last name<span style=\"color:red\">*</span></label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: Dela Cruz\" [(ngModel)]=\"lname\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Middle name</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: Santos\" [(ngModel)]=\"mname\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Address<span style=\"color:red\">*</span></label>\r\n              <textarea class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\" [(ngModel)]=\"address\"></textarea>\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Birthday<span style=\"color:red\">*</span></label>\r\n              <input type=\"text\" placeholder=\"Pick a date\" class=\"form-control\" bsDatepicker [bsConfig]=\"bsConfig\" [(ngModel)]=\"bday\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Religion</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\" [(ngModel)]=\"religion\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Nationality</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\" [(ngModel)]=\"nationality\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            \r\n          </div>\r\n\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>Occupation</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\" [(ngModel)]=\"occupation\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Office No.</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\" [(ngModel)]=\"office_no\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Gender</label>\r\n              <select class=\"form-control\" [(ngModel)]=\"gender\">\r\n                <option value=\"Male\">Male</option>\r\n                <option value=\"Female\">Female</option>\r\n              </select>\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Nickname</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\" [(ngModel)]=\"nickname\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Mobile/Home No.</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: ######\" [(ngModel)]=\"contact_no\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n\r\n            <h4 class=\"card-title\">For Minors</h4>\r\n            \r\n            <div class=\"form-group\">\r\n              <label>Parents/Guardian Name</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: Juan Dela Cruz\" [(ngModel)]=\"guardian_name\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label>Occupation</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: Dentist\" [(ngModel)]=\"guardian_occupation\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"checkbox checkbox--inline\">\r\n              <input type=\"checkbox\" id=\"customCheck4\" [(ngModel)]=\"isActive\">\r\n              <label class=\"checkbox__label\" for=\"customCheck4\">Is Active</label>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"savePatient()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalPatient.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/patients/list/list.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/branch-head/patients/list/list.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJhbmNoLWhlYWQvcGF0aWVudHMvbGlzdC9DOlxcVXNlcnNcXEFETUlOXFxEZXNrdG9wXFxQcm9qZWN0c1xcZGVudGFsLWZlL3NyY1xcYXBwXFxwYWdlc1xcYnJhbmNoLWhlYWRcXHBhdGllbnRzXFxsaXN0XFxsaXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUdNLG9CQUFvQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYnJhbmNoLWhlYWQvcGF0aWVudHMvbGlzdC9saXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bi1kZW1vIHtcclxuICAgICYgPiAuYnRuLFxyXG4gICAgJiA+IC5idG4tZ3JvdXAge1xyXG4gICAgICBtYXJnaW46IDAgNXB4IDEwcHggMDtcclxuICAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/branch-head/patients/list/list.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/branch-head/patients/list/list.component.ts ***!
  \*******************************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");








var ListComponent = /** @class */ (function () {
    function ListComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.pageTitle = "Patients";
        this.widgetSubTitle = "Manage Patients";
        this.patients_list = [];
        this.branches_list = [];
        this.patient_total_count = 0;
        this.key = null;
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.bday = null;
        this.gender = "Male";
        this.address = "";
        this.religion = "";
        this.nationality = "";
        this.occupation = "";
        this.office_no = "";
        this.nickname = "";
        this.contact_no = "";
        this.isActive = true;
        this.guardian_name = "";
        this.guardian_occupation = "";
        this.branch = null;
        this.loggedUserInfo = null;
        this.searchKey = "";
        this.page = 1;
        this.row = 10;
        this.mode = "add";
        this.loggedUserInfo = this.common.getLoggedInUser();
    }
    ListComponent.prototype.ngOnInit = function () {
        this.getBranches();
        this.getPatientList();
    };
    ListComponent.prototype.getPatientList = function () {
        var _this = this;
        this.API.post("patients/get", {
            page: this.page,
            row: this.row,
            search: this.searchKey
        }).subscribe(function (response) {
            _this.patients_list = response.devMessage;
            _this.patient_total_count = response.total;
        }, function (error) {
        });
    };
    ListComponent.prototype.addPatient = function () {
        this.clearFields();
        this.modalPatient.show();
    };
    ListComponent.prototype.savePatient = function () {
        var _this = this;
        if (this.branch === null) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Please select a branch", "warning");
            return;
        }
        if (this.fname == "" || this.lname == "" || Object(util__WEBPACK_IMPORTED_MODULE_6__["isNull"])(this.bday) || this.address == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required Field(s) Missing", "warning");
        }
        else {
            if (this.bday == "Invalid Date") {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Invalid Birthdate", "warning");
            }
            else {
                this.API.post("patients/save", {
                    mode: this.mode,
                    key: this.key,
                    fname: this.fname,
                    mname: this.mname,
                    lname: this.lname,
                    bday: this.bday,
                    gender: this.gender,
                    address: this.address,
                    religion: this.religion,
                    nationality: this.nationality,
                    occupation: this.occupation,
                    office_no: this.office_no,
                    created_by: this.loggedUserInfo.id,
                    contact_no: this.contact_no,
                    is_active: this.isActive,
                    guardian_name: this.guardian_name,
                    guardian_occupation: this.guardian_occupation,
                    branch: this.branch
                }).subscribe(function (response) {
                    if (response.devMessage == "exists") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Record Exists", "warning");
                    }
                    else {
                        _this.modalPatient.hide();
                        _this.growlService.addSuccess("Record has been saved.");
                        _this.clearFields();
                        _this.getPatientList();
                    }
                }, function (error) {
                });
            }
        }
    };
    ListComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getPatientList();
    };
    ListComponent.prototype.editPatient = function (data) {
        this.key = data.id;
        this.fname = data.fname;
        this.mname = data.mname;
        this.lname = data.lname;
        this.bday = data.birthday_raw;
        this.gender = data.gender;
        this.address = data.address;
        this.religion = data.religion;
        this.nationality = data.nationality;
        this.occupation = data.occupation;
        this.office_no = data.office_no;
        this.nickname = data.nickname;
        this.contact_no = data.contact_no;
        this.guardian_name = data.guardian_name;
        this.guardian_occupation = data.guardian_occupation;
        this.mode = "edit";
        this.modalPatient.show();
    };
    ListComponent.prototype.clearFields = function () {
        this.key = null;
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.bday = "";
        this.gender = "Male";
        this.address = "";
        this.religion = "";
        this.nationality = "";
        this.occupation = "";
        this.office_no = "";
        this.nickname = "";
        this.contact_no = "";
        this.mode = "add";
        this.guardian_occupation = "";
        this.guardian_name = "";
        this.branch = null;
    };
    ListComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            _this.branches_list = response.devMessage;
        }, function (error) {
        });
    };
    ListComponent.prototype.deletePatient = function (data) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Are you sure?",
            text: "Delete this Record?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes"
        }).then(function (result) {
            if (result.value) {
                _this.API.post("patients/delete-patient", {
                    key: data.id
                }).subscribe(function (response) {
                    if (response.devMessage == "exists") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "This record cannot be deleted", "warning");
                    }
                    else {
                        _this.growlService.addSuccess("Record Deleted.");
                        _this.getPatientList();
                    }
                }, function (error) {
                });
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalPatient"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], ListComponent.prototype, "modalPatient", void 0);
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/pages/branch-head/patients/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.scss */ "./src/app/pages/branch-head/patients/list/list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_5__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/patients/patients.component.html":
/*!********************************************************************!*\
  !*** ./src/app/pages/branch-head/patients/patients.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  patients works!ss\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/pages/branch-head/patients/patients.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/branch-head/patients/patients.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3BhdGllbnRzL3BhdGllbnRzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/branch-head/patients/patients.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/branch-head/patients/patients.component.ts ***!
  \******************************************************************/
/*! exports provided: PatientsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientsComponent", function() { return PatientsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PatientsComponent = /** @class */ (function () {
    function PatientsComponent() {
    }
    PatientsComponent.prototype.ngOnInit = function () {
    };
    PatientsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-patients',
            template: __webpack_require__(/*! ./patients.component.html */ "./src/app/pages/branch-head/patients/patients.component.html"),
            styles: [__webpack_require__(/*! ./patients.component.scss */ "./src/app/pages/branch-head/patients/patients.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PatientsComponent);
    return PatientsComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/patients/patients.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/branch-head/patients/patients.module.ts ***!
  \***************************************************************/
/*! exports provided: PatientsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientsModule", function() { return PatientsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _patients_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./patients.component */ "./src/app/pages/branch-head/patients/patients.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./list/list.component */ "./src/app/pages/branch-head/patients/list/list.component.ts");















var ROUTE = [
    {
        path: "",
        component: _patients_component__WEBPACK_IMPORTED_MODULE_4__["PatientsComponent"]
    },
    {
        path: "list",
        component: _list_list_component__WEBPACK_IMPORTED_MODULE_14__["ListComponent"]
    }
];
var PatientsModule = /** @class */ (function () {
    function PatientsModule() {
    }
    PatientsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _patients_component__WEBPACK_IMPORTED_MODULE_4__["PatientsComponent"],
                _list_list_component__WEBPACK_IMPORTED_MODULE_14__["ListComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_13__["BsDatepickerModule"].forRoot(),
            ]
        })
    ], PatientsModule);
    return PatientsModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-branch-head-patients-patients-module.js.map