(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-secretaries-secretaries-module"],{

/***/ "./src/app/pages/admin/secretaries/secretaries.component.html":
/*!********************************************************************!*\
  !*** ./src/app/pages/admin/secretaries/secretaries.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n<div class=\"card\">\n  <div class=\"toolbar toolbar--inner\">\n    <div class=\"toolbar__label\">Secretary List</div>\n\n    <div class=\"actions\">\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\"></i>\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getSecretary()\" tooltip=\"Refresh list\"></a>\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addSecretary()\" tooltip=\"Add new secretary\"></a>\n    </div>\n\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getSecretary()\" tooltip=\"Close search box\"></i>\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getSecretary()\" tooltip=\"Search\"></i>\n\n    </div>\n    \n  </div>\n  <div class=\"card-body\">\n    <div class=\"table-responsive \">\n      <table class=\"table table-striped\">\n        <thead>\n          <tr>\n            <th>Fullname</th>\n            <th>Branch</th>\n            <th>Is Active</th>\n            <th>Date Created</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let data of secretary_list\" class=\"hover-edit\" (click)=\"editSecretary(data)\">\n            <td>{{data.fullname}}</td>\n            <td>{{data.branch_name}}</td>\n            <td>\n              <i class=\"zmdi zmdi-check c-success\" *ngIf=\"data.is_active\"></i>\n              <i class=\"zmdi zmdi-close c-danger\" *ngIf=\"!data.is_active\"></i>\n            </td>\n            <td>\n              {{data.created_by}} <br>\n              {{data.date_created}}\n            </td>\n          </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td colspan=\"4\">\n              <pagination (pageChanged)=\"pageChange($event)\"\n                class=\"justify-content-center\"\n                [totalItems]=\"total\"\n                [maxSize]=\"10\"\n                [itemsPerPage]=\"rows\" >\n              </pagination>\n            </td>\n          </tr>\n        </tfoot>\n      </table>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #modalSecretary=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5>{{mode == 'add' ? 'Add Secretary' : 'Edit Secretary'}}</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n            <div class=\"form-group\">\n              <label>Branch<span style=\"color:red\">*</span></label>\n              <ng-select\n                [items]=\"branch_list\"\n                bindLabel=\"branch_name\"\n                placeholder=\"Select a Branch\"\n                [(ngModel)]=\"branch\"\n                bindValue=\"id\"\n                >\n              </ng-select>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n              <div class=\"form-group\">\n                  <label>First name<span style=\"color:red\">*</span></label>\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"fname\" placeholder=\"First name...\" id=\"\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n              <div class=\"form-group\">\n                  <label>Middle name(Optional)</label>\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"mname\" placeholder=\"Middle name...\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n              <div class=\"form-group\">\n                  <label>Last name<span style=\"color:red\">*</span></label>\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"lname\" placeholder=\"Last name...\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\"></div>\n          <div class=\"col-sm-12 col-md-12\">\n            <div class=\"checkbox checkbox--inline\">\n              <input type=\"checkbox\" id=\"customCheck4\" [(ngModel)]=\"isActive\">\n              <label class=\"checkbox__label\" for=\"customCheck4\">Is Active</label>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveSecretary()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalSecretary.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/secretaries/secretaries.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/admin/secretaries/secretaries.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL3NlY3JldGFyaWVzL3NlY3JldGFyaWVzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/admin/secretaries/secretaries.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/admin/secretaries/secretaries.component.ts ***!
  \******************************************************************/
/*! exports provided: SecretariesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecretariesComponent", function() { return SecretariesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");








var SecretariesComponent = /** @class */ (function () {
    function SecretariesComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.loggedUserInfo = null;
        this.branch_list = [];
        this.secretary_list = [];
        this.total = 0;
        this.branch = null;
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.isActive = true;
        this.key = null;
        this.mode = "add";
        this.page = 1;
        this.rows = 10;
        this.searchKey = "";
        this.pageTitle = "Secretaries";
        this.widgetSubTitle = "Manage Secretaries";
        this.loggedUserInfo = this.common.getLoggedInUser();
    }
    SecretariesComponent.prototype.ngOnInit = function () {
        this.getBranches();
        this.getSecretary();
    };
    SecretariesComponent.prototype.addSecretary = function () {
        this.modalSecretary.show();
    };
    SecretariesComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            _this.branch_list = response.devMessage;
        }, function (error) {
        });
    };
    SecretariesComponent.prototype.saveSecretary = function () {
        var _this = this;
        if (this.fname == "" || this.lname == "" || Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.branch)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required field(s) missing!", "warning");
        }
        else {
            this.API.post("secretary/save", {
                fname: this.fname,
                mname: this.mname,
                lname: this.lname,
                branch: this.branch,
                key: this.key,
                mode: this.mode,
                is_active: this.isActive,
                created_by: this.loggedUserInfo.id
            }).subscribe(function (response) {
                if (response.devMessage) {
                    var msg = _this.mode == "add" ? "Record Saved" : "Record Updated";
                    _this.growlService.addSuccess(msg);
                    _this.modalSecretary.hide();
                    _this.getSecretary();
                    _this.clearFields();
                }
            }, function (error) {
            });
        }
    };
    SecretariesComponent.prototype.getSecretary = function () {
        var _this = this;
        this.API.post("secretary/get", {
            search: this.searchKey,
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            _this.secretary_list = response.devMessage;
            _this.total = response.total;
        }, function (error) {
        });
    };
    SecretariesComponent.prototype.editSecretary = function (data) {
        this.mode = "edit";
        this.key = data.id;
        this.fname = data.fname;
        this.mname = data.mname;
        this.lname = data.lname;
        this.branch = data.branch_id;
        this.isActive = data.is_active;
        this.modalSecretary.show();
    };
    SecretariesComponent.prototype.clearFields = function () {
        this.mode = "add";
        this.key = null;
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.branch = null;
        this.isActive = true;
    };
    SecretariesComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getSecretary();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalSecretary"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], SecretariesComponent.prototype, "modalSecretary", void 0);
    SecretariesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-secretaries',
            template: __webpack_require__(/*! ./secretaries.component.html */ "./src/app/pages/admin/secretaries/secretaries.component.html"),
            styles: [__webpack_require__(/*! ./secretaries.component.scss */ "./src/app/pages/admin/secretaries/secretaries.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_6__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
    ], SecretariesComponent);
    return SecretariesComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/secretaries/secretaries.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/admin/secretaries/secretaries.module.ts ***!
  \***************************************************************/
/*! exports provided: SecretariesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecretariesModule", function() { return SecretariesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _secretaries_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./secretaries.component */ "./src/app/pages/admin/secretaries/secretaries.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");













var ROUTE = [
    {
        path: "",
        component: _secretaries_component__WEBPACK_IMPORTED_MODULE_4__["SecretariesComponent"]
    },
];
var SecretariesModule = /** @class */ (function () {
    function SecretariesModule() {
    }
    SecretariesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _secretaries_component__WEBPACK_IMPORTED_MODULE_4__["SecretariesComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], SecretariesModule);
    return SecretariesModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-secretaries-secretaries-module.js.map