(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-branch-head-transactions-transactions-module"],{

/***/ "./src/app/pages/branch-head/transactions/add/add.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/add/add.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n<div class=\"card\">\n  <div class=\"card-header\">\n    <h5>Transaction Details</h5>\n  </div>\n  <div class=\"card-body\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 col-md-12\">\n        <div class=\"form-group\">\n            <label>Patient</label>\n            <div style=\"display:flex;justify-content: center;\">\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"patient.fullname\" disabled placeholder=\"Select a Patient\">\n              <button (click)=\"searchPatient()\" class=\"btn btn-primary\"><i class=\"zmdi zmdi-search\"></i></button>\n            </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-sm-6 col-md-6\">\n        <div class=\"form-group\">\n          <label>Mode of Payment</label>\n          <select class=\"form-control\" [(ngModel)]=\"mop\" (ngModelChange)=\"mopChange()\">\n            <option>CASH</option>\n            <option>GCASH</option>\n            <option>CARD</option>\n            <option>PAYMAYA</option>\n            <option>BANK TRANSFER</option>\n            <option>SPLIT PAYMENT</option>\n            <option>PARTIAL PAYMENT</option>\n          </select>\n          <i class=\"form-group__bar\"></i>\n        </div>\n      </div>\n\n      <div class=\"col-sm-6 col-md-6\">\n        <div class=\"form-group\">\n          <label>Branch</label>\n          <ng-select\n            [items]=\"branch_list\"\n            bindLabel=\"branch_name\"\n            placeholder=\"Select a Branch\"\n            [(ngModel)]=\"branch\"\n            bindValue=\"id\"\n            >\n          </ng-select>\n          <i class=\"form-group__bar\"></i>\n        </div>\n      </div>\n    </div>\n  \n    <div class=\"row\">\n      <div class=\"col-sm-12 col-md-12\">\n        <div class=\"form-group\">\n            <label>Procedure</label>\n            <ng-select\n              [items]=\"procedures_list\"\n              bindLabel=\"procedure_name\"\n              placeholder=\"Select a Procedure\"\n              groupBy=\"category\"\n              [multiple]=\"true\"\n              (change)=\"procedureChanged(procedure);getTotalProcedurePrice()\"\n              [(ngModel)]=\"procedure\">\n            </ng-select>\n            <i class=\"form-group__bar\"></i>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"table-responsive \" style=\"min-height: 270px;\">\n        <table class=\"table table-striped table-sales\">\n          <thead>\n            <tr>\n              <th>Procedure</th>\n              <th>Quantity</th>\n              <th>Amount</th>\n              <th>Dentist</th>\n              <th>Assistant(s)</th>\n              <th>Secretaries</th>\n              <th colspan=\"2\">Date</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngIf=\"procedure.length==0\">\n              <td colspan=\"7\" align=\"center\">Select a procedure first</td>\n            </tr>\n            <tr *ngFor=\"let data_proc of procedure\">\n              <td>\n                {{ data_proc.procedure_name }}\n              </td>\n              <td style=\"max-width: 80px;\">\n                <div class=\"form-group\">\n                  <input type=\"number\" class=\"form-control\" min=\"1\" (click)=\"$event.target.select()\" [(ngModel)]=\"data_proc.quantity\" (ngModelChange)=\"data_proc.quantity = $event\" placeholder=\"0.00\" (change)=\"getTotalProcedurePrice()\" >\n                  <i class=\"form-group__bar\"></i>\n                </div>\n              </td>\n              <td style=\"max-width: 120px;\">\n                <div class=\"form-group amount\">\n                  <input type=\"number\" class=\"form-control\" min=\"1\" step=\"1\" (click)=\"$event.target.select()\" [(ngModel)]=\"data_proc.price\" (ngModelChange)=\"data_proc.price = $event\" placeholder=\"0.00\" (change)=\"getTotalProcedurePrice()\">\n                  <i class=\"form-group__bar\"></i>\n                </div>\n              </td>\n              <td>\n                <div class=\"form-group\">\n                  <ng-select\n                    [items]=\"doctors_list\"\n                    bindLabel=\"fullname\"\n                    bindValue=\"id\"\n                    groupBy=\"branch_name\"\n                    dropdownPosition=\"bottom\"\n                    placeholder=\"Select a Dentist\"\n                    [(ngModel)]=\"data_proc.dentist\"\n                    (ngModelChange)=\"data_proc.dentist = $event\"\n                    [disabled]=\"!data_proc.isDentistAllowed\"\n                    >\n                  </ng-select>\n                </div>\n              </td>\n              <td style=\"max-width: 200px;\">\n                <div class=\"form-group\">\n                  <ng-select\n                          [items]=\"assistants_list\"\n                          bindLabel=\"fullname\"\n                          bindValue=\"id\"\n                          groupBy=\"branch_name\"\n                          dropdownPosition=\"bottom\"\n                          [multiple]=\"true\"\n                          placeholder=\"Select Assistants\"\n                          [(ngModel)]=\"data_proc.assistants\"\n                          (ngModelChange)=\"data_proc.assistants = $event\"\n                          [disabled]=\"!data_proc.isAssistantAllowed\"\n                          >\n                  </ng-select>\n                </div>\n              </td>\n              <td style=\"max-width: 200px;\">\n                <div class=\"form-group\">\n                  <ng-select\n                          [items]=\"secretaries_list\"\n                          bindLabel=\"fullname\"\n                          bindValue=\"id\"\n                          groupBy=\"branch_name\"\n                          dropdownPosition=\"bottom\"\n                          [multiple]=\"true\"\n                          placeholder=\"Select Secretaries\"\n                          [(ngModel)]=\"data_proc.secretaries\"\n                          (ngModelChange)=\"data_proc.secretaries = $event\"\n                          [disabled]=\"!data_proc.isSecretaryAllowed\"\n                          >\n                  </ng-select>\n                </div>\n              </td>\n              <td>\n                <div class=\"form-group\">\n                  <input type=\"date\" class=\"form-control\" [(ngModel)]=\"data_proc.date\" (ngModelChange)=\"data_proc.date = $event\">\n                  <i class=\"form-group__bar\"></i>\n                </div>\n              </td>\n              <td>\n                <div class=\"form-group\">\n                  <input type=\"time\" class=\"form-control\" [(ngModel)]=\"data_proc.time\" (ngModelChange)=\"data_proc.time = $event\">\n                  <i class=\"form-group__bar\"></i>\n                </div>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n\n    <hr>\n\n    <div class=\"row\">\n      <div class=\"col-sm-6 col-md-6\">\n        <h5>Payment Details</h5>\n        <div class=\"table-responsive\">\n          <table class=\"table table-striped table-sales\">\n            <thead>\n              <th>Amount</th>\n              <th>Mode of Payment</th>\n              <th colspan=\"2\">Payment Date</th>\n              <th width=\"30px\"></th>\n            </thead>\n            <tbody>\n              <tr *ngIf=\"splitPartialArr.length==0\">\n                <td colspan='4' align=\"center\">Please add payments by pressing Add Payment button</td>\n              </tr>\n              <tr *ngFor=\"let splitData of splitPartialArr;let i = index \">\n                <td>\n                  <div class=\"form-group amount\">\n                    <input type=\"number\" class=\"form-control\" min=\"1\" step=\"1\" (click)=\"$event.target.select()\" [(ngModel)]=\"splitData.amount\" (ngModelChange)=\"splitData.amount = $event\" (change)=\"partialPaymentRemainingBalance()\" placeholder=\"0.00\" [disabled]=\"!isSplitOrPartial\">\n                    <i class=\"form-group__bar\"></i>\n                  </div>\n                </td>\n                <td>\n                  <div class=\"form-group\">\n                    <select class=\"form-control\" [(ngModel)]=\"splitData.mop\" [disabled]=\"!isSplitOrPartial\">\n                      <option default>CASH</option>\n                      <option>GCASH</option>\n                      <option>CARD</option>\n                      <option>PAYMAYA</option>\n                      <option>BANK TRANSFER</option>\n                    </select>\n                    <i class=\"form-group__bar\"></i>\n                  </div>\n                </td>\n                <td>\n                  <div class=\"form-group\">\n                    <input type=\"date\" class=\"form-control\" [(ngModel)]=\"splitData.payment_date\"> \n                    <i class=\"form-group__bar\"></i>\n                  </div>\n                </td>\n\n                <td>\n                  <div class=\"form-group\">\n                    <input type=\"time\" class=\"form-control\" [(ngModel)]=\"splitData.payment_time\" > \n                    <i class=\"form-group__bar\"></i>\n                  </div>\n                </td>\n                <td>\n                    <div class=\"actions\" *ngIf=\"isSplitOrPartial\">\n                      <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-close\" (click)=\"deleteRowPayment(i)\" ></a>\n                    </div>\n                </td>\n              </tr>\n            </tbody>\n            <tfoot *ngIf=\"isSplitOrPartial\">\n              <tr>\n                <td colspan=\"5\"><button class=\"btn btn-primary w-100\" (click)=\"addPaymentMethod()\"><i class=\"zmdi zmdi-plus\"></i> Add Payment</button></td>\n              </tr>\n            </tfoot>\n          </table>\n          <hr>\n        </div>\n      </div>\n\n      <div class=\"col-sm-6 col-md-6\">\n        <h5>Billing Details</h5>\n        <table align=\"left\" class=\"table table-striped\">\n          <tr>\n            <td>Total Amount:</td>\n          </tr>\n          <tr>\n            <td><h3>{{totalProcedurePrice | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</h3></td>\n          </tr>\n          <tr *ngIf=\"isSplitOrPartial\">\n            <td>Remaining Balance</td>\n          </tr>\n          <tr *ngIf=\"isSplitOrPartial\">\n            <td><h3>{{partialTotal | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</h3></td>\n          </tr>\n        </table>\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-sm-12 col-md-12\">\n        <h5>Remarks</h5>\n        <div class=\"form-group\">\n          <textarea class=\"form-control\" placeholder=\"Enter your remarks here...\" [(ngModel)]=\"remarks\"></textarea>\n          <i class=\"form-group__bar\"></i>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"card-footer\">\n    <div class=\"btn-demo pull-right\">\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"showConfirmationModal()\" [disabled]=\"disableSaveButton()\"><i class=\"zmdi zmdi-save\"></i> PROCEED</button>\n      <button type=\"button\" class=\"btn btn-danger\" (click)=\"cancelButton()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalSearchPatient=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\" style=\"border: 1px solid #888 !important;\">\n      <div class=\"modal-header\">\n        <h5>Search Patient</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-3\">\n              <div class=\"form-group\">\n                  <label class=\"col-form-label\">Last Name</label>\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"search_lname\" placeholder=\"Last name ....\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n          <div class=\"col-sm-12 col-md-3\">\n            <div class=\"form-group\">\n                <label class=\"col-form-label\">First Name</label>\n                <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"search_fname\" placeholder=\"First name ....\">\n                <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-3\">\n            <div class=\"form-group\">\n                <label class=\"col-form-label\">Middle Name</label>\n                <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"search_mname\" placeholder=\"Middle name ....\">\n                <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          \n          <div class=\"table-responsive\">\n            <h5>Search Result</h5>\n            <table class=\"table table-bordered\">\n              <thead>\n                <tr>\n                  <th>Action</th>\n                  <th>Fullname</th>\n                  <th>Birthday</th>\n                  <th>Address</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngIf=\"search_res_list.length==0\">\n                  <td colspan=\"4\" align=\"center\">No record(s) found.</td>\n                </tr>\n                <tr *ngFor=\"let search of search_res_list\">\n                  <td>\n                    <button class=\"btn btn-primary\" (click)=\"selectPatient(search)\">\n                      <i class=\"zmdi zmdi-arrow-left\"></i>\n                      Select\n                    </button>\n                  </td>\n                  <td>{{ search.fullname }} </td>\n                  <td>{{ search.birthday }} </td>\n                  <td>{{ search.address }} </td>\n                </tr>\n              </tbody>\n              <tfoot>\n                <tr>\n                  <td colspan=\"4\">\n                    <pagination (pageChanged)=\"pageChange($event)\"\n                      class=\"justify-content-center\"\n                      [totalItems]=\"search_res_total\"\n                      [maxSize]=\"10\"\n                      [itemsPerPage]=\"10\" >\n                    </pagination>\n                  </td>\n                </tr>\n              </tfoot>\n            </table>\n          </div>\n        </div>\n\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchPatientByName()\"><i class=\"zmdi zmdi-search\"></i> Search</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalSearchPatient.hide()\"><i class=\"zmdi zmdi-close\"></i> Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalSalesConfirmation=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-xl\">\n    <div class=\"modal-content\" style=\"border: 1px solid #888 !important;\">\n      <div class=\"modal-header\">\n        <h5>CONFIRMATION</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <h4 class=\"card-title\">Transaction Details</h4>\n          \n          <table style=\"width: 100%;\">\n            <tr>\n              <td>Patient</td>\n              <td>{{ patient.fullname }}</td>\n            </tr>\n            <tr>\n              <td>Mode of Payment</td>\n              <td>{{ mop }}</td>\n            </tr>\n            <tr>\n              <td>Branch</td>\n              <td>{{ getBranchname(branch) }}</td>\n            </tr>\n          </table>\n          \n        </div>\n        \n        <br>\n        <div class=\"row\">\n          <h4 class=\"card-title\">Procedure Details</h4>\n          <table class=\"table\">\n            <thead>\n              <th>Procedure</th>\n              <th>Price</th>\n              <th>Quantity</th>\n              <th>Dentist</th>\n              <th>Assistant</th>\n              <th>Secretary</th>\n              <th>Date</th>\n            </thead>\n            <tr *ngFor=\"let view_procedure of procedure\">\n              <td>{{ view_procedure.procedure_name}}</td>\n              <td>{{ view_procedure.price | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n              <td>{{ view_procedure.quantity}}</td>\n              <td>{{ getDentistName(view_procedure.dentist) }}</td>\n              <td>\n                <span *ngFor=\"let assistant of view_procedure.assistants\">\n                  {{ getAssistantsName(assistant) }} <br>\n                </span>\n                \n              </td>\n              <td>\n                <span *ngFor=\"let secretary of view_procedure.secretaries\">\n                  {{ getSecretaryName(secretary) }} <br>\n                </span>\n              </td>\n              <td>\n                {{ view_procedure.date +' '+view_procedure.time | date: 'MMM d, y h:mm a' }}\n              </td>\n\n\n            </tr>\n          </table>\n        </div>\n        <div class=\"row\">\n          <h4 class=\"card-title\">Payment Details</h4>\n  \n          <table class=\"table\">\n            <thead>\n              <th>Mode of Payment</th>\n              <th>Amount</th>\n              <th>Payment Date</th>\n            </thead>\n            <tr *ngFor = \"let payments of splitPartialArr\">\n              <td>\n                {{ payments.mop }}\n              </td>\n              <td>\n                {{ payments.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'  }}\n              </td>\n              <td>\n                {{ payments.payment_date +' '+payments.payment_time | date: 'MMM d, y h:mm a' }}\n              </td>\n            </tr>\n          </table>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveSalesTransaction()\"><i class=\"zmdi zmdi-save\"></i> SAVE</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalSalesConfirmation.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/add/add.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/add/add.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\n.btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n.table-sales tbody td input[type=text], .table-sales tbody td input[type=number], .table-sales tbody td select {\n  background-color: #fff; }\n.amount {\n  position: relative;\n  background-color: #dedede;\n  display: inline; }\n.amount input[type=number] {\n    margin-left: 10px; }\n.amount:before {\n  content: '₱';\n  position: absolute;\n  top: 6px;\n  left: 0;\n  z-index: 1; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJhbmNoLWhlYWQvdHJhbnNhY3Rpb25zL2FkZC9hZGQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3RyYW5zYWN0aW9ucy9hZGQvQzpcXFVzZXJzXFxBRE1JTlxcRGVza3RvcFxcUHJvamVjdHNcXGRlbnRhbC1mZS9zcmNcXGFwcFxccGFnZXNcXGJyYW5jaC1oZWFkXFx0cmFuc2FjdGlvbnNcXGFkZFxcYWRkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjs7RUFHTSxvQkFBb0IsRUFBQTtBQUl4QjtFQUtRLHNCQUFzQixFQUFBO0FBTWhDO0VBQ0ksa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixlQUFlLEVBQUE7QUFIbkI7SUFPTSxpQkFBaUIsRUFBQTtBQUl2QjtFQUNJLFlBQVM7RUFDVCxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLE9BQU87RUFDUCxVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9icmFuY2gtaGVhZC90cmFuc2FjdGlvbnMvYWRkL2FkZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5idG4tZGVtbyA+IC5idG4sXG4uYnRuLWRlbW8gPiAuYnRuLWdyb3VwIHtcbiAgbWFyZ2luOiAwIDVweCAxMHB4IDA7IH1cblxuLnRhYmxlLXNhbGVzIHRib2R5IHRkIGlucHV0W3R5cGU9dGV4dF0sIC50YWJsZS1zYWxlcyB0Ym9keSB0ZCBpbnB1dFt0eXBlPW51bWJlcl0sIC50YWJsZS1zYWxlcyB0Ym9keSB0ZCBzZWxlY3Qge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmOyB9XG5cbi5hbW91bnQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkZWRlZGU7XG4gIGRpc3BsYXk6IGlubGluZTsgfVxuICAuYW1vdW50IGlucHV0W3R5cGU9bnVtYmVyXSB7XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7IH1cblxuLmFtb3VudDpiZWZvcmUge1xuICBjb250ZW50OiAn4oKxJztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDZweDtcbiAgbGVmdDogMDtcbiAgei1pbmRleDogMTsgfVxuIiwiLmJ0bi1kZW1vIHtcclxuICAgICYgPiAuYnRuLFxyXG4gICAgJiA+IC5idG4tZ3JvdXAge1xyXG4gICAgICBtYXJnaW46IDAgNXB4IDEwcHggMDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC50YWJsZS1zYWxlcyB7XHJcbiAgICB0Ym9keSB7XHJcbiAgICAgIHRkIHtcclxuICAgICAgICBpbnB1dFt0eXBlPXRleHRdLGlucHV0W3R5cGU9bnVtYmVyXSxzZWxlY3RcclxuICAgICAgICB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbi5hbW91bnQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RlZGVkZTtcclxuICAgIGRpc3BsYXk6IGlubGluZTtcclxuXHJcbiAgICBpbnB1dFt0eXBlPW51bWJlcl1cclxuICAgIHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5hbW91bnQ6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6ICfigrEnO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA2cHg7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgei1pbmRleDogMTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/add/add.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/add/add.component.ts ***!
  \*********************************************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_9__);










var AddComponent = /** @class */ (function () {
    function AddComponent(API, router, common, growlService) {
        this.API = API;
        this.router = router;
        this.common = common;
        this.growlService = growlService;
        this.pageTitle = "Sales Transaction";
        this.loggedUserInfo = null;
        this.patient_list = [];
        this.doctors_list = [];
        this.procedures_list = [];
        this.assistants_list = [];
        this.sales_list = [];
        this.branch_list = [];
        this.secretaries_list = [];
        this.patient = { id: null, fullname: "" };
        this.procedure = [];
        this.doctor_fee = 0;
        this.mop = "CASH";
        this.amount = 0;
        this.branch = null;
        this.totalProcedurePrice = 0.00;
        //searchPatient Variables
        this.search_lname = "";
        this.search_fname = "";
        this.search_mname = "";
        this.search_res_list = [];
        this.search_res_total = 0;
        this.patient_page = 1;
        this.patient_row = 10;
        this.isSplitOrPartial = false;
        this.splitPartialArr = [];
        this.partialTotal = 0;
        this.remarks = "";
        this.loggedUserInfo = this.common.getLoggedInUser();
        this.branch = this.loggedUserInfo.branch;
    }
    AddComponent.prototype.ngOnInit = function () {
        this.getPatientList();
        this.getDoctorList();
        this.getProcedures();
        this.getAssistantsList();
        // this.getSalesList()
        this.getBranches();
        this.getSecretariesList();
        this.setPaymentMethod(this.mop);
        this.searchPatientByName();
    };
    AddComponent.prototype.getSalesList = function () {
        var _this = this;
        this.API.post("sales/get", {}).subscribe(function (response) {
            _this.sales_list = response.devMessage;
        }, function (error) {
        });
    };
    AddComponent.prototype.getPatientList = function () {
        var _this = this;
        this.API.post("lookups/get-patients", {}).subscribe(function (response) {
            _this.patient_list = response.devMessage;
        }, function (error) {
        });
    };
    AddComponent.prototype.getDoctorList = function () {
        var _this = this;
        this.API.post("lookups/get-doctors", {}).subscribe(function (response) {
            _this.doctors_list = response.devMessage;
        }, function (error) {
        });
    };
    AddComponent.prototype.getProcedures = function () {
        var _this = this;
        this.API.post("lookups/get-procedures", {}).subscribe(function (response) {
            _this.procedures_list = response.devMessage;
        }, function (error) {
        });
    };
    AddComponent.prototype.getAssistantsList = function () {
        var _this = this;
        this.API.post("lookups/get-assistants", {}).subscribe(function (response) {
            _this.assistants_list = response.devMessage;
        }, function (error) {
        });
    };
    AddComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            _this.branch_list = response.devMessage;
        }, function (error) {
        });
    };
    AddComponent.prototype.getSecretariesList = function () {
        var _this = this;
        this.API.post("lookups/get-secretaries", {}).subscribe(function (response) {
            _this.secretaries_list = response.devMessage;
        }, function (error) {
        });
    };
    AddComponent.prototype.searchPatient = function () {
        this.modalSearchPatient.show();
    };
    AddComponent.prototype.searchPatientByName = function () {
        var _this = this;
        this.API.post("patients/search-patient-by-name", {
            lname: this.search_lname,
            fname: this.search_fname,
            mname: this.search_mname,
            page: this.patient_page,
            rows: this.patient_row
        }).subscribe(function (response) {
            _this.search_res_list = response.devMessage;
            _this.search_res_total = response.total;
        }, function (error) {
        });
    };
    AddComponent.prototype.pageChange = function (event) {
        this.patient_page = event.page;
        this.searchPatientByName();
    };
    AddComponent.prototype.selectPatient = function (data) {
        this.patient.id = data.id;
        this.patient.fullname = data.fullname;
        this.modalSearchPatient.hide();
    };
    AddComponent.prototype.procedureChanged = function (data) {
        this.amount = data.price;
        this.staffFee(data);
    };
    AddComponent.prototype.staffFee = function (data) {
        this.doctor_fee = (data.commission_rate / 100) * this.amount;
    };
    AddComponent.prototype.getTotalProcedurePrice = function () {
        this.totalProcedurePrice = 0.00;
        for (var i = 0; i < this.procedure.length; i++) {
            if (this.procedure[i].proc_status == 'done' || this.procedure[i].proc_status == 'paid-pending') {
                this.totalProcedurePrice = parseFloat(this.totalProcedurePrice) + (parseFloat(this.procedure[i].price) * parseInt(this.procedure[i].quantity));
            }
        }
        switch (this.mop) {
            case "SPLIT PAYMENT":
                break;
            case "PARTIAL PAYMENT":
                break;
            default:
                this.splitPartialArr[0].amount = this.totalProcedurePrice;
                break;
        }
        if (this.mop == "PARTIAL PAYMENT") {
            this.partialPaymentRemainingBalance();
        }
    };
    AddComponent.prototype.saveSalesTransaction = function () {
        var _this = this;
        if (Object(util__WEBPACK_IMPORTED_MODULE_6__["isNull"])(this.patient.id)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Oops", "Please select a patient", "warning");
        }
        else if (Object(util__WEBPACK_IMPORTED_MODULE_6__["isNull"])(this.branch)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Oops", "Please select a branch", "warning");
        }
        else if (this.procedure.length == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Oops", "Please select atleast one procedure", "warning");
        }
        else {
            this.API.post("sales/save", {
                mop: this.mop,
                branch: this.branch,
                patient_id: this.patient.id,
                procedures: this.procedure,
                total_amount: this.totalProcedurePrice,
                created_by: this.loggedUserInfo.id,
                paymentMethod: this.splitPartialArr,
                remarks: this.remarks
            }).subscribe(function (response) {
                _this.modalSalesConfirmation.hide();
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                    title: "Transaction Saved.",
                    text: "Add another Transaction?",
                    icon: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes",
                    cancelButtonText: "Back to List"
                }).then(function (result) {
                    if (result.value) {
                        location.reload();
                    }
                    else {
                        _this.router.navigate(['/head/transactions/sales']);
                    }
                });
            }, function (error) {
            });
        }
    };
    AddComponent.prototype.cancelButton = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: "Are you sure?",
            text: "Cancel Transaction?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes"
        }).then(function (result) {
            if (result.value) {
                _this.router.navigate(['/head/transactions/sales']);
            }
        });
    };
    AddComponent.prototype.mopChange = function () {
        var mop = this.mop;
        this.splitPartialArr = [];
        switch (mop) {
            case "SPLIT PAYMENT":
                this.isSplitOrPartial = true;
                break;
            case "PARTIAL PAYMENT":
                this.isSplitOrPartial = true;
                break;
            default:
                this.isSplitOrPartial = false;
                this.setPaymentMethod(mop);
        }
    };
    AddComponent.prototype.setPaymentMethod = function (mop) {
        this.splitPartialArr = [];
        var currTime = moment__WEBPACK_IMPORTED_MODULE_9__().format("HH:MM");
        var currDate = moment__WEBPACK_IMPORTED_MODULE_9__().format("YYYY-MM-DD");
        var tmp = {
            amount: this.totalProcedurePrice,
            mop: mop,
            payment_date: currDate,
            payment_time: currTime
        };
        this.splitPartialArr.push(tmp);
    };
    AddComponent.prototype.addPaymentMethod = function () {
        var currTime = moment__WEBPACK_IMPORTED_MODULE_9__().format("HH:MM");
        var currDate = moment__WEBPACK_IMPORTED_MODULE_9__().format("YYYY-MM-DD");
        var tmp = {
            amount: this.totalProcedurePrice,
            mop: "CASH",
            payment_date: currDate,
            payment_time: currTime
        };
        this.splitPartialArr.push(tmp);
        this.partialPaymentRemainingBalance();
    };
    AddComponent.prototype.partialPaymentRemainingBalance = function () {
        this.partialTotal = 0;
        console.log("wew", this.splitPartialArr);
        for (var i = 0; i < this.splitPartialArr.length; i++) {
            this.partialTotal += this.splitPartialArr[i].amount;
        }
        this.partialTotal = parseFloat(this.totalProcedurePrice) - parseFloat(this.partialTotal);
    };
    AddComponent.prototype.deleteRowPayment = function (data) {
        this.splitPartialArr.splice(data, 1);
        this.partialPaymentRemainingBalance();
    };
    AddComponent.prototype.disableSaveButton = function () {
        if (this.procedure.length == 0) {
            return true;
        }
        else {
            if (this.mop == "SPLIT PAYMENT") {
                if (this.partialTotal < 0 || this.partialTotal > 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    };
    AddComponent.prototype.showConfirmationModal = function () {
        if (Object(util__WEBPACK_IMPORTED_MODULE_6__["isNull"])(this.patient.id)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Oops", "Please select a patient", "warning");
        }
        else if (Object(util__WEBPACK_IMPORTED_MODULE_6__["isNull"])(this.branch)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Oops", "Please select a branch", "warning");
        }
        else if (this.procedure.length == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Oops", "Please select atleast one procedure", "warning");
        }
        else {
            this.modalSalesConfirmation.show();
        }
    };
    AddComponent.prototype.getDentistName = function (item) {
        for (var i = 0; i < this.doctors_list.length; i++) {
            if (this.doctors_list[i].id == item) {
                return this.doctors_list[i].fullname;
            }
        }
    };
    AddComponent.prototype.getAssistantsName = function (item) {
        for (var i = 0; i < this.assistants_list.length; i++) {
            if (this.assistants_list[i].id == item) {
                return this.assistants_list[i].fullname;
            }
        }
    };
    AddComponent.prototype.getSecretaryName = function (item) {
        for (var i = 0; i < this.secretaries_list.length; i++) {
            if (this.secretaries_list[i].id == item) {
                return this.secretaries_list[i].fullname;
            }
        }
    };
    AddComponent.prototype.getBranchname = function (item) {
        for (var i = 0; i < this.branch_list.length; i++) {
            if (this.branch_list[i].id == item) {
                return this.branch_list[i].branch_name;
            }
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalSearchPatient"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["ModalDirective"])
    ], AddComponent.prototype, "modalSearchPatient", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalSalesConfirmation"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["ModalDirective"])
    ], AddComponent.prototype, "modalSalesConfirmation", void 0);
    AddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add',
            template: __webpack_require__(/*! ./add.component.html */ "./src/app/pages/branch-head/transactions/add/add.component.html"),
            styles: [__webpack_require__(/*! ./add.component.scss */ "./src/app/pages/branch-head/transactions/add/add.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_8__["GrowlService"]])
    ], AddComponent);
    return AddComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n\n<div class=\"card\">\n  <div class=\"toolbar toolbar--inner\">\n    <div class=\"toolbar__label\">Pending Payment List</div>\n\n    <div class=\"actions\">\n        <!-- <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\"></i> -->\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getPendingTransactions()\" tooltip=\"Refresh list\"></a>\n    </div>\n  </div>\n  <div class=\"card-body\">\n    <div class=\"table-responsive \">\n      <table class=\"table table-striped\">\n        <thead>\n          <tr>\n            <th width=\"70px\"></th>\n            <th>Branch</th>\n            <th>Total Amount</th>\n            <th>Total Payment</th>\n            <th>Remaining</th>\n            <th>Date Created</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngIf=\"pending_payment_list.length==0\">\n            <td colspan=\"6\" align=\"center\">No record(s) found.</td>\n          </tr>\n          <tr *ngFor = \"let data of pending_payment_list\">\n            <td>\n              <div class=\"btn-demo\">\n                <button class=\"btn btn-primary\" (click)=\"viewDetails(data)\" ><i class=\"zmdi zmdi-eye\"></i></button>\n              </div>\n            </td>\n            <td>{{ data.branch_name }}</td>\n            <td>{{ data.total_amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'  }}</td>\n            <td>{{ data.total_payment | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n            <td>{{ data.remaining_bal | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n            <td>\n              {{ data.date_created }}\n              <br>\n              {{ data.created_by }}\n            </td>\n          </tr>\n        </tbody>\n        <tfoot *ngIf=\"pending_payment_list.length!=0\">\n          <tr>\n            <td colspan=\"6\">\n              <pagination (pageChanged)=\"pageChange($event)\"\n                class=\"justify-content-center\"\n                [totalItems]=\"pending_payment_total\"\n                [maxSize]=\"10\"\n                [itemsPerPage]=\"rows\" >\n              </pagination>\n            </td>\n          </tr>\n        </tfoot>\n      </table>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalPayments=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-lg modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5>View Transaction Details</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <h5>Transaction Details</h5>\n          <div class=\"table-responsive \">\n            <table class=\"table table-striped\">\n              <thead>\n                <tr>\n                  <th>Patient</th>\n                  <th>Dentist</th>\n                  <th>Procedure</th>\n                  <th>Quantity</th>\n                  <th>Amount</th>\n                  <th>Total Amount</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let view of view_data.transaction_details\">\n                  <td>{{ view.patient_name }}</td>\n                  <td>{{ view.dentist_name }}</td>\n                  <td>{{ view.procedure_name }}</td>\n                  <td>{{ view.qty }}</td>\n                  <td>{{ view.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</td>\n                  <td>{{ view.total_amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n        <div class=\"row\">\n          <h5>Payment Details</h5>\n          <div class=\"table-responsive \">\n            <table class=\"table table-striped\">\n              <thead>\n                <tr>\n                  <th>Amount</th>\n                  <th>MOP</th>\n                  <th>Payment Date</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let view of view_data.payment_details\">\n                  <td>{{ view.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                  <td>{{ view.mop }}</td>\n                  <td>{{ view.payment_date }}</td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"table-responsive \">\n            <table class=\"table table-striped\">\n              <thead>\n                <tr>\n                  <th>Total Amount</th>\n                  <th>Total Payment</th>\n                  <th>Remaining Balance</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr>\n                  <td>{{ view_data.total_amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'  }}</td>\n                  <td>{{ view_data.total_payment | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                  <td>\n                    <span style=\"color:red\">\n                      {{ view_data.remaining_bal | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}\n                    </span>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"addPayment()\"><i class=\"zmdi zmdi-plus\"></i> ADD PAYMENT</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalPayments.hide()\"><i class=\"zmdi zmdi-close\"></i> CLOSE</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalAddPayment=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5>Add Payment</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <label class=\"col-sm-12 col-md-4 col-form-label\">Amount<span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-8\">\n              <div class=\"form-group\">\n                  <input type=\"number\" id=\"input_payment_amount\" class=\"form-control\" (click)=\"$event.target.select()\" [(ngModel)]=\"amount\" (ngModelChange)=\"amount= $event\" placeholder=\"0.00\" (change)=\"checkIfExceed()\">\n                  <div class=\"valid-feedback\">Fully Paid!</div>\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <label class=\"col-sm-12 col-md-4 col-form-label\">Payment Method<span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-8\">\n            <div class=\"form-group\">\n              <select class=\"form-control\" [(ngModel)]=\"mop\" (ngModelChange)=\"mopChange()\">\n                <option default>CASH</option>\n                <option>GCASH</option>\n                <option>CARD</option>\n                <option>PAYMAYA</option>\n                <option>BANK TRANSFER</option>\n              </select>\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <label class=\"col-sm-12 col-md-4 col-form-label\">Payment Date<span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-8\">\n            <div class=\"form-group\">\n              <input type=\"datetime-local\" placeholder=\"Pick a date\" class=\"form-control\" [(ngModel)]=\"date\">\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"savePayment()\"><i class=\"zmdi zmdi-save\"></i> SAVE</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalAddPayment.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJhbmNoLWhlYWQvdHJhbnNhY3Rpb25zL3BlbmRpbmctcGF5bWVudHMvQzpcXFVzZXJzXFxBRE1JTlxcRGVza3RvcFxcUHJvamVjdHNcXGRlbnRhbC1mZS9zcmNcXGFwcFxccGFnZXNcXGJyYW5jaC1oZWFkXFx0cmFuc2FjdGlvbnNcXHBlbmRpbmctcGF5bWVudHNcXHBlbmRpbmctcGF5bWVudHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBR00sb0JBQW9CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9icmFuY2gtaGVhZC90cmFuc2FjdGlvbnMvcGVuZGluZy1wYXltZW50cy9wZW5kaW5nLXBheW1lbnRzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bi1kZW1vIHtcclxuICAgICYgPiAuYnRuLFxyXG4gICAgJiA+IC5idG4tZ3JvdXAge1xyXG4gICAgICBtYXJnaW46IDAgNXB4IDEwcHggMDtcclxuICAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: PendingPaymentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingPaymentsComponent", function() { return PendingPaymentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");









var PendingPaymentsComponent = /** @class */ (function () {
    function PendingPaymentsComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.loggedUserInfo = null;
        this.pageTitle = "Pending Payments";
        this.widgetSubTitle = "Manage pending payments";
        this.page = 1;
        this.rows = 10;
        this.pending_payment_list = [];
        this.pending_payment_total = 0;
        this.view_data = [];
        this.amount = 0;
        this.mop = "CASH";
        this.branch = null;
        this.date = moment__WEBPACK_IMPORTED_MODULE_7__().format("YYYY-MM-DDTHH:mm");
        this.loggedUserInfo = this.common.getLoggedInUser();
        this.branch = this.loggedUserInfo.branch;
    }
    PendingPaymentsComponent.prototype.ngOnInit = function () {
        this.getPendingTransactions();
    };
    PendingPaymentsComponent.prototype.getPendingTransactions = function () {
        var _this = this;
        this.API.post("transactions/get-pending-payments", {
            page: this.page,
            rows: this.rows,
            branch: this.branch
        }).subscribe(function (response) {
            _this.pending_payment_list = response.devMessage;
            _this.pending_payment_total = response.total;
        }, function (error) {
        });
    };
    PendingPaymentsComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getPendingTransactions();
    };
    PendingPaymentsComponent.prototype.viewDetails = function (data) {
        this.view_data = data;
        this.modalPayments.show();
    };
    PendingPaymentsComponent.prototype.addPayment = function () {
        this.modalAddPayment.show();
    };
    PendingPaymentsComponent.prototype.savePayment = function () {
        var _this = this;
        if (Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.date) || this.date == "" || this.amount == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required field(s) Missing!", "warning");
        }
        else {
            this.API.post("transactions/add-payment", {
                transaction_id: this.view_data.id,
                remaining_bal: this.view_data.remaining_bal,
                amount: this.amount,
                mop: this.mop,
                date: this.date,
            }).subscribe(function (response) {
                if (response.devMessage) {
                    _this.growlService.addSuccess("Payment has been added");
                    _this.modalAddPayment.hide();
                    _this.modalPayments.hide();
                    _this.clearFields();
                    _this.getPendingTransactions();
                }
            }, function (error) {
            });
        }
    };
    PendingPaymentsComponent.prototype.clearFields = function () {
        this.amount = 0;
        this.mop = "CASH";
        this.date;
    };
    PendingPaymentsComponent.prototype.checkIfExceed = function () {
        if (this.view_data.remaining_bal < this.amount) {
            this.amount = this.view_data.remaining_bal;
            document.getElementById("input_payment_amount").classList.add("is-valid");
        }
        else {
            document.getElementById("input_payment_amount").classList.remove("is-valid");
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalPayments"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], PendingPaymentsComponent.prototype, "modalPayments", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalAddPayment"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], PendingPaymentsComponent.prototype, "modalAddPayment", void 0);
    PendingPaymentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pending-payments',
            template: __webpack_require__(/*! ./pending-payments.component.html */ "./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.html"),
            styles: [__webpack_require__(/*! ./pending-payments.component.scss */ "./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_6__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_8__["CommonService"]])
    ], PendingPaymentsComponent);
    return PendingPaymentsComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n\n<div class=\"card\">\n  <div class=\"toolbar toolbar--inner\">\n    <div class=\"toolbar__label\">Pending List</div>\n\n    <div class=\"actions\">\n        <!-- <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\"></i> -->\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getPendingTransactions()\" tooltip=\"Refresh list\"></a>\n    </div>\n\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getAssistants()\" tooltip=\"Close search box\"></i>\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getAssistants()\" tooltip=\"Search\"></i>\n\n    </div>\n    \n  </div>\n  <div class=\"card-body\">\n    <div class=\"table-responsive \">\n      <table class=\"table table-striped\">\n        <thead>\n          <tr>\n            <th></th>\n            <th>Branch</th>\n            <th>Patient</th>\n            <th>Dentist</th>\n            <th>Amount</th>\n            <th>Date</th>\n            <th>Status</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngIf=\"pending_transactions_list.length ==0\">\n            <td align=\"center\" colspan=\"7\">No record(s) found.</td>\n          </tr>\n          <tr *ngFor = \"let data of pending_transactions_list\">\n            <td style=\"max-width: 100px;\">\n              <div class=\"btn-demo\">\n                <button class=\"btn btn-primary w-100\" (click)=\"markAsDone(data)\">\n                  Mark as Done\n                </button>\n                <button class=\"btn btn-success w-100\" (click)=\"rescheduleTransaction(data)\">\n                  Re-schedule\n                </button>\n              </div>\n            </td>\n            <td>{{ data.branch_name }}</td>\n            <td>{{ data.patient_name }}</td>\n            <td>{{ data.dentist_name }}</td>\n            <td>{{ data.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'  }}</td>\n            <td>{{ data.covered_date_formatted }}</td>\n            <td>\n              <span class=\"badge badge-warning\" *ngIf=\"data.proc_status=='pending'\">PENDING</span>\n              <span class=\"badge badge-success\" *ngIf=\"data.proc_status=='paid-pending'\">PAID</span>\n              <span class=\"badge badge-danger\" *ngIf=\"data.proc_status=='voided'\">VOIDED</span>\n\n            </td>\n\n          </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td colspan=\"7\">\n              <pagination (pageChanged)=\"pageChange($event)\"\n                class=\"justify-content-center\"\n                [totalItems]=\"pending_transaction_total\"\n                [maxSize]=\"10\"\n                [itemsPerPage]=\"rows\" >\n              </pagination>\n            </td>\n          </tr>\n        </tfoot>\n      </table>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalReschedule=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5>Re-schedule</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Date<span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-9\">\n            <div class=\"form-group\">\n              <input type=\"datetime-local\" class=\"form-control\" [(ngModel)]=\"date\" (ngModelChange)=\"date = $event\">\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"updateSchedule()\"><i class=\"zmdi zmdi-save\"></i> UPDATE</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalReschedule.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJhbmNoLWhlYWQvdHJhbnNhY3Rpb25zL3BlbmRpbmctdHJhbnNhY3Rpb25zL0M6XFxVc2Vyc1xcQURNSU5cXERlc2t0b3BcXFByb2plY3RzXFxkZW50YWwtZmUvc3JjXFxhcHBcXHBhZ2VzXFxicmFuY2gtaGVhZFxcdHJhbnNhY3Rpb25zXFxwZW5kaW5nLXRyYW5zYWN0aW9uc1xccGVuZGluZy10cmFuc2FjdGlvbnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBR00sb0JBQW9CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9icmFuY2gtaGVhZC90cmFuc2FjdGlvbnMvcGVuZGluZy10cmFuc2FjdGlvbnMvcGVuZGluZy10cmFuc2FjdGlvbnMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLWRlbW8ge1xyXG4gICAgJiA+IC5idG4sXHJcbiAgICAmID4gLmJ0bi1ncm91cCB7XHJcbiAgICAgIG1hcmdpbjogMCA1cHggMTBweCAwO1xyXG4gICAgfVxyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: PendingTransactionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingTransactionsComponent", function() { return PendingTransactionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_5__);






var PendingTransactionsComponent = /** @class */ (function () {
    function PendingTransactionsComponent(API, growlService) {
        this.API = API;
        this.growlService = growlService;
        this.pageTitle = "Pending Transactions";
        this.widgetSubTitle = "Manage Pending transactions";
        this.page = 1;
        this.rows = 10;
        this.pending_transactions_list = [];
        this.pending_transaction_total = 0;
        this.key = null;
        this.date = null;
    }
    PendingTransactionsComponent.prototype.ngOnInit = function () {
        this.getPendingTransactions();
    };
    PendingTransactionsComponent.prototype.getPendingTransactions = function () {
        var _this = this;
        this.API.post("transactions/get-pending-transactions", {
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            _this.pending_transactions_list = response.devMessage;
            _this.pending_transaction_total = response.total;
        }, function (error) {
        });
    };
    PendingTransactionsComponent.prototype.markAsDone = function (data) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Are you sure?",
            text: "Mark Transaction as Done??",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes"
        }).then(function (result) {
            if (result.value) {
                console.log("wew", data);
                _this.API.post("transactions/update-pending-transaction", {
                    id: data.id,
                    procedure_id: data.procedure_id,
                    transaction_id: data.transaction_id,
                    proc_status: data.proc_status
                }).subscribe(function (response) {
                    if (response.devMessage) {
                        _this.growlService.addSuccess("Transaction Complete.");
                        _this.getPendingTransactions();
                    }
                }, function (error) {
                });
            }
        });
    };
    PendingTransactionsComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getPendingTransactions();
    };
    PendingTransactionsComponent.prototype.rescheduleTransaction = function (data) {
        this.key = data.id;
        this.date = data.covered_date;
        this.modalReschedule.show();
    };
    PendingTransactionsComponent.prototype.updateSchedule = function () {
        var _this = this;
        console.log(this.date);
        this.API.post("transactions/update-schedule", {
            key: this.key,
            date: this.date
        }).subscribe(function (response) {
            if (response.devMessage) {
                _this.growlService.addSuccess("Update complete.");
                _this.modalReschedule.hide();
                _this.getPendingTransactions();
            }
        }, function (error) {
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalReschedule"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], PendingTransactionsComponent.prototype, "modalReschedule", void 0);
    PendingTransactionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pending-transactions',
            template: __webpack_require__(/*! ./pending-transactions.component.html */ "./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.html"),
            styles: [__webpack_require__(/*! ./pending-transactions.component.scss */ "./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_5__["GrowlService"]])
    ], PendingTransactionsComponent);
    return PendingTransactionsComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/transactions/sales/sales.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/sales/sales.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n</header>\r\n\r\n<div class=\"card\">\r\n  <div class=\"toolbar toolbar--inner\">\r\n    <div class=\"toolbar__label\">Sales List</div>\r\n\r\n    <div class=\"actions\">\r\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\" *ngIf=\"!todoSearch\"></i>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getSalesList()\" tooltip=\"Refresh list\"></a>\r\n        <!-- <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addSales()\"></a> -->\r\n        <a [routerLink]=\"['/head/transactions/sales/add']\" class=\"actions__item zmdi zmdi-plus\"></a>\r\n\r\n    </div>\r\n    \r\n  </div>\r\n\r\n  <div class=\"card-body\">\r\n    <div class=\"row\" *ngIf=\"todoSearch\">\r\n      <div class=\"col-sm-12 col-md-3\">\r\n        <div class=\"form-group\" style=\"padding-top:30px\">\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"Patient name (e.g: Dela Cruz, Juan)\" [(ngModel)]=\"search_patient\">\r\n          <i class=\"form-group__bar\"></i>\r\n        </div>\r\n      </div>\r\n\r\n      <!-- <div class=\"col-sm-12 col-md-3\">\r\n        <div style=\"padding-top:30px\">\r\n          <ng-select\r\n            class=\"search__select\"\r\n            [items]=\"doctors_list\"\r\n            bindLabel=\"fullname\"\r\n            bindValue=\"id\"\r\n            [multiple]=\"true\"\r\n            groupBy=\"branch_name\"\r\n            dropdownPosition=\"bottom\"\r\n            placeholder=\"Select a Dentist\"\r\n            [(ngModel)]=\"search_dentist\"\r\n            >\r\n          </ng-select> \r\n        </div>\r\n      </div> -->\r\n\r\n      <div class=\"col-sm-12 col-md-2\">\r\n        <div class=\"form-group\" style=\"padding-top:30px\">\r\n          <input type=\"date\" class=\"form-control\" [(ngModel)]=\"search_date\">\r\n          <i class=\"form-group__bar\"></i>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-2\">\r\n        <div class=\"form-group\" style=\"padding-top:30px\">\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"Remarks\" [(ngModel)]=\"search_remarks\" >\r\n          <i class=\"form-group__bar\"></i>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-2\">\r\n        <i class=\"toolbar__search__close zmdi zmdi-close\" (click)=\"todoSearch = false;getSalesList('clear')\" tooltip=\"Close search box\"></i>\r\n        <i class=\"toolbar__search__search zmdi zmdi-search \" (click)=\"getSalesList()\" tooltip=\"Search\"></i>\r\n        \r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead>\r\n        <tr>\r\n          <th style=\"max-width: 70px;\"></th>\r\n          <th>Patient</th>\r\n          <th>Remarks</th>\r\n          <th>Procedure</th>\r\n          <th>Branch</th>\r\n          <th>Amount</th>\r\n          <th>MOP</th>\r\n          <th>Status</th>\r\n          <th>Date Created</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngIf=\"sales_list.length == 0\">\r\n            <td colspan=\"10\" align=\"center\">No record(s) found.</td>\r\n          </tr>\r\n          <tr *ngFor=\"let data of sales_list\">\r\n            <td style=\"max-width: 200px;\">\r\n              <div class=\"btn-demo\">\r\n                <button class=\"btn btn-danger\" (click)=\"voidTransaction(data)\">\r\n                  <i class=\"zmdi zmdi-delete\"></i>\r\n                  VOID\r\n                </button>\r\n                <button class=\"btn btn-primary\" (click)=\"viewSalesDetails(data)\">\r\n                  <i class=\"zmdi zmdi-eye\"></i>\r\n                  VIEW\r\n                </button>\r\n              </div>\r\n            </td>\r\n            <td>\r\n              {{data.patient}}\r\n            </td>\r\n            <td>\r\n              {{data.remarks}}\r\n            </td>\r\n            <td>\r\n              <div *ngFor = \" let sub of data.details\">\r\n                {{ sub.procedure_name }}\r\n              </div>\r\n            </td>\r\n            <td>\r\n              {{ data.branch_name }}\r\n            </td>\r\n            <td>\r\n              {{ data.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'  }}\r\n            </td>\r\n            <td>\r\n              {{ data.mop }}\r\n            </td>\r\n            <td>\r\n              <span class=\"badge badge-danger\" *ngIf=\"data.status=='VOIDED'\">VOIDED</span>\r\n              <span class=\"badge badge-success\" *ngIf=\"data.status=='DONE'\">PAID</span>\r\n              <span class=\"badge badge-warning\" *ngIf=\"data.status=='PENDING'\">PENDING</span>\r\n\r\n            </td>\r\n            <td>\r\n              {{ data.date_created }} <br>\r\n              {{ data.created_by }}\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"sales_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"9\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                class=\"justify-content-center\"\r\n                [totalItems]=\"total_sales\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalViewSalesDetails=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog modal-lg\">\r\n    <div class=\"modal-content\" style=\"border: 1px solid #888 !important;\">\r\n      <div class=\"modal-header\">\r\n        <h5>Sales Details</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <h4 class=\"card-title\">Procedures</h4>\r\n  \r\n          <table class=\"table\">\r\n              <thead>\r\n                <th>Procedure</th>\r\n                <th>Dentist</th>\r\n                <th>Patient</th>\r\n                <th>Amount</th>\r\n              </thead>\r\n              <tr *ngFor = \"let procedures of viewDetails?.details\">\r\n                <td>\r\n                  {{ procedures.procedure_name }}\r\n                </td>\r\n                <td>\r\n                  {{ procedures.dentist_name }}\r\n                </td>\r\n                <td>\r\n                  {{ procedures.patient_name }}\r\n                </td>\r\n                <td>\r\n                  {{ procedures.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}\r\n                </td>\r\n              </tr>\r\n          </table>\r\n        </div>\r\n        <div class=\"row\">\r\n          <h4 class=\"card-title\">Payment Details</h4>\r\n  \r\n          <table class=\"table\">\r\n            <thead>\r\n              <th>Mode of Payment</th>\r\n              <th>Amount</th>\r\n              <th>Payment Date</th>\r\n            </thead>\r\n            <tr *ngFor = \"let payments of viewDetails?.payments\">\r\n              <td>\r\n                {{ payments.mop }}\r\n              </td>\r\n              <td>\r\n                {{ payments.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'  }}\r\n              </td>\r\n              <td>\r\n                {{ payments.payment_date }}\r\n              </td>\r\n            </tr>\r\n          </table>\r\n        </div>\r\n\r\n\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"voidTransaction(viewDetails)\"><i class=\"zmdi zmdi-delete\"></i> VOID TRANSACTION</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalViewSalesDetails.hide()\"><i class=\"zmdi zmdi-close\"></i> Close</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/sales/sales.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/sales/sales.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJhbmNoLWhlYWQvdHJhbnNhY3Rpb25zL3NhbGVzL0M6XFxVc2Vyc1xcQURNSU5cXERlc2t0b3BcXFByb2plY3RzXFxkZW50YWwtZmUvc3JjXFxhcHBcXHBhZ2VzXFxicmFuY2gtaGVhZFxcdHJhbnNhY3Rpb25zXFxzYWxlc1xcc2FsZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBR00sb0JBQW9CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9icmFuY2gtaGVhZC90cmFuc2FjdGlvbnMvc2FsZXMvc2FsZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLWRlbW8ge1xyXG4gICAgJiA+IC5idG4sXHJcbiAgICAmID4gLmJ0bi1ncm91cCB7XHJcbiAgICAgIG1hcmdpbjogMCA1cHggMTBweCAwO1xyXG4gICAgfVxyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/sales/sales.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/sales/sales.component.ts ***!
  \*************************************************************************/
/*! exports provided: SalesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesComponent", function() { return SalesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");








var SalesComponent = /** @class */ (function () {
    function SalesComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.page = 1;
        this.rows = 10;
        this.patient_list = [];
        this.doctors_list = [];
        this.procedures_list = [];
        this.assistants_list = [];
        this.sales_list = [];
        this.branch_list = [];
        this.secretaries_list = [];
        this.total_sales = 0;
        this.patient = { id: null, fullname: "" };
        this.procedure = [];
        this.doctor_fee = 0;
        this.mop = "CASH";
        this.amount = 0;
        this.branch = null;
        this.totalProcedurePrice = 0.00;
        this.mode = "add";
        //searchPatient Variables
        this.search_lname = "";
        this.search_fname = "";
        this.search_mname = "";
        this.search_res_list = [];
        this.search_patient = null;
        this.search_dentist = null;
        this.search_date = null;
        this.search_remarks = null;
        this.pageTitle = "Sales";
        this.widgetSubTitle = "Manage Sales";
        this.loggedUserInfo = this.common.getLoggedInUser();
        this.branch = this.loggedUserInfo.branch;
    }
    SalesComponent.prototype.ngOnInit = function () {
        this.getPatientList();
        this.getDoctorList();
        this.getProcedures();
        this.getAssistantsList();
        this.getSalesList();
        this.getBranches();
        this.getSecretariesList();
    };
    SalesComponent.prototype.addSales = function () {
        this.modalSales.show();
    };
    SalesComponent.prototype.getSalesList = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = ''; }
        if (opt == 'clear') {
            this.search_patient = null;
            this.search_dentist = null;
            this.search_date = null;
            this.search_remarks = null;
        }
        this.API.post("sales/get-sales", {
            branch: this.branch,
            rows: this.rows,
            page: this.page,
            search_patient: this.search_patient,
            search_dentist: this.search_dentist,
            search_date: this.search_date,
            search_remarks: this.search_remarks
        }).subscribe(function (response) {
            _this.sales_list = response.devMessage;
            _this.total_sales = response.total;
        }, function (error) {
        });
    };
    SalesComponent.prototype.getPatientList = function () {
        var _this = this;
        this.API.post("lookups/get-patients", {}).subscribe(function (response) {
            _this.patient_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.getDoctorList = function () {
        var _this = this;
        this.API.post("lookups/get-doctors", {}).subscribe(function (response) {
            _this.doctors_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.getProcedures = function () {
        var _this = this;
        this.API.post("lookups/get-procedures", {}).subscribe(function (response) {
            _this.procedures_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.getAssistantsList = function () {
        var _this = this;
        this.API.post("lookups/get-assistants", {}).subscribe(function (response) {
            _this.assistants_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.saveSalesTransaction = function () {
        var _this = this;
        if (Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.patient.id)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Please select a patient", "warning");
        }
        else if (Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.branch)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Please select a branch", "warning");
        }
        else if (this.procedure.length == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Please select atleast one procedure", "warning");
        }
        else {
            this.API.post("sales/save", {
                mop: this.mop,
                branch: this.branch,
                patient_id: this.patient.id,
                procedures: this.procedure,
                total_amount: this.totalProcedurePrice
            }).subscribe(function (response) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Success", "Record Saved.", "success");
                _this.modalSales.hide();
            }, function (error) {
            });
        }
    };
    SalesComponent.prototype.procedureChanged = function (data) {
        this.amount = data.price;
        this.staffFee(data);
    };
    SalesComponent.prototype.staffFee = function (data) {
        this.doctor_fee = (data.commission_rate / 100) * this.amount;
    };
    SalesComponent.prototype.getTotalProcedurePrice = function () {
        console.log(this.procedure);
        this.totalProcedurePrice = 0.00;
        for (var i = 0; i < this.procedure.length; i++) {
            this.totalProcedurePrice = parseFloat(this.totalProcedurePrice) + parseFloat(this.procedure[i].price);
        }
    };
    SalesComponent.prototype.searchPatient = function () {
        this.modalSearchPatient.show();
    };
    SalesComponent.prototype.searchPatientByName = function () {
        var _this = this;
        this.API.post("patients/search-patient-by-name", {
            lname: this.search_lname,
            fname: this.search_fname,
            mname: this.search_mname,
        }).subscribe(function (response) {
            _this.search_res_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.selectPatient = function (data) {
        this.patient.id = data.id;
        this.patient.fullname = data.fullname;
        this.modalSearchPatient.hide();
    };
    SalesComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            _this.branch_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.getSecretariesList = function () {
        var _this = this;
        this.API.post("lookups/get-secretaries", {}).subscribe(function (response) {
            _this.secretaries_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.voidTransaction = function (data) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Are you sure?",
            text: "Void Transaction?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes"
        }).then(function (result) {
            if (result.value) {
                _this.API.post("sales/void-transaction", {
                    id: data.id
                }).subscribe(function (response) {
                    if (response.devMessage) {
                        _this.growlService.addSuccess("Transaction voided.");
                        _this.modalViewSalesDetails.hide();
                        _this.getSalesList();
                    }
                }, function (error) {
                });
            }
        });
    };
    SalesComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getSalesList();
    };
    SalesComponent.prototype.viewSalesDetails = function (data) {
        this.viewDetails = data;
        this.modalViewSalesDetails.show();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalSales"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], SalesComponent.prototype, "modalSales", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalSearchPatient"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], SalesComponent.prototype, "modalSearchPatient", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalViewSalesDetails"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], SalesComponent.prototype, "modalViewSalesDetails", void 0);
    SalesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sales',
            template: __webpack_require__(/*! ./sales.component.html */ "./src/app/pages/branch-head/transactions/sales/sales.component.html"),
            styles: [__webpack_require__(/*! ./sales.component.scss */ "./src/app/pages/branch-head/transactions/sales/sales.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_6__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
    ], SalesComponent);
    return SalesComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/transactions/transactions.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/transactions.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/transactions.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/transactions.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3RyYW5zYWN0aW9ucy90cmFuc2FjdGlvbnMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/branch-head/transactions/transactions.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/transactions.component.ts ***!
  \**************************************************************************/
/*! exports provided: TransactionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsComponent", function() { return TransactionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TransactionsComponent = /** @class */ (function () {
    function TransactionsComponent() {
    }
    TransactionsComponent.prototype.ngOnInit = function () {
    };
    TransactionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-transactions',
            template: __webpack_require__(/*! ./transactions.component.html */ "./src/app/pages/branch-head/transactions/transactions.component.html"),
            styles: [__webpack_require__(/*! ./transactions.component.scss */ "./src/app/pages/branch-head/transactions/transactions.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TransactionsComponent);
    return TransactionsComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/transactions/transactions.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/branch-head/transactions/transactions.module.ts ***!
  \***********************************************************************/
/*! exports provided: TransactionsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsModule", function() { return TransactionsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _transactions_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./transactions.component */ "./src/app/pages/branch-head/transactions/transactions.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _sales_sales_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./sales/sales.component */ "./src/app/pages/branch-head/transactions/sales/sales.component.ts");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./add/add.component */ "./src/app/pages/branch-head/transactions/add/add.component.ts");
/* harmony import */ var _pending_transactions_pending_transactions_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pending-transactions/pending-transactions.component */ "./src/app/pages/branch-head/transactions/pending-transactions/pending-transactions.component.ts");
/* harmony import */ var _pending_payments_pending_payments_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pending-payments/pending-payments.component */ "./src/app/pages/branch-head/transactions/pending-payments/pending-payments.component.ts");
/* harmony import */ var angular2_moment__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! angular2-moment */ "./node_modules/angular2-moment/index.js");
/* harmony import */ var angular2_moment__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(angular2_moment__WEBPACK_IMPORTED_MODULE_18__);



















var ROUTE = [
    {
        path: "",
        redirectTo: 'sales'
    },
    {
        path: "sales",
        component: _sales_sales_component__WEBPACK_IMPORTED_MODULE_14__["SalesComponent"]
    },
    {
        path: "sales/add",
        component: _add_add_component__WEBPACK_IMPORTED_MODULE_15__["AddComponent"]
    },
    {
        path: "pending/list",
        component: _pending_transactions_pending_transactions_component__WEBPACK_IMPORTED_MODULE_16__["PendingTransactionsComponent"]
    },
    {
        path: "payments/list",
        component: _pending_payments_pending_payments_component__WEBPACK_IMPORTED_MODULE_17__["PendingPaymentsComponent"]
    }
];
var TransactionsModule = /** @class */ (function () {
    function TransactionsModule() {
    }
    TransactionsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _transactions_component__WEBPACK_IMPORTED_MODULE_4__["TransactionsComponent"],
                _sales_sales_component__WEBPACK_IMPORTED_MODULE_14__["SalesComponent"],
                _add_add_component__WEBPACK_IMPORTED_MODULE_15__["AddComponent"],
                _pending_transactions_pending_transactions_component__WEBPACK_IMPORTED_MODULE_16__["PendingTransactionsComponent"],
                _pending_payments_pending_payments_component__WEBPACK_IMPORTED_MODULE_17__["PendingPaymentsComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_13__["BsDatepickerModule"].forRoot(),
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"],
                angular2_moment__WEBPACK_IMPORTED_MODULE_18__["MomentModule"]
            ]
        })
    ], TransactionsModule);
    return TransactionsModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-branch-head-transactions-transactions-module.js.map