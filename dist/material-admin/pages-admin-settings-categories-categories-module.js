(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-settings-categories-categories-module"],{

/***/ "./src/app/pages/admin/settings/categories/categories.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/admin/settings/categories/categories.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n\r\n  <div class=\"actions actions\">\r\n    <div class=\"btn-demo\">\r\n      <button class=\"btn btn-primary btn--icon-text\" (click)=\"addCategory()\"><i class=\"zmdi zmdi-plus\"></i> Add New</button>\r\n      <button class=\"btn btn-success btn--icon-text\"  (click)=\"getCategories()\"><i class=\"zmdi zmdi-refresh\"></i> Refresh</button>\r\n    </div>\r\n  </div>\r\n</header>\r\n\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead class=\"thead-dark\">\r\n        <tr>\r\n          <th width=\"110px\"></th>\r\n          <th>Category Name</th>\r\n          <th>Date Created</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let item of categories_list\">\r\n            <td>\r\n              <div class=\"btn-demo\">\r\n                <button type=\"button\" class=\"btn btn-success\"  (click)=\"editCategory(item)\"><i class=\"zmdi zmdi-edit\"></i> Edit</button>\r\n              </div>\r\n            </td>\r\n            <td>{{ item.category_name }}</td>\r\n            <td>{{ item.date_created }}</td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"categories_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"2\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                [totalItems]=\"categories_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n            <td align=\"right\">\r\n              {{((page-1)*rows)+1 | number : '1.0-0'}} of {{categories_count | number : '1.0-0'}}\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalCategories=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog  modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Category' : 'Edit Category'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Name<span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"category\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveCategory()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalCategories.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/admin/settings/categories/categories.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/admin/settings/categories/categories.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3MvY2F0ZWdvcmllcy9DOlxcVXNlcnNcXEFETUlOXFxEZXNrdG9wXFxQcm9qZWN0c1xcZGVudGFsLWZlL3NyY1xcYXBwXFxwYWdlc1xcYWRtaW5cXHNldHRpbmdzXFxjYXRlZ29yaWVzXFxjYXRlZ29yaWVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUdNLG9CQUFvQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3MvY2F0ZWdvcmllcy9jYXRlZ29yaWVzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bi1kZW1vIHtcclxuICAgICYgPiAuYnRuLFxyXG4gICAgJiA+IC5idG4tZ3JvdXAge1xyXG4gICAgICBtYXJnaW46IDAgNXB4IDEwcHggMDtcclxuICAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/admin/settings/categories/categories.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/admin/settings/categories/categories.component.ts ***!
  \*************************************************************************/
/*! exports provided: CategoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesComponent", function() { return CategoriesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);





var CategoriesComponent = /** @class */ (function () {
    function CategoriesComponent(API) {
        this.API = API;
        this.pageTitle = "Categories";
        this.widgetSubTitle = "Settings for Item Categories";
        this.page = 1;
        this.rows = 10;
        this.categories_list = [];
        this.categories_count = 0;
        this.mode = "add";
        this.category = "";
    }
    CategoriesComponent.prototype.ngOnInit = function () {
        this.getCategories();
    };
    CategoriesComponent.prototype.getCategories = function () {
        var _this = this;
        this.API.post("settings/get-categories", {
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                _this.categories_list = response.devMessage;
                _this.categories_count = response.total;
            }
        }, function (error) {
        });
    };
    CategoriesComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getCategories();
    };
    CategoriesComponent.prototype.addCategory = function () {
        this.mode = "add";
        this.modalCategories.show();
    };
    CategoriesComponent.prototype.saveCategory = function () {
        var _this = this;
        if (this.category == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required Field(s) Missing", "warning");
        }
        else {
            this.API.post("settings/save-category", {
                category_name: this.category,
                mode: this.mode,
                key: this.key_category
            }).subscribe(function (response) {
                console.log(response);
                if (response.statusCode == 200) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Success", _this.mode == 'add' ? "Record has been Saved" : "Record has been Updated", "success");
                    _this.clearFields();
                    _this.getCategories();
                    _this.modalCategories.hide();
                }
            }, function (error) {
            });
        }
    };
    CategoriesComponent.prototype.editCategory = function (data) {
        this.mode = "edit";
        this.key_category = data.id;
        this.category = data.category_name;
        this.modalCategories.show();
    };
    CategoriesComponent.prototype.clearFields = function () {
        this.category = "";
        this.mode = "add";
        this.key_category = "";
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalCategories"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], CategoriesComponent.prototype, "modalCategories", void 0);
    CategoriesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-categories',
            template: __webpack_require__(/*! ./categories.component.html */ "./src/app/pages/admin/settings/categories/categories.component.html"),
            styles: [__webpack_require__(/*! ./categories.component.scss */ "./src/app/pages/admin/settings/categories/categories.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], CategoriesComponent);
    return CategoriesComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/settings/categories/categories.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/admin/settings/categories/categories.module.ts ***!
  \**********************************************************************/
/*! exports provided: CategoriesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesModule", function() { return CategoriesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _categories_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./categories.component */ "./src/app/pages/admin/settings/categories/categories.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");













var ROUTE = [{ path: "", component: _categories_component__WEBPACK_IMPORTED_MODULE_4__["CategoriesComponent"] }];
var CategoriesModule = /** @class */ (function () {
    function CategoriesModule() {
    }
    CategoriesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _categories_component__WEBPACK_IMPORTED_MODULE_4__["CategoriesComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], CategoriesModule);
    return CategoriesModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-settings-categories-categories-module.js.map