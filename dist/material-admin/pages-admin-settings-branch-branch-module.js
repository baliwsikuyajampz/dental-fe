(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-settings-branch-branch-module"],{

/***/ "./src/app/pages/admin/settings/branch/branch.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/admin/settings/branch/branch.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n\r\n  <div class=\"actions actions\">\r\n\r\n    <div class=\"btn-demo\">\r\n      <button class=\"btn btn-primary btn--icon-text\" (click)=\"addBranch()\"><i class=\"zmdi zmdi-plus\"></i> Add New</button>\r\n      <button class=\"btn btn-success btn--icon-text\"  (click)=\"getBranches()\"><i class=\"zmdi zmdi-refresh\"></i> Refresh</button>\r\n    </div>\r\n  </div>\r\n</header>\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-body d-none d-sm-block\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead class=\"thead-dark\">\r\n        <tr>\r\n          <th width=\"150px\"></th>\r\n          <th>Branch name</th>\r\n          <th>Location</th>\r\n          <th>Contact No</th>\r\n          <th>Date Created</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let data of branches_list\">\r\n            <td>\r\n              <div class=\"btn-demo\">\r\n                <button type=\"button\" class=\"btn btn-success w-100\" (click)=\"editBranch(data)\"><i class=\"zmdi zmdi-edit\"></i> Edit</button>\r\n              </div>\r\n            </td>\r\n            <td>{{ data.branch_name }}</td>\r\n            <td>{{ data.location }}</td>\r\n            <td>{{ data.contact_no }}</td>\r\n            <td>{{ data.date_created }}</td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"branches_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"4\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                [totalItems]=\"branches_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n            <td align=\"right\">\r\n              {{((page-1)*rows)+1 | number : '1.0-0'}} of {{branches_count | number : '1.0-0'}}\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n  <div class=\"card-body d-block d-sm-none\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead class=\"thead-dark\">\r\n        <tr>\r\n          <th width=\"190px\"></th>\r\n          <th>Branch name</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let data of branches_list\">\r\n            <td>\r\n              <div class=\"btn-demo\">\r\n                <button type=\"button\" class=\"btn btn-success\" (click)=\"editBranch(data)\"><i class=\"zmdi zmdi-edit\"></i> Edit</button>\r\n              </div>\r\n            </td>\r\n            <td>\r\n              {{ data.branch_name }}\r\n              <small>{{ data.location }}</small>\r\n              <small>{{ data.contact_no }}</small>\r\n              <small>{{ data.date_created }}</small>\r\n            </td>\r\n  \r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"branches_list.length!=0\">\r\n          <tr>\r\n            <td>\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                [totalItems]=\"branches_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n            <td align=\"right\">\r\n              {{((page-1)*rows)+1 | number : '1.0-0'}} of {{branches_count | number : '1.0-0'}}\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalBranch=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog  modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Branch' : 'Edit Branch'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Code<span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"branch_code\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Name<span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"branch_name\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Location</label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"location\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Contact No</label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"contact_no\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Address</label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                <textarea [(ngModel)]=\"address\" class=\"form-control\" ></textarea>\r\n                <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveBranch()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalBranch.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/admin/settings/branch/branch.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/admin/settings/branch/branch.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3MvYnJhbmNoL0M6XFxVc2Vyc1xcQURNSU5cXERlc2t0b3BcXFByb2plY3RzXFxkZW50YWwtZmUvc3JjXFxhcHBcXHBhZ2VzXFxhZG1pblxcc2V0dGluZ3NcXGJyYW5jaFxcYnJhbmNoLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUdNLG9CQUFvQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3MvYnJhbmNoL2JyYW5jaC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG4tZGVtbyB7XHJcbiAgICAmID4gLmJ0bixcclxuICAgICYgPiAuYnRuLWdyb3VwIHtcclxuICAgICAgbWFyZ2luOiAwIDVweCAxMHB4IDA7XHJcbiAgICB9XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/admin/settings/branch/branch.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/settings/branch/branch.component.ts ***!
  \*****************************************************************/
/*! exports provided: BranchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchComponent", function() { return BranchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






var BranchComponent = /** @class */ (function () {
    function BranchComponent(API, router) {
        this.API = API;
        this.router = router;
        this.pageTitle = "Branches";
        this.widgetSubTitle = "Settings for Branch Information";
        this.page = 1;
        this.rows = 10;
        this.branches_list = [];
        this.branches_count = 0;
        this.mode = "add";
        this.branch_code = "";
        this.branch_name = "";
        this.location = "";
        this.address = "";
        this.contact_no = "";
    }
    BranchComponent.prototype.ngOnInit = function () {
        this.getUserLoggedIn();
        this.getBranches();
        this.getUserLoggedIn();
    };
    BranchComponent.prototype.getUserLoggedIn = function () {
        if (localStorage.getItem("user_info") === null) {
            this.router.navigate(['']);
        }
        else {
            this.userLoggedInfo = JSON.parse(localStorage.getItem("user_info"));
        }
    };
    BranchComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("settings/get-branches", {
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                _this.branches_list = response.devMessage;
                _this.branches_count = response.total;
            }
        }, function (error) {
        });
    };
    BranchComponent.prototype.addBranch = function () {
        this.clearFields();
        this.modalBranch.show();
    };
    BranchComponent.prototype.saveBranch = function () {
        var _this = this;
        if (this.branch_code == "" || this.branch_name == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Oops", "Required Field(s) Missing", "warning");
        }
        else {
            this.API.post("settings/save-branch", {
                branch_code: this.branch_code,
                branch_name: this.branch_name,
                location: this.location,
                contact_no: this.contact_no,
                address: this.address,
                mode: this.mode,
                key: this.branch_key,
                created_by: this.userLoggedInfo.id
            }).subscribe(function (response) {
                console.log(response);
                if (response.statusCode == 200) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Success", _this.mode == 'add' ? "Record has been Saved" : "Record has been Updated", "success");
                    _this.clearFields();
                    _this.getBranches();
                    _this.modalBranch.hide();
                }
            }, function (error) {
            });
        }
    };
    BranchComponent.prototype.clearFields = function () {
        this.branch_key = "";
        this.branch_code = "";
        this.branch_name = "";
        this.location = "";
        this.contact_no = "";
        this.address = "";
        this.mode = "add";
    };
    BranchComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getBranches();
    };
    BranchComponent.prototype.editBranch = function (data) {
        this.branch_key = data.id;
        this.branch_code = data.branch_code;
        this.branch_name = data.branch_name;
        this.location = data.location;
        this.contact_no = data.contact_no;
        this.address = data.address;
        this.mode = "edit";
        this.modalBranch.show();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalBranch"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], BranchComponent.prototype, "modalBranch", void 0);
    BranchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-branch',
            template: __webpack_require__(/*! ./branch.component.html */ "./src/app/pages/admin/settings/branch/branch.component.html"),
            styles: [__webpack_require__(/*! ./branch.component.scss */ "./src/app/pages/admin/settings/branch/branch.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], BranchComponent);
    return BranchComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/settings/branch/branch.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/admin/settings/branch/branch.module.ts ***!
  \**************************************************************/
/*! exports provided: BranchModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchModule", function() { return BranchModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _branch_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./branch.component */ "./src/app/pages/admin/settings/branch/branch.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");













var ROUTE = [{ path: "", component: _branch_component__WEBPACK_IMPORTED_MODULE_4__["BranchComponent"] }];
var BranchModule = /** @class */ (function () {
    function BranchModule() {
    }
    BranchModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _branch_component__WEBPACK_IMPORTED_MODULE_4__["BranchComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], BranchModule);
    return BranchModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-settings-branch-branch-module.js.map