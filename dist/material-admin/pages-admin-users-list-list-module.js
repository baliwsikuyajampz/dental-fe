(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-users-list-list-module"],{

/***/ "./src/app/pages/admin/users/list/list.component.html":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/users/list/list.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n</header>\r\n\r\n\r\n<div class=\"card\">\r\n  <div class=\"toolbar toolbar--inner\">\r\n    <div class=\"toolbar__label\">Users List</div>\r\n\r\n    <div class=\"actions\">\r\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\" tooltip=\"Search\"></i>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getUserList()\" tooltip=\"Refresh list\"></a>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"showModal()\" tooltip=\"Add\"></a>\r\n    </div>\r\n\r\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\r\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \r\n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getProcedureList()\" tooltip=\"Close search box\"></i>\r\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getProcedureList()\" tooltip=\"Search\"></i>\r\n\r\n    </div>\r\n    \r\n  </div>\r\n  <div class=\"card-body d-none d-sm-block\">\r\n    <div class=\"table-responsive\">\r\n      <table class=\"table table-striped\">\r\n        <thead>\r\n        <tr>\r\n          <th width=\"60px\"></th>\r\n          <th>Fullname</th>\r\n          <th>Role</th>\r\n          <th>Branch name</th>\r\n          <th>Date Created</th>\r\n\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngIf=\"users_list.length == 0\">\r\n            <td colspan=\"5\" align=\"center\">No record(s) found.</td>\r\n\r\n          </tr>\r\n          <tr *ngFor=\"let data of users_list\">\r\n            <th scope=\"row\">\r\n              \r\n              <div class=\"btn-demo\">\r\n                <!-- <button type=\"button\" class=\"btn btn-primary w-100\" [routerLink]=\"['logs',data.id]\"><i class=\"zmdi zmdi-eye\"></i> View</button> -->\r\n                <button type=\"button\" class=\"btn btn-success w-100\" (click)=\"editUser(data)\"><i class=\"zmdi zmdi-edit\"></i></button>\r\n                <!-- <button type=\"button\" class=\"btn btn-danger w-100\" (click)=\"setUserAsInactive(data)\"><i class=\"zmdi zmdi-delete\"></i></button> -->\r\n              </div>\r\n\r\n            </th>\r\n            <td>{{ data.fullName }}</td>\r\n            <td>{{ data.role_name }}</td>\r\n            <td>{{ data.branch_name }}</td>\r\n            <td>{{ data.date_created }}</td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"users_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"4\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                [totalItems]=\"users_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n            <td align=\"right\">\r\n              {{((page-1)*rows)+1 | number : '1.0-0'}} of {{users_count | number : '1.0-0'}}\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n        \r\n      </table>\r\n    </div>\r\n  </div>\r\n  <div class=\"card-body d-block d-sm-none\">\r\n    <div class=\"table-responsive\">\r\n      <table class=\"table table-striped\">\r\n        <thead class=\"thead-dark\">\r\n          <tr>\r\n            <th width=\"40px\"></th>\r\n            <th>User Info</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngIf=\"users_list.length == 0\">\r\n            <td colspan=\"2\" align=\"center\">No record(s) found.</td>\r\n          </tr>\r\n          <tr *ngFor=\"let data of users_list\">\r\n            <td width=\"40px\">\r\n              <div class=\"btn-demo\">\r\n                <button type=\"button\" class=\"btn btn-primary w-100\" [routerLink]=\"['logs',data.id]\"><i class=\"zmdi zmdi-eye\"></i> View</button>\r\n                <button type=\"button\" class=\"btn btn-success w-100\" (click)=\"editUser(data)\"><i class=\"zmdi zmdi-edit\"></i> Edit</button>\r\n                <button type=\"button\" class=\"btn btn-danger w-100\" (click)=\"setUserAsInactive(data)\"><i class=\"zmdi zmdi-delete\"></i> Delete</button>\r\n              </div>\r\n            </td>\r\n            <td>\r\n              <strong>{{ data.fullName }}</strong> <br>\r\n              <small>{{ data.role_name }}</small> <br>\r\n              <small>{{ data.branch_name }}</small> <br>\r\n              <small>{{ data.date_created }}</small> <br>\r\n\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"users_list.length!=0\">\r\n          <tr>\r\n            <td>\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                [totalItems]=\"users_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n            <td align=\"right\">\r\n              {{((page-1)*rows)+1 | number : '1.0-0'}} of {{users_count | number : '1.0-0'}}\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n        \r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalUsers=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog  modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{modeModal == 'add' ? 'Add User' : 'Edit User'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Username <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"username\" placeholder=\"e.g: juandelacruz21\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <!-- <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Email</label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"email\" placeholder=\"e.g: juandelacruz@gmail.com\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div> -->\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">First Name <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"fname\" placeholder=\"e.g: Juan\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Middle Name</label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"mname\" placeholder=\"e.g: Santos\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Last Name <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"lname\" placeholder=\"e.g: Dela Cruz\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Roles <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n            <div class=\"form-group\">\r\n              <ng-select\r\n                [items]=\"roles_list\"\r\n                bindLabel=\"role_name\"\r\n                placeholder=\"Select a Role\"\r\n                [(ngModel)]=\"role_selected\">\r\n              </ng-select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Branch <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n            <div class=\"form-group\">\r\n              <ng-select\r\n                [items]=\"branches_lookup\"\r\n                bindLabel=\"branch_name\"\r\n                placeholder=\"Select a Branch\"\r\n                [(ngModel)]=\"branch_selected\">\r\n              </ng-select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"save()\"><i class=\"zmdi zmdi-save\"></i> SAVE</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalUsers.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/admin/users/list/list.component.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/users/list/list.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vdXNlcnMvbGlzdC9DOlxcVXNlcnNcXEFETUlOXFxEZXNrdG9wXFxQcm9qZWN0c1xcZGVudGFsLWZlL3NyY1xcYXBwXFxwYWdlc1xcYWRtaW5cXHVzZXJzXFxsaXN0XFxsaXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUdNLG9CQUFvQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWRtaW4vdXNlcnMvbGlzdC9saXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bi1kZW1vIHtcclxuICAgICYgPiAuYnRuLFxyXG4gICAgJiA+IC5idG4tZ3JvdXAge1xyXG4gICAgICBtYXJnaW46IDAgNXB4IDEwcHggMDtcclxuICAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/admin/users/list/list.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/admin/users/list/list.component.ts ***!
  \**********************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var ListComponent = /** @class */ (function () {
    function ListComponent(API, router) {
        this.API = API;
        this.router = router;
        this.pageTitle = 'User List';
        this.widgetSubTitle = "List of all Users";
        this.page = 1;
        this.rows = 10;
        this.users_list = [];
        this.users_count = 0;
        this.branches_lookup = [];
        this.username = "";
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.email = "";
        this.roles_list = [];
        this.role = "";
        this.modeModal = "";
        this.branch_selected = null;
        this.role_selected = null;
    }
    ListComponent.prototype.ngOnInit = function () {
        this.getUserLoggedIn();
        this.getUserList();
        this.getRoles();
        this.getBranches();
    };
    ListComponent.prototype.showModal = function () {
        this.modeModal = "add";
        this.clearFields();
        this.modalUsers.show();
    };
    ListComponent.prototype.getUserLoggedIn = function () {
        if (localStorage.getItem("user_info") === null) {
            this.router.navigate(['']);
        }
        else {
            this.userLoggedInfo = JSON.parse(localStorage.getItem("user_info"));
        }
    };
    ListComponent.prototype.getUserList = function () {
        var _this = this;
        this.API.post("users/get-users", {
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            if (response.statusCode == 200) {
                _this.users_list = response.devMessage;
                _this.users_count = response.total;
            }
        }, function (error) {
        });
    };
    ListComponent.prototype.save = function () {
        var _this = this;
        if (this.username == "" || this.fname == "" || this.lname == "" || this.role === null || this.branch_selected === null) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops!", "Required field(s) missing.", "warning");
        }
        else {
            this.API.post("users/save-user", {
                fname: this.fname,
                mname: this.mname,
                lname: this.lname,
                username: this.username,
                role: this.role_selected.id,
                branch: this.branch_selected.id,
                mode: this.modeModal,
                email: this.email,
                key: this.key,
                created_by: this.userLoggedInfo.id
            }).subscribe(function (response) {
                if (response.statusCode == 200) {
                    if (response.devMessage == "userExist") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Username Already Exists", "warning");
                    }
                    else {
                        if (response.devMessage == true) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Success", _this.modeModal == "add" ? "Record Added" : "Record Updated", "success");
                            _this.getUserList();
                            _this.modalUsers.hide();
                        }
                    }
                }
            }, function (error) {
            });
        }
    };
    ListComponent.prototype.editUser = function (data) {
        this.modeModal = "edit";
        this.fname = data.fname;
        this.mname = data.mname;
        this.lname = data.lname;
        this.username = data.username;
        this.email = data.email;
        var roleIndx = this.roles_list.findIndex(function (e) { return e.id == data.role_id; });
        this.role_selected = this.roles_list[roleIndx];
        var branchIndx = this.branches_lookup.findIndex(function (e) { return e.id == data.branch_id; });
        this.branch_selected = this.branches_lookup[branchIndx];
        this.modalUsers.show();
        this.key = data.id;
    };
    ListComponent.prototype.clearFields = function () {
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.username = "";
        this.email = "";
        this.branch_selected = null;
        this.role_selected = null;
    };
    ListComponent.prototype.getRoles = function () {
        var _this = this;
        this.API.post("users/get-roles", {}).subscribe(function (response) {
            if (response.statusCode == 200) {
                _this.roles_list = response.devMessage;
            }
        }, function (error) {
        });
    };
    ListComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                _this.branches_lookup = response.devMessage;
            }
        }, function (error) {
        });
    };
    ListComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getUserList();
    };
    ListComponent.prototype.setUserAsInactive = function (data) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value == true) {
                _this.API.post("users/delete-user", {
                    id: data.id,
                    created_by: _this.userLoggedInfo.id
                }).subscribe(function (response) {
                    if (response.statusCode == 200) {
                        _this.getUserList();
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Success", "User Deleted", "success");
                    }
                }, function (error) {
                });
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalUsers"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["ModalDirective"])
    ], ListComponent.prototype, "modalUsers", void 0);
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/pages/admin/users/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.scss */ "./src/app/pages/admin/users/list/list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/users/list/list.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/admin/users/list/list.module.ts ***!
  \*******************************************************/
/*! exports provided: ListModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListModule", function() { return ListModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./list.component */ "./src/app/pages/admin/users/list/list.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");











var ROUTE = [{ path: "", component: _list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"] }];
var ListModule = /** @class */ (function () {
    function ListModule() {
    }
    ListModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__["NgSelectModule"]
            ]
        })
    ], ListModule);
    return ListModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-users-list-list-module.js.map