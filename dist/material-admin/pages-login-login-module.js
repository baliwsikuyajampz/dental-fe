(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./src/app/pages/login/login.component.html":
/*!**************************************************!*\
  !*** ./src/app/pages/login/login.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login\">\r\n\r\n  <!-- Login -->\r\n  <div *ngIf=\"loginStats.login\" class=\"login__block active\">\r\n    <div class=\"login__header bg-blue\">\r\n        <img src=\"../../../assets/img/logo/logo.png\">\r\n\r\n        <div class=\"actions actions--inverse login__actions\">\r\n            <div class=\"dropdown\" dropdown>\r\n                <i dropdownToggle class=\"zmdi zmdi-more-vert actions__item\"></i>\r\n\r\n                <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\r\n                    <a class=\"dropdown-item\" [routerLink]=\"['registration/ortho']\">Register Ortho Patient</a>\r\n                    <a class=\"dropdown-item\" (click)=\"toggleBlock('login', 'forgotPassword')\">Register Non-Ortho Patient</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"login__body\">\r\n        <div class=\"form-group form-group--float form-group--centered\">\r\n            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"username\" InputFloat>\r\n            <label>Username</label>\r\n            <i class=\"form-group__bar\"></i>\r\n        </div>\r\n\r\n        <div class=\"form-group form-group--float form-group--centered\">\r\n            <input type=\"password\" class=\"form-control\" [(ngModel)]=\"password\" InputFloat>\r\n            <label>Password</label>\r\n            <i class=\"form-group__bar\"></i>\r\n        </div>\r\n\r\n        <a (click)=\"auth()\" class=\"btn btn--icon bg-blue\"><i class=\"zmdi zmdi-long-arrow-right\"></i></a>\r\n    </div>\r\n  </div>\r\n\r\n  <!-- Register -->\r\n  <div *ngIf=\"loginStats.signUp\" class=\"login__block\">\r\n      <div class=\"login__header bg-blue\">\r\n          <i class=\"zmdi zmdi-account-circle\"></i>\r\n          Create an account\r\n\r\n          <div class=\"actions actions--inverse login__actions\">\r\n              <div class=\"dropdown\" dropdown>\r\n                  <i dropdownToggle class=\"zmdi zmdi-more-vert actions__item\"></i>\r\n\r\n                  <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\r\n                      <a class=\"dropdown-item\" (click)=\"toggleBlock('signUp', 'login')\">Already have an account?</a>\r\n                      <a class=\"dropdown-item\" (click)=\"toggleBlock('signUp', 'forgotPassword')\">Forgot password?</a>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </div>\r\n\r\n      <div class=\"login__body\">\r\n          <div class=\"form-group form-group--float form-group--centered\">\r\n              <input type=\"text\" class=\"form-control\" InputFloat>\r\n              <label>Name</label>\r\n              <i class=\"form-group__bar\"></i>\r\n          </div>\r\n\r\n          <div class=\"form-group form-group--float form-group--centered\">\r\n              <input type=\"text\" class=\"form-control\" InputFloat>\r\n              <label>Email Address</label>\r\n              <i class=\"form-group__bar\"></i>\r\n          </div>\r\n\r\n          <div class=\"form-group form-group--float form-group--centered\">\r\n              <input type=\"password\" class=\"form-control\" InputFloat>\r\n              <label>Password</label>\r\n              <i class=\"form-group__bar\"></i>\r\n          </div>\r\n\r\n          <div class=\"form-group\" style=\"display: inline-block\">\r\n              <div class=\"checkbox\">\r\n                  <input type=\"checkbox\" id=\"agreement\">\r\n                  <label class=\"checkbox__label\" for=\"agreement\">I have read and accept agreement</label>\r\n              </div>\r\n          </div>\r\n\r\n          <button [routerLink]=\"['/']\" class=\"btn btn--icon bg-blue\"><i class=\"zmdi zmdi-check\"></i></button>\r\n      </div>\r\n  </div>\r\n\r\n  <!-- Forgot Password -->\r\n  <div *ngIf=\"loginStats.forgotPassword\" class=\"login__block\">\r\n      <div class=\"login__header bg-red\">\r\n          <i class=\"zmdi zmdi-account-circle\"></i>\r\n          Forgot Password?\r\n\r\n          <div class=\"actions actions--inverse login__actions\">\r\n              <div class=\"dropdown\" dropdown>\r\n                  <i dropdownToggle class=\"zmdi zmdi-more-vert actions__item\"></i>\r\n\r\n                  <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\r\n                      <a class=\"dropdown-item\" (click)=\"toggleBlock('forgotPassword', 'login')\">Already have an account?</a>\r\n                      <a class=\"dropdown-item\" (click)=\"toggleBlock('forgotPassword', 'signUp')\">Create an account</a>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </div>\r\n\r\n      <div class=\"login__body\">\r\n          <p class=\"mt-4\">Lorem ipsum dolor fringilla enim feugiat commodo sed ac lacus.</p>\r\n\r\n          <div class=\"form-group form-group--float form-group--centered\">\r\n              <input type=\"text\" class=\"form-control\" InputFloat>\r\n              <label>Email Address</label>\r\n              <i class=\"form-group__bar\"></i>\r\n          </div>\r\n\r\n          <button [routerLink]=\"['/']\" class=\"btn btn--icon bg-red\"><i class=\"zmdi zmdi-check\"></i></button>\r\n      </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(API, router, common) {
        this.API = API;
        this.router = router;
        this.common = common;
        this.username = "";
        this.password = "";
        this.loginStats = {
            login: true,
            signUp: false,
            forgotPassword: false
        };
    }
    LoginComponent.prototype.toggleBlock = function (currentBlock, nextBlock) {
        this.loginStats[currentBlock] = false;
        this.loginStats[nextBlock] = true;
    };
    ;
    LoginComponent.prototype.ngOnInit = function () {
        this.getOnlineUser();
    };
    LoginComponent.prototype.getOnlineUser = function () {
        if (localStorage.getItem("user_info") !== null) {
            var uInfo = JSON.parse(localStorage.getItem("user_info"));
            console.log("check", uInfo);
            switch (uInfo.role) {
                case 1:
                    this.router.navigate(['admin']);
                    break;
                case 2:
                    this.router.navigate(['head']);
                default:
                    break;
            }
        }
    };
    LoginComponent.prototype.auth = function () {
        var _this = this;
        if (this.username == "" || this.password == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops!", "Required field(s) missing.", "warning");
        }
        else {
            this.API.post("users/auth", {
                username: this.username,
                password: this.password
            }).subscribe(function (response) {
                if (response.statusCode == 200) {
                    if (response.devMessage == false) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops!", "Invalid username/password.", "warning");
                    }
                    else {
                        localStorage.setItem("user_info", JSON.stringify(response.devMessage));
                        _this.common.setLoggedInUser(response.devMessage); //invoke new Data
                        switch (response.devMessage.role) {
                            case 1:
                                _this.router.navigate(['admin']);
                                break;
                            case 2:
                                _this.router.navigate(['head']);
                            default:
                                break;
                        }
                    }
                }
            }, function (error) {
            });
        }
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/pages/login/login.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _directive_input_float_input_float_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../directive/input-float/input-float.module */ "./src/app/directive/input-float/input-float.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login.component */ "./src/app/pages/login/login.component.ts");








var LOGIN_ROUTE = [{ path: '', component: _login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"] }];
var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"]],
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"].forRoot(),
                _directive_input_float_input_float_module__WEBPACK_IMPORTED_MODULE_5__["InputFloatModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(LOGIN_ROUTE)
            ]
        })
    ], LoginModule);
    return LoginModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module.js.map