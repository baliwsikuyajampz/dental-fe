(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-doctors-view-view-module"],{

/***/ "./src/app/pages/admin/doctors/view/about/about.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/about/about.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-body\">\r\n    <h4 class=\"card-body__title mb-4\">About {{ aboutInfo.name }}</h4>\r\n\r\n    <p>{{ aboutInfo.about }}</p>\r\n    <p>{{ aboutInfo.aboutMore }}</p>\r\n\r\n    <br>\r\n\r\n    <h4 class=\"card-body__title mb-4\">Contact Information</h4>\r\n\r\n    <ul class=\"icon-list\">\r\n      <li *ngFor=\"let contact of aboutInfo.contacts\"><i [class]=\"['zmdi zmdi-'+contact.icon]\"></i>{{ contact.value }}</li>\r\n    </ul>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/doctors/view/about/about.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/about/about.component.ts ***!
  \*******************************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");





var AboutComponent = /** @class */ (function () {
    function AboutComponent(route, API, service) {
        this.route = route;
        this.API = API;
        this.service = service;
        this.aboutInfo = {
            name: 'Malinda Hollaway',
            about: 'Pellentesque vitae quam quis tellus dignissim faucibus. Suspendisse mattis felis at faucibus lobortis. Sed sit amet tellus dolor. Fusce quis sollicitudin velit. Praesent gravida ullamcorper lectus et tincidunt. Phasellus lectus quam, porta pharetra feugiat in, auctor eget dolor.',
            aboutMore: 'Vestibulum tincidunt imperdiet egestas. In in nunc vitae elit tincidunt tristique id eu justo. Quisque gravida maximus orci, vulputate pharetra mauris commodo at. Mauris eget fermentum ipsum, quis faucibus neque. Fusce eleifend sapien sit amet convallis rhoncus. Proin commodo lacinia lectus, et tempus turpis.',
            contacts: [
                {
                    icon: 'phone',
                    value: '308-360-8938'
                },
                {
                    icon: 'email',
                    value: 'malinda@inbound.plus'
                },
                {
                    icon: 'twitter',
                    value: '@mallinda-hollaway'
                },
                {
                    icon: 'facebook',
                    value: 'mallinda-hollaway'
                },
                {
                    icon: 'pin',
                    value: '5470 Madison Street Severna Park, MD 21146'
                }
            ]
        };
    }
    AboutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.data$.subscribe(function (res) { return _this.key = res; });
        this.getPatientInfo();
    };
    AboutComponent.prototype.getPatientInfo = function () {
        var _this = this;
        this.API.post("patients/get-patient-info-by-id", {
            id: this.key
        }).subscribe(function (response) {
            _this.patient_details = response.devMessage;
        }, function (error) {
        });
    };
    AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-about',
            template: __webpack_require__(/*! ./about.component.html */ "./src/app/pages/admin/doctors/view/about/about.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"]])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/doctors/view/commissions/commissions.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/commissions/commissions.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead>\r\n        <tr>\r\n          <th>Patient</th>\r\n          <th>Procedure</th>\r\n          <th>Amount</th>\r\n          <th>Date</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngIf=\"commission_list.length==0\">\r\n            <td align=\"center\" colspan=\"4\">No record(s) found.</td>\r\n          </tr>\r\n          <tr *ngFor=\"let data of commission_list\">\r\n            <td>\r\n              {{ data.patient_name }}\r\n            </td>\r\n            <td>\r\n              {{ data.procedure_name }}\r\n            </td>\r\n            <td>\r\n              {{ data.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'  }}\r\n            </td>\r\n            <td>\r\n              {{ data.covered_date }}\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n        \r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/doctors/view/commissions/commissions.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/commissions/commissions.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL2RvY3RvcnMvdmlldy9jb21taXNzaW9ucy9jb21taXNzaW9ucy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/admin/doctors/view/commissions/commissions.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/commissions/commissions.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CommissionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionsComponent", function() { return CommissionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");




var CommissionsComponent = /** @class */ (function () {
    function CommissionsComponent(API, service) {
        var _this = this;
        this.API = API;
        this.service = service;
        this.key = null;
        this.commission_list = [];
        this.service.doctorKey$.subscribe(function (res) { return _this.key = res; });
    }
    CommissionsComponent.prototype.ngOnInit = function () {
        this.getDoctorCommissionById();
    };
    CommissionsComponent.prototype.getDoctorCommissionById = function () {
        var _this = this;
        this.API.post("doctors/get-doctor-commission-by-id", {
            key: this.key
        }).subscribe(function (response) {
            _this.commission_list = response.devMessage;
        }, function (error) {
        });
    };
    CommissionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-commissions',
            template: __webpack_require__(/*! ./commissions.component.html */ "./src/app/pages/admin/doctors/view/commissions/commissions.component.html"),
            styles: [__webpack_require__(/*! ./commissions.component.scss */ "./src/app/pages/admin/doctors/view/commissions/commissions.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
    ], CommissionsComponent);
    return CommissionsComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/doctors/view/contacts/contacts.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/contacts/contacts.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"contacts row\">\r\n  <div class=\"col-md-3 col-sm-4 col-6\" *ngFor=\"let contact of contacts\">\r\n    <div class=\"contacts__item\">\r\n      <a [routerLink]=\"\" class=\"contacts__img\">\r\n        <img [src]=\"['./assets/demo/img/contacts/'+contact.img]\" alt=\"\">\r\n      </a>\r\n\r\n      <div class=\"contacts__info\">\r\n        <strong>{{ contact.name }}</strong>\r\n        <small>cathy.shelton31@example.com</small>\r\n      </div>\r\n\r\n      <button class=\"contacts__btn\">Following</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<a [routerLink]=\"\" class=\"view-more\">Load more..</a>"

/***/ }),

/***/ "./src/app/pages/admin/doctors/view/contacts/contacts.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/contacts/contacts.component.ts ***!
  \*************************************************************************/
/*! exports provided: ContactsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsComponent", function() { return ContactsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../app.service */ "./src/app/app.service.ts");



var ContactsComponent = /** @class */ (function () {
    function ContactsComponent(service) {
        this.service = service;
        this.contacts = service.contactsItems;
    }
    ContactsComponent.prototype.ngOnInit = function () {
    };
    ContactsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contacts',
            template: __webpack_require__(/*! ./contacts.component.html */ "./src/app/pages/admin/doctors/view/contacts/contacts.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"]])
    ], ContactsComponent);
    return ContactsComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/doctors/view/pictures/pictures.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/pictures/pictures.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row photos\" lightbox-group>\r\n  <div class=\"col-md-2 col-4 photos__item\" *ngFor=\"let item of picturesItems; let index = index\" lightbox [fullImage]=\"{path: './assets/demo/img/photo-gallery/'+ (index + 1) + '.jpg'}\">\r\n    <img [src]=\"['./assets/demo/img/photo-gallery/'+ (index + 1) + '.jpg']\" alt=\"\"/>\r\n  </div>\r\n</div>\r\n\r\n<a [routerLink]=\"\" class=\"view-more text-xs-center\">Load more...</a>"

/***/ }),

/***/ "./src/app/pages/admin/doctors/view/pictures/pictures.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/pictures/pictures.component.ts ***!
  \*************************************************************************/
/*! exports provided: PicturesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PicturesComponent", function() { return PicturesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PicturesComponent = /** @class */ (function () {
    function PicturesComponent() {
        this.picturesItems = new Array(24); // Number of pictures currently available is 24
    }
    PicturesComponent.prototype.ngOnInit = function () {
    };
    PicturesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pictures',
            template: __webpack_require__(/*! ./pictures.component.html */ "./src/app/pages/admin/doctors/view/pictures/pictures.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PicturesComponent);
    return PicturesComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/doctors/view/view.component.html":
/*!**************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/view.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content__inner\">\r\n    <header class=\"content__title\">\r\n      <h1>{{ doctor_info?.fullname }}</h1>\r\n      <small>{{doctor_info?.rank_name}}</small>\r\n\r\n      <div class=\"actions\">\r\n        <a class=\"actions__item zmdi zmdi-long-arrow-left\" tooltip=\"Back to list\" [routerLink]=\"'/admin/dentists/list'\"></a>\r\n      </div>\r\n    </header>\r\n  \r\n    <div class=\"card profile\">\r\n      <div class=\"profile__img\">\r\n        <img [src]=\"profileInfo.img\" alt=\"\">\r\n  \r\n        <a [routerLink]=\"\" class=\"zmdi zmdi-camera profile__img__edit\"></a>\r\n      </div>\r\n  \r\n      <div class=\"profile__info\">\r\n        <p>{{ patient_info?.fullname }}</p>\r\n  \r\n        <ul class=\"icon-list\">\r\n          <li><i class=\"zmdi zmdi-phone\"></i> {{doctor_info?.contact_no}}</li>\r\n          <li><i class=\"zmdi zmdi-email\"></i> {{doctor_info?.email}}</li>\r\n          <li><i class=\"zmdi zmdi-pin\"></i> {{doctor_info?.branch_name}}</li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  \r\n    <div class=\"toolbar\">\r\n      <nav class=\"toolbar__nav\">\r\n        <a routerLinkActive=\"active\" [routerLink]=\"['about']\">About</a>\r\n        <a routerLinkActive=\"active\" [routerLink]=\"['commissions']\">Commissions</a>\r\n      </nav>\r\n  \r\n      <div class=\"actions\">\r\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"profileSearch = true\"></i>\r\n      </div>\r\n  \r\n      <div *ngIf=\"profileSearch\" class=\"toolbar__search\">\r\n        <input type=\"text\" placeholder=\"Search...\">\r\n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"profileSearch = false\"></i>\r\n      </div>\r\n    </div>\r\n  \r\n    <router-outlet></router-outlet>\r\n  </div>"

/***/ }),

/***/ "./src/app/pages/admin/doctors/view/view.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/view.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media (min-width: 768px) {\n  .profile {\n    display: flex;\n    align-items: center;\n    flex-direction: row; } }\n\n@media (max-width: 767.98px) {\n  .profile {\n    margin-top: 75px;\n    text-align: center; } }\n\n.profile__img {\n  padding: 5px;\n  position: relative; }\n\n.profile__img img {\n    max-width: 200px;\n    border-radius: 2px; }\n\n@media (max-width: 767.98px) {\n    .profile__img img {\n      margin: -55px 0 -10px;\n      width: 120px;\n      border: 5px solid #FFFFFF;\n      border-radius: 50%; } }\n\n.profile__img__edit {\n  position: absolute;\n  font-size: 1.2rem;\n  top: 15px;\n  left: 15px;\n  background-color: rgba(0, 0, 0, 0.4);\n  width: 30px;\n  height: 30px;\n  line-height: 30px;\n  border-radius: 50%;\n  text-align: center;\n  color: #FFFFFF; }\n\n.profile__img__edit:hover {\n    background-color: rgba(0, 0, 0, 0.65);\n    color: #FFFFFF; }\n\n.profile__info {\n  padding: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vZG9jdG9ycy92aWV3L0M6XFxVc2Vyc1xcQURNSU5cXERlc2t0b3BcXFByb2plY3RzXFxkZW50YWwtZmUvbm9kZV9tb2R1bGVzXFxib290c3RyYXBcXHNjc3NcXG1peGluc1xcX2JyZWFrcG9pbnRzLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2FkbWluL2RvY3RvcnMvdmlldy9DOlxcVXNlcnNcXEFETUlOXFxEZXNrdG9wXFxQcm9qZWN0c1xcZGVudGFsLWZlL3NyY1xcYXBwXFxwYWdlc1xcYWRtaW5cXGRvY3RvcnNcXHZpZXdcXHZpZXcuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2FkbWluL2RvY3RvcnMvdmlldy9DOlxcVXNlcnNcXEFETUlOXFxEZXNrdG9wXFxQcm9qZWN0c1xcZGVudGFsLWZlL3NyY1xcYXNzZXRzXFxzY3NzXFxfdmFyaWFibGVzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBNERJO0VDMURKO0lBRUksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixtQkFBbUIsRUFBQSxFQU90Qjs7QUQ0REc7RUN2RUo7SUFRSSxnQkFBZ0I7SUFDaEIsa0JBQWtCLEVBQUEsRUFFckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRnBCO0lBS0ksZ0JBQWdCO0lBQ2hCLGtCQ2dJZSxFQUFBOztBRjVFZjtJQzFESjtNQVdNLHFCQUFxQjtNQUNyQixZQUFZO01BQ1oseUJDVVM7TURUVCxrQkFBa0IsRUFBQSxFQUNuQjs7QUFJTDtFQUNFLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsU0FBUztFQUNULFVBQVU7RUFDVixvQ0NGYTtFREdiLFdBQVc7RUFDWCxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsY0NQYSxFQUFBOztBREpmO0lBY0kscUNDWFc7SURZWCxjQ1hXLEVBQUE7O0FEZWY7RUFDRSxhQUFhLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9hZG1pbi9kb2N0b3JzL3ZpZXcvdmlldy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEJyZWFrcG9pbnQgdmlld3BvcnQgc2l6ZXMgYW5kIG1lZGlhIHF1ZXJpZXMuXG4vL1xuLy8gQnJlYWtwb2ludHMgYXJlIGRlZmluZWQgYXMgYSBtYXAgb2YgKG5hbWU6IG1pbmltdW0gd2lkdGgpLCBvcmRlciBmcm9tIHNtYWxsIHRvIGxhcmdlOlxuLy9cbi8vICAgICh4czogMCwgc206IDU3NnB4LCBtZDogNzY4cHgsIGxnOiA5OTJweCwgeGw6IDEyMDBweClcbi8vXG4vLyBUaGUgbWFwIGRlZmluZWQgaW4gdGhlIGAkZ3JpZC1icmVha3BvaW50c2AgZ2xvYmFsIHZhcmlhYmxlIGlzIHVzZWQgYXMgdGhlIGAkYnJlYWtwb2ludHNgIGFyZ3VtZW50IGJ5IGRlZmF1bHQuXG5cbi8vIE5hbWUgb2YgdGhlIG5leHQgYnJlYWtwb2ludCwgb3IgbnVsbCBmb3IgdGhlIGxhc3QgYnJlYWtwb2ludC5cbi8vXG4vLyAgICA+PiBicmVha3BvaW50LW5leHQoc20pXG4vLyAgICBtZFxuLy8gICAgPj4gYnJlYWtwb2ludC1uZXh0KHNtLCAoeHM6IDAsIHNtOiA1NzZweCwgbWQ6IDc2OHB4LCBsZzogOTkycHgsIHhsOiAxMjAwcHgpKVxuLy8gICAgbWRcbi8vICAgID4+IGJyZWFrcG9pbnQtbmV4dChzbSwgJGJyZWFrcG9pbnQtbmFtZXM6ICh4cyBzbSBtZCBsZyB4bCkpXG4vLyAgICBtZFxuQGZ1bmN0aW9uIGJyZWFrcG9pbnQtbmV4dCgkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cywgJGJyZWFrcG9pbnQtbmFtZXM6IG1hcC1rZXlzKCRicmVha3BvaW50cykpIHtcbiAgJG46IGluZGV4KCRicmVha3BvaW50LW5hbWVzLCAkbmFtZSk7XG4gIEByZXR1cm4gaWYoJG4gIT0gbnVsbCBhbmQgJG4gPCBsZW5ndGgoJGJyZWFrcG9pbnQtbmFtZXMpLCBudGgoJGJyZWFrcG9pbnQtbmFtZXMsICRuICsgMSksIG51bGwpO1xufVxuXG4vLyBNaW5pbXVtIGJyZWFrcG9pbnQgd2lkdGguIE51bGwgZm9yIHRoZSBzbWFsbGVzdCAoZmlyc3QpIGJyZWFrcG9pbnQuXG4vL1xuLy8gICAgPj4gYnJlYWtwb2ludC1taW4oc20sICh4czogMCwgc206IDU3NnB4LCBtZDogNzY4cHgsIGxnOiA5OTJweCwgeGw6IDEyMDBweCkpXG4vLyAgICA1NzZweFxuQGZ1bmN0aW9uIGJyZWFrcG9pbnQtbWluKCRuYW1lLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gICRtaW46IG1hcC1nZXQoJGJyZWFrcG9pbnRzLCAkbmFtZSk7XG4gIEByZXR1cm4gaWYoJG1pbiAhPSAwLCAkbWluLCBudWxsKTtcbn1cblxuLy8gTWF4aW11bSBicmVha3BvaW50IHdpZHRoLiBOdWxsIGZvciB0aGUgbGFyZ2VzdCAobGFzdCkgYnJlYWtwb2ludC5cbi8vIFRoZSBtYXhpbXVtIHZhbHVlIGlzIGNhbGN1bGF0ZWQgYXMgdGhlIG1pbmltdW0gb2YgdGhlIG5leHQgb25lIGxlc3MgMC4wMnB4XG4vLyB0byB3b3JrIGFyb3VuZCB0aGUgbGltaXRhdGlvbnMgb2YgYG1pbi1gIGFuZCBgbWF4LWAgcHJlZml4ZXMgYW5kIHZpZXdwb3J0cyB3aXRoIGZyYWN0aW9uYWwgd2lkdGhzLlxuLy8gU2VlIGh0dHBzOi8vd3d3LnczLm9yZy9UUi9tZWRpYXF1ZXJpZXMtNC8jbXEtbWluLW1heFxuLy8gVXNlcyAwLjAycHggcmF0aGVyIHRoYW4gMC4wMXB4IHRvIHdvcmsgYXJvdW5kIGEgY3VycmVudCByb3VuZGluZyBidWcgaW4gU2FmYXJpLlxuLy8gU2VlIGh0dHBzOi8vYnVncy53ZWJraXQub3JnL3Nob3dfYnVnLmNnaT9pZD0xNzgyNjFcbi8vXG4vLyAgICA+PiBicmVha3BvaW50LW1heChzbSwgKHhzOiAwLCBzbTogNTc2cHgsIG1kOiA3NjhweCwgbGc6IDk5MnB4LCB4bDogMTIwMHB4KSlcbi8vICAgIDc2Ny45OHB4XG5AZnVuY3Rpb24gYnJlYWtwb2ludC1tYXgoJG5hbWUsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMpIHtcbiAgJG5leHQ6IGJyZWFrcG9pbnQtbmV4dCgkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgQHJldHVybiBpZigkbmV4dCwgYnJlYWtwb2ludC1taW4oJG5leHQsICRicmVha3BvaW50cykgLSAuMDIsIG51bGwpO1xufVxuXG4vLyBSZXR1cm5zIGEgYmxhbmsgc3RyaW5nIGlmIHNtYWxsZXN0IGJyZWFrcG9pbnQsIG90aGVyd2lzZSByZXR1cm5zIHRoZSBuYW1lIHdpdGggYSBkYXNoIGluIGZyb250LlxuLy8gVXNlZnVsIGZvciBtYWtpbmcgcmVzcG9uc2l2ZSB1dGlsaXRpZXMuXG4vL1xuLy8gICAgPj4gYnJlYWtwb2ludC1pbmZpeCh4cywgKHhzOiAwLCBzbTogNTc2cHgsIG1kOiA3NjhweCwgbGc6IDk5MnB4LCB4bDogMTIwMHB4KSlcbi8vICAgIFwiXCIgIChSZXR1cm5zIGEgYmxhbmsgc3RyaW5nKVxuLy8gICAgPj4gYnJlYWtwb2ludC1pbmZpeChzbSwgKHhzOiAwLCBzbTogNTc2cHgsIG1kOiA3NjhweCwgbGc6IDk5MnB4LCB4bDogMTIwMHB4KSlcbi8vICAgIFwiLXNtXCJcbkBmdW5jdGlvbiBicmVha3BvaW50LWluZml4KCRuYW1lLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gIEByZXR1cm4gaWYoYnJlYWtwb2ludC1taW4oJG5hbWUsICRicmVha3BvaW50cykgPT0gbnVsbCwgXCJcIiwgXCItI3skbmFtZX1cIik7XG59XG5cbi8vIE1lZGlhIG9mIGF0IGxlYXN0IHRoZSBtaW5pbXVtIGJyZWFrcG9pbnQgd2lkdGguIE5vIHF1ZXJ5IGZvciB0aGUgc21hbGxlc3QgYnJlYWtwb2ludC5cbi8vIE1ha2VzIHRoZSBAY29udGVudCBhcHBseSB0byB0aGUgZ2l2ZW4gYnJlYWtwb2ludCBhbmQgd2lkZXIuXG5AbWl4aW4gbWVkaWEtYnJlYWtwb2ludC11cCgkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWluOiBicmVha3BvaW50LW1pbigkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgQGlmICRtaW4ge1xuICAgIEBtZWRpYSAobWluLXdpZHRoOiAkbWluKSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2Uge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8vIE1lZGlhIG9mIGF0IG1vc3QgdGhlIG1heGltdW0gYnJlYWtwb2ludCB3aWR0aC4gTm8gcXVlcnkgZm9yIHRoZSBsYXJnZXN0IGJyZWFrcG9pbnQuXG4vLyBNYWtlcyB0aGUgQGNvbnRlbnQgYXBwbHkgdG8gdGhlIGdpdmVuIGJyZWFrcG9pbnQgYW5kIG5hcnJvd2VyLlxuQG1peGluIG1lZGlhLWJyZWFrcG9pbnQtZG93bigkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWF4OiBicmVha3BvaW50LW1heCgkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgQGlmICRtYXgge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAkbWF4KSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2Uge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8vIE1lZGlhIHRoYXQgc3BhbnMgbXVsdGlwbGUgYnJlYWtwb2ludCB3aWR0aHMuXG4vLyBNYWtlcyB0aGUgQGNvbnRlbnQgYXBwbHkgYmV0d2VlbiB0aGUgbWluIGFuZCBtYXggYnJlYWtwb2ludHNcbkBtaXhpbiBtZWRpYS1icmVha3BvaW50LWJldHdlZW4oJGxvd2VyLCAkdXBwZXIsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMpIHtcbiAgJG1pbjogYnJlYWtwb2ludC1taW4oJGxvd2VyLCAkYnJlYWtwb2ludHMpO1xuICAkbWF4OiBicmVha3BvaW50LW1heCgkdXBwZXIsICRicmVha3BvaW50cyk7XG5cbiAgQGlmICRtaW4gIT0gbnVsbCBhbmQgJG1heCAhPSBudWxsIHtcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogJG1pbikgYW5kIChtYXgtd2lkdGg6ICRtYXgpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSBpZiAkbWF4ID09IG51bGwge1xuICAgIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtdXAoJGxvd2VyLCAkYnJlYWtwb2ludHMpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSBpZiAkbWluID09IG51bGwge1xuICAgIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtZG93bigkdXBwZXIsICRicmVha3BvaW50cykge1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbi8vIE1lZGlhIGJldHdlZW4gdGhlIGJyZWFrcG9pbnQncyBtaW5pbXVtIGFuZCBtYXhpbXVtIHdpZHRocy5cbi8vIE5vIG1pbmltdW0gZm9yIHRoZSBzbWFsbGVzdCBicmVha3BvaW50LCBhbmQgbm8gbWF4aW11bSBmb3IgdGhlIGxhcmdlc3Qgb25lLlxuLy8gTWFrZXMgdGhlIEBjb250ZW50IGFwcGx5IG9ubHkgdG8gdGhlIGdpdmVuIGJyZWFrcG9pbnQsIG5vdCB2aWV3cG9ydHMgYW55IHdpZGVyIG9yIG5hcnJvd2VyLlxuQG1peGluIG1lZGlhLWJyZWFrcG9pbnQtb25seSgkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWluOiBicmVha3BvaW50LW1pbigkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgJG1heDogYnJlYWtwb2ludC1tYXgoJG5hbWUsICRicmVha3BvaW50cyk7XG5cbiAgQGlmICRtaW4gIT0gbnVsbCBhbmQgJG1heCAhPSBudWxsIHtcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogJG1pbikgYW5kIChtYXgtd2lkdGg6ICRtYXgpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSBpZiAkbWF4ID09IG51bGwge1xuICAgIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtdXAoJG5hbWUsICRicmVha3BvaW50cykge1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9IEBlbHNlIGlmICRtaW4gPT0gbnVsbCB7XG4gICAgQGluY2x1ZGUgbWVkaWEtYnJlYWtwb2ludC1kb3duKCRuYW1lLCAkYnJlYWtwb2ludHMpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuIiwiQGltcG9ydCBcIi4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zY3NzL3Njc3MtaW1wb3J0c1wiO1xyXG5cclxuLnByb2ZpbGUge1xyXG4gIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtdXAobWQpIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICB9XHJcblxyXG4gIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtZG93bihzbSkge1xyXG4gICAgbWFyZ2luLXRvcDogNzVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbn1cclxuXHJcbi5wcm9maWxlX19pbWcge1xyXG4gIHBhZGRpbmc6IDVweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gIGltZyB7XHJcbiAgICBtYXgtd2lkdGg6IDIwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogJGJvcmRlci1yYWRpdXM7XHJcbiAgfVxyXG5cclxuICBAaW5jbHVkZSBtZWRpYS1icmVha3BvaW50LWRvd24oc20pe1xyXG4gICAgaW1nIHtcclxuICAgICAgbWFyZ2luOiAtNTVweCAwIC0xMHB4O1xyXG4gICAgICB3aWR0aDogMTIwcHg7XHJcbiAgICAgIGJvcmRlcjogNXB4IHNvbGlkICR3aGl0ZTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnByb2ZpbGVfX2ltZ19fZWRpdCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xyXG4gIHRvcDogMTVweDtcclxuICBsZWZ0OiAxNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoJGJsYWNrLDAuNCk7XHJcbiAgd2lkdGg6IDMwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6ICR3aGl0ZTtcclxuXHJcbiAgJjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKCRibGFjaywwLjY1KTtcclxuICAgIGNvbG9yOiAkd2hpdGU7XHJcbiAgfVxyXG59XHJcblxyXG4ucHJvZmlsZV9faW5mbyB7XHJcbiAgcGFkZGluZzogMzBweDtcclxufVxyXG4iLCIvLyBPcHRpb25zXHJcbiRlbmFibGUtdHJhbnNpdGlvbnM6IHRydWU7XHJcbiRlbmFibGUtc2hhZG93czogZmFsc2U7XHJcblxyXG5cclxuLy8gQ29sb3JzXHJcbiRncmF5LTUwOiAjZjlmOWY5O1xyXG4kZ3JheS0xMDA6ICNmNmY2ZjY7XHJcbiRncmF5LTIwMDogI2U5ZWNlZjtcclxuJGdyYXktMzAwOiAjZGVlMmU2O1xyXG4kZ3JheS00MDA6ICNjZWQ0ZGE7XHJcbiRncmF5LTUwMDogI2FkYjViZDtcclxuJGdyYXktNjAwOiAjODY4ZTk2O1xyXG4kZ3JheS03MDA6ICM0OTUwNTc7XHJcbiRncmF5LTgwMDogIzM0M2E0MDtcclxuJGdyYXktOTAwOiAjMjEyNTI5O1xyXG5cclxuJHR1cnF1b2lzZTogIzMwRDVDODtcclxuJHJlZDogI2ZmNmI2ODtcclxuJHBpbms6ICNGRkMwQ0I7XHJcbiRwdXJwbGU6ICNkMDY2ZTI7XHJcbiRkZWVwLXB1cnBsZTogIzY3M0FCNztcclxuJGluZGlnbzogIzNGNTFCNTtcclxuJGJsdWU6ICMyZTc4YmQ7XHJcbiRsaWdodC1ibHVlOiAjMDNBOUY0O1xyXG4kY3lhbjogIzE3NzU3NDtcclxuJHRlYWw6ICMyRUJEQkI7XHJcbiRncmVlbjogIzMyYzc4NztcclxuJGxpZ2h0LWdyZWVuOiAjOEJDMzRBO1xyXG4kbGltZTogI0NEREMzOTtcclxuJHllbGxvdzogI0ZGRUIzQjtcclxuJGFtYmVyOiAjZmZjNzIxO1xyXG4kb3JhbmdlOiAjRkY5ODAwO1xyXG4kZGVlcC1vcmFuZ2U6ICNGRjU3MjI7XHJcbiRicm93bjogIzc5NTU0ODtcclxuJGdyZXk6ICM5RTlFOUU7XHJcbiRibHVlLWdyZXk6ICM2MDdEOEI7XHJcbiRibGFjazogIzAwMDAwMDtcclxuJHdoaXRlOiAjRkZGRkZGO1xyXG5cclxuJGNvbG9yczogKFxyXG4gIHdoaXRlOiAkd2hpdGUsXHJcbiAgYmxhY2s6ICRibGFjayxcclxuICByZWQ6ICRyZWQsXHJcbiAgcGluazogJHBpbmssXHJcbiAgcHVycGxlOiAkcHVycGxlLFxyXG4gIGRlZXAtcHVycGxlOiAkZGVlcC1wdXJwbGUsXHJcbiAgaW5kaWdvOiAkaW5kaWdvLFxyXG4gIGJsdWU6ICRibHVlLFxyXG4gIGxpZ2h0LWJsdWU6ICRsaWdodC1ibHVlLFxyXG4gIGN5YW46ICRjeWFuLFxyXG4gIHRlYWw6ICR0ZWFsLFxyXG4gIGdyZWVuOiAkZ3JlZW4sXHJcbiAgbGlnaHQtZ3JlZW46ICRsaWdodC1ncmVlbixcclxuICBsaW1lOiAkbGltZSxcclxuICB5ZWxsb3c6ICR5ZWxsb3csXHJcbiAgYW1iZXI6ICRhbWJlcixcclxuICBvcmFuZ2U6ICRvcmFuZ2UsXHJcbiAgZGVlcC1vcmFuZ2U6ICRkZWVwLW9yYW5nZSxcclxuICBicm93bjogJGJyb3duLFxyXG4gIGJsdWUtZ3JleTogJGJsdWUtZ3JleSxcclxuKTtcclxuXHJcbi8vIFRoZW1lIENvbG9yc1xyXG4kdGhlbWUtY29sb3JzOiAoXHJcbiAgcHJpbWFyeTogJGJsdWUsXHJcbiAgc2Vjb25kYXJ5OiAkZ3JheS02MDAsXHJcbiAgc3VjY2VzczogJGdyZWVuLFxyXG4gIGluZm86ICRsaWdodC1ibHVlLFxyXG4gIHdhcm5pbmc6ICRhbWJlcixcclxuICBkYW5nZXI6ICRyZWQsXHJcbiAgZGFyazogJGdyYXktNzAwXHJcbik7XHJcblxyXG4keWlxLXRleHQtZGFyazogIzUyNWE2MjtcclxuXHJcblxyXG4vLyBUeXBvZ3JhcGh5XHJcbiRmb250LWZhbWlseS1zYW5zLXNlcmlmOiAnUm9ib3RvJywgc2Fucy1zZXJpZjtcclxuJHRleHQtbXV0ZWQ6ICM5YzljOWM7XHJcbiR0ZXh0LW11dGVkLWhvdmVyOiBkYXJrZW4oJHRleHQtbXV0ZWQsIDE1JSk7XHJcbiRoZWFkaW5ncy1jb2xvcjogIzMzMztcclxuJGZvbnQtd2VpZ2h0LWJvbGQ6IDUwMDtcclxuJGZvbnQtZmFtaWx5LWljb246ICdNYXRlcmlhbC1EZXNpZ24tSWNvbmljLUZvbnQnO1xyXG4kZm9udC1zaXplLXJvb3Q6IDEzLjVweDtcclxuXHJcblxyXG4vLyBMaW5rc1xyXG4kbGluay1jb2xvcjogJGJsdWU7XHJcbiRsaW5rLWhvdmVyLWRlY29yYXRpb246IG5vbmU7XHJcblxyXG5cclxuLy8gQm9keVxyXG4kYm9keS1iZzogI2YzZjNmMztcclxuJGJvZHktY29sb3I6ICM3NDdhODA7XHJcblxyXG5cclxuLy8gRm9ybVxyXG4kaW5wdXQtYmc6IHRyYW5zcGFyZW50O1xyXG4kaW5wdXQtZGlzYWJsZWQtYmc6IHRyYW5zcGFyZW50O1xyXG4kaW5wdXQtYm94LXNoYWRvdzogcmdiYSgkYmxhY2sgLDApO1xyXG4kaW5wdXQtYm9yZGVyLWNvbG9yOiBkYXJrZW4oJGdyYXktMjAwLCAxJSk7XHJcbiRpbnB1dC1mb2N1cy1ib3JkZXItY29sb3I6ICRpbnB1dC1ib3JkZXItY29sb3I7XHJcbiRpbnB1dC1ib3JkZXItcmFkaXVzOiAwO1xyXG4kaW5wdXQtYm9yZGVyLXJhZGl1cy1sZzogMDtcclxuJGlucHV0LWJvcmRlci1yYWRpdXMtc206IDA7XHJcbiRmb3JtLWdyb3VwLW1hcmdpbi1ib3R0b206IDJyZW07XHJcbiRpbnB1dC1mb2N1cy1ib3gtc2hhZG93OiBub25lO1xyXG4kaW5wdXQtcGFkZGluZy14OiAwO1xyXG4kaW5wdXQtcGFkZGluZy14LWxnOiAkaW5wdXQtcGFkZGluZy14O1xyXG4kaW5wdXQtcGFkZGluZy14LXNtOiAkaW5wdXQtcGFkZGluZy14O1xyXG5cclxuXHJcbi8vIENoZWNib3ggYW5kIFJhZGlvXHJcbiRjaGVja2JveC1yYWRpby1zaXplOiAxOHB4O1xyXG4kY2hlY2tib3gtcmFkaW8tYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiRjaGVja2JveC1yYWRpby1jaGVja2VkLWJhY2tncm91bmQtY29sb3I6ICRibHVlO1xyXG4kY2hlY2tib3gtcmFkaW8tYm9yZGVyLWNvbG9yOiAjNmU2ZTZlO1xyXG4kY2hlY2tib3gtcmFkaW8tY2hlY2tlZC1ib3JkZXItY29sb3I6ICRjaGVja2JveC1yYWRpby1jaGVja2VkLWJhY2tncm91bmQtY29sb3I7XHJcblxyXG5cclxuLy8gTGF5b3V0XHJcbiRjb250ZW50LXRpdGxlLWhlYWRpbmctY29sb3I6ICM2NzY3Njc7XHJcblxyXG5cclxuLy8gSGVhZGVyXHJcbiRoZWFkZXItaGVpZ2h0OiA3MnB4O1xyXG4kaGVhZGVyLXNoYWRvdzogMCA1cHggNXB4IC0zcHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcclxuJGhlYWRlci16LWluZGV4OiAyMDtcclxuXHJcblxyXG4vLyBEcm9wZG93blxyXG4kZHJvcGRvd24tYm9yZGVyLXdpZHRoOiAwO1xyXG4kZHJvcGRvd24tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuJGRyb3Bkb3duLWJveC1zaGFkb3c6IDAgNHB4IDE4cHggcmdiYSgkYmxhY2ssIDAuMTEpO1xyXG4kZHJvcGRvd24tbGluay1jb2xvcjogIzZkNmQ2ZDtcclxuJGRyb3Bkb3duLWxpbmstYWN0aXZlLWNvbG9yOiAkZHJvcGRvd24tbGluay1jb2xvcjtcclxuJGRyb3Bkb3duLWxpbmstYWN0aXZlLWJnOiAkZ3JheS0xMDA7XHJcbiRkcm9wZG93bi1saW5rLWRpc2FibGVkLWNvbG9yOiAkZ3JheS01MDA7XHJcbiRkcm9wZG93bi1saW5rLWhvdmVyLWJnOiAkZ3JheS01MDtcclxuJGRyb3Bkb3duLXBhZGRpbmcteTogMC44cmVtO1xyXG4kZHJvcGRvd24taXRlbS1wYWRkaW5nLXg6IDEuNXJlbTtcclxuJGRyb3Bkb3duLWl0ZW0tcGFkZGluZy15OiAwLjVyZW07XHJcbiRkcm9wZG93bi1zcGFjZXI6IDA7XHJcbiRkcm9wZG93bi1kaXZpZGVyLWJnOiAkZ3JheS0xMDA7XHJcbiRkcm9wZG93bi1oZWFkZXItY29sb3I6ICRncmF5LTUwMDtcclxuXHJcblxyXG4vLyBCb3JkZXIgUmFkaXVzXHJcbiRib3JkZXItcmFkaXVzOiAycHg7XHJcbiRib3JkZXItcmFkaXVzLWxnOiAycHg7XHJcbiRib3JkZXItcmFkaXVzLXNtOiAycHg7XHJcblxyXG5cclxuLy8gTGlzdCBHcm91cFxyXG4kbGlzdC1ncm91cC1iZzogdHJhbnNwYXJlbnQ7XHJcbiRsaXN0LWdyb3VwLWJvcmRlci13aWR0aDogMDtcclxuJGxpc3QtZ3JvdXAtaG92ZXItYmc6IGJsYWNrO1xyXG4kbGlzdC1ncm91cC1hY3RpdmUtYmc6ICRibHVlO1xyXG4kbGlzdC1ncm91cC1pdGVtLXBhZGRpbmcteDogMnJlbTtcclxuJGxpc3QtZ3JvdXAtaXRlbS1wYWRkaW5nLXk6IDFyZW07XHJcblxyXG5cclxuLy8gUHJvZ3Jlc3MgQmFyXHJcbiRwcm9ncmVzcy1ib3gtc2hhZG93OiBub25lO1xyXG4kcHJvZ3Jlc3MtYmFyLWNvbG9yOiAkYmx1ZTtcclxuJHByb2dyZXNzLWJnOiAkZ3JheS0yMDA7XHJcbiRwcm9ncmVzcy1oZWlnaHQ6IDVweDtcclxuXHJcblxyXG4vLyBDYXJkXHJcbiRjYXJkLWlubmVyLWJvcmRlci1yYWRpdXM6ICRib3JkZXItcmFkaXVzO1xyXG4kY2FyZC1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xyXG4kY2FyZC1ib3JkZXItcmFkaXVzOiAkYm9yZGVyLXJhZGl1cztcclxuJGNhcmQtc3BhY2VyLXg6IDIuMnJlbTtcclxuJGNhcmQtc3BhY2VyLXk6IDIuMXJlbTtcclxuJGNhcmQtY2FwLWJnOiB0cmFuc3BhcmVudDtcclxuJGNhcmQtaW1nLW92ZXJsYXktcGFkZGluZzogMDtcclxuJGNhcmQtc2hhZG93OiAwIDFweCAycHggcmdiYSgkYmxhY2ssMC4wNzUpO1xyXG4kY2FyZC1jb2x1bW5zLW1hcmdpbjogMi4zcmVtO1xyXG4kY2FyZC1oZWFkZXItYmc6ICRncmF5LTUwO1xyXG5cclxuLy8gU2lkZWJhcnNcclxuJHNpZGViYXItd2lkdGg6IDI4MHB4O1xyXG4kc2lkZWJhci16LWluZGV4OiAkaGVhZGVyLXotaW5kZXggLSAxO1xyXG4kbmF2aWdhdGlvbi1saW5rLWNvbG9yOiAkYm9keS1jb2xvcjtcclxuJG5hdmlnYXRpb24tbGluay1hY3RpdmUtY29sb3I6ICR3aGl0ZTtcclxuJG5hdmlnYXRpb24tbGluay1ob3Zlci1iZzogcmdiYSgwLCAwLCAwLCAwLjA0KTtcclxuXHJcblxyXG4vLyBUYWJlbHNcclxuJHRhYmxlLWJvcmRlci1jb2xvcjogbGlnaHRlbigkZ3JheS0yMDAsIDMlKTtcclxuJHRhYmxlLWRhcmstYmc6ICMzMTNhNDQ7XHJcbiR0YWJsZS1kYXJrLWJvcmRlci1jb2xvcjogIzNlNDY0ZTtcclxuJHRhYmxlLWRhcmstY29sb3I6ICNmM2YzZjM7XHJcbiR0YWJsZS1hY2NlbnQtYmc6ICR0YWJsZS1ib3JkZXItY29sb3I7XHJcbiR0YWJsZS1ob3Zlci1iZzogJHRhYmxlLWFjY2VudC1iZztcclxuJHRhYmxlLWNlbGwtcGFkZGluZzogMXJlbSAxLjVyZW07XHJcbiR0YWJsZS1jZWxsLXBhZGRpbmctc206IDAuNzVyZW0gMXJlbTtcclxuJHRhYmxlLWhlYWQtYmc6IGxpZ2h0ZW4oJGdyYXktMjAwLCAzJSk7XHJcblxyXG5cclxuLy8gUGFnaW5hdGlvblxyXG4kcGFnaW5hdGlvbi1ib3JkZXItd2lkdGg6IDA7XHJcbiRwYWdpbmF0aW9uLXBhZGRpbmcteTogMDtcclxuJHBhZ2luYXRpb24tcGFkZGluZy14OiAwO1xyXG4kcGFnaW5hdGlvbi1iZzogJGJvZHktYmc7XHJcbiRwYWdpbmF0aW9uLWhvdmVyLWJnOiBkYXJrZW4oJHBhZ2luYXRpb24tYmcsIDUlKTtcclxuJHBhZ2luYXRpb24tY29sb3I6IGxpZ2h0ZW4oJGJvZHktY29sb3IsIDEwJSk7XHJcbiRwYWdpbmF0aW9uLWhvdmVyLWNvbG9yOiBkYXJrZW4oJHBhZ2luYXRpb24tY29sb3IsIDUlKTtcclxuJHBhZ2luYXRpb24tZGlzYWJsZWQtYmc6ICRwYWdpbmF0aW9uLWJnO1xyXG4kcGFnaW5hdGlvbi1kaXNhYmxlZC1jb2xvcjogJHBhZ2luYXRpb24tY29sb3I7XHJcbiRwYWdpbmF0aW9uLWFjdGl2ZS1iZzogJGJsdWU7XHJcblxyXG5cclxuLy8gQ29kZVxyXG4kcHJlLWNvbG9yOiAkd2hpdGU7XHJcbiRwcmUtYmc6ICRncmF5LTgwMDtcclxuJHByZS1ib3JkZXItY29sb3I6ICRwcmUtYmc7XHJcblxyXG5cclxuLy8gQWxlcnRcclxuJGFsZXJ0LWxpbmstZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuJGFsZXJ0LWJvcmRlci13aWR0aDogMDtcclxuJGFsZXJ0LXBhZGRpbmcteDogMS41cmVtO1xyXG4kYWxlcnQtcGFkZGluZy15OiAxLjFyZW07XHJcbiRhbGVydC1iZy1sZXZlbDogMDtcclxuJGFsZXJ0LWNvbG9yLWxldmVsOiAtMTIuNTtcclxuXHJcblxyXG4vLyBDbG9zZVxyXG4kY2xvc2UtZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuJGNsb3NlLXRleHQtc2hhZG93OiBub25lO1xyXG5cclxuXHJcbi8vIEJhZGdlc1xyXG4kYmFkZ2UtcGFkZGluZy15OiAwLjVyZW07XHJcbiRiYWRnZS1wYWRkaW5nLXg6IDFyZW07XHJcbiRiYWRnZS1mb250LXNpemU6IDkwJTtcclxuJGJhZGdlLWZvbnQtd2VpZ2h0OiA1MDA7XHJcbiRiYWRnZS1waWxsLXBhZGRpbmcteDogJGJhZGdlLXBhZGRpbmcteDtcclxuJGJhZGdlLXBpbGwtcGFkZGluZy15OiAkYmFkZ2UtcGFkZGluZy15O1xyXG5cclxuXHJcbi8vIEJyZWFkY3J1bWJzXHJcbiRicmVhZGNydW1iLWRpdmlkZXI6ICdcXGYzMGYnO1xyXG4kYnJlYWRjcnVtYi1iZzogdHJhbnNwYXJlbnQ7XHJcbiRicmVhZGNydW1iLXBhZGRpbmcteDogMC4yNXJlbTtcclxuXHJcblxyXG4vLyBDYXJvc3VlbFxyXG4kY2Fyb3VzZWwtY29udHJvbC1wcmV2LWljb24tYmc6IG5vbmU7XHJcbiRjYXJvdXNlbC1jb250cm9sLW5leHQtaWNvbi1iZzogbm9uZTtcclxuJGNhcm91c2VsLWNvbnRyb2wtaWNvbi13aWR0aDogNDBweDtcclxuJGNhcm91c2VsLWNvbnRyb2wtb3BhY2l0eTogMC44O1xyXG4kY2Fyb3VzZWwtY2FwdGlvbi1jb2xvcjogcmdiYSgkd2hpdGUsIDAuOSk7XHJcblxyXG5cclxuLy8gTW9kYWxcclxuJG1vZGFsLWJhY2tkcm9wLW9wYWNpdHk6IDAuMjtcclxuJG1vZGFsLWNvbnRlbnQtYm94LXNoYWRvdy14czogMCA1cHggMjBweCByZ2JhKCRibGFjaywuMDcpO1xyXG4kbW9kYWwtY29udGVudC1ib3gtc2hhZG93LXNtLXVwOiAkbW9kYWwtY29udGVudC1ib3gtc2hhZG93LXhzO1xyXG4kbW9kYWwtY29udGVudC1ib3JkZXItd2lkdGg6IDA7XHJcbiRtb2RhbC1oZWFkZXItYm9yZGVyLXdpZHRoOiAwO1xyXG4kbW9kYWwtZm9vdGVyLWJvcmRlci13aWR0aDogMDtcclxuJG1vZGFsLWhlYWRlci1wYWRkaW5nOiAyNXB4IDMwcHggMDtcclxuJG1vZGFsLWlubmVyLXBhZGRpbmc6IDI1cHggMzBweDtcclxuJG1vZGFsLWxnOiAxMDAwcHg7XHJcblxyXG5cclxuLy8gUG9wb3ZlcnNcclxuJHBvcG92ZXItYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuJHBvcG92ZXItYm94LXNoYWRvdzogMCAycHggMzBweCByZ2JhKCRibGFjaywgMC4xKTtcclxuJHBvcG92ZXItYm9keS1wYWRkaW5nLXk6IDEuMjVyZW07XHJcbiRwb3BvdmVyLWJvZHktcGFkZGluZy14OiAxLjVyZW07XHJcbiRwb3BvdmVyLWhlYWRlci1wYWRkaW5nLXg6ICRwb3BvdmVyLWJvZHktcGFkZGluZy14O1xyXG4kcG9wb3Zlci1oZWFkZXItcGFkZGluZy15OiAkcG9wb3Zlci1ib2R5LXBhZGRpbmcteTtcclxuJHBvcG92ZXItaGVhZGVyLWJnOiAkd2hpdGU7XHJcblxyXG5cclxuLy8gVGFic1xyXG4kbmF2LXRhYnMtYm9yZGVyLXdpZHRoOiAxcHg7XHJcbiRuYXYtdGFicy1ib3JkZXItY29sb3I6ICRncmF5LTIwMDtcclxuJG5hdi10YWJzLWJvcmRlci1yYWRpdXM6IDA7XHJcbiRuYXYtbGluay1wYWRkaW5nLXk6IDFyZW07XHJcbiRuYXYtbGluay1wYWRkaW5nLXg6IDEuMnJlbTtcclxuJG5hdi1saW5rLXBhZGRpbmc6IDFyZW0gMS4ycmVtO1xyXG4kbmF2LXRhYnMtbGluay1hY3RpdmUtYmc6IHRyYW5zcGFyZW50O1xyXG4kbmF2LXRhYnMtbGluay1hY3RpdmUtY29sb3I6IGluaGVyaXQ7XHJcblxyXG5cclxuLy8gVG9vbHRpcHNcclxuJHRvb2x0aXAtcGFkZGluZy15OiAwLjdyZW07XHJcbiR0b29sdGlwLXBhZGRpbmcteDogIDEuMXJlbTtcclxuJHRvb2x0aXAtYmc6ICRncmF5LTcwMDtcclxuJHRvb2x0aXAtYXJyb3ctY29sb3I6ICR0b29sdGlwLWJnO1xyXG4kdG9vbHRpcC1vcGFjaXR5OiAxO1xyXG5cclxuXHJcbi8vIEJhY2tkcm9wXHJcbiRiYWNrZHJvcC16LWluZGV4OiAxMDA7XHJcblxyXG5cclxuLy8gTGlzdHZpZXdcclxuJGxpc3R2aWV3LWl0ZW0tYWN0aXZlLWJnOiAkZ3JheS01MDtcclxuJGxpc3R2aWV3LWl0ZW0taG92ZXItYmc6ICRsaXN0dmlldy1pdGVtLWFjdGl2ZS1iZztcclxuJGxpc3R2aWV3LWl0ZW0tc3RyaXBlZC1iZzogI2Y5ZjlmOTtcclxuJGxpc3R2aWV3LWludmVydC1pdGVtLWFjdGl2ZS1iZzogcmdiYSgkd2hpdGUsIDAuMDI1KTtcclxuJGxpc3R2aWV3LWludmVydC1pdGVtLWhvdmVyLWJnOiByZ2JhKCR3aGl0ZSwgMC4wMjUpO1xyXG4kbGlzdHZpZXctaW52ZXJ0LWl0ZW0tc3RyaXBlZC1iZzogcmdiYSgkd2hpdGUsIDAuMSk7XHJcbiRsaXN0dmlldy1ib3JkZXItY29sb3I6ICRncmF5LTEwMDtcclxuXHJcblxyXG4vLyBIUlxyXG4kaHItYm9yZGVyLWNvbG9yOiAkZ3JheS0yMDA7XHJcblxyXG5cclxuLy8gSW5wdXQgR3JvdXBcclxuJGlucHV0LWdyb3VwLWFkZG9uLWJnOiAkd2hpdGU7XHJcblxyXG5cclxuLy8gSnVtYm90cm9uXHJcbiRqdW1ib3Ryb24tYmc6ICR3aGl0ZTtcclxuXHJcblxyXG4vLyBUcmVlIFZpZXdcclxuJHRyZWV2aWV3LWl0ZW0tYm9yZGVyLWNvbG9yOiAjZjFmNGY3O1xyXG4kdHJlZXZpZXctaXRlbS1hY3RpdmUtYm9yZGVyLWNvbG9yOiAkZ3JheS01MDtcclxuJHRyZWV2aWV3LWl0ZW0tYWN0aXZlLWJnOiAkdHJlZXZpZXctaXRlbS1hY3RpdmUtYm9yZGVyLWNvbG9yO1xyXG4kdHJlZXZpZXctaXRlbS1ob3Zlci1iZzogbGlnaHRlbigkZ3JheS01MCwgMSUpO1xyXG5cclxuXHJcbi8vIFlJUSBDb250cmFzdFxyXG4keWlxLWNvbnRyYXN0ZWQtdGhyZXNob2xkOiAyMDA7XHJcblxyXG5cclxuLy8gQnV0dG9uc1xyXG4kYnRuLWZvY3VzLXdpZHRoOiAwO1xyXG4kYnRuLWJvcmRlci13aWR0aDogMnB4O1xyXG5cclxuXHJcbi8vIEFjY29yZGlvblxyXG4kYWNjb3JkaW9uLWJvcmRlci1jb2xvcjogJGdyYXktMjAwO1xyXG4kYWNjb3JkaW9uLXBhZGRpbmc6IDAuODVyZW0gMS4zNXJlbTtcclxuXHJcblxyXG4vLyBEYXRlcGlja2VyXHJcbiRkYXRlLXBpY2tlci1oZWFkLWNvbG9yOiAkdGVhbDtcclxuXHJcblxyXG4vLyBTb3J0YWJsZVxyXG4kc29ydGFibGUtaXRlbS1ib3JkZXItY29sb3I6ICRncmF5LTIwMDtcclxuJHNvcnRhYmxlLWl0ZW0tYWN0aXZlLWJnOiAkZ3JheS0xMDA7XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/admin/doctors/view/view.component.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/view.component.ts ***!
  \************************************************************/
/*! exports provided: ViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewComponent", function() { return ViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");





var ViewComponent = /** @class */ (function () {
    function ViewComponent(route, API, service) {
        this.route = route;
        this.API = API;
        this.service = service;
        this.key = null;
        this.profileInfo = {
            name: 'Malinda Hollaway',
            profession: 'Web/UI Developer',
            location: 'Edinburgh, Scotland',
            img: './assets/img/avatar/Blank-Avatar.png',
            summary: 'Cras mattis consectetur purus sit amet fermentum. Maecenas sed diam eget risus varius blandit sit amet non magnae tiam porta sem malesuada magna mollis euismod.',
            contacts: [
                {
                    icon: 'phone',
                    value: '308-360-8938'
                },
                {
                    icon: 'email',
                    value: 'malinda@inbound.plus'
                },
                {
                    icon: 'twitter',
                    value: '@mallinda-hollaway'
                }
            ]
        };
        this.profileSearch = false;
        this.key = this.route.snapshot.paramMap.get('id');
        this.service.setDentistKey(this.key); //invoke new Data
    }
    ViewComponent.prototype.ngOnInit = function () {
        this.getDoctorDetails();
    };
    ViewComponent.prototype.getDoctorDetails = function () {
        var _this = this;
        this.API.post("doctors/get-doctor-details-by-id", {
            id: this.key
        }).subscribe(function (response) {
            _this.doctor_info = response.devMessage;
        }, function (error) {
        });
    };
    ViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-view',
            template: __webpack_require__(/*! ./view.component.html */ "./src/app/pages/admin/doctors/view/view.component.html"),
            styles: [__webpack_require__(/*! ./view.component.scss */ "./src/app/pages/admin/doctors/view/view.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"]])
    ], ViewComponent);
    return ViewComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/doctors/view/view.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/admin/doctors/view/view.module.ts ***!
  \*********************************************************/
/*! exports provided: ViewModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewModule", function() { return ViewModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var _view_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./view.component */ "./src/app/pages/admin/doctors/view/view.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./about/about.component */ "./src/app/pages/admin/doctors/view/about/about.component.ts");
/* harmony import */ var _pictures_pictures_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pictures/pictures.component */ "./src/app/pages/admin/doctors/view/pictures/pictures.component.ts");
/* harmony import */ var _contacts_contacts_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./contacts/contacts.component */ "./src/app/pages/admin/doctors/view/contacts/contacts.component.ts");
/* harmony import */ var _crystalui_angular_lightbox__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @crystalui/angular-lightbox */ "./node_modules/@crystalui/angular-lightbox/fesm5/crystalui-angular-lightbox.js");
/* harmony import */ var _commissions_commissions_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./commissions/commissions.component */ "./src/app/pages/admin/doctors/view/commissions/commissions.component.ts");


















var ROUTE = [
    {
        path: "",
        component: _view_component__WEBPACK_IMPORTED_MODULE_12__["ViewComponent"],
        children: [
            {
                path: '',
                redirectTo: 'about',
                pathMatch: 'full'
            },
            {
                path: 'about',
                component: _about_about_component__WEBPACK_IMPORTED_MODULE_13__["AboutComponent"]
            },
            {
                path: 'pictures',
                component: _pictures_pictures_component__WEBPACK_IMPORTED_MODULE_14__["PicturesComponent"]
            },
            {
                path: 'contacts',
                component: _contacts_contacts_component__WEBPACK_IMPORTED_MODULE_15__["ContactsComponent"]
            },
            {
                path: 'commissions',
                component: _commissions_commissions_component__WEBPACK_IMPORTED_MODULE_17__["CommissionsComponent"]
            },
        ]
    },
];
var ViewModule = /** @class */ (function () {
    function ViewModule() {
    }
    ViewModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _view_component__WEBPACK_IMPORTED_MODULE_12__["ViewComponent"],
                _about_about_component__WEBPACK_IMPORTED_MODULE_13__["AboutComponent"],
                _pictures_pictures_component__WEBPACK_IMPORTED_MODULE_14__["PicturesComponent"],
                _contacts_contacts_component__WEBPACK_IMPORTED_MODULE_15__["ContactsComponent"],
                _commissions_commissions_component__WEBPACK_IMPORTED_MODULE_17__["CommissionsComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_7__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_8__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_9__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_11__["QuillModule"],
                _crystalui_angular_lightbox__WEBPACK_IMPORTED_MODULE_16__["CrystalLightboxModule"],
            ]
        })
    ], ViewModule);
    return ViewModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-doctors-view-view-module.js.map