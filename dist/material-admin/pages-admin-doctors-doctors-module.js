(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-doctors-doctors-module"],{

/***/ "./src/app/pages/admin/doctors/doctors.component.html":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/doctors/doctors.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  doctors works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/pages/admin/doctors/doctors.component.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/doctors/doctors.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL2RvY3RvcnMvZG9jdG9ycy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/admin/doctors/doctors.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/admin/doctors/doctors.component.ts ***!
  \**********************************************************/
/*! exports provided: DoctorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorsComponent", function() { return DoctorsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DoctorsComponent = /** @class */ (function () {
    function DoctorsComponent() {
    }
    DoctorsComponent.prototype.ngOnInit = function () {
    };
    DoctorsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-doctors',
            template: __webpack_require__(/*! ./doctors.component.html */ "./src/app/pages/admin/doctors/doctors.component.html"),
            styles: [__webpack_require__(/*! ./doctors.component.scss */ "./src/app/pages/admin/doctors/doctors.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DoctorsComponent);
    return DoctorsComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/doctors/doctors.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/admin/doctors/doctors.module.ts ***!
  \*******************************************************/
/*! exports provided: DoctorsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorsModule", function() { return DoctorsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _doctors_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./doctors.component */ "./src/app/pages/admin/doctors/doctors.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./list/list.component */ "./src/app/pages/admin/doctors/list/list.component.ts");














var ROUTE = [
    {
        path: "",
        component: _doctors_component__WEBPACK_IMPORTED_MODULE_4__["DoctorsComponent"]
    },
    {
        path: "list",
        component: _list_list_component__WEBPACK_IMPORTED_MODULE_13__["ListComponent"]
    }
];
var DoctorsModule = /** @class */ (function () {
    function DoctorsModule() {
    }
    DoctorsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _doctors_component__WEBPACK_IMPORTED_MODULE_4__["DoctorsComponent"],
                _list_list_component__WEBPACK_IMPORTED_MODULE_13__["ListComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], DoctorsModule);
    return DoctorsModule;
}());



/***/ }),

/***/ "./src/app/pages/admin/doctors/list/list.component.html":
/*!**************************************************************!*\
  !*** ./src/app/pages/admin/doctors/list/list.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n</header>\r\n\r\n\r\n<div class=\"card\">\r\n  <div class=\"toolbar toolbar--inner\">\r\n    <div class=\"toolbar__label\">Dentist List</div>\r\n\r\n    <div class=\"actions\">\r\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\"></i>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getDoctorsList()\" tooltip=\"Refresh list\"></a>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addDoctor()\" tooltip=\"New\"></a>\r\n    </div>\r\n\r\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\r\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \r\n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getDoctorsList()\" tooltip=\"Close search box\"></i>\r\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getDoctorsList()\" tooltip=\"Search\"></i>\r\n\r\n    </div>\r\n    \r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead>\r\n          <tr>\r\n            <th width=\"150px\"></th>\r\n            <th>Fullname</th>\r\n            <th>Rank</th>\r\n            <th>Branch</th>\r\n            <th>Is Active</th>\r\n            <th>Date Created</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngIf=\"dentist_list.length==0\">\r\n            <td align=\"center\" colspan=\"4\"> No record(s) found.</td>\r\n          </tr>\r\n          <tr *ngFor=\"let data of dentist_list\" class=\"hover-edit\" > \r\n            <td>\r\n              <div class=\"btn-demo\">\r\n                <button class=\"btn btn-success\" (click)=\"editDentist(data)\"><i class=\"zmdi zmdi-edit\"></i></button>\r\n                <button class=\"btn btn-primary\" [routerLink]=\"['/admin/dentist',data.id]\"><i class=\"zmdi zmdi-eye\"></i></button>\r\n              </div>\r\n            </td>\r\n            <td>\r\n              {{ data.fullname }}\r\n            </td>\r\n            <td>\r\n              {{ data.rank_name }}\r\n            </td>\r\n            <td>\r\n              {{ data.branch_name }}\r\n            </td>\r\n            <td>\r\n              <i class=\"zmdi zmdi-check c-success\" *ngIf=\"data.is_active\"></i>\r\n              <i class=\"zmdi zmdi-close c-danger\" *ngIf=\"!data.is_active\"></i>\r\n            </td>\r\n            <td>\r\n              {{ data.created_by }} <br>\r\n              {{ data.date_created }}\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"dentist_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"6\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                class=\"justify-content-center\"\r\n                [totalItems]=\"total_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"row\" >\r\n              </pagination>\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<div class=\"modal fade\" bsModal #modalDoctor=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Dentist' : 'Edit Dentist'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"form-group\">\r\n              <label>Branch<span style=\"color:red\">*</span></label>\r\n              <ng-select\r\n                [items]=\"branch_list\"\r\n                bindLabel=\"branch_name\"\r\n                placeholder=\"Select a Branch\"\r\n                [(ngModel)]=\"branch\"\r\n                bindValue=\"id\"\r\n                >\r\n              </ng-select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"form-group\">\r\n              <label>Rank<span style=\"color:red\">*</span></label>\r\n              <ng-select\r\n                [items]=\"ranks_list\"\r\n                bindLabel=\"rank_name\"\r\n                placeholder=\"Select a Rank\"\r\n                [(ngModel)]=\"rank\"\r\n                bindValue=\"id\"\r\n                >\r\n              </ng-select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n              <div class=\"form-group\">\r\n                  <label>First name<span style=\"color:red\">*</span></label>\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"fname\" placeholder=\"First name...\" id=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n              <div class=\"form-group\">\r\n                  <label>Middle name(Optional)</label>\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"mname\" placeholder=\"Middle name...\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n              <div class=\"form-group\">\r\n                  <label>Last name<span style=\"color:red\">*</span></label>\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"lname\" placeholder=\"Last name...\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"checkbox checkbox--inline\">\r\n              <input type=\"checkbox\" id=\"customCheck4\" [(ngModel)]=\"isActive\">\r\n              <label class=\"checkbox__label\" for=\"customCheck4\">Is Active</label>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveDentist()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalDoctor.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/doctors/list/list.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/admin/doctors/list/list.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vZG9jdG9ycy9saXN0L0M6XFxVc2Vyc1xcQURNSU5cXERlc2t0b3BcXFByb2plY3RzXFxkZW50YWwtZmUvc3JjXFxhcHBcXHBhZ2VzXFxhZG1pblxcZG9jdG9yc1xcbGlzdFxcbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7RUFHTSxvQkFBb0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL2RvY3RvcnMvbGlzdC9saXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bi1kZW1vIHtcclxuICAgICYgPiAuYnRuLFxyXG4gICAgJiA+IC5idG4tZ3JvdXAge1xyXG4gICAgICBtYXJnaW46IDAgNXB4IDEwcHggMDtcclxuICAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/admin/doctors/list/list.component.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/doctors/list/list.component.ts ***!
  \************************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");








var ListComponent = /** @class */ (function () {
    function ListComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.pageTitle = "Dentist";
        this.widgetSubTitle = "Manage Dentist";
        this.loggedUserInfo = null;
        this.dentist_list = [];
        this.branch_list = [];
        this.ranks_list = [];
        this.total_count = 0;
        this.searchKey = "";
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.branch = null;
        this.rank = null;
        this.key = null;
        this.page = 1;
        this.row = 10;
        this.mode = "add";
        this.isActive = true;
        this.loggedUserInfo = this.common.getLoggedInUser();
    }
    ListComponent.prototype.ngOnInit = function () {
        this.getDoctorsList();
        this.getBranches();
        this.getRanks();
    };
    ListComponent.prototype.addDoctor = function () {
        this.clearFields();
        this.modalDoctor.show();
    };
    ListComponent.prototype.getDoctorsList = function () {
        var _this = this;
        this.API.post("doctors/get", {
            page: this.page,
            row: this.row,
            search: this.searchKey
        }).subscribe(function (response) {
            _this.dentist_list = response.devMessage;
            _this.total_count = response.total;
        }, function (error) {
        });
    };
    ListComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            _this.branch_list = response.devMessage;
        }, function (error) {
        });
    };
    ListComponent.prototype.getRanks = function () {
        var _this = this;
        this.API.post("lookups/get-ranks", {}).subscribe(function (response) {
            _this.ranks_list = response.devMessage;
        }, function (error) {
        });
    };
    ListComponent.prototype.saveDentist = function () {
        var _this = this;
        if (Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.branch) || Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.rank) || this.fname == "" || this.lname == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required Field(s) Missing.", "warning");
        }
        else {
            this.API.post("doctors/save", {
                mode: this.mode,
                key: this.key,
                fname: this.fname,
                mname: this.mname,
                lname: this.lname,
                branch: this.branch,
                rank: this.rank,
                created_by: this.loggedUserInfo.id,
                is_active: this.isActive
            }).subscribe(function (response) {
                _this.modalDoctor.hide();
                var msg = _this.mode == "add" ? "Record Saved." : "Record Updated";
                _this.growlService.addSuccess(msg);
                _this.getDoctorsList();
                _this.clearFields();
            }, function (error) {
            });
        }
    };
    ListComponent.prototype.clearFields = function () {
        this.fname = "";
        this.mname = "";
        this.lname = "";
        this.mode = "add";
        this.branch = null;
        this.rank = null;
        this.key = null;
    };
    ListComponent.prototype.editDentist = function (data) {
        this.key = data.id;
        this.mode = "edit";
        this.branch = data.branch_id;
        this.rank = data.rank_id;
        this.fname = data.fname;
        this.mname = data.mname;
        this.lname = data.lname;
        this.modalDoctor.show();
    };
    ListComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getDoctorsList();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalDoctor"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], ListComponent.prototype, "modalDoctor", void 0);
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/pages/admin/doctors/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.scss */ "./src/app/pages/admin/doctors/list/list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_6__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-doctors-doctors-module.js.map