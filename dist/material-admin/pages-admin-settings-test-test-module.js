(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-settings-test-test-module"],{

/***/ "./src/app/components/form/form-teeth/form-teeth.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/components/form/form-teeth/form-teeth.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table width=\"100%\">\r\n  <tr >\r\n    <td *ngFor=\"let data of teeth_list\">\r\n      {{data.tooth_number}}\r\n      <div class=\"directional-buttons\">\r\n        <button class=\"direction-button up\">\r\n          <span class=\"visually-hidden\">up</span>\r\n        </button>\r\n        <button class=\"direction-button left\">\r\n          <span class=\"visually-hidden\">left</span>\r\n        </button>\r\n          <button class=\"direction-button middle\">\r\n          <span class=\"visually-hidden\"></span>\r\n        </button>\r\n        <button class=\"direction-button right\">\r\n          <span class=\"visually-hidden\">right</span>\r\n        </button>\r\n        <button class=\"direction-button down\">\r\n          <span class=\"visually-hidden\">down</span>\r\n        </button>\r\n      </div>\r\n\r\n    </td>\r\n  </tr>\r\n</table>"

/***/ }),

/***/ "./src/app/components/form/form-teeth/form-teeth.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/form/form-teeth/form-teeth.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* button layout */\n.directional-buttons {\n  /* Define the size of the grid */\n  /* Other things rely on this! Change at your own peril! */\n  width: 3rem;\n  height: 4rem;\n  /* display the buttons in a grid */\n  display: grid;\n  /* leave a little gap between columns/rows */\n  grid-gap: 0rem;\n  /* define a 3 x 3 grid with middle column/row larger */\n  grid-template-columns: 60fr 60fr 60fr;\n  grid-template-rows: 60fr 60fr 60fr;\n  /* name the cells in the grid */\n  grid-template-areas: \"up up up\"\r \"left  middle  right\"\r \"down down down\"; }\n/* hides the button text */\n.visually-hidden {\n  position: absolute !important;\n  height: 1px;\n  width: 1px;\n  overflow: hidden;\n  clip: rect(1px 1px 1px 1px);\n  /* IE6, IE7 */\n  clip: rect(1px, 1px, 1px, 1px);\n  white-space: nowrap; }\n/* base button */\n.direction-button {\n  /* the button colours */\n  color: white;\n  border: 1px solid #000;\n  /* since there's no text, we can use the color value for the background */\n  background: currentcolor;\n  /* clears default button styles */\n  /* makes sure the triangles are moved relative to the button */\n  position: relative; }\n/* the overlapping outside triangle \r\n   * actually, it's just a rotated square\r\n   */\n/* when the button is hovered */\n.direction-button:hover {\n  /* make the button a different color */\n  color: #553737; }\n/* when the button is pressed */\n.direction-button:active:after {\n  /* make the inner triangle a different color! */\n  color: #ff9898; }\n/* individual button styles */\n.up {\n  /* puts the button in the named grid cell */\n  grid-area: up;\n  /* only curves the outer corners */\n  border-style: solid;\n  border-top-left-radius: 50px;\n  border-top-right-radius: 50px; }\n.up:before {\n  /* center the overlapping triangle horizontally */\n  left: calc(50% - 2.125rem);\n  /* position it so it overlaps just right */\n  bottom: -2.125rem; }\n.up:after {\n  /* do the border-based triangle trick */\n  border-bottom-color: currentcolor;\n  /* center inner triangle horizontally */\n  left: calc(50% - 2rem);\n  /* position it just right */\n  top: -1rem; }\n.left {\n  /* puts the button in the named grid cell */\n  grid-area: left;\n  /* only curves the outer corners */ }\n.right {\n  /* puts the button in the named grid cell */\n  grid-area: right;\n  /* only curves the outer corners */ }\n.right:before {\n  /* position it so it overlaps just right */\n  left: -2.125rem;\n  /* center the overlapping triangle vertically */\n  top: calc(50% - 2.125rem); }\n.right:after {\n  /* do the border-based triangle trick */\n  border-left-color: currentcolor;\n  /* center inner triangle vertically */\n  top: calc(50% - 2rem);\n  /* position it just right */\n  right: -1rem; }\n.down {\n  /* puts the button in the named grid cell */\n  grid-area: down;\n  /* only curves the outer corners */\n  border-bottom-left-radius: 50px;\n  border-bottom-right-radius: 50px; }\n.down:before {\n  /* center the overlapping triangle horizontally */\n  left: calc(50% - 2.125rem);\n  /* position it so it overlaps just right */\n  top: -2.125rem; }\n.down:after {\n  /* do the border-based triangle trick */\n  border-top-color: currentcolor;\n  /* center inner triangle horizontally */\n  left: calc(50% - 2rem);\n  /* position it just right */\n  bottom: -1rem; }\n.middle {\n  /* puts the button in the named grid cell */\n  grid-area: middle;\n  /* only curves the outer corners */ }\n/* other styles */\nhtml,\nbody {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: darkgrey; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mb3JtL2Zvcm0tdGVldGgvQzpcXFVzZXJzXFxBRE1JTlxcRGVza3RvcFxcUHJvamVjdHNcXGRlbnRhbC1mZS9zcmNcXGFwcFxcY29tcG9uZW50c1xcZm9ybVxcZm9ybS10ZWV0aFxcZm9ybS10ZWV0aC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50cy9mb3JtL2Zvcm0tdGVldGgvZm9ybS10ZWV0aC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxrQkFBQTtBQUNBO0VBQ0ksZ0NBQUE7RUFDQSx5REFBQTtFQUNBLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0NBQUE7RUFDQSxhQUFhO0VBQ2IsNENBQUE7RUFDQSxjQUFjO0VBQ2Qsc0RBQUE7RUFDQSxxQ0FBcUM7RUFDckMsa0NBQWtDO0VBQ2xDLCtCQUFBO0VBQ0Esd0VBS0YsRUFBQTtBQ0RGLDBCREdjO0FDRmQ7RURJSSw2QkFBVTtFQUNWLFdBQVU7RUFDVixVQUFNO0VBQXVCLGdCQUFjO0VBQzNDLDJCQUFNO0VBQ04sYUFBYTtFQ0RmLDhCQUE4QjtFREk5QixtQkFBaUIsRUFBQTtBQ0RuQixnQkRHSTtBQ0ZKO0VESUksdUJBQXNCO0VBQ3RCLFlBQUE7RUFDQSxzQkFBWTtFQUNaLHlFQUFrQztFQUNsQyx3QkFBQTtFQUNBLGlDQUNEO0VDSEQsOERBQThEO0VESzlELGtCQUFBLEVBQUE7QUNGRjs7SURNRTtBQ0hGLCtCRElJO0FDSEo7RUFDRSxzQ0FBc0M7RURNdEMsY0FBQSxFQUFBO0FDSEYsK0JES0k7QUNKSjtFQUNFLCtDQUErQztFRE8vQyxjQUFBLEVBQUE7QUNKRiw2QkRNSTtBQ0xKO0VEUUksMkNBQW1DO0VBQ25DLGFBQVk7RUFDWixrQ0FBNkI7RUFDN0IsbUJBQUE7RUNORiw0QkFBNEI7RURTNUIsNkJBQVcsRUFBQTtBQ05iO0VEU0ksaURBQTJDO0VBQzNDLDBCQUNEO0VDUkQsMENBQTBDO0VEUzFDLGlCQUFVLEVBQUE7QUNOWjtFRFNJLHVDQUFBO0VBQ0EsaUNBQXNCO0VBQ3RCLHVDQUE0QjtFQUM1QixzQkFDRDtFQ1JELDJCQUEyQjtFRFUzQixVQUFNLEVBQUE7QUNQUjtFRFVJLDJDQUVEO0VDVkQsZUFBZTtFRFlmLGtDQUFPLEVBQUE7QUNUVDtFRFlJLDJDQUVEO0VDWkQsZ0JBQWdCO0VEYWhCLGtDQUFjLEVBQUE7QUNWaEI7RURhSSwwQ0FBQTtFQUNBLGVBQUs7RUNYUCwrQ0FBK0M7RURhL0MseUJBQWEsRUFBQTtBQ1ZmO0VEYUksdUNBQXNDO0VBQ3RDLCtCQUFxQjtFQUNyQixxQ0FBNEI7RUFDNUIscUJBQ0Q7RUNaRCwyQkFBMkI7RURjM0IsWUFBTSxFQUFBO0FDWFI7RURjSSwyQ0FBbUM7RUFDbkMsZUFBQTtFQUNBLGtDQUFpQztFQ1puQywrQkFBK0I7RURjL0IsZ0NBQWEsRUFBQTtBQ1hmO0VEY0ksaURBQTJDO0VBQzNDLDBCQUNEO0VDYkQsMENBQTBDO0VEYzFDLGNBQVksRUFBQTtBQ1hkO0VEY0ksdUNBQUE7RUFDQSw4QkFBc0I7RUFDdEIsdUNBQTRCO0VBQzVCLHNCQUNEO0VDYkQsMkJBQTJCO0VEZ0IzQixhQUFRLEVBQUE7QUNiVjtFRGlCSSwyQ0FDRDtFQ2hCRCxpQkFBaUI7RURxQmpCLGtDQUFrQixFQUFBO0FBRWxCLGlCQUFLO0FDbkJQOztFRHNCSSxXQUFTO0VBQ1QsWUFBVztFQUNYLGFBQUE7RUFDQSxtQkFBWTtFQ25CZCx1QkFBdUI7RUFDdkIsb0JBQW9CLEVBQUUiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2Zvcm0vZm9ybS10ZWV0aC9mb3JtLXRlZXRoLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogYnV0dG9uIGxheW91dCAqL1xyXG4uZGlyZWN0aW9uYWwtYnV0dG9ucyB7XHJcbiAgICAvKiBEZWZpbmUgdGhlIHNpemUgb2YgdGhlIGdyaWQgKi9cclxuICAgIC8qIE90aGVyIHRoaW5ncyByZWx5IG9uIHRoaXMhIENoYW5nZSBhdCB5b3VyIG93biBwZXJpbCEgKi9cclxuICAgIHdpZHRoOiAzcmVtO1xyXG4gICAgaGVpZ2h0OiA0cmVtO1xyXG4gICAgLyogZGlzcGxheSB0aGUgYnV0dG9ucyBpbiBhIGdyaWQgKi9cclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbiAgICAvKiBsZWF2ZSBhIGxpdHRsZSBnYXAgYmV0d2VlbiBjb2x1bW5zL3Jvd3MgKi9cclxuICAgIGdyaWQtZ2FwOiAwcmVtO1xyXG4gICAgLyogZGVmaW5lIGEgMyB4IDMgZ3JpZCB3aXRoIG1pZGRsZSBjb2x1bW4vcm93IGxhcmdlciAqL1xyXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiA2MGZyIDYwZnIgNjBmcjtcclxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogNjBmciA2MGZyIDYwZnI7XHJcbiAgICAvKiBuYW1lIHRoZSBjZWxscyBpbiB0aGUgZ3JpZCAqL1xyXG4gICAgZ3JpZC10ZW1wbGF0ZS1hcmVhczogXCJ1cCB1cCB1cFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBcImxlZnQgIG1pZGRsZSAgcmlnaHRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgXCJkb3duIGRvd24gZG93blwiOyAgICBcclxuICB9XHJcbiAgXHJcbiAgLyogaGlkZXMgdGhlIGJ1dHRvbiB0ZXh0ICovXHJcbiAgLnZpc3VhbGx5LWhpZGRlbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogMXB4OyBcclxuICAgIHdpZHRoOiAxcHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgY2xpcDogcmVjdCgxcHggMXB4IDFweCAxcHgpOyAvKiBJRTYsIElFNyAqL1xyXG4gICAgY2xpcDogcmVjdCgxcHgsIDFweCwgMXB4LCAxcHgpO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICB9XHJcbiAgXHJcbiAgLyogYmFzZSBidXR0b24gKi9cclxuICAuZGlyZWN0aW9uLWJ1dHRvbiB7XHJcbiAgICAvKiB0aGUgYnV0dG9uIGNvbG91cnMgKi9cclxuICAgIGNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xyXG4gICAgLyogc2luY2UgdGhlcmUncyBubyB0ZXh0LCB3ZSBjYW4gdXNlIHRoZSBjb2xvciB2YWx1ZSBmb3IgdGhlIGJhY2tncm91bmQgKi9cclxuICAgIGJhY2tncm91bmQ6IGN1cnJlbnRjb2xvcjtcclxuICAgIC8qIGNsZWFycyBkZWZhdWx0IGJ1dHRvbiBzdHlsZXMgKi9cclxuICAgIC8qIG1ha2VzIHN1cmUgdGhlIHRyaWFuZ2xlcyBhcmUgbW92ZWQgcmVsYXRpdmUgdG8gdGhlIGJ1dHRvbiAqL1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuICBcclxuICAvKiB0aGUgb3ZlcmxhcHBpbmcgb3V0c2lkZSB0cmlhbmdsZSBcclxuICAgKiBhY3R1YWxseSwgaXQncyBqdXN0IGEgcm90YXRlZCBzcXVhcmVcclxuICAgKi9cclxuICAvKiB3aGVuIHRoZSBidXR0b24gaXMgaG92ZXJlZCAqL1xyXG4gIC5kaXJlY3Rpb24tYnV0dG9uOmhvdmVyIHtcclxuICAgIC8qIG1ha2UgdGhlIGJ1dHRvbiBhIGRpZmZlcmVudCBjb2xvciAqL1xyXG4gICAgY29sb3I6IHJnYig4NSw1NSw1NSk7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIHdoZW4gdGhlIGJ1dHRvbiBpcyBwcmVzc2VkICovXHJcbiAgLmRpcmVjdGlvbi1idXR0b246YWN0aXZlOmFmdGVyIHtcclxuICAgIC8qIG1ha2UgdGhlIGlubmVyIHRyaWFuZ2xlIGEgZGlmZmVyZW50IGNvbG9yISAqL1xyXG4gICAgY29sb3I6IHJnYigyNTUsIDE1MiwgMTUyKTtcclxuICB9XHJcbiAgXHJcbiAgLyogaW5kaXZpZHVhbCBidXR0b24gc3R5bGVzICovXHJcbiAgLnVwIHtcclxuICAgIC8qIHB1dHMgdGhlIGJ1dHRvbiBpbiB0aGUgbmFtZWQgZ3JpZCBjZWxsICovXHJcbiAgICBncmlkLWFyZWE6IHVwO1xyXG4gICAgXHJcbiAgICAvKiBvbmx5IGN1cnZlcyB0aGUgb3V0ZXIgY29ybmVycyAqL1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXMgOiA1MHB4O1xyXG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXMgOiA1MHB4O1xyXG4gIFxyXG4gIH1cclxuICAudXA6YmVmb3JlIHtcclxuICAgIC8qIGNlbnRlciB0aGUgb3ZlcmxhcHBpbmcgdHJpYW5nbGUgaG9yaXpvbnRhbGx5ICovXHJcbiAgICBsZWZ0OiBjYWxjKDUwJSAtIDIuMTI1cmVtKTtcclxuICAgIC8qIHBvc2l0aW9uIGl0IHNvIGl0IG92ZXJsYXBzIGp1c3QgcmlnaHQgKi9cclxuICAgIGJvdHRvbTogLTIuMTI1cmVtO1xyXG4gIH1cclxuICAudXA6YWZ0ZXIge1xyXG4gICAgLyogZG8gdGhlIGJvcmRlci1iYXNlZCB0cmlhbmdsZSB0cmljayAqL1xyXG4gICAgYm9yZGVyLWJvdHRvbS1jb2xvcjogY3VycmVudGNvbG9yO1xyXG4gICAgLyogY2VudGVyIGlubmVyIHRyaWFuZ2xlIGhvcml6b250YWxseSAqL1xyXG4gICAgbGVmdDogY2FsYyg1MCUgLSAycmVtKTtcclxuICAgIC8qIHBvc2l0aW9uIGl0IGp1c3QgcmlnaHQgKi9cclxuICAgIHRvcDogLTFyZW07XHJcbiAgfVxyXG4gIFxyXG4gIC5sZWZ0IHtcclxuICAgIC8qIHB1dHMgdGhlIGJ1dHRvbiBpbiB0aGUgbmFtZWQgZ3JpZCBjZWxsICovXHJcbiAgICBncmlkLWFyZWE6IGxlZnQ7XHJcbiAgICAvKiBvbmx5IGN1cnZlcyB0aGUgb3V0ZXIgY29ybmVycyAqL1xyXG4gICAgXHJcbiAgfVxyXG4gIFxyXG4gIC5yaWdodCB7XHJcbiAgICAvKiBwdXRzIHRoZSBidXR0b24gaW4gdGhlIG5hbWVkIGdyaWQgY2VsbCAqL1xyXG4gICAgZ3JpZC1hcmVhOiByaWdodDtcclxuICAgIC8qIG9ubHkgY3VydmVzIHRoZSBvdXRlciBjb3JuZXJzICovXHJcbiAgXHJcbiAgfVxyXG4gIC5yaWdodDpiZWZvcmUge1xyXG4gICAgLyogcG9zaXRpb24gaXQgc28gaXQgb3ZlcmxhcHMganVzdCByaWdodCAqL1xyXG4gICAgbGVmdDogLTIuMTI1cmVtO1xyXG4gICAgLyogY2VudGVyIHRoZSBvdmVybGFwcGluZyB0cmlhbmdsZSB2ZXJ0aWNhbGx5ICovXHJcbiAgICB0b3A6IGNhbGMoNTAlIC0gMi4xMjVyZW0pO1xyXG4gIH1cclxuICAucmlnaHQ6YWZ0ZXIge1xyXG4gICAgLyogZG8gdGhlIGJvcmRlci1iYXNlZCB0cmlhbmdsZSB0cmljayAqL1xyXG4gICAgYm9yZGVyLWxlZnQtY29sb3I6IGN1cnJlbnRjb2xvcjtcclxuICAgIC8qIGNlbnRlciBpbm5lciB0cmlhbmdsZSB2ZXJ0aWNhbGx5ICovXHJcbiAgICB0b3A6IGNhbGMoNTAlIC0gMnJlbSk7XHJcbiAgICAvKiBwb3NpdGlvbiBpdCBqdXN0IHJpZ2h0ICovXHJcbiAgICByaWdodDogLTFyZW07XHJcbiAgfVxyXG4gIFxyXG4gIC5kb3duIHtcclxuICAgIC8qIHB1dHMgdGhlIGJ1dHRvbiBpbiB0aGUgbmFtZWQgZ3JpZCBjZWxsICovXHJcbiAgICBncmlkLWFyZWE6IGRvd247XHJcbiAgICAvKiBvbmx5IGN1cnZlcyB0aGUgb3V0ZXIgY29ybmVycyAqL1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1cyA6IDUwcHg7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1cyA6IDUwcHg7XHJcbiAgfVxyXG4gIC5kb3duOmJlZm9yZSB7XHJcbiAgICAvKiBjZW50ZXIgdGhlIG92ZXJsYXBwaW5nIHRyaWFuZ2xlIGhvcml6b250YWxseSAqL1xyXG4gICAgbGVmdDogY2FsYyg1MCUgLSAyLjEyNXJlbSk7XHJcbiAgICAvKiBwb3NpdGlvbiBpdCBzbyBpdCBvdmVybGFwcyBqdXN0IHJpZ2h0ICovXHJcbiAgICB0b3A6IC0yLjEyNXJlbTtcclxuICB9XHJcbiAgLmRvd246YWZ0ZXIge1xyXG4gICAgLyogZG8gdGhlIGJvcmRlci1iYXNlZCB0cmlhbmdsZSB0cmljayAqL1xyXG4gICAgYm9yZGVyLXRvcC1jb2xvcjogY3VycmVudGNvbG9yO1xyXG4gICAgLyogY2VudGVyIGlubmVyIHRyaWFuZ2xlIGhvcml6b250YWxseSAqL1xyXG4gICAgbGVmdDogY2FsYyg1MCUgLSAycmVtKTtcclxuICAgIC8qIHBvc2l0aW9uIGl0IGp1c3QgcmlnaHQgKi9cclxuICAgIGJvdHRvbTogLTFyZW07XHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gIC5taWRkbGUge1xyXG4gICAgLyogcHV0cyB0aGUgYnV0dG9uIGluIHRoZSBuYW1lZCBncmlkIGNlbGwgKi9cclxuICAgIGdyaWQtYXJlYTogbWlkZGxlO1xyXG4gIFxyXG4gICAgLyogb25seSBjdXJ2ZXMgdGhlIG91dGVyIGNvcm5lcnMgKi9cclxuICB9XHJcbiAgXHJcbiAgXHJcbiAgXHJcbiAgXHJcbiAgLyogb3RoZXIgc3R5bGVzICovXHJcbiAgaHRtbCxcclxuICBib2R5IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQ6IGRhcmtncmV5O1xyXG4gIH0iLCIvKiBidXR0b24gbGF5b3V0ICovXG4uZGlyZWN0aW9uYWwtYnV0dG9ucyB7XG4gIC8qIERlZmluZSB0aGUgc2l6ZSBvZiB0aGUgZ3JpZCAqL1xuICAvKiBPdGhlciB0aGluZ3MgcmVseSBvbiB0aGlzISBDaGFuZ2UgYXQgeW91ciBvd24gcGVyaWwhICovXG4gIHdpZHRoOiAzcmVtO1xuICBoZWlnaHQ6IDRyZW07XG4gIC8qIGRpc3BsYXkgdGhlIGJ1dHRvbnMgaW4gYSBncmlkICovXG4gIGRpc3BsYXk6IGdyaWQ7XG4gIC8qIGxlYXZlIGEgbGl0dGxlIGdhcCBiZXR3ZWVuIGNvbHVtbnMvcm93cyAqL1xuICBncmlkLWdhcDogMHJlbTtcbiAgLyogZGVmaW5lIGEgMyB4IDMgZ3JpZCB3aXRoIG1pZGRsZSBjb2x1bW4vcm93IGxhcmdlciAqL1xuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDYwZnIgNjBmciA2MGZyO1xuICBncmlkLXRlbXBsYXRlLXJvd3M6IDYwZnIgNjBmciA2MGZyO1xuICAvKiBuYW1lIHRoZSBjZWxscyBpbiB0aGUgZ3JpZCAqL1xuICBncmlkLXRlbXBsYXRlLWFyZWFzOiBcInVwIHVwIHVwXCJcciBcImxlZnQgIG1pZGRsZSAgcmlnaHRcIlxyIFwiZG93biBkb3duIGRvd25cIjsgfVxuXG4vKiBoaWRlcyB0aGUgYnV0dG9uIHRleHQgKi9cbi52aXN1YWxseS1oaWRkZW4ge1xuICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxcHg7XG4gIHdpZHRoOiAxcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGNsaXA6IHJlY3QoMXB4IDFweCAxcHggMXB4KTtcbiAgLyogSUU2LCBJRTcgKi9cbiAgY2xpcDogcmVjdCgxcHgsIDFweCwgMXB4LCAxcHgpO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwOyB9XG5cbi8qIGJhc2UgYnV0dG9uICovXG4uZGlyZWN0aW9uLWJ1dHRvbiB7XG4gIC8qIHRoZSBidXR0b24gY29sb3VycyAqL1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XG4gIC8qIHNpbmNlIHRoZXJlJ3Mgbm8gdGV4dCwgd2UgY2FuIHVzZSB0aGUgY29sb3IgdmFsdWUgZm9yIHRoZSBiYWNrZ3JvdW5kICovXG4gIGJhY2tncm91bmQ6IGN1cnJlbnRjb2xvcjtcbiAgLyogY2xlYXJzIGRlZmF1bHQgYnV0dG9uIHN0eWxlcyAqL1xuICAvKiBtYWtlcyBzdXJlIHRoZSB0cmlhbmdsZXMgYXJlIG1vdmVkIHJlbGF0aXZlIHRvIHRoZSBidXR0b24gKi9cbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XG5cbi8qIHRoZSBvdmVybGFwcGluZyBvdXRzaWRlIHRyaWFuZ2xlIFxyXG4gICAqIGFjdHVhbGx5LCBpdCdzIGp1c3QgYSByb3RhdGVkIHNxdWFyZVxyXG4gICAqL1xuLyogd2hlbiB0aGUgYnV0dG9uIGlzIGhvdmVyZWQgKi9cbi5kaXJlY3Rpb24tYnV0dG9uOmhvdmVyIHtcbiAgLyogbWFrZSB0aGUgYnV0dG9uIGEgZGlmZmVyZW50IGNvbG9yICovXG4gIGNvbG9yOiAjNTUzNzM3OyB9XG5cbi8qIHdoZW4gdGhlIGJ1dHRvbiBpcyBwcmVzc2VkICovXG4uZGlyZWN0aW9uLWJ1dHRvbjphY3RpdmU6YWZ0ZXIge1xuICAvKiBtYWtlIHRoZSBpbm5lciB0cmlhbmdsZSBhIGRpZmZlcmVudCBjb2xvciEgKi9cbiAgY29sb3I6ICNmZjk4OTg7IH1cblxuLyogaW5kaXZpZHVhbCBidXR0b24gc3R5bGVzICovXG4udXAge1xuICAvKiBwdXRzIHRoZSBidXR0b24gaW4gdGhlIG5hbWVkIGdyaWQgY2VsbCAqL1xuICBncmlkLWFyZWE6IHVwO1xuICAvKiBvbmx5IGN1cnZlcyB0aGUgb3V0ZXIgY29ybmVycyAqL1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1MHB4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNTBweDsgfVxuXG4udXA6YmVmb3JlIHtcbiAgLyogY2VudGVyIHRoZSBvdmVybGFwcGluZyB0cmlhbmdsZSBob3Jpem9udGFsbHkgKi9cbiAgbGVmdDogY2FsYyg1MCUgLSAyLjEyNXJlbSk7XG4gIC8qIHBvc2l0aW9uIGl0IHNvIGl0IG92ZXJsYXBzIGp1c3QgcmlnaHQgKi9cbiAgYm90dG9tOiAtMi4xMjVyZW07IH1cblxuLnVwOmFmdGVyIHtcbiAgLyogZG8gdGhlIGJvcmRlci1iYXNlZCB0cmlhbmdsZSB0cmljayAqL1xuICBib3JkZXItYm90dG9tLWNvbG9yOiBjdXJyZW50Y29sb3I7XG4gIC8qIGNlbnRlciBpbm5lciB0cmlhbmdsZSBob3Jpem9udGFsbHkgKi9cbiAgbGVmdDogY2FsYyg1MCUgLSAycmVtKTtcbiAgLyogcG9zaXRpb24gaXQganVzdCByaWdodCAqL1xuICB0b3A6IC0xcmVtOyB9XG5cbi5sZWZ0IHtcbiAgLyogcHV0cyB0aGUgYnV0dG9uIGluIHRoZSBuYW1lZCBncmlkIGNlbGwgKi9cbiAgZ3JpZC1hcmVhOiBsZWZ0O1xuICAvKiBvbmx5IGN1cnZlcyB0aGUgb3V0ZXIgY29ybmVycyAqLyB9XG5cbi5yaWdodCB7XG4gIC8qIHB1dHMgdGhlIGJ1dHRvbiBpbiB0aGUgbmFtZWQgZ3JpZCBjZWxsICovXG4gIGdyaWQtYXJlYTogcmlnaHQ7XG4gIC8qIG9ubHkgY3VydmVzIHRoZSBvdXRlciBjb3JuZXJzICovIH1cblxuLnJpZ2h0OmJlZm9yZSB7XG4gIC8qIHBvc2l0aW9uIGl0IHNvIGl0IG92ZXJsYXBzIGp1c3QgcmlnaHQgKi9cbiAgbGVmdDogLTIuMTI1cmVtO1xuICAvKiBjZW50ZXIgdGhlIG92ZXJsYXBwaW5nIHRyaWFuZ2xlIHZlcnRpY2FsbHkgKi9cbiAgdG9wOiBjYWxjKDUwJSAtIDIuMTI1cmVtKTsgfVxuXG4ucmlnaHQ6YWZ0ZXIge1xuICAvKiBkbyB0aGUgYm9yZGVyLWJhc2VkIHRyaWFuZ2xlIHRyaWNrICovXG4gIGJvcmRlci1sZWZ0LWNvbG9yOiBjdXJyZW50Y29sb3I7XG4gIC8qIGNlbnRlciBpbm5lciB0cmlhbmdsZSB2ZXJ0aWNhbGx5ICovXG4gIHRvcDogY2FsYyg1MCUgLSAycmVtKTtcbiAgLyogcG9zaXRpb24gaXQganVzdCByaWdodCAqL1xuICByaWdodDogLTFyZW07IH1cblxuLmRvd24ge1xuICAvKiBwdXRzIHRoZSBidXR0b24gaW4gdGhlIG5hbWVkIGdyaWQgY2VsbCAqL1xuICBncmlkLWFyZWE6IGRvd247XG4gIC8qIG9ubHkgY3VydmVzIHRoZSBvdXRlciBjb3JuZXJzICovXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDUwcHg7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA1MHB4OyB9XG5cbi5kb3duOmJlZm9yZSB7XG4gIC8qIGNlbnRlciB0aGUgb3ZlcmxhcHBpbmcgdHJpYW5nbGUgaG9yaXpvbnRhbGx5ICovXG4gIGxlZnQ6IGNhbGMoNTAlIC0gMi4xMjVyZW0pO1xuICAvKiBwb3NpdGlvbiBpdCBzbyBpdCBvdmVybGFwcyBqdXN0IHJpZ2h0ICovXG4gIHRvcDogLTIuMTI1cmVtOyB9XG5cbi5kb3duOmFmdGVyIHtcbiAgLyogZG8gdGhlIGJvcmRlci1iYXNlZCB0cmlhbmdsZSB0cmljayAqL1xuICBib3JkZXItdG9wLWNvbG9yOiBjdXJyZW50Y29sb3I7XG4gIC8qIGNlbnRlciBpbm5lciB0cmlhbmdsZSBob3Jpem9udGFsbHkgKi9cbiAgbGVmdDogY2FsYyg1MCUgLSAycmVtKTtcbiAgLyogcG9zaXRpb24gaXQganVzdCByaWdodCAqL1xuICBib3R0b206IC0xcmVtOyB9XG5cbi5taWRkbGUge1xuICAvKiBwdXRzIHRoZSBidXR0b24gaW4gdGhlIG5hbWVkIGdyaWQgY2VsbCAqL1xuICBncmlkLWFyZWE6IG1pZGRsZTtcbiAgLyogb25seSBjdXJ2ZXMgdGhlIG91dGVyIGNvcm5lcnMgKi8gfVxuXG4vKiBvdGhlciBzdHlsZXMgKi9cbmh0bWwsXG5ib2R5IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IGRhcmtncmV5OyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/form/form-teeth/form-teeth.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/form/form-teeth/form-teeth.component.ts ***!
  \********************************************************************/
/*! exports provided: FormTeethComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormTeethComponent", function() { return FormTeethComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");



var FormTeethComponent = /** @class */ (function () {
    function FormTeethComponent(API) {
        this.API = API;
    }
    FormTeethComponent.prototype.ngOnInit = function () {
        this.getTeethStructure();
    };
    FormTeethComponent.prototype.getTeethStructure = function () {
        var _this = this;
        this.API.post("settings/get-teeth-structure", {}).subscribe(function (response) {
            _this.teeth_list = response.devMessage;
        }, function (error) {
        });
    };
    FormTeethComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-form-teeth',
            template: __webpack_require__(/*! ./form-teeth.component.html */ "./src/app/components/form/form-teeth/form-teeth.component.html"),
            styles: [__webpack_require__(/*! ./form-teeth.component.scss */ "./src/app/components/form/form-teeth/form-teeth.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], FormTeethComponent);
    return FormTeethComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/settings/test/test.component.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/admin/settings/test/test.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-body\">\r\n    <app-form-teeth></app-form-teeth>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/settings/test/test.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/admin/settings/test/test.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL3NldHRpbmdzL3Rlc3QvdGVzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/admin/settings/test/test.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/admin/settings/test/test.component.ts ***!
  \*************************************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TestComponent = /** @class */ (function () {
    function TestComponent() {
    }
    TestComponent.prototype.ngOnInit = function () {
    };
    TestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-test',
            template: __webpack_require__(/*! ./test.component.html */ "./src/app/pages/admin/settings/test/test.component.html"),
            styles: [__webpack_require__(/*! ./test.component.scss */ "./src/app/pages/admin/settings/test/test.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TestComponent);
    return TestComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/settings/test/test.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/admin/settings/test/test.module.ts ***!
  \**********************************************************/
/*! exports provided: TestModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestModule", function() { return TestModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _test_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./test.component */ "./src/app/pages/admin/settings/test/test.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var src_app_components_form_form_teeth_form_teeth_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/components/form/form-teeth/form-teeth.component */ "./src/app/components/form/form-teeth/form-teeth.component.ts");














var ROUTE = [{ path: "", component: _test_component__WEBPACK_IMPORTED_MODULE_4__["TestComponent"] }];
var TestModule = /** @class */ (function () {
    function TestModule() {
    }
    TestModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _test_component__WEBPACK_IMPORTED_MODULE_4__["TestComponent"],
                src_app_components_form_form_teeth_form_teeth_component__WEBPACK_IMPORTED_MODULE_13__["FormTeethComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], TestModule);
    return TestModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-settings-test-test-module.js.map