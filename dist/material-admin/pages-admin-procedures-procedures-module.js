(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-procedures-procedures-module"],{

/***/ "./src/app/pages/admin/procedures/categories/categories.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/admin/procedures/categories/categories.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n<div class=\"card\">\n  <div class=\"toolbar toolbar--inner\">\n    <div class=\"toolbar__label\">Procedures List</div>\n\n    <div class=\"actions\">\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\" tooltip=\"Search\"></i>\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getCategories()\" tooltip=\"Refresh list\"></a>\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addCategory()\" tooltip=\"Add\"></a>\n    </div>\n\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getCategories()\" tooltip=\"Close search box\"></i>\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getCategories()\" tooltip=\"Search\"></i>\n\n    </div>\n    \n  </div>\n  <div class=\"card-body\">\n    <div class=\"table-responsive\">\n      <table class=\"table table-hover\">\n        <thead>\n          <tr>\n            <th>Category</th>\n            <th>Category Description</th>\n            <th>Date Created</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor = \"let data of category_list\" (click)=\"editCategory(data)\" class=\"hover-edit\">\n            <td>{{data.procedure_name}}</td>\n            <td>{{data.procedure_desc}}</td>\n            <td>{{data.date_created}}</td>\n          </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td colspan=\"3\">\n              <pagination (pageChanged)=\"pageChange($event)\"\n                class=\"justify-content-center\"\n                [totalItems]=\"category_total\"\n                [maxSize]=\"10\"\n                [itemsPerPage]=\"rows\" >\n              </pagination>\n            </td>\n          </tr>\n        </tfoot>\n      </table>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalCategory=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5>{{mode == 'add' ? 'Add Category' : 'Edit Category'}}</h5>\n      </div>\n      <div class=\"modal-body\">\n\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n            <div class=\"form-group\">\n              <label>Category name<span style=\"color:red\">*</span></label>\n              <input type=\"text\" class=\"form-control\" placeholder=\"...\" [(ngModel)]=\"category_name\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12\">\n            <div class=\"form-group\">\n              <label>Category Description</label>\n              <textarea [(ngModel)]=\"category_desc\" placeholder=\"...\" class=\"form-control\"></textarea>\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveCategory()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalCategory.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/procedures/categories/categories.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/admin/procedures/categories/categories.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL3Byb2NlZHVyZXMvY2F0ZWdvcmllcy9jYXRlZ29yaWVzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/admin/procedures/categories/categories.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/admin/procedures/categories/categories.component.ts ***!
  \***************************************************************************/
/*! exports provided: CategoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesComponent", function() { return CategoriesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");







var CategoriesComponent = /** @class */ (function () {
    function CategoriesComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.loggedUserInfo = null;
        this.mode = "add";
        this.key = null;
        this.category_name = "";
        this.category_desc = "";
        this.category_list = [];
        this.category_total = 0;
        this.page = 1;
        this.rows = 10;
        this.searchKey = "";
        this.pageTitle = "Procedures Categories";
        this.widgetSubTitle = "Manage Procedures Categories";
        this.loggedUserInfo = this.common.getLoggedInUser();
    }
    CategoriesComponent.prototype.ngOnInit = function () {
        this.getCategories();
    };
    CategoriesComponent.prototype.addCategory = function () {
        this.clearFields();
        this.modalCategory.show();
    };
    CategoriesComponent.prototype.saveCategory = function () {
        var _this = this;
        if (this.category_name == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Require field(s) missing!", "warning");
        }
        else {
            this.API.post("procedures/category/save", {
                mode: this.mode,
                key: this.key,
                category_name: this.category_name,
                category_desc: this.category_desc,
                created_by: this.loggedUserInfo.id
            }).subscribe(function (response) {
                if (response.statusCode == 200) {
                    _this.growlService.addSuccess("Record Saved.");
                    _this.modalCategory.hide();
                    _this.getCategories();
                    _this.clearFields();
                }
            }, function (error) {
            });
        }
    };
    CategoriesComponent.prototype.getCategories = function () {
        var _this = this;
        this.API.post("procedures/category/get", {
            page: this.page,
            rows: this.rows,
            search: this.searchKey
        }).subscribe(function (response) {
            _this.category_list = response.devMessage;
            _this.category_total = response.total;
        }, function (error) {
        });
    };
    CategoriesComponent.prototype.clearFields = function () {
        this.mode = "add";
        this.key = null;
        this.category_name = "";
        this.category_desc = "";
        this.searchKey = "";
    };
    CategoriesComponent.prototype.editCategory = function (data) {
        this.mode = "edit";
        this.key = data.id;
        this.category_name = data.procedure_name;
        this.category_desc = data.procedure_desc;
        this.modalCategory.show();
    };
    CategoriesComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getCategories();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalCategory"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], CategoriesComponent.prototype, "modalCategory", void 0);
    CategoriesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-categories',
            template: __webpack_require__(/*! ./categories.component.html */ "./src/app/pages/admin/procedures/categories/categories.component.html"),
            styles: [__webpack_require__(/*! ./categories.component.scss */ "./src/app/pages/admin/procedures/categories/categories.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_5__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"]])
    ], CategoriesComponent);
    return CategoriesComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/procedures/list/list.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/procedures/list/list.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n<div class=\"card\">\n  <div class=\"toolbar toolbar--inner\">\n    <div class=\"toolbar__label\">Procedures List</div>\n\n    <div class=\"actions\">\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\" tooltip=\"Search\"></i>\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getProcedureList()\" tooltip=\"Refresh list\"></a>\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addProcedure()\" tooltip=\"Add new procedure\"></a>\n    </div>\n\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getProcedureList()\" tooltip=\"Close search box\"></i>\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getProcedureList()\" tooltip=\"Search\"></i>\n\n    </div>\n    \n  </div>\n  <div class=\"card-body\">\n    <div class=\"table-responsive\">\n      <table class=\"table table-hover\">\n        <thead>\n        <tr>\n          <th rowspan=\"2\">Category</th>\n          <th rowspan=\"2\">Procedure Name</th>\n          <th rowspan=\"2\">Procedure Description</th>\n          <th class=\"hidden-sm-down\" style=\"horizontal-align : middle;text-align:center;\" colspan=\"3\">Commissions</th>\n          <th rowspan=\"2\" class=\"hidden-sm-down\">Date Created</th>\n        </tr>\n        <tr>\n          <td scope=\"col\">Dentist</td>\n          <td scope=\"col\">Assistant</td>\n          <td scope=\"col\">Secretary</td>\n          <td></td>\n        </tr>\n        </thead>\n        <tbody>\n          <tr *ngIf=\"procedure_list.length==0\">\n            <td colspan=\"6\" align=\"center\">\n              No record(s) found.\n            </td>\n          </tr>\n          <tr *ngFor=\"let data of procedure_list\" class=\"hover-edit\" (click)=\"onEdit(data)\">\n            <td>\n              {{ data.category }}\n            </td>\n            <td>\n              {{ data.procedure_name }}\n            </td>\n            <td>\n              {{ data.procedure_desc }}\n            </td>\n            <td class=\"hidden-sm-down\">\n              <span *ngIf=\"data.commission_type =='percentage'\">{{ data.commission_rate }}%</span>\n              <span *ngIf=\"data.commission_type =='fixed'\">\n                {{ data.commission_rate | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }} \n              </span>\n            </td>\n            <td class=\"hidden-sm-down\">\n              <span *ngIf=\"data.assistants_type =='percentage'\">{{ data.assistants_rate }}%</span>\n              <span *ngIf=\"data.assistants_type =='fixed'\">\n                {{ data.assistants_rate | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }} \n              </span>\n            </td>\n            <td class=\"hidden-sm-down\">\n              <span *ngIf=\"data.secretary_type =='percentage'\">{{ data.secretary_rate }}%</span>\n              <span *ngIf=\"data.secretary_type =='fixed'\">\n                {{ data.secretary_rate | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }} \n              </span>\n            </td>\n            <td class=\"hidden-sm-down\">\n              {{ data.date_created }}\n            </td>\n          </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td colspan=\"5\">\n              <pagination (pageChanged)=\"pageChange($event)\"\n                class=\"justify-content-center\"\n                [totalItems]=\"procedure_total\"\n                [maxSize]=\"10\"\n                [itemsPerPage]=\"rows\" >\n              </pagination>\n            </td>\n          </tr>\n        </tfoot>\n      </table>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalProcedures=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n  <div class=\"modal-dialog modal-lg modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5>{{mode == 'add' ? 'Add Procedure' : 'Edit Procedure'}}</h5>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n            <label class=\"col-sm-12 col-md-3 col-form-label\">Category<span style=\"color:red\">*</span></label>\n            <div class=\"col-sm-12 col-md-9\">\n              <div class=\"form-group\">\n                <ng-select\n                  [items]=\"procedure_parents_list\"\n                  bindLabel=\"procedure_name\"\n                  placeholder=\"Select a Category\"\n                  [(ngModel)]=\"parent_id\"\n                  bindValue=\"id\"\n                  >\n                </ng-select>\n              </div>\n            </div>\n        </div>\n        <div class=\"row\">\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Procedure name <span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-9\">\n              <div class=\"form-group\">\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"procedure_name\" placeholder=\"Procedure name...\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Procedure Description</label>\n          <div class=\"col-sm-12 col-md-9\">\n              <div class=\"form-group\">\n                  <textarea class=\"form-control\" [(ngModel)]=\"procedure_desc\" placeholder=\"Description here...\"></textarea>\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n        </div>\n        <div class=\"row\" *ngIf=\"!haveQuantity\">\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Dentist rate <span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-5\">\n              <div class=\"form-group\">\n                  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"commission_rate\" placeholder=\"0.00\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <select class=\"form-control\" [(ngModel)]=\"commission_type\">\n              <option value=\"percentage\" default>Percentage</option>\n              <option value=\"fixed\">Fixed</option>\n            </select>\n          </div>\n          <div class=\"col-sm-12 col-md-2 p-t-7\">\n            <div class=\"checkbox\">\n              <input type=\"checkbox\" id=\"dentistCheck\" [(ngModel)]=\"isDentistAllowed\">\n              <label class=\"checkbox__label\" for=\"dentistCheck\">Enable</label>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\" *ngIf=\"!haveQuantity\">\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Assistants rate <span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-5\">\n              <div class=\"form-group\">\n                  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"assistants_rate\" placeholder=\"0.00\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <select class=\"form-control\" [(ngModel)]=\"assistants_type\">\n              <option value=\"percentage\">Percentage</option>\n              <option value=\"fixed\" default>Fixed</option>\n            </select>\n          </div>\n          <div class=\"col-sm-12 col-md-2 p-t-7\">\n            <div class=\"checkbox\">\n              <input type=\"checkbox\" id=\"assistCheck\" [(ngModel)]=\"isAssistantAllowed\">\n              <label class=\"checkbox__label\" for=\"assistCheck\">Enable</label>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\" *ngIf=\"!haveQuantity\">\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Secretary rate <span style=\"color:red\">*</span></label>\n          <div class=\"col-sm-12 col-md-5\">\n              <div class=\"form-group\">\n                  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"secretary_rate\" placeholder=\"0.00\">\n                  <i class=\"form-group__bar\"></i>\n              </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <select class=\"form-control\" [(ngModel)]=\"secretary_type\">\n              <option value=\"percentage\">Percentage</option>\n              <option value=\"fixed\" default>Fixed</option>\n            </select>\n          </div>\n          <div class=\"col-sm-12 col-md-2 p-t-7\">\n            <div class=\"checkbox\">\n              <input type=\"checkbox\" id=\"secretaryCheck\" [(ngModel)]=\"isSecretaryAllowed\">\n              <label class=\"checkbox__label\" for=\"secretaryCheck\">Enable</label>\n            </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-12 p-t-7\">\n            <div class=\"checkbox\">\n              <input type=\"checkbox\" id=\"haveQuantity\" [(ngModel)]=\"haveQuantity\" (change)=\"quantityReqChange()\">\n              <label class=\"checkbox__label\" for=\"haveQuantity\">Quantity Required</label>\n            </div>\n          </div>\n        </div>\n        <br>\n        <div class=\"row\" *ngIf=\"haveQuantity\">\n          <div class=\"col-sm-12 col-md-12\">\n            <div class=\"toolbar toolbar--inner\" style=\"position: sticky;\">\n              <div class=\"toolbar__label\">Commissions with quantity requirements</div>\n          \n              <div class=\"actions\">\n                  <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addQtyReq()\"></a>\n              </div>\n  \n            </div>\n          </div>\n          <div class=\"table-responsive\" style=\"max-height: 400px;\">\n  \n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th rowspan=\"2\" style=\"horizontal-align : middle;text-align:center;vertical-align: middle;\">Qty</th>\n                  <th colspan=\"3\" style=\"horizontal-align : middle;text-align:center;\" >Commissions</th>\n                  <th rowspan=\"2\" style=\"horizontal-align : middle;text-align:center;vertical-align: middle;\"></th>\n                </tr>\n                <tr>\n                  <th scope=\"col\">Dentist</th>\n                  <th scope=\"col\">Assistant</th>\n                  <th scope=\"col\">Secretary</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngIf=\"procedure_quantity_req.length ==0\">\n                  <td colspan=\"7\" align=\"center\">Please click + button to add a record.</td>\n                </tr>\n                <tr *ngFor=\"let data_qty_req of procedure_quantity_req;let i = index\">\n                  <td style=\"max-width: 80px;\">\n                    <div class=\"form-group\">\n                      <input type=\"number\" class=\"form-control\" [(ngModel)]=\"data_qty_req.qty\" placeholder=\"0.00\">\n                      <i class=\"form-group__bar\"></i>\n                    </div>\n                    \n                  </td>\n                  <td>\n                    <div class=\"row\">\n                      <div class=\"col-sm-12 col-md-12\">\n                        <div class=\"form-group\">\n                          <input type=\"number\" class=\"form-control\" [(ngModel)]=\"data_qty_req.dentist_rate\" placeholder=\"0.00\">\n                          <i class=\"form-group__bar\"></i>\n                        </div>\n                      </div>\n                      <div class=\"col-sm-12 col-md-12\">\n                        <select class=\"form-control\" [(ngModel)]=\"data_qty_req.dentist_type\">\n                          <option value=\"percentage\">Percentage</option>\n                          <option value=\"fixed\" default>Fixed</option>\n                        </select>\n                      </div>\n                    </div>\n                  </td>\n                  <td>\n                    <div class=\"col-sm-12 col-md-12\">\n                      <div class=\"form-group\">\n                        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"data_qty_req.assistant_rate\" placeholder=\"0.00\">\n                        <i class=\"form-group__bar\"></i>\n                      </div>\n                    </div>\n\n                    <div class=\"col-sm-12 col-md-12\">\n                      <select class=\"form-control\" [(ngModel)]=\"data_qty_req.assistant_type\">\n                        <option value=\"percentage\">Percentage</option>\n                        <option value=\"fixed\" default>Fixed</option>\n                      </select>\n                    </div>\n                  </td>\n                  <td>\n                    <div class=\"col-sm-12 col-md-12\">\n                      <div class=\"form-group\">\n                        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"data_qty_req.secretary_rate\" placeholder=\"0.00\">\n                        <i class=\"form-group__bar\"></i>\n                      </div>\n                    </div>\n                    <div class=\"col-sm-12 col-md-12\">\n                      <select class=\"form-control\" [(ngModel)]=\"data_qty_req.secretary_type\">\n                        <option value=\"percentage\">Percentage</option>\n                        <option value=\"fixed\" default>Fixed</option>\n                      </select>\n                    </div>\n                    \n                  </td>\n                  <td>\n                    <a (click)=\"deleteRowQty(i)\"><i class=\"zmdi zmdi-delete\"></i></a>\n                  </td>\n                </tr>\n              </tbody>\n\n            </table>\n          <div>\n        </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveProcedure()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalProcedures.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/procedures/list/list.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/procedures/list/list.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\n.p-t-7 {\n  padding-top: 7px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vcHJvY2VkdXJlcy9saXN0L0M6XFxVc2Vyc1xcQURNSU5cXERlc2t0b3BcXFByb2plY3RzXFxkZW50YWwtZmUvc3JjXFxhcHBcXHBhZ2VzXFxhZG1pblxccHJvY2VkdXJlc1xcbGlzdFxcbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7RUFHTSxvQkFBb0IsRUFBQTs7QUFJeEI7RUFDRSxnQkFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWRtaW4vcHJvY2VkdXJlcy9saXN0L2xpc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLWRlbW8ge1xyXG4gICAgJiA+IC5idG4sXHJcbiAgICAmID4gLmJ0bi1ncm91cCB7XHJcbiAgICAgIG1hcmdpbjogMCA1cHggMTBweCAwO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnAtdC03IHtcclxuICAgIHBhZGRpbmctdG9wOjdweDtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/admin/procedures/list/list.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/admin/procedures/list/list.component.ts ***!
  \***************************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");








var ListComponent = /** @class */ (function () {
    function ListComponent(API, growlService, common) {
        this.API = API;
        this.growlService = growlService;
        this.common = common;
        this.pageTitle = "Procedures List";
        this.widgetSubTitle = "Manage Procedures List";
        this.loggedUserInfo = null;
        this.page = 1;
        this.row = 10;
        this.mode = "add";
        this.procedure_list = [];
        this.procedure_parents_list = [];
        this.procedure_quantity_req = [];
        this.procedure_total = 0;
        this.parent_id = null;
        this.commission_type = "percentage";
        this.commission_rate = 0.00;
        this.assistants_rate = 0.00;
        this.assistants_type = "fixed";
        this.secretary_rate = 0;
        this.secretary_type = "fixed";
        this.price = 0.00;
        this.procedure_desc = "";
        this.procedure_name = "";
        this.isDentistAllowed = true;
        this.isAssistantAllowed = true;
        this.isSecretaryAllowed = true;
        this.haveQuantity = false;
        this.quantityComsThreshold = 0;
        this.greaterThanQty = 0;
        this.lessThanQty = 0;
        this.searchKey = "";
        this.loggedUserInfo = this.common.getLoggedInUser();
    }
    ListComponent.prototype.ngOnInit = function () {
        this.loadInit();
    };
    ListComponent.prototype.loadInit = function () {
        this.getProcedureList();
        this.getProcedureParents();
        this.clearFields();
    };
    ListComponent.prototype.addProcedure = function () {
        this.clearFields();
        this.modalProcedures.show();
    };
    ListComponent.prototype.getProcedureList = function () {
        var _this = this;
        this.API.post("procedures/get", {
            page: this.page,
            row: this.row,
            search: this.searchKey
        }).subscribe(function (response) {
            _this.procedure_list = response.devMessage;
            _this.procedure_total = response.total;
        }, function (error) {
        });
    };
    ListComponent.prototype.saveProcedure = function () {
        var _this = this;
        if (Object(util__WEBPACK_IMPORTED_MODULE_5__["isNull"])(this.parent_id) || this.procedure_name == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required Field(s) Missing!", "warning");
        }
        else {
            this.API.post("procedures/save", {
                mode: this.mode,
                parent_id: this.parent_id,
                key: this.key,
                commission_type: this.commission_type,
                commission_rate: this.commission_rate,
                procedure_desc: this.procedure_desc,
                price: this.price,
                procedure_name: this.procedure_name,
                assistants_rate: this.assistants_rate,
                assistants_type: this.assistants_type,
                secretary_rate: this.secretary_rate,
                secretary_type: this.secretary_type,
                isDentistAllowed: this.isDentistAllowed,
                isAssistantAllowed: this.isAssistantAllowed,
                isSecretaryAllowed: this.isSecretaryAllowed,
                haveQuantity: this.haveQuantity,
                greaterThanQty: this.greaterThanQty,
                lessThanQty: this.lessThanQty,
                quantityComsThreshold: this.haveQuantity ? this.quantityComsThreshold : 0,
                procedure_quantity_req: this.procedure_quantity_req,
                created_by: this.loggedUserInfo.id
            }).subscribe(function (response) {
                if (response.devMessage) {
                    var msg = _this.mode == "add" ? "Record Saved." : "Record Updated.";
                    _this.growlService.addSuccess(msg);
                    _this.modalProcedures.hide();
                    _this.loadInit();
                }
            }, function (error) {
            });
        }
    };
    ListComponent.prototype.clearFields = function () {
        this.mode = "add";
        this.haveQuantity = false;
        this.commission_type = "percentage";
        this.commission_rate = 0.00;
        this.procedure_desc = "";
        this.price = 0.00;
        this.procedure_name = "";
        this.key = null;
        this.parent_id = null;
        this.assistants_type = "fixed";
        this.assistants_rate = 0.00;
        this.isDentistAllowed = true;
        this.isAssistantAllowed = true;
        this.isSecretaryAllowed = true;
        this.quantityComsThreshold = 0;
        this.haveQuantity = false;
        this.greaterThanQty = 0;
        this.lessThanQty = 0;
        this.procedure_quantity_req = [];
    };
    ListComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getProcedureList();
    };
    ListComponent.prototype.onEdit = function (data) {
        this.mode = "edit";
        this.key = data.id;
        this.parent_id = data.parent_id;
        this.commission_type = data.commission_type;
        this.commission_rate = data.commission_rate;
        this.procedure_desc = data.procedure_desc;
        this.price = data.price;
        this.procedure_name = data.procedure_name;
        this.assistants_type = data.assistants_type;
        this.assistants_rate = data.assistants_rate;
        this.secretary_rate = data.secretary_rate;
        this.secretary_type = data.secretary_type;
        this.isDentistAllowed = data.isDentistAllowed;
        this.isAssistantAllowed = data.isAssistantAllowed;
        this.isSecretaryAllowed = data.isSecretaryAllowed;
        this.quantityComsThreshold = data.quantityComsThreshold;
        this.haveQuantity = data.haveQuantity;
        this.procedure_quantity_req = data.procedure_quantity_req;
        this.modalProcedures.show();
    };
    ListComponent.prototype.getProcedureParents = function () {
        var _this = this;
        this.API.post("lookups/get-procedure-parents", {}).subscribe(function (response) {
            _this.procedure_parents_list = response.devMessage;
        }, function (error) {
        });
    };
    ListComponent.prototype.addQtyReq = function () {
        var tmp = {
            qty: this.procedure_quantity_req.length + 1,
            dentist_rate: 0,
            assistant_rate: 0,
            secretary_rate: 0
        };
        this.procedure_quantity_req.push(tmp);
    };
    ListComponent.prototype.deleteRowQty = function (data) {
        this.procedure_quantity_req.splice(data, 1);
    };
    ListComponent.prototype.quantityReqChange = function () {
        if (this.haveQuantity) {
        }
        else {
            this.procedure_quantity_req = [];
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalProcedures"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], ListComponent.prototype, "modalProcedures", void 0);
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/pages/admin/procedures/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.scss */ "./src/app/pages/admin/procedures/list/list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_6__["GrowlService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/procedures/procedures.component.html":
/*!******************************************************************!*\
  !*** ./src/app/pages/admin/procedures/procedures.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n</header>\r\n\r\n<div class=\"card\">\r\n  <div class=\"toolbar toolbar--inner\">\r\n    <div class=\"toolbar__label\">Procedures List</div>\r\n\r\n    <div class=\"actions\">\r\n        <i class=\"actions__item zmdi zmdi-search\" (click)=\"todoSearch = true\" tooltip=\"Search\"></i>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-refresh\" (click)=\"getProcedureList()\" tooltip=\"Refresh list\"></a>\r\n        <a [routerLink]=\"\" class=\"actions__item zmdi zmdi-plus\" (click)=\"addProcedure()\" tooltip=\"Add new procedure\"></a>\r\n    </div>\r\n\r\n    <div class=\"toolbar__search\" *ngIf=\"todoSearch\">\r\n        <input type=\"text\" placeholder=\"Search...\" [(ngModel)]=\"searchKey\"> \r\n        <i class=\"toolbar__search__close zmdi zmdi-long-arrow-left\" (click)=\"todoSearch = false;searchKey='';getProcedureList()\" tooltip=\"Close search box\"></i>\r\n        <i class=\"toolbar__search__search zmdi zmdi-search\" (click)=\"getProcedureList()\" tooltip=\"Search\"></i>\r\n\r\n    </div>\r\n    \r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive\">\r\n      <table class=\"table table-hover\">\r\n        <thead>\r\n        <tr>\r\n          <th rowspan=\"2\">Category</th>\r\n          <th rowspan=\"2\">Procedure Name</th>\r\n          <th rowspan=\"2\">Procedure Description</th>\r\n          <th class=\"hidden-sm-down\" style=\"horizontal-align : middle;text-align:center;\" colspan=\"3\">Commissions</th>\r\n          <th rowspan=\"2\" class=\"hidden-sm-down\">Date Created</th>\r\n        </tr>\r\n        <tr>\r\n          <td scope=\"col\">Dentist</td>\r\n          <td scope=\"col\">Assistant</td>\r\n          <td scope=\"col\">Secretary</td>\r\n          <td></td>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngIf=\"procedure_list.length==0\">\r\n            <td colspan=\"6\" align=\"center\">\r\n              No record(s) found.\r\n            </td>\r\n          </tr>\r\n          <tr *ngFor=\"let data of procedure_list\" class=\"hover-edit\" (click)=\"onEdit(data)\">\r\n            <td>\r\n              {{ data.category }}\r\n            </td>\r\n            <td>\r\n              {{ data.procedure_name }}\r\n            </td>\r\n            <td>\r\n              {{ data.procedure_desc }}\r\n            </td>\r\n            <td class=\"hidden-sm-down\">\r\n              <span *ngIf=\"data.commission_type =='percentage'\">{{ data.commission_rate }}%</span>\r\n              <span *ngIf=\"data.commission_type =='fixed'\">\r\n                {{ data.commission_rate | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }} \r\n              </span>\r\n            </td>\r\n            <td class=\"hidden-sm-down\">\r\n              <span *ngIf=\"data.assistants_type =='percentage'\">{{ data.assistants_rate }}%</span>\r\n              <span *ngIf=\"data.assistants_type =='fixed'\">\r\n                {{ data.assistants_rate | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }} \r\n              </span>\r\n            </td>\r\n            <td class=\"hidden-sm-down\">\r\n              <span *ngIf=\"data.secretary_type =='percentage'\">{{ data.secretary_rate }}%</span>\r\n              <span *ngIf=\"data.secretary_type =='fixed'\">\r\n                {{ data.secretary_rate | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }} \r\n              </span>\r\n            </td>\r\n            <td class=\"hidden-sm-down\">\r\n              {{ data.date_created }}\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n        <tfoot>\r\n          <tr>\r\n            <td colspan=\"5\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                class=\"justify-content-center\"\r\n                [totalItems]=\"procedure_total\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalProcedures=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog modal-lg modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Procedure' : 'Edit Procedure'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n            <label class=\"col-sm-12 col-md-3 col-form-label\">Category<span style=\"color:red\">*</span></label>\r\n            <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                <ng-select\r\n                  [items]=\"procedure_parents_list\"\r\n                  bindLabel=\"procedure_name\"\r\n                  placeholder=\"Select a Category\"\r\n                  [(ngModel)]=\"parent_id\"\r\n                  bindValue=\"id\"\r\n                  >\r\n                </ng-select>\r\n              </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Procedure name <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"procedure_name\" placeholder=\"Procedure name...\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Procedure Description</label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <textarea class=\"form-control\" [(ngModel)]=\"procedure_desc\" placeholder=\"Description here...\"></textarea>\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Commission rate <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-6\">\r\n              <div class=\"form-group\">\r\n                  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"commission_rate\" placeholder=\"0.00\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-3\">\r\n            <select class=\"form-control\" [(ngModel)]=\"commission_type\">\r\n              <option value=\"percentage\" default>Percentage</option>\r\n              <option value=\"fixed\">Fixed</option>\r\n            </select>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Assistants rate <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-6\">\r\n              <div class=\"form-group\">\r\n                  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"assistants_rate\" placeholder=\"0.00\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-3\">\r\n            <select class=\"form-control\" [(ngModel)]=\"assistants_type\">\r\n              <option value=\"percentage\">Percentage</option>\r\n              <option value=\"fixed\" default>Fixed</option>\r\n            </select>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Secretary rate <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-6\">\r\n              <div class=\"form-group\">\r\n                  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"secretary_rate\" placeholder=\"0.00\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-3\">\r\n            <select class=\"form-control\" [(ngModel)]=\"secretary_type\">\r\n              <option value=\"percentage\">Percentage</option>\r\n              <option value=\"fixed\" default>Fixed</option>\r\n            </select>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveProcedure()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalProcedures.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/procedures/procedures.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/pages/admin/procedures/procedures.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL3Byb2NlZHVyZXMvcHJvY2VkdXJlcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/admin/procedures/procedures.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/admin/procedures/procedures.component.ts ***!
  \****************************************************************/
/*! exports provided: ProceduresComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProceduresComponent", function() { return ProceduresComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ProceduresComponent = /** @class */ (function () {
    function ProceduresComponent() {
    }
    ProceduresComponent.prototype.ngOnInit = function () {
    };
    ProceduresComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-procedures',
            template: __webpack_require__(/*! ./procedures.component.html */ "./src/app/pages/admin/procedures/procedures.component.html"),
            styles: [__webpack_require__(/*! ./procedures.component.scss */ "./src/app/pages/admin/procedures/procedures.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProceduresComponent);
    return ProceduresComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/procedures/procedures.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/admin/procedures/procedures.module.ts ***!
  \*************************************************************/
/*! exports provided: ProceduresModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProceduresModule", function() { return ProceduresModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _procedures_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./procedures.component */ "./src/app/pages/admin/procedures/procedures.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var _categories_categories_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./categories/categories.component */ "./src/app/pages/admin/procedures/categories/categories.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./list/list.component */ "./src/app/pages/admin/procedures/list/list.component.ts");















var ROUTE = [
    {
        path: "",
        redirectTo: 'list'
    },
    {
        path: "categories",
        component: _categories_categories_component__WEBPACK_IMPORTED_MODULE_13__["CategoriesComponent"]
    },
    {
        path: "list",
        component: _list_list_component__WEBPACK_IMPORTED_MODULE_14__["ListComponent"]
    },
];
var ProceduresModule = /** @class */ (function () {
    function ProceduresModule() {
    }
    ProceduresModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _procedures_component__WEBPACK_IMPORTED_MODULE_4__["ProceduresComponent"],
                _categories_categories_component__WEBPACK_IMPORTED_MODULE_13__["CategoriesComponent"],
                _list_list_component__WEBPACK_IMPORTED_MODULE_14__["ListComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], ProceduresModule);
    return ProceduresModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-procedures-procedures-module.js.map