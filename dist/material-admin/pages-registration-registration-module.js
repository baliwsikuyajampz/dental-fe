(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-registration-registration-module"],{

/***/ "./src/app/pages/registration/ortho/ortho.component.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/registration/ortho/ortho.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content--full registration-form\">\r\n  <div class=\"content__inner\">\r\n    <header class=\"content__title\">\r\n      <h1>{{ pageTitle }}</h1>\r\n    </header>\r\n  \r\n    <div class=\"card\">\r\n      <div class=\"card-body card-overflow-scroll\">\r\n        <h4 class=\"card-title\">Personal Information</h4>\r\n  \r\n        <form class=\"row\">\r\n\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>First Name</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Last name</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Middle name</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Address</label>\r\n              <textarea class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\"></textarea>\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Birthday</label>\r\n              <input type=\"date\" class=\"form-control\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Religion</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Nationality</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: davidsmith@gmail.com\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>Occupation</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Office No.</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Gender</label>\r\n              <select class=\"form-control\">\r\n                <option>Male</option>\r\n                <option>Female</option>\r\n              </select>\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Nickname</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Mobile/Home No.</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: ######\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n          </div>\r\n        </form>\r\n\r\n        <h4 class=\"card-title\">For Minors</h4>\r\n\r\n        <form class=\"row\">\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>Parents/Guardian Name</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Whom we thank for refering you?</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>Occupation</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n          </div>\r\n        </form>\r\n\r\n        <h4 class=\"card-title\">Dental History</h4>\r\n\r\n        <form class=\"row\">\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>Previous Dentist</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-6\">\r\n            <div class=\"form-group\">\r\n              <label>Last Dental Visit</label>\r\n              <input type=\"text\" class=\"form-control\" placeholder=\"e.g: David Smith\">\r\n              <i class=\"form-group__bar\"></i>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/registration/ortho/ortho.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/registration/ortho/ortho.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".registration-form {\n  min-height: 100vh;\n  padding-top: 1.2rem;\n  background: radial-gradient(circle, #2196F3, white); }\n\n.card-overflow-scroll {\n  overflow-y: scroll;\n  max-height: 85vh; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVnaXN0cmF0aW9uL29ydGhvL0M6XFxVc2Vyc1xcQURNSU5cXERlc2t0b3BcXFByb2plY3RzXFxkZW50YWwtZmUvc3JjXFxhcHBcXHBhZ2VzXFxyZWdpc3RyYXRpb25cXG9ydGhvXFxvcnRoby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsbURBQW1ELEVBQUE7O0FBR3ZEO0VBQ0ksa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVnaXN0cmF0aW9uL29ydGhvL29ydGhvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJlZ2lzdHJhdGlvbi1mb3Jte1xyXG4gICAgbWluLWhlaWdodDogMTAwdmg7XHJcbiAgICBwYWRkaW5nLXRvcDogMS4ycmVtO1xyXG4gICAgYmFja2dyb3VuZDogcmFkaWFsLWdyYWRpZW50KGNpcmNsZSwgICMyMTk2RjMsd2hpdGUpO1xyXG59XHJcblxyXG4uY2FyZC1vdmVyZmxvdy1zY3JvbGx7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICBtYXgtaGVpZ2h0OiA4NXZoO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/registration/ortho/ortho.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/registration/ortho/ortho.component.ts ***!
  \*************************************************************/
/*! exports provided: OrthoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrthoComponent", function() { return OrthoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var OrthoComponent = /** @class */ (function () {
    function OrthoComponent() {
        this.pageTitle = "Registration for Ortho Patients";
    }
    OrthoComponent.prototype.ngOnInit = function () {
    };
    OrthoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ortho',
            template: __webpack_require__(/*! ./ortho.component.html */ "./src/app/pages/registration/ortho/ortho.component.html"),
            styles: [__webpack_require__(/*! ./ortho.component.scss */ "./src/app/pages/registration/ortho/ortho.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], OrthoComponent);
    return OrthoComponent;
}());



/***/ }),

/***/ "./src/app/pages/registration/registration.component.html":
/*!****************************************************************!*\
  !*** ./src/app/pages/registration/registration.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/registration/registration.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/pages/registration/registration.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/registration/registration.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/registration/registration.component.ts ***!
  \**************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent() {
    }
    RegistrationComponent.prototype.ngOnInit = function () {
    };
    RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registration',
            template: __webpack_require__(/*! ./registration.component.html */ "./src/app/pages/registration/registration.component.html"),
            styles: [__webpack_require__(/*! ./registration.component.scss */ "./src/app/pages/registration/registration.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RegistrationComponent);
    return RegistrationComponent;
}());



/***/ }),

/***/ "./src/app/pages/registration/registration.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/registration/registration.module.ts ***!
  \***********************************************************/
/*! exports provided: RegistrationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationModule", function() { return RegistrationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registration_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registration.component */ "./src/app/pages/registration/registration.component.ts");
/* harmony import */ var _ortho_ortho_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ortho/ortho.component */ "./src/app/pages/registration/ortho/ortho.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");







var ROUTE = [
    { path: "", component: _registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"] },
    { path: "ortho", component: _ortho_ortho_component__WEBPACK_IMPORTED_MODULE_5__["OrthoComponent"] }
];
var RegistrationModule = /** @class */ (function () {
    function RegistrationModule() {
    }
    RegistrationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_ortho_ortho_component__WEBPACK_IMPORTED_MODULE_5__["OrthoComponent"], _registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"]],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"]
            ]
        })
    ], RegistrationModule);
    return RegistrationModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-registration-registration-module.js.map