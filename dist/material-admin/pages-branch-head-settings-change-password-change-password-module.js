(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-branch-head-settings-change-password-change-password-module"],{

/***/ "./src/app/pages/branch-head/settings/change-password/change-password.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/branch-head/settings/change-password/change-password.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content__inner\">\r\n\r\n  <header class=\"content__title\">\r\n    <h1>{{ pageTitle }}</h1>\r\n  </header>\r\n\r\n  <div class=\"card\">\r\n    <div class=\"card-body\">\r\n\r\n      <div class=\"form-group form-group--float form-group--centered\">\r\n        <input\r\n          type=\"password\"\r\n          class=\"form-control\"\r\n          [(ngModel)]=\"oldPass\"\r\n          InputFloat\r\n          minlength=\"6\"\r\n        />\r\n        <label>Old Password</label>\r\n        <i class=\"form-group__bar\"></i>\r\n      </div>\r\n\r\n      <div class=\"form-group form-group--float form-group--centered\">\r\n        <input\r\n          type=\"password\"\r\n          class=\"form-control\"\r\n          [(ngModel)]=\"newPass\"\r\n          InputFloat\r\n          minlength=\"6\"\r\n        />\r\n        <label>New Password</label>\r\n        <i class=\"form-group__bar\"></i>\r\n      </div>\r\n\r\n      <div class=\"form-group form-group--float form-group--centered\">\r\n        <input\r\n          type=\"password\"\r\n          class=\"form-control\"\r\n          [(ngModel)]=\"confirmPass\"\r\n          InputFloat\r\n          maxlength=\"16\"\r\n          minlength=\"6\"\r\n        />\r\n        <label>Confirm new password</label>\r\n        <i class=\"form-group__bar\"></i>\r\n      </div>\r\n      <div class=\"form-group form-group--centered\">\r\n        <button\r\n          type=\"button\"\r\n          class=\"btn btn-primary\"\r\n          (click)=\"onChangePassword()\"\r\n        >\r\n          Save\r\n        </button>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n  \r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/branch-head/settings/change-password/change-password.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/branch-head/settings/change-password/change-password.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3NldHRpbmdzL2NoYW5nZS1wYXNzd29yZC9jaGFuZ2UtcGFzc3dvcmQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/branch-head/settings/change-password/change-password.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/branch-head/settings/change-password/change-password.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ChangePasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordComponent", function() { return ChangePasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);




var ChangePasswordComponent = /** @class */ (function () {
    function ChangePasswordComponent(API) {
        this.API = API;
        this.pageTitle = "Change Password";
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        this.getOnlineUser();
        console.log(this.userInfo);
    };
    ChangePasswordComponent.prototype.getOnlineUser = function () {
        if (localStorage.getItem("user_info") !== null) {
            this.userInfo = JSON.parse(localStorage.getItem("user_info"));
        }
    };
    ChangePasswordComponent.prototype.onChangePassword = function () {
        if (this.oldPass == "" || this.newPass == "" || this.confirmPass == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Oops!", "Required fields missing", "warning");
        }
        else if (this.newPass !== this.confirmPass) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Oops!", "New password didn't match", "warning");
        }
        else {
            this.API.post("users/change-password", {
                userid: this.userInfo.id,
                oldpass: this.oldPass,
                newpass: this.newPass,
            }).subscribe(function (response) {
                console.log(response);
                if (response.statusCode == 200) {
                    if (response.devMessage == "pass_mismatch") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Oops!", "Password Mismatch", "warning");
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Success!", "Password Updated", "success");
                    }
                }
            }, function (error) {
            });
        }
    };
    ChangePasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-change-password',
            template: __webpack_require__(/*! ./change-password.component.html */ "./src/app/pages/branch-head/settings/change-password/change-password.component.html"),
            styles: [__webpack_require__(/*! ./change-password.component.scss */ "./src/app/pages/branch-head/settings/change-password/change-password.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/settings/change-password/change-password.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/branch-head/settings/change-password/change-password.module.ts ***!
  \**************************************************************************************/
/*! exports provided: ChangePasswordModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordModule", function() { return ChangePasswordModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _change_password_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./change-password.component */ "./src/app/pages/branch-head/settings/change-password/change-password.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var src_app_directive_input_float_input_float_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/directive/input-float/input-float.module */ "./src/app/directive/input-float/input-float.module.ts");














var ROUTE = [{ path: "", component: _change_password_component__WEBPACK_IMPORTED_MODULE_4__["ChangePasswordComponent"] }];
var ChangePasswordModule = /** @class */ (function () {
    function ChangePasswordModule() {
    }
    ChangePasswordModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _change_password_component__WEBPACK_IMPORTED_MODULE_4__["ChangePasswordComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"],
                src_app_directive_input_float_input_float_module__WEBPACK_IMPORTED_MODULE_13__["InputFloatModule"]
            ]
        })
    ], ChangePasswordModule);
    return ChangePasswordModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-branch-head-settings-change-password-change-password-module.js.map