(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-branch-head-reports-reports-module"],{

/***/ "./src/app/pages/branch-head/reports/commissions/commissions.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/commissions/commissions.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n<div class=\"card\">\n  <div class=\"card-body\">\n\n    <tabset>\n      <tab heading=\"Dentists\">\n        <div class=\"table-responsive \" style=\"overflow-x: scroll;\">\n          <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th style=\"min-width: 200px !important;position: sticky !important;left:0;background-color: #fff;\">Dentist name</th>\n                <th *ngFor=\"let date of cutoff_dates\" style=\"min-width: 170px !important;\">\n                  {{ date.display}} \n                </th>\n              </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let doctor of doctors_list\">\n                  <td style=\"min-width: 200px !important;position: sticky !important;left:0;background-color: #fff;\">{{ doctor.fullname }}</td>\n                  <td *ngFor=\"let commission of doctor.commissions\">{{ commission | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                </tr>\n              </tbody>\n          </table>\n        </div>\n      </tab>\n      <tab heading=\"Assistants\">\n        <div class=\"table-responsive \" style=\"overflow-x: scroll;\">\n          <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th style=\"min-width: 200px !important;position: sticky !important;left:0;background-color: #fff;\">Assistant name</th>\n                <th *ngFor=\"let date of cutoff_dates\" style=\"min-width: 170px !important;\">\n                  {{ date.display}} \n                </th>\n              </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let assistant of assistant_list\">\n                  <td style=\"min-width: 200px !important;position: sticky !important;left:0;background-color: #fff;\">{{ assistant.fullname }}</td>\n                  <td *ngFor=\"let commission of assistant.commissions\">{{ commission | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                </tr>\n              </tbody>\n          </table>\n        </div>\n      </tab>\n      <tab heading=\"Secretaries\">\n        <div class=\"table-responsive \" style=\"overflow-x: scroll;\">\n          <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th style=\"min-width: 200px !important;position: sticky !important;left:0;background-color: #fff;\">Secretary name</th>\n                <th *ngFor=\"let date of cutoff_dates\" style=\"min-width: 170px !important;\">\n                  {{ date.display}} \n                </th>\n              </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let secretary of secretary_list\">\n                  <td style=\"min-width: 200px !important;position: sticky !important;left:0;background-color: #fff;\">{{ secretary.fullname }}</td>\n                  <td *ngFor=\"let commission of secretary.commissions\">{{ commission | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                </tr>\n              </tbody>\n          </table>\n        </div>\n      </tab>\n    </tabset>\n\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/commissions/commissions.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/commissions/commissions.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3JlcG9ydHMvY29tbWlzc2lvbnMvY29tbWlzc2lvbnMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/commissions/commissions.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/commissions/commissions.component.ts ***!
  \********************************************************************************/
/*! exports provided: CommissionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionsComponent", function() { return CommissionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_3__);




var CommissionsComponent = /** @class */ (function () {
    function CommissionsComponent(API, growlService) {
        this.API = API;
        this.growlService = growlService;
        this.pageTitle = "Commissions";
        this.widgetSubTitle = "Commission Report";
        this.cutoff_dates = [];
        this.doctors_list = [];
        this.assistant_list = [];
        this.secretary_list = [];
    }
    CommissionsComponent.prototype.ngOnInit = function () {
        this.getCutOffDates();
        this.getDoctorCommissions();
        this.getAssistantsCommissions();
        this.getSecretaryYearCommission();
    };
    CommissionsComponent.prototype.getCutOffDates = function () {
        var _this = this;
        this.API.post("reports/test", {}).subscribe(function (response) {
            _this.cutoff_dates = response.devMessage;
        }, function (error) {
        });
    };
    CommissionsComponent.prototype.getDoctorCommissions = function () {
        var _this = this;
        this.API.post("reports/get-commissions", {}).subscribe(function (response) {
            _this.doctors_list = response.devMessage;
        }, function (error) {
        });
    };
    CommissionsComponent.prototype.getAssistantsCommissions = function () {
        var _this = this;
        this.API.post("reports/get-assistants-commissions", {}).subscribe(function (response) {
            _this.assistant_list = response.devMessage;
        }, function (error) {
        });
    };
    CommissionsComponent.prototype.getSecretaryYearCommission = function () {
        var _this = this;
        this.API.post("reports/get-secretary-commissions", {}).subscribe(function (response) {
            _this.secretary_list = response.devMessage;
        }, function (error) {
        });
    };
    CommissionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-commissions',
            template: __webpack_require__(/*! ./commissions.component.html */ "./src/app/pages/branch-head/reports/commissions/commissions.component.html"),
            styles: [__webpack_require__(/*! ./commissions.component.scss */ "./src/app/pages/branch-head/reports/commissions/commissions.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_3__["GrowlService"]])
    ], CommissionsComponent);
    return CommissionsComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n</header>\r\n\r\n<div class=\"card\" *ngFor=\"let data of profitAndLoss\">\r\n  <div class=\"card-header\">\r\n    <h4 class=\"card-title\">{{data.branch_name}}</h4>\r\n  </div>\r\n  <div class=\"card-body\">\r\n      <div >\r\n        <div class=\"table-responsive \" style=\"overflow-y: scroll;\">\r\n          <table class=\"table table-striped\">\r\n            <thead>\r\n              <tr>\r\n                <th [attr.colspan]=\"expense_categories.length+3\" style=\"vertical-align : middle;text-align:center ;background-color: rgb(105, 248, 248);\">Income</th>\r\n                <th [attr.colspan]=\"expense_categories.length+3\" style=\"vertical-align : middle;text-align:center ;background-color: rgb(105, 248, 248);\">Tally Sheet</th>\r\n              </tr>\r\n              <tr>\r\n                <th rowspan=\"2\" style=\"vertical-align : middle;text-align:center ;\">Months</th>\r\n                <th rowspan=\"2\" style=\"vertical-align : middle;text-align:center;\">Gross Sales</th>\r\n                <th [attr.colspan]=\"expense_categories.length\" style=\"vertical-align : middle;text-align:center;background-color: orange;\" >Expenses</th>\r\n                <th rowspan=\"2\" style=\"vertical-align : middle;text-align:center;\">Net Worth</th>\r\n\r\n              </tr>\r\n              <tr>\r\n                <th *ngFor=\"let expense of expense_categories\" style=\"border: 1px solid #ddd; vertical-align : middle;text-align:center;background-color: rgb(253, 208, 125);\">{{expense.expense_category_name}}</th>\r\n              </tr>\r\n            </thead>\r\n            <tr *ngFor=\"let sub of data.data\">\r\n              <td style=\"max-width: 100px;width: 100px;\">{{ sub.month_name }}</td>\r\n              <td style=\"max-width: 100px;width: 100px;\">{{ sub.total_sales | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</td>\r\n              <td *ngFor=\"let expense of sub.expenses\" style=\"border: 1px solid #ddd; max-width: 90px;width: 90px;\">{{expense.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</td>\r\n              <td style=\"max-width: 100px;width: 100px;border-right: 1px solid #ddd;\">\r\n                <span *ngIf=\"sub.net_worth >0\" style=\"color:green\">{{ sub.net_worth | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</span>\r\n                <span *ngIf=\"sub.net_worth <=0\" style=\"color:red\">{{ sub.net_worth | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</span>\r\n\r\n              </td>\r\n            </tr>\r\n          </table>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3JlcG9ydHMvcHJvZml0LWFuZC1sb3N0L3Byb2ZpdC1hbmQtbG9zdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.ts ***!
  \****************************************************************************************/
/*! exports provided: ProfitAndLostComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfitAndLostComponent", function() { return ProfitAndLostComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-growl */ "./node_modules/ngx-growl/ngx-growl.umd.js");
/* harmony import */ var ngx_growl__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ngx_growl__WEBPACK_IMPORTED_MODULE_3__);




var ProfitAndLostComponent = /** @class */ (function () {
    function ProfitAndLostComponent(API, growlService) {
        this.API = API;
        this.growlService = growlService;
        this.pageTitle = "Profit and Loss";
        this.widgetSubTitle = "";
        this.profitAndLoss = [];
        this.expense_categories = [];
    }
    ProfitAndLostComponent.prototype.ngOnInit = function () {
        this.getProfitAndLoss();
        this.getExpenseCategories();
    };
    ProfitAndLostComponent.prototype.getProfitAndLoss = function () {
        var _this = this;
        this.API.post("reports/get-profit-and-loss", {}).subscribe(function (response) {
            _this.profitAndLoss = response.devMessage;
        }, function (error) {
        });
    };
    ProfitAndLostComponent.prototype.getExpenseCategories = function () {
        var _this = this;
        this.API.post("lookups/get-expense-categories", {}).subscribe(function (response) {
            _this.expense_categories = response.devMessage;
        }, function (error) {
        });
    };
    ProfitAndLostComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profit-and-lost',
            template: __webpack_require__(/*! ./profit-and-lost.component.html */ "./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.html"),
            styles: [__webpack_require__(/*! ./profit-and-lost.component.scss */ "./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_growl__WEBPACK_IMPORTED_MODULE_3__["GrowlService"]])
    ], ProfitAndLostComponent);
    return ProfitAndLostComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/reports/reports.component.html":
/*!******************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/reports.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  reports works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/reports.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/reports.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3JlcG9ydHMvcmVwb3J0cy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/reports.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/reports.component.ts ***!
  \****************************************************************/
/*! exports provided: ReportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsComponent", function() { return ReportsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ReportsComponent = /** @class */ (function () {
    function ReportsComponent() {
    }
    ReportsComponent.prototype.ngOnInit = function () {
    };
    ReportsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-reports',
            template: __webpack_require__(/*! ./reports.component.html */ "./src/app/pages/branch-head/reports/reports.component.html"),
            styles: [__webpack_require__(/*! ./reports.component.scss */ "./src/app/pages/branch-head/reports/reports.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ReportsComponent);
    return ReportsComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/reports/reports.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/reports.module.ts ***!
  \*************************************************************/
/*! exports provided: ReportsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsModule", function() { return ReportsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _reports_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./reports.component */ "./src/app/pages/branch-head/reports/reports.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _profit_and_lost_profit_and_lost_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./profit-and-lost/profit-and-lost.component */ "./src/app/pages/branch-head/reports/profit-and-lost/profit-and-lost.component.ts");
/* harmony import */ var _commissions_commissions_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./commissions/commissions.component */ "./src/app/pages/branch-head/reports/commissions/commissions.component.ts");
/* harmony import */ var _sales_sales_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./sales/sales.component */ "./src/app/pages/branch-head/reports/sales/sales.component.ts");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm5/ngx-bootstrap-tabs.js");
/* harmony import */ var _search_commissions_search_commissions_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./search-commissions/search-commissions.component */ "./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.ts");



















var ROUTE = [
    {
        path: "",
        component: _reports_component__WEBPACK_IMPORTED_MODULE_4__["ReportsComponent"]
    },
    {
        path: "profit-and-loss",
        component: _profit_and_lost_profit_and_lost_component__WEBPACK_IMPORTED_MODULE_14__["ProfitAndLostComponent"]
    },
    {
        path: "commissions",
        component: _commissions_commissions_component__WEBPACK_IMPORTED_MODULE_15__["CommissionsComponent"]
    },
    {
        path: "sales",
        component: _sales_sales_component__WEBPACK_IMPORTED_MODULE_16__["SalesComponent"]
    },
    {
        path: "search-commissions",
        component: _search_commissions_search_commissions_component__WEBPACK_IMPORTED_MODULE_18__["SearchCommissionsComponent"]
    },
];
var ReportsModule = /** @class */ (function () {
    function ReportsModule() {
    }
    ReportsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _reports_component__WEBPACK_IMPORTED_MODULE_4__["ReportsComponent"],
                _profit_and_lost_profit_and_lost_component__WEBPACK_IMPORTED_MODULE_14__["ProfitAndLostComponent"],
                _commissions_commissions_component__WEBPACK_IMPORTED_MODULE_15__["CommissionsComponent"],
                _sales_sales_component__WEBPACK_IMPORTED_MODULE_16__["SalesComponent"],
                _search_commissions_search_commissions_component__WEBPACK_IMPORTED_MODULE_18__["SearchCommissionsComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_13__["BsDatepickerModule"].forRoot(),
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"],
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_17__["TabsModule"].forRoot(),
            ]
        })
    ], ReportsModule);
    return ReportsModule;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/reports/sales/sales.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/sales/sales.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n<div class=\"card\">\n  <div class=\"card-body\">\n\n    <tabset>\n      <tab heading=\"Daily\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-4\">\n            <div class=\"form-group\">\n              <label>Date</label>\n              <input type=\"date\" placeholder=\"Pick a date\" class=\"form-control\" [(ngModel)]=\"dailyDateSelected\" (change)=\"getBranches();getDailySales();\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n        </div>\n        <div *ngFor=\"let data of daily_sales_list;let i = index\">\n          <h5>{{data.branch_name}}</h5>\n          <div class=\"table-responsive \">\n            <table class=\"table table-striped\">\n              <thead>\n                <tr>\n                  <th rowspan=\"2\">Procedure</th>\n                  <th rowspan=\"2\">Quantity</th>\n                  <th rowspan=\"2\">Price</th>\n                  <th rowspan=\"2\">Total Amount</th>\n                  <th style=\"horizontal-align : middle;text-align:center;\" colspan=\"3\">Commissions</th>\n                  <th rowspan=\"2\">Revenue</th>\n                </tr>\n                <tr>\n                  <td scope=\"col\">Dentist</td>\n                  <td scope=\"col\">Assistant</td>\n                  <td scope=\"col\">Secretary</td>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngIf = \"data.details.length==0\">\n                  <td colspan=\"8\" align=\"center\">No record(s) found.</td>\n                </tr>\n                <tr *ngFor=\"let sub of data.details\">\n                  <td>{{ sub.procedure_name }}</td>\n                  <td>{{ sub.qty }}</td>\n                  <td>{{ sub.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                  <td>{{ sub.total_amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                  <td>{{ sub.commission_distributed | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                  <td>{{ sub.assistants_commission_distributed | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                  <td>{{ sub.secretary_commission_distributed | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                  <td><b>{{ sub.company_revenue | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</b></td>\n                </tr>\n              </tbody>\n              <tfoot *ngIf=\"data.details.length!=0\">\n                <tr>\n                  <td colspan=\"8\">\n                    <pagination (pageChanged)=\"pageChange(data.id,$event,i)\"\n                      class=\"justify-content-center\"\n                      [totalItems]=\"data.count\"\n                      [maxSize]=\"10\"\n                      [itemsPerPage]=\"10\" >\n                    </pagination>\n                  </td>\n                </tr>\n              </tfoot>\n            </table>\n          </div>\n          <hr>\n          <div>\n            <div style=\"display: flex;justify-content: left;\">\n              <table width=\"400px\" style=\"padding-left: 20px;\" class=\"hidden-sm-down\">\n                <tr>\n                  <td>Total Amount:</td><td>{{ data.totals.payments | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                </tr>\n                <tr>\n                  <td>Total Expense:</td><td>{{ data.totals.expense | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                </tr>\n                <tr>\n                  <td>Grand Total:</td><td>{{ data.totals.payments -  data.totals.expense | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                </tr>\n              </table>\n\n              <table width=\"400px\" style=\"padding-left: 20px;\" class=\"hidden-sm-down\">\n                <tr *ngFor=\"let payment of data.payments\">\n                  <td>{{ payment.mop }}</td><td>{{ payment.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                </tr>\n              </table>\n            </div>\n\n            <table class=\"table hidden-sm-up\">\n              <tr>\n                <td>Total Amount:</td><td>{{ data.totals.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n              </tr>\n              <tr>\n                <td>Total Dentist Commission:</td><td>{{ data.totals.commission_distributed | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n              </tr>\n              <tr>\n                <td>Total Assistant Commission:</td><td>{{ data.totals.assistants_commission_distributed | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n              </tr>\n              <tr>\n                <td>Total Secretary Commission:</td><td>{{ data.totals.secretary_commission_distributed | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n              </tr>\n              <tr>\n                <td>Total Company Revenue:</td><td><b>{{ data.totals.company_revenue | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</b></td>\n              </tr>\n            </table>\n\n            <table class=\"table hidden-sm-up\">\n              <tr *ngFor=\"let payment of data.payments\">\n                <td>{{ payment.mop }}</td><td>{{ payment.amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n              </tr>\n            </table>\n          </div>\n          <br><br>\n        </div>\n      </tab>\n    </tabset>\n\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/sales/sales.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/sales/sales.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3JlcG9ydHMvc2FsZXMvc2FsZXMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/sales/sales.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/sales/sales.component.ts ***!
  \********************************************************************/
/*! exports provided: SalesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesComponent", function() { return SalesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");





var SalesComponent = /** @class */ (function () {
    function SalesComponent(API, common) {
        this.API = API;
        this.common = common;
        this.pageTitle = "Sales";
        this.widgetSubTitle = "Sales Report";
        this.daily_sales_list = [];
        this.monthly_sales_list = [];
        this.annual_sales_list = [];
        this.branches_list = [];
        this.userLoggedInfo = this.common.getLoggedInUser();
        this.branch = this.userLoggedInfo.branch;
    }
    SalesComponent.prototype.ngOnInit = function () {
        this.getBranches();
        this.dailyDateSelected = moment__WEBPACK_IMPORTED_MODULE_3__().format("YYYY-MM-DD");
        this.getDailySales();
        // this.getMonthlySales()
        // this.getAnnualSales()
    };
    SalesComponent.prototype.getBranches = function () {
        var _this = this;
        this.API.post("lookups/get-branch-lookup", {}).subscribe(function (response) {
            _this.branches_list = response.devMessage;
            for (var i = 0; i < _this.branches_list.length; i++) {
                _this.declareVar(_this.branches_list[i].id);
            }
        }, function (error) {
        });
    };
    SalesComponent.prototype.declareVar = function (varname) {
        this[varname + "__page"] = 1;
        this[varname + "__rows"] = 10;
        this["monthly__" + varname + "__page"] = 1;
        this["monthly__" + varname + "__rows"] = 10;
        this["annual__" + varname + "__page"] = 1;
        this["annual__" + varname + "__rows"] = 10;
    };
    SalesComponent.prototype.getDailySales = function () {
        var _this = this;
        this.API.post("reports/get-daily-sales", {
            date: this.dailyDateSelected,
            branch: this.branch
        }).subscribe(function (response) {
            _this.daily_sales_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.getMonthlySales = function () {
        var _this = this;
        this.API.post("reports/get-montly-sales", {}).subscribe(function (response) {
            _this.monthly_sales_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.getAnnualSales = function () {
        var _this = this;
        this.API.post("reports/get-annual-sales", {}).subscribe(function (response) {
            _this.annual_sales_list = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.pageChange = function (branchId, event, index) {
        this[branchId + "__page"] = event.page;
        this.getDailySalesDetailsByPageChange(this[branchId + "__page"], branchId, index);
    };
    SalesComponent.prototype.getDailySalesDetailsByPageChange = function (page, branchId, index) {
        var _this = this;
        this.API.post("reports/get-daily-sales-details-by-page-change", {
            date: this.dailyDateSelected,
            page: page,
            branch: branchId
        }).subscribe(function (response) {
            _this.daily_sales_list[index].details = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.pageChangeMonthly = function (branchId, event, index) {
        this["monthly__" + branchId + "__page"] = event.page;
        this.getMonthlySalesDetailsByPageChange(this["monthly__" + branchId + "__page"], branchId, index);
    };
    SalesComponent.prototype.getMonthlySalesDetailsByPageChange = function (page, branchId, index) {
        var _this = this;
        this.API.post("reports/get-monthly-sales-details-by-page-change", {
            page: page,
            branch: branchId
        }).subscribe(function (response) {
            _this.monthly_sales_list[index].details = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent.prototype.pageChangeAnnual = function (branchId, event, index) {
        this["annual__" + branchId + "__page"] = event.page;
        this.getAnnualSalesDetailsByPageChange(this["annual__" + branchId + "__page"], branchId, index);
    };
    SalesComponent.prototype.getAnnualSalesDetailsByPageChange = function (page, branchId, index) {
        var _this = this;
        this.API.post("reports/get-annual-sales-details-by-page-change", {
            page: page,
            branch: branchId
        }).subscribe(function (response) {
            _this.annual_sales_list[index].details = response.devMessage;
        }, function (error) {
        });
    };
    SalesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sales',
            template: __webpack_require__(/*! ./sales.component.html */ "./src/app/pages/branch-head/reports/sales/sales.component.html"),
            styles: [__webpack_require__(/*! ./sales.component.scss */ "./src/app/pages/branch-head/reports/sales/sales.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"]])
    ], SalesComponent);
    return SalesComponent;
}());



/***/ }),

/***/ "./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\n  <h1>{{ pageTitle }}</h1>\n  <small>{{ widgetSubTitle }}</small>\n</header>\n\n<div class=\"card\">\n  <div class=\"card-body\">\n    <tabset>\n      <tab heading=\"Dentists\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-3\">\n            <div class=\"form-group\">\n              <label>Dentist</label>\n              <ng-select\n                [items]=\"doctors_list\"\n                bindLabel=\"fullname\"\n                bindValue=\"id\"\n                groupBy=\"branch_name\"\n                [multiple]=\"true\"\n                dropdownPosition=\"bottom\"\n                placeholder=\"Select a Dentist\"\n                [(ngModel)]=\"doctors\"\n                >\n              </ng-select>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\">\n              <label>From</label>\n              <input type=\"text\" placeholder=\"Pick a date\" class=\"form-control\" bsDatepicker [bsConfig]=\"bsConfig\" [(ngModel)]=\"from\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\">\n              <label>To</label>\n              <input type=\"text\" placeholder=\"Pick a date\" class=\"form-control\" bsDatepicker [bsConfig]=\"bsConfig\" [(ngModel)]=\"to\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\" style=\"padding-top:20px\">\n              <button type=\"button\" class=\"btn btn-primary\" (click)=\"getDoctorCommissionsSearch()\"><i class=\"zmdi zmdi-search\"></i> Search</button>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"table-responsive\">\n          <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th>Dentist</th>\n                <th>Patient</th>\n                <th>Procedure</th>\n                <th>Total Amount</th>\n                <th>Dentist Commission</th>\n                <th>Covered Date</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngIf=\"dentist_commission_list.length==0\">\n                <td colspan=\"6\" align=\"center\">No record(s) found.</td>\n              </tr>\n              <tr *ngFor=\"let dataDentist of dentist_commission_list\">\n                <td>{{ dataDentist.dentist}}</td>\n                <td>{{ dataDentist.patient}}</td>\n                <td>{{ dataDentist.procedure_name}}</td>\n                <td>{{ dataDentist.total_amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</td>\n                <td>{{ dataDentist.commission | currency: 'PHP' : 'symbol-narrow' : '1.2-2'}}</td>\n                <td>{{ dataDentist.covered_date}}</td>\n              </tr>\n            </tbody>\n            <tfoot *ngIf=\"dentist_commission_list.length!=0\">\n              <tr>\n                <td colspan=\"6\">\n                  <pagination (pageChanged)=\"pageChangeDentist($event)\"\n                    class=\"justify-content-center\"\n                    [totalItems]=\"dentist_commission_total\"\n                    [maxSize]=\"10\"\n                    [itemsPerPage]=\"10\" >\n                  </pagination>\n                </td>\n              </tr>\n            </tfoot>\n          </table>\n        </div>\n\n      </tab>\n      <tab heading=\"Assistants\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-3\">\n            <div class=\"form-group\">\n              <label>Assistants</label>\n              <ng-select\n                [items]=\"assistants_list\"\n                bindLabel=\"fullname\"\n                bindValue=\"id\"\n                groupBy=\"branch_name\"\n                [multiple]=\"true\"\n                dropdownPosition=\"bottom\"\n                placeholder=\"Select Assistants\"\n                [(ngModel)]=\"assistants\"\n                >\n              </ng-select>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\">\n              <label>From</label>\n              <input type=\"text\" placeholder=\"Pick a date\" class=\"form-control\" bsDatepicker [bsConfig]=\"bsConfig\" [(ngModel)]=\"assistantDateFrom\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\">\n              <label>To</label>\n              <input type=\"text\" placeholder=\"Pick a date\" class=\"form-control\" bsDatepicker [bsConfig]=\"bsConfig\" [(ngModel)]=\"assistantDateTo\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\" style=\"padding-top:20px\">\n              <button type=\"button\" class=\"btn btn-primary\" (click)=\"getAssistantsCommissionsSearch()\"><i class=\"zmdi zmdi-search\"></i> Search</button>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"table-responsive\">\n          <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th>Assistant</th>\n                <th>Patient</th>\n                <th>Procedure</th>\n                <th>Total Amount</th>\n                <th>Assistant Commission</th>\n                <th>Covered Date</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngIf=\"assistant_commission_list.length==0\">\n                <td colspan=\"6\" align=\"center\">No record(s) found.</td>\n              </tr>\n              <tr *ngFor = \"let dataAssist of assistant_commission_list\">\n                <td>{{ dataAssist.assistant }}</td>\n                <td>{{ dataAssist.patient }}</td>\n                <td>{{ dataAssist.procedure_name }}</td>\n                <td>{{ dataAssist.total_amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                <td>{{ dataAssist.commission | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                <td>{{ dataAssist.covered_date }}</td>\n              </tr>\n            </tbody>\n            <tfoot *ngIf=\"assistant_commission_list.length!=0\">\n              <tr>\n                <td colspan=\"6\">\n                  <pagination (pageChanged)=\"pageChangeAssistant($event)\"\n                    class=\"justify-content-center\"\n                    [totalItems]=\"assistant_commission_total\"\n                    [maxSize]=\"10\"\n                    [itemsPerPage]=\"10\" >\n                  </pagination>\n                </td>\n              </tr>\n            </tfoot>\n          </table>\n        </div>\n      </tab>\n      <tab heading=\"Secretaries\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 col-md-3\">\n            <div class=\"form-group\">\n              <label>Secretary</label>\n              <ng-select\n                [items]=\"secretaries_list\"\n                bindLabel=\"fullname\"\n                bindValue=\"id\"\n                groupBy=\"branch_name\"\n                [multiple]=\"true\"\n                dropdownPosition=\"bottom\"\n                placeholder=\"Select Secretaries\"\n                [(ngModel)]=\"secretaries\"\n                >\n              </ng-select>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\">\n              <label>From</label>\n              <input type=\"text\" placeholder=\"Pick a date\" class=\"form-control\" bsDatepicker [bsConfig]=\"bsConfig\" [(ngModel)]=\"secretaryDateFrom\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\">\n              <label>To</label>\n              <input type=\"text\" placeholder=\"Pick a date\" class=\"form-control\" bsDatepicker [bsConfig]=\"bsConfig\" [(ngModel)]=\"secretaryDateTo\">\n              <i class=\"form-group__bar\"></i>\n            </div>\n          </div>\n          <div class=\"col-sm-12 col-md-2\">\n            <div class=\"form-group\" style=\"padding-top:20px\">\n              <button type=\"button\" class=\"btn btn-primary\" (click)=\"getSecretaryCommissionsSearch()\"><i class=\"zmdi zmdi-search\"></i> Search</button>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"table-responsive\">\n          <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th>Secretary</th>\n                <th>Patient</th>\n                <th>Procedure</th>\n                <th>Total Amount</th>\n                <th>Secretary Commission</th>\n                <th>Covered Date</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngIf=\"secretary_commission_list.length==0\">\n                <td colspan=\"6\" align=\"center\">No record(s) found.</td>\n              </tr>\n              <tr *ngFor = \"let dataSecretary of secretary_commission_list\">\n                <td>{{ dataSecretary.secretary }}</td>\n                <td>{{ dataSecretary.patient }}</td>\n                <td>{{ dataSecretary.procedure_name }}</td>\n                <td>{{ dataSecretary.total_amount | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                <td>{{ dataSecretary.commission | currency: 'PHP' : 'symbol-narrow' : '1.2-2' }}</td>\n                <td>{{ dataSecretary.covered_date }}</td>\n              </tr>\n            </tbody>\n            <tfoot *ngIf=\"secretary_commission_list.length!=0\">\n              <tr>\n                <td colspan=\"6\">\n                  <pagination (pageChanged)=\"pageChangeSecretary($event)\"\n                    class=\"justify-content-center\"\n                    [totalItems]=\"secretary_commission_total\"\n                    [maxSize]=\"10\"\n                    [itemsPerPage]=\"10\" >\n                  </pagination>\n                </td>\n              </tr>\n            </tfoot>\n          </table>\n        </div>\n      </tab>\n    </tabset>\n\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JyYW5jaC1oZWFkL3JlcG9ydHMvc2VhcmNoLWNvbW1pc3Npb25zL3NlYXJjaC1jb21taXNzaW9ucy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: SearchCommissionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchCommissionsComponent", function() { return SearchCommissionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");



var SearchCommissionsComponent = /** @class */ (function () {
    function SearchCommissionsComponent(API) {
        this.API = API;
        this.pageTitle = "Search Commissions";
        this.widgetSubTitle = "Filtering for Commissions";
        //dentist
        this.doctors_list = [];
        this.doctors = [];
        this.pageDoctor = 1;
        this.rowsDoctor = 10;
        this.dentist_commission_list = [];
        this.dentist_commission_total = 0;
        this.assistants_list = [];
        this.assistants = [];
        this.pageAssist = 1;
        this.rowsAssist = 10;
        this.assistant_commission_list = [];
        this.assistant_commission_total = 0;
        this.secretaries_list = [];
        this.secretaries = [];
        this.pageSecretary = 1;
        this.rowsSecretary = 10;
        this.secretary_commission_list = [];
        this.secretary_commission_total = 0;
        this.from = new Date();
        this.to = new Date();
        this.assistantDateFrom = new Date();
        this.assistantDateTo = new Date();
        this.secretaryDateFrom = new Date();
        this.secretaryDateTo = new Date();
    }
    //DENTIST
    SearchCommissionsComponent.prototype.ngOnInit = function () {
        this.getDoctorList();
        this.getAssistantsList();
        this.getSecretariesList();
        this.getDoctorCommissionsSearch();
        this.getAssistantsCommissionsSearch();
        this.getSecretaryCommissionsSearch();
    };
    SearchCommissionsComponent.prototype.getDoctorCommissionsSearch = function () {
        var _this = this;
        this.API.post("reports/get-commissions-search", {
            doctors: this.doctors,
            from: this.from,
            to: this.to,
            page: this.pageDoctor,
            rows: this.rowsDoctor
        }).subscribe(function (response) {
            _this.dentist_commission_list = response.devMessage;
            _this.dentist_commission_total = response.total;
        }, function (error) {
        });
    };
    SearchCommissionsComponent.prototype.getDoctorList = function () {
        var _this = this;
        this.API.post("lookups/get-doctors", {}).subscribe(function (response) {
            _this.doctors_list = response.devMessage;
        }, function (error) {
        });
    };
    SearchCommissionsComponent.prototype.pageChangeDentist = function (event) {
        this.pageDoctor = event.page;
        this.getDoctorCommissionsSearch();
    };
    //END DENTIST
    //ASSISTANTS
    SearchCommissionsComponent.prototype.getAssistantsList = function () {
        var _this = this;
        this.API.post("lookups/get-assistants", {}).subscribe(function (response) {
            _this.assistants_list = response.devMessage;
        }, function (error) {
        });
    };
    SearchCommissionsComponent.prototype.getAssistantsCommissionsSearch = function () {
        var _this = this;
        this.API.post("reports/get-assistants-commissions-search", {
            assistants: this.assistants,
            from: this.assistantDateFrom,
            to: this.assistantDateTo,
            page: this.pageAssist,
            rows: this.rowsAssist
        }).subscribe(function (response) {
            _this.assistant_commission_list = response.devMessage;
            _this.assistant_commission_total = response.total;
        }, function (error) {
        });
    };
    SearchCommissionsComponent.prototype.pageChangeAssistant = function (event) {
        this.pageAssist = event.page;
        this.getAssistantsCommissionsSearch();
    };
    //END ASSISTANTS
    //SECRETARY
    SearchCommissionsComponent.prototype.getSecretariesList = function () {
        var _this = this;
        this.API.post("lookups/get-secretaries", {}).subscribe(function (response) {
            _this.secretaries_list = response.devMessage;
        }, function (error) {
        });
    };
    SearchCommissionsComponent.prototype.getSecretaryCommissionsSearch = function () {
        var _this = this;
        this.API.post("reports/get-secretary-commissions-search", {
            secretaries: this.secretaries,
            from: this.secretaryDateFrom,
            to: this.secretaryDateTo,
            page: this.pageSecretary,
            rows: this.rowsSecretary
        }).subscribe(function (response) {
            _this.secretary_commission_list = response.devMessage;
            _this.secretary_commission_total = response.total;
        }, function (error) {
        });
    };
    SearchCommissionsComponent.prototype.pageChangeSecretary = function (event) {
        this.pageSecretary = event.page;
        this.getSecretaryCommissionsSearch();
    };
    SearchCommissionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-search-commissions',
            template: __webpack_require__(/*! ./search-commissions.component.html */ "./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.html"),
            styles: [__webpack_require__(/*! ./search-commissions.component.scss */ "./src/app/pages/branch-head/reports/search-commissions/search-commissions.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], SearchCommissionsComponent);
    return SearchCommissionsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=pages-branch-head-reports-reports-module.js.map