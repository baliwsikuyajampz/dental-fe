(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-settings-items-items-module"],{

/***/ "./src/app/pages/admin/settings/items/items.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/settings/items/items.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"content__title\">\r\n  <h1>{{ pageTitle }}</h1>\r\n  <small>{{ widgetSubTitle }}</small>\r\n\r\n  <div class=\"actions actions\">\r\n\r\n    <div class=\"btn-demo\">\r\n      <button class=\"btn btn-primary btn--icon-text\" (click)=\"addItem()\"><i class=\"zmdi zmdi-plus\"></i> Add New</button>\r\n      <button class=\"btn btn-success btn--icon-text\"  (click)=\"getItems()\"><i class=\"zmdi zmdi-refresh\"></i> Refresh</button>\r\n    </div>\r\n  </div>\r\n</header>\r\n\r\n\r\n<div class=\"card d-none d-sm-block\">\r\n  <div class=\"card-body\">\r\n    <div class=\"table-responsive \">\r\n      <table class=\"table table-striped\">\r\n        <thead class=\"thead-dark\">\r\n          <tr>\r\n            <th></th>\r\n            <th></th>\r\n            <th>Item Name</th>\r\n            <th>Description</th>\r\n            <th>Category</th>\r\n            <th>Date Created</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let data of items_list\">\r\n            <td>\r\n              <div class=\"btn-demo\">\r\n                <button type=\"button\" class=\"btn btn-success w-100\" (click)=\"editItem(data)\"><i class=\"zmdi zmdi-edit\"></i> Edit</button>\r\n              </div>\r\n            </td>\r\n            <td>\r\n              <a [href]=\"data.attachment\" target=\"_blank\"  *ngIf=\"data.attachment!=''\">\r\n                <img [src]=\"data.attachment\" style=\"height:90px;width: 100px;\">\r\n              </a>\r\n            </td>\r\n            <td>{{ data.item_name }}</td>\r\n            <td>{{ data.item_description }}</td>\r\n            <td>{{ data.category_name }}</td>\r\n\r\n\r\n            <td>{{ data.date_created }}</td>\r\n\r\n          </tr>\r\n        </tbody>\r\n        <tfoot *ngIf=\"items_list.length!=0\">\r\n          <tr>\r\n            <td colspan=\"5\">\r\n              <pagination (pageChanged)=\"pageChange($event)\"\r\n                [totalItems]=\"items_count\"\r\n                [maxSize]=\"10\"\r\n                [itemsPerPage]=\"rows\" >\r\n              </pagination>\r\n            </td>\r\n            <td align=\"right\">\r\n              {{((page-1)*rows)+1 | number : '1.0-0'}} of {{items_count | number : '1.0-0'}}\r\n            </td>\r\n          </tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n\r\n<div class=\"card text-center d-block d-sm-none\" *ngFor=\"let data of items_list\">\r\n  <div class=\"card-header\">\r\n    <a [href]=\"data.attachment\" target=\"_blank\" *ngIf=\"data.attachment!=''\" >\r\n      <img [src]=\"data.attachment\" style=\"height:100px;width: 100px;\" *ngIf=\"data.attachment!=''\">\r\n    </a>\r\n  </div>\r\n\r\n  <div class=\"card-body\">\r\n    <h5 class=\"card-title\">{{data.item_name}}</h5>\r\n    <strong>{{data.category_name}}</strong>\r\n    <p class=\"card-text\">{{data.item_description}}</p>\r\n    \r\n  </div>\r\n  <div class=\"card-footer\">\r\n    <div class=\"btn-demo\">\r\n      <button type=\"button\" class=\"btn btn-success w-100\" (click)=\"editItem(data)\"><i class=\"zmdi zmdi-edit\"></i> Edit</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" bsModal #modalItem=\"bs-modal\" [config]=\"{ backdrop: 'static' }\" data-keyboard=\"false\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\r\n  <div class=\"modal-dialog  modal-dialog-centered\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5>{{mode == 'add' ? 'Add Item' : 'Edit Item'}}</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Item Name <span style=\"color:red\">*</span></label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <input name=\"input_fight_no\" type=\"text\" class=\"form-control\" [(ngModel)]=\"item_name\" placeholder=\"\">\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Description</label>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n              <div class=\"form-group\">\r\n                  <textarea  class=\"form-control\" [(ngModel)]=\"item_description\"></textarea>\r\n                  <i class=\"form-group__bar\"></i>\r\n              </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 col-md-3\">\r\n            <label>Category <span style=\"color:red\">*</span></label>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-9\">\r\n            <div class=\"form-group\">\r\n              <ng-select\r\n                [items]=\"categories_list\"\r\n                bindLabel=\"category_name\"\r\n                placeholder=\"Select a Category\"\r\n                [(ngModel)]=\"selected_category\">\r\n              </ng-select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\">Attachment</label>\r\n\r\n          <div class=\"col-sm-12 col-md-9\">\r\n            <!-- <button class=\"btn btn-primary\" (click)=\"uploadClickItems()\">UPLOAD ATTACHMENT</button> -->\r\n            <input #itemAttachment class=\"form-control\" type=\"file\" id=\"uploadItemAttachment\"\r\n            (change)=\"handleFileUploadItem($event)\" accept=\"image/png, image/gif, image/jpeg\">\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"row\">\r\n          <label class=\"col-sm-12 col-md-3 col-form-label\"></label>\r\n\r\n          <div class=\"col-sm-12 col-md-9\" style=\"margin-top:10px\">\r\n            <div class=\"checkbox\">\r\n                <input type=\"checkbox\" id=\"customCheck1\" [(ngModel)]=\"is_active\">\r\n                <label class=\"checkbox__label\" for=\"customCheck1\">Is Active</label>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveItem()\"><i class=\"zmdi zmdi-save\"></i> {{mode == 'add' ? 'SAVE' : 'UPDATE'}}</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"modalItem.hide()\"><i class=\"zmdi zmdi-close\"></i> CANCEL</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/admin/settings/items/items.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/settings/items/items.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hidden {\n  visibility: hidden; }\n\n.btn-demo > .btn,\n.btn-demo > .btn-group {\n  margin: 0 5px 10px 0; }\n\nimg {\n  border-radius: 50%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3MvaXRlbXMvQzpcXFVzZXJzXFxBRE1JTlxcRGVza3RvcFxcUHJvamVjdHNcXGRlbnRhbC1mZS9zcmNcXGFwcFxccGFnZXNcXGFkbWluXFxzZXR0aW5nc1xcaXRlbXNcXGl0ZW1zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQWtCLEVBQUE7O0FBR3RCOztFQUdNLG9CQUFvQixFQUFBOztBQUkxQjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWRtaW4vc2V0dGluZ3MvaXRlbXMvaXRlbXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGlkZGVuIHtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxufVxyXG5cclxuLmJ0bi1kZW1vIHtcclxuICAgICYgPiAuYnRuLFxyXG4gICAgJiA+IC5idG4tZ3JvdXAge1xyXG4gICAgICBtYXJnaW46IDAgNXB4IDEwcHggMDtcclxuICAgIH1cclxuICB9XHJcblxyXG5pbWcge1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/admin/settings/items/items.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/admin/settings/items/items.component.ts ***!
  \***************************************************************/
/*! exports provided: ItemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsComponent", function() { return ItemsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);





var ItemsComponent = /** @class */ (function () {
    function ItemsComponent(API) {
        this.API = API;
        this.pageTitle = "Items";
        this.widgetSubTitle = "Settings for Item Information";
        this.page = 1;
        this.rows = 10;
        this.mode = "add";
        this.items_list = [];
        this.items_count = 0;
        //form
        this.item_name = "";
        this.item_description = "";
        this.itemFormData = new FormData();
        this.itemFormDataFlag = false;
        this.is_active = true;
        this.categories_list = [];
        this.selected_category = null;
    }
    ItemsComponent.prototype.ngOnInit = function () {
        this.getItems();
        this.getCategories();
    };
    ItemsComponent.prototype.addItem = function () {
        this.modalItem.show();
        this.itemFormDataFlag = false;
    };
    ItemsComponent.prototype.uploadClickItems = function () {
        document.getElementById("uploadItemAttachment").click();
    };
    ItemsComponent.prototype.handleFileUploadItem = function (event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var file, formData;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                file = event.target.files[0], formData = new FormData();
                this.itemFormData = new FormData();
                this.itemFormData.append('file', file);
                this.itemFormDataFlag = true;
                return [2 /*return*/];
            });
        });
    };
    ItemsComponent.prototype.saveItem = function () {
        var _this = this;
        console.log(this.is_active);
        var key = this.editItemData === undefined ? "" : this.editItemData.id;
        if (this.item_name == "" || this.selected_category == null) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Required Field(s) Missing", "warning");
        }
        else {
            if (this.itemFormDataFlag == true) {
                this.API.post_form_data("uploads/upload-attachment", this.itemFormData).subscribe(function (response) {
                    if (response.statusCode == 200) {
                        _this.API.post("settings/save-item", {
                            item_name: _this.item_name,
                            item_description: _this.item_description,
                            attachment: response.devMessage,
                            category_id: _this.selected_category.id,
                            mode: _this.mode,
                            key: key,
                            is_active: _this.is_active ? "1" : "0"
                        }).subscribe(function (response) {
                            console.log(response);
                            if (response.statusCode == 200) {
                                _this.clearFields();
                                _this.getItems();
                                _this.modalItem.hide();
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Success", _this.mode == 'add' ? "Record has been Saved" : "Record has been Updated", "success");
                            }
                        }, function (error) {
                        });
                    }
                }, function (error) {
                });
            }
            else {
                this.API.post("settings/save-item", {
                    item_name: this.item_name,
                    item_description: this.item_description,
                    attachment: "",
                    category_id: this.selected_category.id,
                    mode: this.mode,
                    key: key,
                    is_active: this.is_active ? "1" : "0"
                }).subscribe(function (response) {
                    console.log(response);
                    if (response.statusCode == 200) {
                        _this.clearFields();
                        _this.getItems();
                        _this.modalItem.hide();
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Success", _this.mode == 'add' ? "Record has been Saved" : "Record has been Updated", "success");
                    }
                }, function (error) {
                });
            }
        }
    };
    ItemsComponent.prototype.getItems = function () {
        var _this = this;
        this.API.post("settings/get-items", {
            page: this.page,
            rows: this.rows
        }).subscribe(function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                _this.items_list = response.devMessage;
                _this.items_count = response.total;
            }
        }, function (error) {
        });
    };
    ItemsComponent.prototype.clearFields = function () {
        this.item_name = "";
        this.item_description = "";
        this.itemFormDataFlag = false;
        this.itemAttachment.nativeElement.value = '';
    };
    ItemsComponent.prototype.pageChange = function (event) {
        this.page = event.page;
        this.getItems();
    };
    ItemsComponent.prototype.getCategories = function () {
        var _this = this;
        this.API.post("settings/get-categories-list", {}).subscribe(function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                _this.categories_list = response.devMessage;
            }
        }, function (error) {
        });
    };
    ItemsComponent.prototype.editItem = function (data) {
        this.mode = "edit";
        this.editItemData = data;
        this.item_name = data.item_name;
        this.item_description = data.item_description;
        this.is_active = data.is_active;
        var index = this.categories_list.findIndex(function (e) { return data.category_id == e.id; });
        this.selected_category = this.categories_list[index];
        this.modalItem.show();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modalItem"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalDirective"])
    ], ItemsComponent.prototype, "modalItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('itemAttachment'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ItemsComponent.prototype, "itemAttachment", void 0);
    ItemsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-items',
            template: __webpack_require__(/*! ./items.component.html */ "./src/app/pages/admin/settings/items/items.component.html"),
            styles: [__webpack_require__(/*! ./items.component.scss */ "./src/app/pages/admin/settings/items/items.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], ItemsComponent);
    return ItemsComponent;
}());



/***/ }),

/***/ "./src/app/pages/admin/settings/items/items.module.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/admin/settings/items/items.module.ts ***!
  \************************************************************/
/*! exports provided: ItemsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsModule", function() { return ItemsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _items_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./items.component */ "./src/app/pages/admin/settings/items/items.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/fesm5/ngx-quill.js");













var ROUTE = [{ path: "", component: _items_component__WEBPACK_IMPORTED_MODULE_4__["ItemsComponent"] }];
var ItemsModule = /** @class */ (function () {
    function ItemsModule() {
    }
    ItemsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _items_component__WEBPACK_IMPORTED_MODULE_4__["ItemsComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTE),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_8__["NgxMaskModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_9__["PaginationModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_12__["QuillModule"]
            ]
        })
    ], ItemsModule);
    return ItemsModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-admin-settings-items-items-module.js.map