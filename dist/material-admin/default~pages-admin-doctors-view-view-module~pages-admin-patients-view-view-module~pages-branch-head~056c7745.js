(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-admin-doctors-view-view-module~pages-admin-patients-view-view-module~pages-branch-head~056c7745"],{

/***/ "./node_modules/@crystalui/angular-lightbox/fesm5/crystalui-angular-lightbox.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/@crystalui/angular-lightbox/fesm5/crystalui-angular-lightbox.js ***!
  \**************************************************************************************/
/*! exports provided: CrystalLightboxModule, EventService, LightboxDirective, LightboxGroupDirective, ɵa, ɵb, ɵc */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrystalLightboxModule", function() { return CrystalLightboxModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventService", function() { return EventService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LightboxDirective", function() { return LightboxDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LightboxGroupDirective", function() { return LightboxGroupDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return LightboxCommonComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return LightboxComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return CrystalLightbox; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");





var EventService = /** @class */ (function () {
    function EventService() {
        this.emitter = new rxjs__WEBPACK_IMPORTED_MODULE_3__["ReplaySubject"](1);
    }
    EventService.prototype.emitChangeEvent = function (data) {
        this.emitter.next(data);
    };
    EventService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], EventService);
    return EventService;
}());

var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.mobileCheck = function () {
        var check = false;
        (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true; })(navigator.userAgent || navigator.vendor);
        return check;
    };
    ;
    return Utils;
}());

var LightboxCommonComponent = /** @class */ (function () {
    function LightboxCommonComponent(eventService) {
        this.eventService = eventService;
        this.currentImageIndex = 0;
        this.indexCurrentSlide = 1;
        this.containerStyles = {
            transition: '',
            transform: '',
            width: '',
            height: '',
            opacity: ''
        };
        this.currImageLoadingState = 'not-loaded';
        this.isMobile = Utils.mobileCheck();
    }
    Object.defineProperty(LightboxCommonComponent.prototype, "lightboxImage", {
        get: function () {
            return this._lightboxImage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "lightboxImageElement", {
        get: function () {
            if (this.lightboxImage) {
                return this.lightboxImage.nativeElement;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "lightboxImageNaturalHeight", {
        get: function () {
            if (this.lightboxImageElement) {
                return this.lightboxImageElement.naturalHeight;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "lightboxImageNaturalWidth", {
        get: function () {
            if (this.lightboxImageElement) {
                return this.lightboxImageElement.naturalWidth;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "index", {
        get: function () {
            return this.currentImageIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "properties", {
        get: function () {
            return this.lightboxData.properties;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "images", {
        get: function () {
            return this.lightboxData.images || [this.lightboxData.image];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "thumbnailImage", {
        get: function () {
            return this.images[this.currentImageIndex].nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "thumbnailImagePosition", {
        get: function () {
            return this.thumbnailImage.getBoundingClientRect();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "virtualImageDimension", {
        // Image size if it is larger than the window size
        get: function () {
            var height = this.lightboxImageNaturalHeight;
            var width = height * this.imageAspectRatio;
            var windowWidth = document.body.clientWidth;
            var windowHeight = window.innerHeight;
            if (this.isImageLargerWindow) {
                if (height > windowHeight) {
                    height = windowHeight;
                    width = height * this.imageAspectRatio;
                }
                if (width > windowWidth) {
                    width = windowWidth;
                    height = width / this.imageAspectRatio;
                }
            }
            else {
                width = this.lightboxImageNaturalWidth;
                height = this.lightboxImageNaturalHeight;
            }
            if (width === 0 || Number.parseInt(height) === 0) {
                return { width: 200, height: 200 };
            }
            return { width: width, height: height };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "containerInitialPosition", {
        get: function () {
            var scale = (this.showState === 'initial-thumbnail-image') ? 1 : this.containerScale;
            var top = this.thumbnailImagePosition.top;
            var left = this.thumbnailImagePosition.left;
            return 'matrix(' + scale + ', 0, 0, ' + scale + ',' + left + ',' + top + ')';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "containerFullscreenPosition", {
        get: function () {
            var left = (document.body.clientWidth - this.virtualImageDimension.width) / 2;
            var top = (window.innerHeight - this.virtualImageDimension.height) / 2;
            return 'translate3d(' + left + 'px, ' + top + 'px, 0)';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "containerScale", {
        get: function () {
            return this.thumbnailImagePosition.width / this.virtualImageDimension.width;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "imageAspectRatio", {
        get: function () {
            return this.thumbnailImage.naturalWidth / this.thumbnailImage.naturalHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "isImageLargerWindow", {
        get: function () {
            var imageNaturalWidth = this.lightboxImageNaturalWidth;
            var imageNaturalHeight = this.lightboxImageNaturalHeight;
            var windowWidth = document.body.clientWidth;
            var windowHeight = window.innerHeight;
            return imageNaturalWidth > windowWidth || imageNaturalHeight > windowHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "isFirstImage", {
        get: function () {
            if (this.properties.loop) {
                return false;
            }
            else {
                return this.index === 0;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "isLastImage", {
        get: function () {
            if (this.properties.loop) {
                return false;
            }
            else {
                return this.index === this.latestImageIndex;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "latestImageIndex", {
        get: function () {
            return this.images.length - 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "backgroundColor", {
        get: function () {
            var opacity = this.properties.backgroundOpacity;
            var color = this.properties.backgroundColor;
            if (color === 'black') {
                return 'rgba(0, 0, 0, ' + opacity + ')';
            }
            else {
                return 'rgba(255, 255, 255, ' + opacity + ')';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "animationDuration", {
        get: function () {
            var animationDuration = this.properties.animationDuration;
            if (typeof animationDuration === "string") {
                return Number.parseInt(animationDuration);
            }
            else {
                return animationDuration;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "animationMode", {
        get: function () {
            if (this.currImageLoadingState === 'error') {
                return 'default';
            }
            return this.properties.animationMode;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "animationTimingFunction", {
        get: function () {
            return this.properties.animationTimingFunction;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "closeButtonText", {
        get: function () {
            return this.properties.closeButtonText;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "counterSeparator", {
        get: function () {
            return this.properties.counterSeparator;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxCommonComponent.prototype, "counter", {
        get: function () {
            return this.currentImageIndex + 1 + this.counterSeparator + this.images.length;
        },
        enumerable: true,
        configurable: true
    });
    LightboxCommonComponent.prototype.emitState = function (type, state) {
        if (state === 'initial-virtual-image' ||
            state === 'initial-styles') {
            return;
        }
        if (state === 'initial-default' ||
            state === 'initial-thumbnail-image') {
            state = 'initial';
        }
        this.eventService.emitChangeEvent({
            type: type + ':' + state
        });
    };
    LightboxCommonComponent.prototype.setShowState = function (state) {
        this.showState = state;
        this.emitState('show-state', state);
    };
    LightboxCommonComponent.prototype.setClosingState = function (state) {
        this.closingState = state;
        this.emitState('closing-state', state);
    };
    LightboxCommonComponent.prototype.setAnimationDuration = function () {
        this.hostStyleTransition = 'background-color ' + this.animationDuration + 'ms';
        this.containerStyles.transition = 'all ' + this.animationDuration + 'ms ' + this.animationTimingFunction;
    };
    LightboxCommonComponent.prototype.setBackgroundColor = function () {
        this.hostStyleBackgroundColor = this.backgroundColor;
    };
    LightboxCommonComponent.prototype.getContainerHeight = function () {
        return this.thumbnailImagePosition.height / this.containerScale + 'px';
    };
    LightboxCommonComponent.prototype.showThumbnailImage = function () {
        this.thumbnailImage.style.opacity = '';
    };
    LightboxCommonComponent.prototype.hideThumbnailImage = function () {
        this.thumbnailImage.style.opacity = 0;
    };
    LightboxCommonComponent.prototype.updateThumbnailPosition = function () {
        this.containerStyles.transform = this.containerInitialPosition;
    };
    LightboxCommonComponent.ctorParameters = function () { return [
        { type: EventService }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxCommonComponent.prototype, "lightboxData", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('style.backgroundColor')
    ], LightboxCommonComponent.prototype, "hostStyleBackgroundColor", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('style.transition')
    ], LightboxCommonComponent.prototype, "hostStyleTransition", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('imageFirst', { static: true })
    ], LightboxCommonComponent.prototype, "_imageFirst", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('imageSecond', { static: true })
    ], LightboxCommonComponent.prototype, "_imageSecond", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('imageLast', { static: true })
    ], LightboxCommonComponent.prototype, "_imageLast", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('lightboxImage', { static: true })
    ], LightboxCommonComponent.prototype, "_lightboxImage", void 0);
    LightboxCommonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'lightbox-common',
            template: ''
        })
    ], LightboxCommonComponent);
    return LightboxCommonComponent;
}());

var LightboxComponent = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(LightboxComponent, _super);
    function LightboxComponent(elementRef, ref, eventService) {
        var _this = _super.call(this, eventService) || this;
        _this.elementRef = elementRef;
        _this.ref = ref;
        _this.eventService = eventService;
        _this.spinnerHeight = 30;
        _this.minTimeout = 30;
        _this.preloaderTimeout = 100;
        _this.spinnerStyles = {
            transform: ''
        };
        _this.configThumbnailPreloader = true;
        _this.events = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.hostShown = false;
        _this.hideControls = false;
        return _this;
    }
    Object.defineProperty(LightboxComponent.prototype, "simpleMode", {
        get: function () {
            return this.properties.simpleMode;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxComponent.prototype, "hostLightTheme", {
        get: function () {
            return this.properties.backgroundColor === 'white';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxComponent.prototype, "currImagePath", {
        get: function () {
            var image = this.images[this.index];
            var path;
            if (!image) {
                return false;
            }
            if (image.fullImage && image.fullImage.path) {
                path = image.fullImage.path;
            }
            else if (image.thumbnailImage && image.thumbnailImage.path) {
                path = image.thumbnailImage.path;
            }
            else if (image.path) {
                path = image.path;
            }
            return path;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxComponent.prototype, "prevImagePath", {
        get: function () {
            return this.images[this.prevIndex];
        },
        set: function (value) {
            this.images[this.prevIndex] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxComponent.prototype, "isHiddenPrevArrow", {
        get: function () {
            return this.isFirstImage && !this.properties.loop || this.isZoomIn;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxComponent.prototype, "isHiddenNextArrow", {
        get: function () {
            return this.isLastImage && !this.properties.loop || this.isZoomIn;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxComponent.prototype, "isPreloader", {
        get: function () {
            return this.animationMode === 'zoom-preloader' &&
                this.showState != 'animation-end' &&
                this.currImageLoadingState === 'loading';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxComponent.prototype, "imageOrientation", {
        get: function () {
            if (this.thumbnailImage.naturalWidth > this.thumbnailImage.naturalHeight) {
                return 'horizontal';
            }
            else {
                return 'vertical';
            }
        },
        enumerable: true,
        configurable: true
    });
    LightboxComponent.prototype.scrolling = function () {
        if (this.showState === 'initial-thumbnail-image' ||
            this.showState === 'initial-virtual-image' ||
            this.closingState === 'animation') {
            this.updateThumbnailPosition();
        }
    };
    LightboxComponent.prototype.onKeyDown = function (event) {
        switch (event.key) {
            case 'ArrowLeft':
                this.prev();
                break;
            case 'ArrowRight':
                this.next();
                break;
            case 'Escape':
                this.closeLightbox();
                break;
        }
    };
    LightboxComponent.prototype.onMouseEnter = function (event) {
        this.hideControls = false;
    };
    LightboxComponent.prototype.transitionEnd = function (event) {
        if (event.propertyName === "transform" && this.hostAnimation) {
            this.hostAnimation = false;
        }
    };
    LightboxComponent.prototype.ngOnInit = function () {
        this.currentImageIndex = this.properties.index;
        this.initialLightbox();
    };
    LightboxComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.currImageLoadingState === 'not-loaded') {
                _this.currImageLoadingState = 'loading';
            }
        }, this.preloaderTimeout);
        // Mode: default
        if (this.animationMode === 'default') {
            setTimeout(function () {
                _this.showLightboxAnimation();
            }, this.minTimeout);
        }
    };
    LightboxComponent.prototype.onImageLoaded = function () {
        var _this = this;
        // When opening lightbox
        if (this.animationMode === 'zoom-preloader' &&
            this.showState === 'initial-thumbnail-image') {
            this.initialLightboxVirtualImage();
            setTimeout(function () {
                _this.currImageLoadingState = 'uploaded';
                _this.showLightboxAnimation();
                if (_this.properties.hideThumbnail) {
                    _this.hideThumbnailImage();
                }
            }, this.minTimeout);
        }
        // When opening next / previous image
        if (this.showState === 'animation-end') {
            this.currImageLoadingState = 'uploaded';
            if (this.properties.hideThumbnail) {
                this.hideThumbnailImage();
            }
        }
        this.ref.detectChanges();
    };
    LightboxComponent.prototype.onImageError = function (event) {
        var _this = this;
        this.currImageLoadingState = 'error';
        this.initialLightboxDefault();
        setTimeout(function () {
            _this.showLightboxAnimation();
        }, this.minTimeout);
    };
    LightboxComponent.prototype.onContainerClick = function (event) {
        if (event.target === this.lightboxContainerElem.nativeElement || this.simpleMode) {
            this.closeLightbox();
        }
    };
    LightboxComponent.prototype.initialLightbox = function () {
        this.setMaxDimensions();
        this.setAnimationDuration();
        switch (this.animationMode) {
            case 'zoom-preloader':
                this.initialLightboxThumbnailImage();
                break;
            case 'default':
                this.initialLightboxDefault();
                break;
        }
    };
    LightboxComponent.prototype.initialLightboxDefault = function () {
        this.showState = 'initial-default';
        this.containerStyles = {
            transform: 'translate3d(0, 0, 0)',
            height: '100%',
            width: '100%',
            opacity: '0'
        };
        // next step: AfterViewInit
    };
    LightboxComponent.prototype.initialLightboxVirtualImage = function () {
        this.setShowState('initial-virtual-image');
        this.containerStyles = {
            transform: this.containerInitialPosition,
            height: this.virtualImageDimension.height + 'px',
            width: this.virtualImageDimension.width + 'px'
        };
        // next step: onImageLoaded() -> showLightboxAnimation()
    };
    LightboxComponent.prototype.initialLightboxThumbnailImage = function () {
        this.setShowState('initial-thumbnail-image');
        this.containerStyles = {
            transform: this.containerInitialPosition,
            height: this.thumbnailImagePosition.height + 'px',
            width: this.thumbnailImagePosition.width + 'px'
        };
        // next step: onImageLoaded()
    };
    LightboxComponent.prototype.showLightboxAnimation = function () {
        this.hostAnimation = true;
        this.setShowState('animation');
        this.hostShown = true;
        this.setBackgroundColor();
        this.setAnimationDuration();
        // Mode: zoom preloader
        if (this.animationMode === 'zoom-preloader' &&
            this.currImageLoadingState !== 'error') {
            this.containerStyles.transform = this.containerFullscreenPosition;
        }
        // Mode: default
        if (this.animationMode === 'default') {
            this.containerStyles.opacity = '1';
        }
        // next step: handleLightboxTransitionEnd
    };
    LightboxComponent.prototype.showLightboxAnimationEnd = function () {
        this.setShowState('animation-end');
        this.containerStyles = {
            transform: 'translate3d(0, 0, 0)',
            height: '100%',
            width: '100%',
        };
    };
    LightboxComponent.prototype.closeLightbox = function () {
        this.setClosingState('initial');
        this.hostShown = false;
        this.closeLightboxInitial();
    };
    LightboxComponent.prototype.closeLightboxInitial = function () {
        var _this = this;
        this.setClosingState('initial-styles');
        // Mode: zoom preloader
        if (this.animationMode === 'zoom-preloader') {
            this.containerStyles = {
                transform: this.containerFullscreenPosition,
                height: this.virtualImageDimension.height + 'px',
                width: this.virtualImageDimension.width + 'px',
            };
        }
        // Mode: default
        if (this.animationMode === 'default') {
            this.containerStyles.opacity = '1';
        }
        setTimeout(function () {
            _this.closeLightboxAnimation();
        }, this.minTimeout);
    };
    LightboxComponent.prototype.closeLightboxAnimation = function () {
        this.setClosingState('animation');
        // Mode: zoom preloader
        if (this.animationMode === 'zoom-preloader') {
            this.hostAnimation = true;
            this.containerStyles = {
                transform: this.containerInitialPosition,
                height: this.getContainerHeight(),
                width: this.getContainerWidth(),
            };
            this.hostStyleBackgroundColor = '';
        }
        // Mode: default
        if (this.animationMode === 'default') {
            this.hostAnimation = true;
            this.containerStyles.opacity = '0';
            this.hostStyleBackgroundColor = '';
        }
        this.setAnimationDuration();
        // next step: handleLightboxTransitionEnd
        if (this.animationDuration === 0) { // in the future, change to a type conversion getter
            this.closeLightboxAnimationEnd();
        }
    };
    LightboxComponent.prototype.closeLightboxAnimationEnd = function () {
        this.setClosingState('animation-end');
        this.events.emit({ type: 'close' });
        // Mode: zoom preloader
        if (this.animationMode === 'zoom-preloader') {
            this.showThumbnailImage();
        }
    };
    /*
     * Transition End
     */
    LightboxComponent.prototype.handleLightboxTransitionEnd = function (event) {
        if (this.showState === 'animation') {
            this.showLightboxAnimationEnd();
        }
        // Last close step
        if (this.closingState === 'animation') {
            this.closeLightboxAnimationEnd();
        }
    };
    LightboxComponent.prototype.next = function () {
        var _this = this;
        if (this.animationMode === 'zoom-preloader') {
            this.showThumbnailImage();
        }
        if (this.isLastImage) {
            if (this.properties.loop) {
                this.currentImageIndex = 0;
            }
            else {
                return;
            }
        }
        else {
            this.currentImageIndex++;
            this.currImageLoadingState = 'loading';
        }
        setTimeout(function () {
            if (_this.currImageLoadingState !== 'uploaded') {
                _this.currImageLoadingState = 'loading';
            }
        }, this.preloaderTimeout);
    };
    LightboxComponent.prototype.prev = function () {
        var _this = this;
        if (this.animationMode === 'zoom-preloader') {
            this.showThumbnailImage();
        }
        if (this.isFirstImage) {
            if (this.properties.loop) {
                this.currentImageIndex = this.latestImageIndex;
            }
            else {
                return;
            }
        }
        else {
            this.currentImageIndex--;
            this.currImageLoadingState = 'loading';
        }
        setTimeout(function () {
            if (_this.currImageLoadingState !== 'uploaded') {
                _this.currImageLoadingState = 'loading';
            }
        }, this.preloaderTimeout);
    };
    LightboxComponent.prototype.setMaxDimensions = function () {
        this.lightboxImage.nativeElement.style.maxHeight = 'calc(' + this.properties.imageMaxHeight + ')';
        this.lightboxImage.nativeElement.style.maxWidth = this.properties.imageMaxWidth;
    };
    LightboxComponent.prototype.handlePinchZoomEvents = function (event) {
        if (event.type === "zoom-in") {
            this.isZoomIn = true;
        }
        if (event.type === "zoom-out") {
            this.isZoomIn = false;
        }
    };
    LightboxComponent.prototype.getContainerWidth = function () {
        return this.thumbnailImagePosition.width / this.containerScale + 'px';
    };
    LightboxComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: EventService }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-shown')
    ], LightboxComponent.prototype, "hostShown", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-hide-controls')
    ], LightboxComponent.prototype, "hideControls", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-animation')
    ], LightboxComponent.prototype, "hostAnimation", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-simple-mode')
    ], LightboxComponent.prototype, "simpleMode", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-light')
    ], LightboxComponent.prototype, "hostLightTheme", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('style.backgroundColor')
    ], LightboxComponent.prototype, "hostStyleBackgroundColor", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('prevImageElem', { static: true })
    ], LightboxComponent.prototype, "prevImageElem", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('lightboxContainer', { static: true })
    ], LightboxComponent.prototype, "lightboxContainerElem", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll')
    ], LightboxComponent.prototype, "scrolling", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:keydown', ['$event'])
    ], LightboxComponent.prototype, "onKeyDown", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("mouseenter", ['$event'])
    ], LightboxComponent.prototype, "onMouseEnter", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('transitionend', ['$event'])
    ], LightboxComponent.prototype, "transitionEnd", null);
    LightboxComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'crystal-lightbox',
            template: "<div class=\"lightbox-counter\" *ngIf=\"properties.counter && images.length > 1\">{{counter}}</div>\n<div class=\"lightbox-close\" *ngIf=\"!simpleMode\" (click)=\"closeLightbox()\">{{closeButtonText}}</div>\n<div class=\"lightbox-spinner\" \n\t*ngIf=\"currImageLoadingState === 'loading' && showState === 'animation-end'\"></div>\n<div class=\"lightbox-container\" \n\t#lightboxContainer\n\t[ngStyle]=\"containerStyles\"\n\t(transitionend)=\"handleLightboxTransitionEnd($event)\"\n\t(click)=\"onContainerClick($event)\">\n\n\t<div class=\"lightbox-preloader\" *ngIf=\"isPreloader\">\n\t\t<div class=\"lightbox-spinner\"></div>\n\t</div>\n\n\t<div class=\"lightbox-error\" *ngIf=\"currImageLoadingState === 'error' && !closingState\">Failed to load image</div>\n\n\t<img class=\"lightbox-curr-image\" \n\t\t#lightboxImage\n\t\t[src]=\"currImagePath\" \n\t\t[class.lightbox-show]=\"currImageLoadingState === 'uploaded'\" \n\t\t(load)=\"onImageLoaded()\"\n\t\t(error)=\"onImageError($event)\" />\n</div>\n\n<div class=\"lightbox-prev\" [hidden]=\"isHiddenPrevArrow\" (click)=\"prev()\"></div>\n<div class=\"lightbox-next\" [hidden]=\"isHiddenNextArrow\" (click)=\"next()\"></div>",
            styles: [".lightbox-spinner,.lightbox-spinner:after,.lightbox-spinner:before{border-radius:50%;width:10px;height:10px;-webkit-animation:1.5s ease-in-out infinite lightbox-load;animation:1.5s ease-in-out infinite lightbox-load}.lightbox-spinner{color:#fff;font-size:10px;margin:0 auto 20px;position:relative;text-indent:-9999em;transform:translateZ(0);-webkit-animation-delay:-.16s;animation-delay:-.16s}.lightbox-spinner:after,.lightbox-spinner:before{content:\"\";position:absolute;top:0}.lightbox-spinner:before{left:-32px;-webkit-animation-delay:-.32s;animation-delay:-.32s}.lightbox-spinner:after{left:32px}@-webkit-keyframes lightbox-load{0%,100%,80%{box-shadow:0 2.5em 0 -1.3em}40%{box-shadow:0 2.5em 0 0}}@keyframes lightbox-load{0%,100%,80%{box-shadow:0 10px 0 -1.3em}40%{box-shadow:0 10px 0 0}}:host.lightbox-shown{opacity:1}:host.lightbox-shown .lightbox-close,:host.lightbox-shown .lightbox-next,:host.lightbox-shown .lightbox-prev{opacity:.5;transition-delay:.1s}:host.lightbox-shown .lightbox-close:hover,:host.lightbox-shown .lightbox-next:hover,:host.lightbox-shown .lightbox-prev:hover{opacity:1;transition-delay:0}:host.lightbox-shown .lightbox-counter{opacity:1}:host.lightbox-hide-controls .lightbox-close,:host.lightbox-hide-controls .lightbox-next,:host.lightbox-hide-controls .lightbox-prev{opacity:0}:host.lightbox-simple-mode .lightbox-container{cursor:zoom-out}:host:not(.lightbox-animation) .lightbox-container{display:flex;align-items:center;justify-content:center;top:0;left:0;height:100%}:host.lightbox-light{color:#000;text-shadow:none}:host{position:fixed;top:0;left:0;width:100%;height:100%;color:#fff;text-shadow:0 0 1px rgba(0,0,0,.65);-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;z-index:10000}:host img{opacity:1;max-width:100%;max-height:100%}:host img.lightbox-curr-image.lightbox-show{opacity:1}:host img.lightbox-curr-image{opacity:0;z-index:10}:host .lightbox-container.lightbox-hide{display:none}:host .lightbox-container{position:absolute;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;transform-origin:top left}:host .lightbox-preloader{background:rgba(0,0,0,.4);display:flex;align-items:center;justify-content:center;width:100%;height:100%;position:absolute}:host .lightbox-spinner{position:absolute;top:51%;left:50%;margin-top:-15px;margin-left:-5px}:host .lightbox-preloader .lightbox-spinner{margin-top:-9px;margin-left:-3px}:host .lightbox-preloader .lightbox-spinner,:host .lightbox-preloader .lightbox-spinner:after,:host .lightbox-preloader .lightbox-spinner:before{width:6px;height:6px}:host .lightbox-preloader .lightbox-spinner:before{left:-21px}:host .lightbox-preloader .lightbox-spinner:after{left:21px}:host .lightbox-counter{text-align:left;position:absolute;left:22px;top:13px;font-size:14px;z-index:30}:host .lightbox-description{text-align:center;max-width:calc(100% - 200px);margin:0 auto;font-size:14px;line-height:43px;position:relative;z-index:50}:host .lightbox-error{color:rgba(255,255,255,.75);font-size:19px}:host .lightbox-close{position:absolute;top:0;right:0;padding:13px 22px;font-size:13px;text-transform:uppercase;cursor:pointer;opacity:.5;z-index:30;transition:opacity .1s ease-in-out}:host .lightbox-next,:host .lightbox-prev{width:100px;height:100%;position:absolute;top:0;cursor:pointer;opacity:.5;z-index:20;transition:opacity .1s ease-in-out;-webkit-tap-highlight-color:rgba(255,255,255,0)}:host .lightbox-prev{left:0;background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAiCAYAAABbXymAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMzMTZCQzFERDgzMTExRTc5QUYxQTUxRDI5MkM5ODZCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMzMTZCQzFFRDgzMTExRTc5QUYxQTUxRDI5MkM5ODZCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzMxNkJDMUJEODMxMTFFNzlBRjFBNTFEMjkyQzk4NkIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMxNkJDMUNEODMxMTFFNzlBRjFBNTFEMjkyQzk4NkIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7VrwZKAAAClElEQVR42qzWSY8SQRgGYLoVHPeTXlwCiIwMArJGtiABNep4IGZi4oEfMJooLjePrnFGf5yJS0hYmq2b/aBxiQ6+lRRJdaUZq5vp5E2TLnioVFd9VZLN+iUxIdeMiW2fRVCmvz2DpJBVCk6X6SVBHQRLpVKb7Xb782g0Usrl8jM8W0P20+9YQn3JZPIhwN6MXoPBoI3nG8iKGZhF14A+AqrOmKtWq31F213kkOgQ69B0Ol0BqrFot9vV/H7/c7RfEoVZ1G+EdjqdvtfrfYn2K8gJkaHQodlstjIej/ssihdH0Ddov4acQo7QlycJo5PJhEc1j8fzmqJk2h1D7Lv1lkUv5nK5xzzaarWMUIcwms/nCTrgUbfbTdCrVtAAQafTqQ5VFEVdCi0UCk/Q0yGLNptN1el0vqLoaRHUxqPoKY/2KFo0g0r0ba4Wi8WnQEcs2mg0VAPULrJsZTqpNzDZ6yxar9dVl8tFVtR10SnFohJdgnZYuh9IkjSTZfknPn5DviO/kL/IjkgtIOhhJJ7JZF5gdU24hdDG6qqg/QJywEzlmg/FSTLhE4nE9nA4nHJFRvH5fJto94gOBfvyjtJxXI/H4x+A63re6/UUVDCCnzeDz6fbceQscjsWi703wFsM7hDBJQN8PRqNbmNn0OGapu0JfoviYx4PBAL30e5dCg+Hw0Z4e8/wfr/P451QKPRgaRzIFjDdksefsbjdKn5zER6JRMiwuP+3Ne2KB4PBLVVVR1xd+YK2O2bOFYvwd8CHbK/x/J6Zc8Ui/AZmxdtqtdrAwlFLpRLZrS/PYcnkEUuiY7hCS+g5sovT55+Qj7QS/jYD8zjp/UF6t9GS+oPed8zCLC5zJ0tSo//Q+8wKLHTw/ifAAMQVS4vHZR2VAAAAAElFTkSuQmCC) center left 22px no-repeat}:host .lightbox-next{right:0;background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAiCAYAAABbXymAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjEyNDE3MDE1RDgzMTExRTc5NjM4QTIzQzI2Rjc2Qjg2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjEyNDE3MDE2RDgzMTExRTc5NjM4QTIzQzI2Rjc2Qjg2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTI0MTcwMTNEODMxMTFFNzk2MzhBMjNDMjZGNzZCODYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTI0MTcwMTREODMxMTFFNzk2MzhBMjNDMjZGNzZCODYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7zv5BKAAACl0lEQVR42rTW224SQRwGcHZb8FT1Sm88BJCiFAE5JpyCBNRE8coYEy94AGqi9XDXS0+JVh/CB/EhiFET5LjAclgWjaa1aazfJDM6u6LuLjrJl2124cd0duY/I9h+NoELabtcTLcFCon0bw+SRE4h28hn2xyNoIvISrlcfqAoSluSpLepVKqCe6cRB/2MYAXei1wfjUbSLm34gX4ymbyN+z6rOBmC/ciNer3+fpdrwGXgd8h/YwVn8Dm/37/e6/UGOnyQTqfXrOBsKI4g571e76Nutzv8De43gwv05S0hx5CLwJ/iBWrwyWQyzGazpnHyITtyCDlBcI/H8wS4ZlhUVbWMO/R4p9P5Bc/lcnfx/Ow8+AW32z0LH+Xz+X+Dt9ttmcen0ynDA1bx4wR3Op2PW62WrOv5uFAo3JsXL1K8r+v5LNxmZrb8wJvNpn5YlGKxeJ/WFruZBcRPxUsul2u90WhocCyqBqk3dLGJiwbgb/T6FflCSqkoiluCIGjqNGzWgQUz9YR8aQ9yBqtyDQtH0q1KNZPJPMTzOHKA4oaHwuPz+SooUm0eHY/H00QisUEWFHKUDYVRdBmVr9Lv9/WoGo/HX+J5iY7/QSMvj003hnb0aCwWe4HnV5GTyGEj002DDgYDDYqdRo1Goxu0p3pU+BvqDQQCqzPQCUWvzINKejQcDltHQ6HQLaBdHh0Oh5ZRO0OBaFD8iIL7z82ibGtyRyKR1T+gl82g/GZ6Dev/HY/KsqwEg0FLKL/93+R7C3QM9Nk8KDtiOWq12jb2s2XS01Kp9Kparb7G/TfIR2QT2TF6UBS42bBEi/UK/SIBPyCfkC0zKA+z6rWPXm30tLlJr6ZQ/ZmYDYnI1eEdejV9Thb+18H7uwADAOG/Wcm4x+knAAAAAElFTkSuQmCC) center right 22px no-repeat}:host .lightbox-close,:host .lightbox-counter,:host .lightbox-next,:host .lightbox-prev{opacity:0;transition:opacity 150ms cubic-bezier(.475,.105,.445,.945)}"]
        })
    ], LightboxComponent);
    return LightboxComponent;
}(LightboxCommonComponent));

var DefaultProperties = {
    loop: false,
    index: 0,
    counter: false,
    imageMaxHeight: "100%",
    imageMaxWidth: "100%",
    animationDuration: 350,
    animationMode: 'zoom-preloader',
    animationTimingFunction: 'cubic-bezier(0.475, 0.105, 0.445, 0.945)',
    closeButtonText: 'Close',
    counterSeparator: '/',
    disable: false,
    simpleMode: false,
    backgroundColor: 'black',
    backgroundOpacity: 1,
    hideThumbnail: true,
    gestureEnable: false
};

var CrystalLightbox = /** @class */ (function () {
    function CrystalLightbox(componentFactoryResolver, appRef, injector) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.appRef = appRef;
        this.injector = injector;
    }
    CrystalLightbox.prototype.appendComponentToBody = function (component, lightboxData) {
        var _this = this;
        var componentRef = this.componentFactoryResolver
            .resolveComponentFactory(component)
            .create(this.injector);
        componentRef.instance.lightboxData = lightboxData;
        this.appRef.attachView(componentRef.hostView);
        var domElem = componentRef.hostView.rootNodes[0];
        // Add to body
        document.body.appendChild(domElem);
        componentRef.instance.events.subscribe(function (event) {
            if (event.type === 'close') {
                _this.appRef.detachView(componentRef.hostView);
                componentRef.destroy();
            }
        });
    };
    CrystalLightbox.prototype.open = function (lightboxData) {
        lightboxData.properties = this.applyPropertieDefaults(DefaultProperties, lightboxData.properties);
        var component = this.getLightboxComponent();
        this.appendComponentToBody(component, lightboxData);
    };
    CrystalLightbox.prototype.getLightboxComponent = function () {
        return LightboxComponent;
    };
    CrystalLightbox.prototype.applyPropertieDefaults = function (defaultProperties, properties) {
        if (!properties) {
            properties = {};
        }
        if (!properties.index) {
            properties.index = 0;
        }
        this._defaultProperties = Object.assign({}, defaultProperties);
        return Object.assign(this._defaultProperties, properties);
    };
    CrystalLightbox.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"] }
    ]; };
    CrystalLightbox = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], CrystalLightbox);
    return CrystalLightbox;
}());

var LightboxDirective = /** @class */ (function () {
    function LightboxDirective(lightbox, eventService, elementRef) {
        var _this = this;
        this.lightbox = lightbox;
        this.eventService = eventService;
        this.elementRef = elementRef;
        this.properties = {};
        this.events = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.hostLightboxGroup = true;
        this.globalEventsSubscription = this.eventService.emitter.subscribe(function (event) {
            _this.handleGlobalEvents(event);
        });
    }
    Object.defineProperty(LightboxDirective.prototype, "hostSimpleMode", {
        get: function () {
            return this.simpleMode;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LightboxDirective.prototype, "isGroupImage", {
        get: function () {
            return this.elementRef.nativeElement.closest(".lightbox-group");
        },
        enumerable: true,
        configurable: true
    });
    LightboxDirective.prototype.onClick = function (event) {
        if (this.disable) {
            return;
        }
        if (this.isGroupImage) {
            this.eventService.emitChangeEvent({
                type: 'thumbnail:click',
                elementRef: this.elementRef,
                properties: this.properties
            });
        }
        else {
            this.image = this.getImage();
            this.lightbox.open({
                images: [this.image],
                properties: this.properties,
                index: 0
            });
        }
    };
    LightboxDirective.prototype.ngOnChanges = function (changes) {
        this.properties = Object.assign({}, this.properties, this.getProperties(changes));
    };
    LightboxDirective.prototype.handleGlobalEvents = function (event) {
        this.events.emit(event);
    };
    LightboxDirective.prototype.getImage = function () {
        var image = {};
        var nativeElement = this.elementRef.nativeElement;
        if (this.fullImage) {
            image.fullImage = this.fullImage;
        }
        image.thumbnailImage = {
            path: nativeElement.src,
            height: nativeElement.naturalHeight,
            width: nativeElement.naturalWidth
        };
        image.nativeElement = nativeElement;
        return image;
    };
    ;
    LightboxDirective.prototype.getProperties = function (changes) {
        var properties = {};
        for (var prop in changes) {
            if (prop !== 'fullImage') {
                properties[prop] = changes[prop].currentValue;
            }
        }
        return properties;
    };
    LightboxDirective.ctorParameters = function () { return [
        { type: CrystalLightbox },
        { type: EventService },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "fullImage", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "properties", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "loop", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "backgroundOpacity", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "counter", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "imageMaxHeight", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "imageMaxWidth", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "animationDuration", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "animationMode", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "animationTimingFunction", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "closeButtonText", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "counterSeparator", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "disable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "simpleMode", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "backgroundColor", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "hideThumbnail", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], LightboxDirective.prototype, "gestureEnable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], LightboxDirective.prototype, "events", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-single')
    ], LightboxDirective.prototype, "hostLightboxGroup", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-simple-mode')
    ], LightboxDirective.prototype, "hostSimpleMode", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click', ['$event'])
    ], LightboxDirective.prototype, "onClick", null);
    LightboxDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[lightbox]'
        })
    ], LightboxDirective);
    return LightboxDirective;
}());

var LightboxGroupDirective = /** @class */ (function () {
    function LightboxGroupDirective(eventService, lightbox) {
        var _this = this;
        this.eventService = eventService;
        this.lightbox = lightbox;
        this.thumbnailImages = [];
        this.images = [];
        this.properties = {};
        this.hostLightboxGroup = true;
        this.globalEventsSubscription = this.eventService.emitter.subscribe(function (event) {
            _this.handleGlobalEvents(event);
        });
    }
    Object.defineProperty(LightboxGroupDirective.prototype, "lightboxDirectiveList", {
        get: function () {
            if (this._lightboxDirectiveList) {
                return this._lightboxDirectiveList.toArray();
            }
            else {
                return [];
            }
        },
        enumerable: true,
        configurable: true
    });
    LightboxGroupDirective.prototype.handleGlobalEvents = function (event) {
        if (event.type === 'thumbnail:click') {
            this.thumbnailImageElement = event.elementRef.nativeElement;
            this.thumbnailImages = this.getThumbnailImages();
            this.thumbnailImageIndex = this.getThumbnailImageIndex(this.thumbnailImageElement);
            if (this.thumbnailImageIndex == undefined) {
                return;
            }
            this.thumbnailLightboxDirective = this.getThumbnailLightboxDirective(this.thumbnailImageIndex);
            this.images = this.getImages();
            this.properties = event.properties;
            this.properties.index = this.thumbnailImageIndex;
            this.lightbox.open({
                images: this.images,
                //index: this.thumbnailImageIndex,
                properties: this.properties
            });
        }
    };
    LightboxGroupDirective.prototype.getThumbnailImageIndex = function (element) {
        var images = this.thumbnailImages;
        for (var i = 0; i < images.length; i++) {
            if (element === images[i]) {
                return i;
            }
        }
    };
    LightboxGroupDirective.prototype.getThumbnailLightboxDirective = function (index) {
        return this.lightboxDirectiveList[index];
    };
    LightboxGroupDirective.prototype.getThumbnailImages = function () {
        var thumbnailImages = [];
        this.lightboxDirectiveList.forEach(function (el) {
            thumbnailImages.push(el['elementRef'].nativeElement);
        });
        return thumbnailImages;
    };
    LightboxGroupDirective.prototype.getImages = function () {
        var images = [];
        this.lightboxDirectiveList.forEach(function (el) {
            var image = {};
            var nativeElement = el['elementRef'].nativeElement;
            if (el.fullImage) {
                image.fullImage = el.fullImage;
            }
            image.thumbnailImage = {
                path: nativeElement.src,
                height: nativeElement.naturalHeight,
                width: nativeElement.naturalWidth
            };
            image.nativeElement = nativeElement;
            images.push(image);
        });
        return images;
    };
    LightboxGroupDirective.ctorParameters = function () { return [
        { type: EventService },
        { type: CrystalLightbox }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.lightbox-group')
    ], LightboxGroupDirective.prototype, "hostLightboxGroup", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChildren"])(LightboxDirective, { descendants: true })
    ], LightboxGroupDirective.prototype, "_lightboxDirectiveList", void 0);
    LightboxGroupDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[lightbox-group]'
        })
    ], LightboxGroupDirective);
    return LightboxGroupDirective;
}());

var CrystalLightboxModule = /** @class */ (function () {
    function CrystalLightboxModule() {
    }
    CrystalLightboxModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                LightboxCommonComponent,
                LightboxComponent,
                LightboxDirective,
                LightboxGroupDirective
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [
                LightboxDirective,
                LightboxGroupDirective
            ],
            providers: [
                CrystalLightbox,
                EventService
            ],
            bootstrap: [],
            entryComponents: [
                LightboxComponent
            ]
        })
    ], CrystalLightboxModule);
    return CrystalLightboxModule;
}());

/*
 * Public API Surface of angular-lightbox
 */

/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=crystalui-angular-lightbox.js.map


/***/ })

}]);
//# sourceMappingURL=default~pages-admin-doctors-view-view-module~pages-admin-patients-view-view-module~pages-branch-head~056c7745.js.map