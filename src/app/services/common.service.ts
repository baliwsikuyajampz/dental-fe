import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class CommonService {
  private data = new BehaviorSubject('');
  private doctorKey = new BehaviorSubject('');
  private loggedInUser = new BehaviorSubject('');

  data$ = this.data.asObservable();
  doctorKey$ = this.doctorKey.asObservable();
  loggedInUser$ = this.loggedInUser.asObservable();

  changeData(data: string) {
    this.data.next(data)
  }

  setDentistKey(data: string) {
    this.doctorKey.next(data)
  }

  setLoggedInUser(data:any){
    this.loggedInUser.next(data)
  }

  getLoggedInUser(){
    if(localStorage.getItem("user_info")!==null){
      return JSON.parse(localStorage.getItem("user_info"))
    }
    else{
      return null
    }
  }
}
