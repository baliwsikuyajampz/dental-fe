import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
// import { BaseComponent } from 'src/app/base-component';

//const API_URL = "http://3.1.159.82:8080/";
//const API_URL = "http://localhost:3000/";
//const API_URL = "http://13.251.41.18:8080/";
const API_URL = "https://api.drds.ayon.ph/";



@Injectable({
    providedIn: 'root',
})
export class ApiService {
    constructor(private httpClient: HttpClient) {}

    private options = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    private options2 = {};

    public get(
        path: string,
        params: HttpParams = new HttpParams()
    ): Observable<any> {
        return this.httpClient
            .get(API_URL + path, { params })
            .pipe(catchError(this.formatErrors));
    }

    public put(path: string, body: object = {}): Observable<any> {
        return this.httpClient
            .put(API_URL + path, JSON.stringify(body), this.options)
            .pipe(catchError(this.formatErrors));
    }

    public post(path: string, body: object = {}): Observable<any> {
        return this.httpClient
            .post(API_URL + path, JSON.stringify(body), this.options)
            .pipe(catchError(this.formatErrors));
    }

    public post_form_data(path: string, body: object = {}): Observable<any> {
        return this.httpClient
            .post(API_URL + path, body)
            .pipe(catchError(this.formatErrors));
    }

    public delete(path: string): Observable<any> {
        return this.httpClient
            .delete(API_URL + path)
            .pipe(catchError(this.formatErrors));
    }

    public formatErrors(error: any): Observable<any> {
        return throwError(error.error);
    }

    public postFormData(url, formdata): Observable<any> {
        return this.httpClient
            .post(API_URL + url, formdata)
            .pipe(catchError(this.formatErrors));
    }
}