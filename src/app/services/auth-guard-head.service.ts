import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute, CanActivateChild } from '@angular/router';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardHeadService implements CanActivate, CanActivateChild {
  userLoggedInfo : any = null

  constructor(
    private router: Router,
    private common : CommonService,
    private route : ActivatedRoute
) {
    // this.common.loggedInUser$.subscribe(res => this.userLoggedInfo = res)
    if(localStorage.getItem("user_info")!== null){
        this.userLoggedInfo = JSON.parse(localStorage.getItem("user_info"))
    }
    else{
        this.userLoggedInfo == null
    }

}

  canActivate(): boolean {
    console.log("wew",this.userLoggedInfo.role)

    if (this.userLoggedInfo!==null) {      
        switch(this.userLoggedInfo.role){
            case 1 : 
                this.router.navigate(['/admin'])
                return false
                break;
            case 2 :
                return true; 
                break;
            default :


        }
    } else {
      return false; // Prevent access to the route
    }
  }

  canActivateChild(): boolean {
    console.log("wew",this.userLoggedInfo.role)
    if (this.userLoggedInfo!==null) {      
        switch(this.userLoggedInfo.role){
            case 1 : 
                this.router.navigate(['/admin'])
                return false
                break;
            case 2 :
                return true; 
                break;
            default :


        }
      
    } else {
      return false; // Prevent access to the route
    }
  }

}