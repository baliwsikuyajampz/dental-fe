import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute, CanActivateChild } from '@angular/router';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {
  userLoggedInfo : any = null

  constructor(
    private router: Router,
    private common : CommonService,
    private route : ActivatedRoute
) {
    // this.common.loggedInUser$.subscribe(res => this.userLoggedInfo = res)
    if(localStorage.getItem("user_info")!== null){
        this.userLoggedInfo = JSON.parse(localStorage.getItem("user_info"))
    }
    else{
        this.userLoggedInfo == null
    }

}

  canActivate(): boolean {
    if (this.userLoggedInfo!==null) {      
        switch(this.userLoggedInfo.role){
            case 1 : 
                
                return true
                break;
            case 2 :
                this.router.navigate(['/head'])
                return false; 
                break;
            default :


        }
      
    } else {
      return false; // Prevent access to the route
    }
  }

  canActivateChild(): boolean {
    if (this.userLoggedInfo!==null) {      
        switch(this.userLoggedInfo.role){
            case 1 : 
                
                return true
                break;
            case 2 :
                this.router.navigate(['/head'])
                return false; 
                break;
            default :


        }
      
    } else {
      return false; // Prevent access to the route
    }
  }

}