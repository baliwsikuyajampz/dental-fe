import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppService} from './app.service'
import {AppComponent} from './app.component';

import {SidebarComponent} from './layout/sidebar/sidebar.component';
import { SidebarHeadComponent } from './layout/sidebar-head/sidebar-head.component';

import {SidebarUserComponent} from './layout/sidebar-user/sidebar-user.component';

import {HeaderComponent} from './layout/header/header.component';
import {FooterComponent} from './layout/footer/footer.component';
import {SearchComponent} from './layout/header/search/search.component';
import {LogoComponent} from './layout/header/logo/logo.component';
import {NavigationTriggerComponent} from './layout/header/navigation-trigger/navigation-trigger.component';
import {UserComponent} from './layout/sidebar/user/user.component';
import {PageLoaderComponent} from './layout/page-loader/page-loader.component';

import { ProgressDisplayComponent } from './layout/layouts/layout-1/progress-display/progress-display.component';

import {Layout1Component} from './layout/layouts/layout-1/layout.component';
import {Layout2Component} from './layout/layouts/layout-2/layout.component';
import {LayoutUserComponent} from './layout/layouts/layout-user/layout-user.component';
import { LayoutHeadComponent } from './layout/layouts/layout-head/layout-head.component';

import { HttpClientModule, /* other http imports */ } from "@angular/common/http";

import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {NgScrollbarModule} from 'ngx-scrollbar';
import {AppRoutingModule} from './app-routing.module';
import { NgxUiLoaderModule,NgxUiLoaderHttpModule,NgxUiLoaderConfig } from 'ngx-ui-loader';

import { ModalModule } from 'ngx-bootstrap/modal';
import {GrowlModule} from 'ngx-growl';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
    blur: 10,
    fgsSize: 60,
    fgsType: "three-strings", // foreground spinner type
    fgsColor: "#2EBDBB",
    pbColor: "#2EBDBB",
    pbThickness: 5, // progress bar thickness
    logoUrl: "../assets/img/logo/dsdr.png",
    logoSize: 120,
    text: "Please Wait...",
    textColor: "#2EBDBB",
};

@NgModule({
    declarations: [
        AppComponent,
        SidebarComponent,
        SidebarHeadComponent,
        SidebarUserComponent,
        HeaderComponent,
        FooterComponent,
        Layout1Component,
        Layout2Component,
        LayoutHeadComponent,
        LayoutUserComponent,
        SearchComponent,
        LogoComponent,
        NavigationTriggerComponent,
        UserComponent,
        PageLoaderComponent,
        ProgressDisplayComponent,
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        NgScrollbarModule,
        BsDropdownModule.forRoot(),
        ButtonsModule.forRoot(),
        NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
        NgxUiLoaderHttpModule.forRoot(
          {
            showForeground: true, 
            exclude: [
                "/notification/get-admin-notification",
                "/notification/get-notification",
              ],
          }),
        ModalModule,
        GrowlModule.forRoot({maxMessages: 10, displayTimeMs: 4000})
    ],
    providers: [AppService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
