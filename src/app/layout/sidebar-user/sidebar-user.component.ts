import {Component, OnInit} from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';
import {AppService} from '../../app.service';

@Component({
    selector: 'app-sidebar-user',
    templateUrl: 'sidebar-user.component.html',
    styleUrls: ['sidebar-user.component.scss'],
    host: {
        'class': 'sidebar'
    },
    animations: [
        trigger('toggleSubMenu', [
            state('inactive', style({
                height: '0',
                opacity: '0'
            })),
            state('active', style({
                height: '*',
                opacity: '1'
            })),
            transition('inactive => active', animate('200ms ease-in')),
            transition('active => inactive', animate('200ms ease-out'))
        ])
    ]
})
export class SidebarUserComponent implements OnInit {
    user_info  : any;
    report_access = new Set()
    client_access = new Set()
    users_access  = new Set()
    client_request_access = new Set()
    settings_access = new Set()

    mainMenu: Array<any> = []

    // Toggle sub menu
    toggleSubMenu(i) {
        this.mainMenu[i].visibility = (this.mainMenu[i].visibility === 'inactive' ? 'active' : 'inactive');
    }

    constructor(private service: AppService) {

    }

    setAccess(){
        this.report_access.add(1)
        this.report_access.add(2)

        this.client_access.add(1)
        this.client_access.add(2)
        this.client_access.add(3)
        this.client_access.add(4)

        this.client_request_access.add(1)
        this.client_request_access.add(2)
        this.client_request_access.add(3)

        this.users_access.add(1)
        this.users_access.add(2)

        this.settings_access.add(1)
        this.settings_access.add(2)

    }

    ngOnInit() {
        this.setAccess()
        if(localStorage.getItem("user_info")!==null){
            this.user_info = JSON.parse(localStorage.getItem("user_info"))
        }
        this.setSidebar(this.user_info)
    }

    setSidebar(user_info){
        this.mainMenu = [
            {
                title: 'Home',
                icon: 'home',
                route: 'home',
                is_shown : user_info.role == 3 ? true : false

            },

            
            {
              title: 'Manage Inventory',
              icon: 'collection-text',
              route: 'inventory',
              is_shown : user_info.role == 3 ? true : false

            },

            {
                title: 'Stock Requests',
                icon: 'mail-reply',
                route: 'stock-requests',
                is_shown : user_info.role == 3 ? true : false
            },

            {
                title: 'Transactions',
                icon: 'money-box',
                sub: [
                    {
                        title: 'Sales Transaction',
                        route: 'sales',
                        is_shown : true
                    },
                    {
                        title: 'Reservations',
                        route: 'reservations',
                        is_shown : true
                    },
                    {
                        title: 'View Transactions',
                        route: 'view-transactions',
                        is_shown : user_info.role == 3 ? true : false
                    },
                ],
                visibility: 'inactive',
                is_shown : true
            },
            {
                title: 'Reports',
                icon: 'assignment',
                sub: [
                    {
                        title: 'Sales Report',
                        route: 'reports/sales',
                        is_shown : true
                    },

                ],
                visibility: 'inactive',
                is_shown : true 
            },
            {
              title: 'Settings',
              icon: 'settings',
              sub: [
                  {
                      title: 'Change Password',
                      route: 'settings/change-password',
                      is_shown : true
                  },
              ],
              visibility: 'inactive',
              is_shown : true
            },
        ];
    }

}
