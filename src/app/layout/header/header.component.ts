import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent implements OnInit {
  maCurrentTheme: string;
  maThemes: Array<string>;
  appShortcuts: Array<any>;
  tasks: Array<any>;
  notifications: Array<any>;
  mobileSearchActive: boolean = false;

  userLoggedInfo : any;

  notification_list : any = []

  notifPage : number = 1
  notifRows : number = 10

  constructor(
    private service: AppService,
    private router : Router,
    private API : ApiService,
    ) {

    // Retrieve current theme from AppService
    this.maCurrentTheme = service.maTheme;

    // Notifications + Messages
    this.notifications = [
      {
        image: '1.jpg',
        user: 'David Belle',
        content: 'Cum sociis natoque penatibus et magnis dis parturient montes',
        date: '12:01 PM'
      },
      {
        image: '2.jpg',
        user: 'Jonathan Morris',
        content: 'Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel',
        date: '02:45 PM'
      },
      {
        image: '6.jpg',
        user: 'Fredric Mitchell Jr.',
        content: 'Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies',
        date: '08:21 PM'
      },
      {
        image: '4.jpg',
        user: 'Glenn Jecobs',
        content: 'Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque',
        date: '08:43 PM'
      },
      {
        image: '5.jpg',
        user: 'Bill Phillips',
        content: 'Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat',
        date: '11:32 PM'
      }
    ];
    
  }

  // Set theme
  setMaTheme() {
    this.service.maTheme = this.maCurrentTheme
  }

  ngOnInit() {

  }
  


}
