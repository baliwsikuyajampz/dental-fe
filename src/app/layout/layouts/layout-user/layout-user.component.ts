import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { AppService } from '../../../app.service';

@Component({
  selector: 'app-layout-user',
  templateUrl: './layout-user.component.html',
  styleUrls: ['./layout-user.component.scss']
})
export class LayoutUserComponent implements OnInit {
  maTheme: string = this.service.maTheme

  constructor(
    public service: AppService,
    ) {

    }

  ngOnInit() {

  }

 
}
