import { Component, OnInit } from '@angular/core';
// import { SocketService } from 'src/app/services/socket.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-progress-display',
  templateUrl: './progress-display.component.html',
  styleUrls: ['./progress-display.component.scss']
})
export class ProgressDisplayComponent implements OnInit {
  downloadArrays : any = []
  constructor(
    // private socket : SocketService,
    private API : ApiService
  ) { }

  ngOnInit() {
  }

  onClose(){
    this.downloadArrays = []
  }

 
}
