import { Component, OnInit,AfterContentChecked } from '@angular/core';
import { AppService } from '../../../app.service';

@Component({
  selector: 'app-layout-head',
  templateUrl: 'layout-head.component.html',
  styleUrls: ['layout-head.component.scss']
})
export class LayoutHeadComponent implements OnInit {
  maTheme: string = this.service.maTheme

  constructor(
    public service: AppService,
    ) {

    }

  ngOnInit() {

  }

 
}
