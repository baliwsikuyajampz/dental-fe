import {Component, OnInit} from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';
import {AppService} from '../../app.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: 'sidebar.component.html',
    styleUrls: ['sidebar.component.scss'],
    host: {
        'class': 'sidebar'
    },
    animations: [
        trigger('toggleSubMenu', [
            state('inactive', style({
                height: '0',
                opacity: '0'
            })),
            state('active', style({
                height: '*',
                opacity: '1'
            })),
            transition('inactive => active', animate('200ms ease-in')),
            transition('active => inactive', animate('200ms ease-out'))
        ])
    ]
})
export class SidebarComponent implements OnInit {
    user_info  : any;
    report_access = new Set()
    client_access = new Set()
    users_access  = new Set()
    client_request_access = new Set()
    settings_access = new Set()

    mainMenu: Array<any> = []

    // Toggle sub menu
    toggleSubMenu(i) {
        this.mainMenu[i].visibility = (this.mainMenu[i].visibility === 'inactive' ? 'active' : 'inactive');
    }


    toggleSubMenuCustom() {
        for(let i=0;i<this.mainMenu.length;i++){
            this.mainMenu[i].visibility = (this.mainMenu[i].visibility = 'inactive');
        }
    }
    
    constructor(private service: AppService) {

    }

    setAccess(){
        this.report_access.add(1)
        this.report_access.add(2)

        this.client_access.add(1)
        this.client_access.add(2)
        this.client_access.add(3)
        this.client_access.add(4)

        this.client_request_access.add(1)
        this.client_request_access.add(2)
        this.client_request_access.add(3)

        this.users_access.add(1)
        this.users_access.add(2)

        this.settings_access.add(1)
        this.settings_access.add(2)

    }

    ngOnInit() {
        this.setAccess()
        if(localStorage.getItem("user_info")!==null){
            this.user_info = JSON.parse(localStorage.getItem("user_info"))
        }
        this.setSidebar(this.user_info)
    }

    setSidebar(user_info){
        this.mainMenu = [
            {
                title: 'Home',
                icon: 'home',
                route: 'home',
                is_shown : true
            },
            {
                title: 'Manage Users',
                icon: 'account-add',
                route: 'users/list',
                is_shown : true
            },
            {
                title: 'Manage Patients',
                icon: 'accounts-add',
                route: 'patients/list',
                is_shown : true
            },
            {
                title: 'Manage Ranks',
                icon: 'star',
                route: 'ranks',
                is_shown : true
            },
            {
                title: 'Manage Employees',
                icon: 'accounts',
                sub: [
                    {
                        title: 'Dentists',
                        route: 'dentists/list',
                        is_shown : true
                    },
                    {
                        title: 'Assistants',
                        route: 'assistants',
                        is_shown : true
                    },
                    {
                        title: 'Secretaries',
                        route: 'secretaries',
                        is_shown : true
                    },

                ],
                visibility: 'inactive',
                is_shown : true
            },
            {
                title: 'Manage Procedures',
                icon: 'functions',
                sub: [
                    {
                        title: 'Categories',
                        route: 'procedures/categories',
                        is_shown : true
                    },
                    {
                        title: 'List',
                        route: 'procedures/list',
                        is_shown : true
                    },
 
 
                ],
                visibility: 'inactive',
                is_shown : true
            },
            {
                title: 'Manage Expenses',
                icon: 'money-off',
                sub: [
                    {
                        title: 'Categories',
                        route: 'expenses/categories',
                        is_shown : true
                    },
                    {
                        title: 'Expenses',
                        route: 'expenses/list',
                        is_shown : true
                    },
 
 
                ],
                visibility: 'inactive',
                is_shown : true
            },
            {
                title: 'Transactions',
                icon: 'money',
                sub: [
                    {
                        title: 'Sales',
                        route: 'transactions/sales',
                        is_shown : true
                    },
                    // {
                    //     title: 'Pending Procedures',
                    //     route: 'transactions/pending/list',
                    //     is_shown : true
                    // },
                    {
                        title: 'Pending Payments',
                        route: 'transactions/payments/list',
                        is_shown : true
                    },
                ],
                visibility: 'inactive',
                is_shown : true
            },
            {
                title: 'Reports',
                icon: 'chart',
                sub: [
                    {
                        title: 'Sales',
                        route: 'reports/sales',
                        is_shown : true,
                    },
                    {
                        title: 'Profit and Loss',
                        route: 'reports/profit-and-loss',
                        is_shown : true
                    },
                    {
                        title: 'Commissions',
                        route: 'reports/commissions',
                        is_shown : true,
                    },
                    {
                        title: 'Search Commissions',
                        route: 'reports/search-commissions',
                        is_shown : true,
                    },
 
                ],
                visibility: 'inactive',
                is_shown : true

               
            },
            {
                title: 'Settings',
                icon: 'settings',
                sub: [
                    {
                        title: 'Change Password',
                        route: 'settings/change-password',
                        is_shown : true
                    },
 
                ],
                visibility: 'inactive',
                is_shown : true

               
            },
            
        ];
    }

}
