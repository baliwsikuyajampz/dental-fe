import { Component, OnInit, Input, Output,EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.scss']
})
export class CardProductComponent implements OnInit {
  @Input() data : any = {} 
  @Input() branch : any ;
  @Input() stock_adjustment_form : any;
  @Output() stock_adjustment_data  = new EventEmitter<any>();

  constructor() { }
  
  ngOnInit() {
    console.log(this.data)
  }

  stockAdjustmentForm(data:any){
    this.stock_adjustment_data.emit(data)
    this.stock_adjustment_form.show()
  }

}
