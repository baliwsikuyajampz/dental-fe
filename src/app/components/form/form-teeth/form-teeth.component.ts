import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-form-teeth',
  templateUrl: './form-teeth.component.html',
  styleUrls: ['./form-teeth.component.scss']
})
export class FormTeethComponent implements OnInit {
  teeth_list : []
  constructor(private API : ApiService) { }

  ngOnInit() {
    this.getTeethStructure()
  }

  getTeethStructure(){
    this.API.post("settings/get-teeth-structure",
    {

    },
  ).subscribe(
      (response: any) => {   
        this.teeth_list = response.devMessage
      },
      (error: any) => {

      },
  )
  }

}
