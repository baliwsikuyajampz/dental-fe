import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {Layout1Component} from "./layout/layouts/layout-1/layout.component";
import {Layout2Component} from "./layout/layouts/layout-2/layout.component";
import { LayoutHeadComponent } from "./layout/layouts/layout-head/layout-head.component";
import { AuthGuardService } from "./services/auth-guard.service";
import { AuthGuardHeadService } from "./services/auth-guard-head.service";

const routes: Routes = [
    {
        path: "admin",
        component: Layout1Component,
        canActivate : [AuthGuardService],
        canActivateChild : [AuthGuardService],
        children: [
            {
                path: "",
                redirectTo: "home",
                pathMatch: "full"
            },
            {
                path: "home",
                loadChildren: "./pages/admin/home/home.module#HomeModule"
            },

            {
                path: "users/list",
                loadChildren: "./pages/admin/users/list/list.module#ListModule"
            },
            {
                path: "ranks",
                loadChildren: "./pages/admin/ranks/ranks.module#RanksModule"
            },
            {
                path: "users/list/logs/:id",
                loadChildren: "./pages/admin/users/list/view-user-log/view-user-log.module#ViewUserLogModule"
            },
            {
                path: "settings/change-password",
                loadChildren: "./pages/admin/settings/change-password/change-password.module#ChangePasswordModule"
            },
            {
                path: "settings/test",
                loadChildren: "./pages/admin/settings/test/test.module#TestModule"
            },

            {
                path: "settings/branch",
                loadChildren: "./pages/admin/settings/branch/branch.module#BranchModule"
            },
            

            {
                path: "settings/sizes",
                loadChildren: "./pages/admin/settings/sizes/sizes.module#SizesModule"
            },

            {
                path: "settings/categories",
                loadChildren: "./pages/admin/settings/categories/categories.module#CategoriesModule"
            },
            {
                path: "settings/items",
                loadChildren: "./pages/admin/settings/items/items.module#ItemsModule"
            },
            { // PATIENTS
                path: "patients",
                loadChildren: "./pages/admin/patients/patients.module#PatientsModule"
            },
            { // PATIENTS
                path: "patient/:id",
                loadChildren: "./pages/admin/patients/view/view.module#ViewModule"
            },
            { // Expenses
                path: "expenses",
                loadChildren: "./pages/admin/expenses/expenses.module#ExpensesModule"
            },
            { // Doctors
                path: "dentists",
                loadChildren: "./pages/admin/doctors/doctors.module#DoctorsModule"
            },
            { // PATIENTS
                path: "dentist/:id",
                loadChildren: "./pages/admin/doctors/view/view.module#ViewModule"
            },
            { // Assistants
                path: "assistants",
                loadChildren: "./pages/admin/assistants/assistants.module#AssistantsModule"
            },
            { // Secretaries
                path: "secretaries",
                loadChildren: "./pages/admin/secretaries/secretaries.module#SecretariesModule"
            },
            { // Procedures
                path: "procedures",
                loadChildren: "./pages/admin/procedures/procedures.module#ProceduresModule"
            },
            { // Transactions
                path: "transactions",
                loadChildren: "./pages/admin/transactions/transactions.module#TransactionsModule"
            },
            { // Reports
                path: "reports",
                loadChildren: "./pages/admin/reports/reports.module#ReportsModule"
            },
            
        ]
    },
    {
        path: "head",
        component: LayoutHeadComponent,
        canActivate : [AuthGuardHeadService],
        canActivateChild : [AuthGuardHeadService],
        children: [
            {
                path: "",
                redirectTo: "home",
                pathMatch: "full"
            },
            {
                path: "home",
                loadChildren: "./pages/branch-head/home/home.module#HomeModule"
            },
            { // PATIENTS
                path: "patients",
                loadChildren: "./pages/branch-head/patients/patients.module#PatientsModule"
            },
            { // PATIENTS
                path: "patient/:id",
                loadChildren: "./pages/branch-head/patients/view/view.module#ViewModule"
            },
            {
                path: "",
                redirectTo: "transactions",
                pathMatch: "full"
            },
            { // Expenses
                path: "expenses",
                loadChildren: "./pages/branch-head/expenses/expenses.module#ExpensesModule"
            },
            { // Transactions
                path: "transactions",
                loadChildren: "./pages/branch-head/transactions/transactions.module#TransactionsModule"
            },       
            {
                path: "settings/change-password",
                loadChildren: "./pages/branch-head/settings/change-password/change-password.module#ChangePasswordModule"
            },
            { // Reports
                path: "reports",
                loadChildren: "./pages/branch-head/reports/reports.module#ReportsModule"
            },
        ]
    },
    {
        path: "",
        component: Layout2Component,
        children: [
            {
                path: "",
                loadChildren: "./pages/login/login.module#LoginModule"
            },
            {
                path: "registration",
                loadChildren: "./pages/registration/registration.module#RegistrationModule"
            },
        ]
    }
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: false})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
