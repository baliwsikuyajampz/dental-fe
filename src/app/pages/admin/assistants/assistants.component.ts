import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import {GrowlService} from 'ngx-growl';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-assistants',
  templateUrl: './assistants.component.html',
  styleUrls: ['./assistants.component.scss']
})
export class AssistantsComponent implements OnInit {
  @ViewChild("modalAssistants") public modalAssistants: ModalDirective;

  branch_list : any = []
  assistants_list : any = []
  loggedUserInfo : any = null

  total : number = 0

  branch : any = null
  fname : string = ""
  mname : string = ""
  lname : string = ""
  isActive : boolean = true

  key : number = null

  page : number = 1
  rows : number = 10
  
  searchKey : string = ""

  mode : string = "add"

  pageTitle : string = "Assistants"
  widgetSubTitle : string = "Manage Assistants"

  constructor(
    private API : ApiService,
    private growlService : GrowlService,
    private common : CommonService
  ) {
    this.loggedUserInfo = this.common.getLoggedInUser()
   }


  ngOnInit() {
    this.getBranches()
    this.getAssistants()
  }

  addAssistant(){
    this.clearFields()
    this.modalAssistants.show()
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {


    },
    ).subscribe(
        (response: any) => {   
          this.branch_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  saveAssistant(){
    if(this.fname == "" || this.lname == "" || isNull(this.branch)){
      Swal.fire("Oops","Required field(s) missing!","warning")
    }
    else{
      this.API.post("assistants/save",
      {
        fname : this.fname,
        mname : this.mname,
        lname : this.lname,
        branch: this.branch,
        key   : this.key,
        mode  : this.mode,
        is_active : this.isActive,
        created_by : this.loggedUserInfo.id
      },
      ).subscribe(
          (response: any) => {   
            if(response.devMessage){
              let msg = this.mode == "add" ? "Record Saved" : "Record Updated"
              this.growlService.addSuccess(msg)
              this.modalAssistants.hide()
              this.getAssistants()
              this.clearFields()
            }
          },
          (error: any) => {
  
          },
      )
    }
  }

  clearFields(){
    this.mode = "add"
    this.key = null
    this.fname = ""
    this.mname = ""
    this.lname = ""
    this.branch = null
    this.isActive = true
  }

  getAssistants(){
    this.API.post("assistants/get",
    {
      search : this.searchKey,
      page : this.page,
      rows : this.rows
    },
    ).subscribe(
        (response: any) => {   
          this.assistants_list = response.devMessage
          this.total = response.total
        },
        (error: any) => {

        },
    )
  }

  pageChange(event: any){
    this.page = event.page
    this.getAssistants()
  }

  editAssistant(data:any){
    this.mode = "edit"
    this.key = data.id
    this.fname = data.fname
    this.mname = data.mname
    this.lname = data.lname
    this.branch = data.branch_id
    this.isActive = data.is_active

    this.modalAssistants.show()
  }
}
