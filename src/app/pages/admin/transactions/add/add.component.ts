import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import { CommonService } from 'src/app/services/common.service';
import { GrowlService } from 'ngx-growl';
import * as moment from 'moment';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  @ViewChild("modalSearchPatient") public modalSearchPatient: ModalDirective;
  @ViewChild("modalSalesConfirmation") public modalSalesConfirmation: ModalDirective;


  pageTitle : string = "Sales Transaction"

  loggedUserInfo: any = null

  patient_list = []
  doctors_list = []
  procedures_list = []
  assistants_list = []
  sales_list = []
  branch_list = []
  secretaries_list = []

  patient = {id:null,fullname:""}
  doctor;
  procedure= []
  assistants;
  doctor_fee=0;
  mop: string = "CASH"
  amount = 0
  branch = null
  date;
  totalProcedurePrice : any = 0.00

  //searchPatient Variables
  search_lname : string = ""
  search_fname : string = ""
  search_mname : string = ""

  search_res_list = []
  search_res_total : number = 0

  patient_page : number = 1
  patient_row : number = 10

  isSplitOrPartial : boolean = false
  splitPartialArr : any = []
  partialTotal : any = 0

  remarks : string = ""

  constructor(
    private API : ApiService,
    private router: Router,
    private common : CommonService,
    private growlService : GrowlService
  ) { 
    this.loggedUserInfo = this.common.getLoggedInUser()
    this.branch = this.loggedUserInfo.branch
  }

  ngOnInit() {
    this.getPatientList()
    this.getDoctorList()
    this.getProcedures()
    this.getAssistantsList()
    // this.getSalesList()
    this.getBranches()
    this.getSecretariesList()
    this.setPaymentMethod(this.mop)
    this.searchPatientByName()

  }

  getSalesList(){
    this.API.post("sales/get",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.sales_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getPatientList(){
    this.API.post("lookups/get-patients",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.patient_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getDoctorList(){
    this.API.post("lookups/get-doctors",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.doctors_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getProcedures(){
    this.API.post("lookups/get-procedures",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.procedures_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getAssistantsList(){
    this.API.post("lookups/get-assistants",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.assistants_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {


    },
    ).subscribe(
        (response: any) => {   
          this.branch_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getSecretariesList(){
    this.API.post("lookups/get-secretaries",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.secretaries_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  searchPatient(){
    this.modalSearchPatient.show()
  }

  searchPatientByName(){
    this.API.post("patients/search-patient-by-name",
    {
      lname : this.search_lname,
      fname : this.search_fname,
      mname : this.search_mname,
      page : this.patient_page,
      rows : this.patient_row

    },
    ).subscribe(
        (response: any) => {   
          this.search_res_list = response.devMessage
          this.search_res_total = response.total
        },
        (error: any) => {

        },
    )
  }

  pageChange(event: any){
    this.patient_page = event.page
    this.searchPatientByName()
  }

  selectPatient(data:any){
    this.patient.id = data.id
    this.patient.fullname = data.fullname
    this.modalSearchPatient.hide()
  }

  procedureChanged(data){
    this.amount = data.price
    this.staffFee(data)
  }

  staffFee(data){
    this.doctor_fee = (data.commission_rate/100) * this.amount
  }

  getTotalProcedurePrice(){
    this.totalProcedurePrice = 0.00
    for(let i=0;i<this.procedure.length;i++){
      if(this.procedure[i].proc_status == 'done' || this.procedure[i].proc_status == 'paid-pending'){
        this.totalProcedurePrice = parseFloat(this.totalProcedurePrice) + (parseFloat(this.procedure[i].price) * parseInt(this.procedure[i].quantity))
      }
    }

    switch(this.mop){
      case "SPLIT PAYMENT" :
        break;
      case "PARTIAL PAYMENT" :
        break;
      default : 
        this.splitPartialArr[0].amount = this.totalProcedurePrice
        break;
    }

    if(this.mop=="PARTIAL PAYMENT"){
      this.partialPaymentRemainingBalance()
    }
  }

  saveSalesTransaction(){
    if(isNull(this.patient.id)){
      Swal.fire("Oops","Please select a patient","warning")
    }
    else if(isNull(this.branch)){
      Swal.fire("Oops","Please select a branch","warning")
    }
    else if(this.procedure.length==0){
      Swal.fire("Oops","Please select atleast one procedure","warning")
    }
    else{

      this.API.post("sales/save",
      {
        mop           : this.mop,
        branch        : this.branch,
        patient_id    : this.patient.id,
        procedures    : this.procedure,
        total_amount  : this.totalProcedurePrice,
        created_by    : this.loggedUserInfo.id,
        paymentMethod : this.splitPartialArr,
        remarks       : this.remarks
      },
      ).subscribe(
          (response: any) => {   
            this.modalSalesConfirmation.hide()
            Swal.fire({
              title: "Transaction Saved.",
              text: "Add another Transaction?",
              icon: "success",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              confirmButtonText: "Yes",
              cancelButtonText : "Back to List"
            }).then((result) => {
              

              if (result.value) {
                location.reload();
              }
              else{
                this.router.navigate(['/admin/transactions/sales'])

              }
            });
            
          },
          (error: any) => {
  
          },
      )
    }
  }

  cancelButton(){
    Swal.fire({
      title: "Are you sure?",
      text: "Cancel Transaction?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then((result) => {
        if(result.value){
          this.router.navigate(['/admin/transactions/sales'])
        }
      }
    );
  }

  mopChange(){
    let mop = this.mop
    this.splitPartialArr = []
    switch(mop){
      case "SPLIT PAYMENT" :
        this.isSplitOrPartial = true
        break;
      case "PARTIAL PAYMENT" :
        this.isSplitOrPartial = true
        break;
      default : 
        this.isSplitOrPartial = false
        this.setPaymentMethod(mop)
    }
  }

  setPaymentMethod(mop:string){
    this.splitPartialArr = []
    let currTime = moment().format("HH:MM")
    let currDate = moment().format("YYYY-MM-DD")

    let tmp = {
      amount : this.totalProcedurePrice,
      mop : mop,
      payment_date : currDate,
      payment_time : currTime
    }

    this.splitPartialArr.push(tmp)
  }

  addPaymentMethod(){
    let currTime = moment().format("HH:MM")
    let currDate = moment().format("YYYY-MM-DD")
    
    let tmp = {
      amount : this.totalProcedurePrice,
      mop : "CASH",
      payment_date : currDate,
      payment_time : currTime
    }

    this.splitPartialArr.push(tmp)
    this.partialPaymentRemainingBalance()
  }

  partialPaymentRemainingBalance(){
    this.partialTotal = 0
    console.log("wew",this.splitPartialArr)

    for(let i=0;i<this.splitPartialArr.length;i++){
      this.partialTotal += this.splitPartialArr[i].amount
    }

    this.partialTotal = parseFloat(this.totalProcedurePrice)-parseFloat(this.partialTotal)

  }

  deleteRowPayment(data:any){
    this.splitPartialArr.splice(data,1)
    this.partialPaymentRemainingBalance()
  }

  disableSaveButton(){
    if(this.procedure.length==0){
      return true
    }
    else{

      if(this.mop == "SPLIT PAYMENT"){
        if(this.partialTotal <0 || this.partialTotal >0){
          return true
        }
        else{
          return false
        }
      }
      else{
        return false
      }
      
    }

  }

  showConfirmationModal(){
    if(isNull(this.patient.id)){
      Swal.fire("Oops","Please select a patient","warning")
    }
    else if(isNull(this.branch)){
      Swal.fire("Oops","Please select a branch","warning")
    }
    else if(this.procedure.length==0){
      Swal.fire("Oops","Please select atleast one procedure","warning")
    }
    else{
      this.modalSalesConfirmation.show()
    }    
  }

  getDentistName(item){
    for(let i=0;i<this.doctors_list.length;i++){
      if(this.doctors_list[i].id == item){
        return this.doctors_list[i].fullname
      }
    }
  }
  
  getAssistantsName(item){
    for(let i=0;i<this.assistants_list.length;i++){
      if(this.assistants_list[i].id == item){
        return this.assistants_list[i].fullname
      }
    }
  }

  getSecretaryName(item){
    for(let i=0;i<this.secretaries_list.length;i++){
      if(this.secretaries_list[i].id == item){
        return this.secretaries_list[i].fullname
      }
    }
  }
  
  getBranchname(item){
    for(let i=0;i<this.branch_list.length;i++){
      if(this.branch_list[i].id == item){
        return this.branch_list[i].branch_name
      }
    }
  }
}
