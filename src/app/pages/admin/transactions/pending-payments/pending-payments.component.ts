import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import { GrowlService } from 'ngx-growl';
import * as moment from 'moment';

@Component({
  selector: 'app-pending-payments',
  templateUrl: './pending-payments.component.html',
  styleUrls: ['./pending-payments.component.scss']
})
export class PendingPaymentsComponent implements OnInit {
  @ViewChild("modalPayments") public modalPayments: ModalDirective;
  @ViewChild("modalAddPayment") public modalAddPayment: ModalDirective;

  
  pageTitle : string = "Pending Payments"
  widgetSubTitle : string = "Manage pending payments"

  page : number = 1
  rows : number = 10

  pending_payment_list : any = []
  pending_payment_total : number = 0

  view_data : any = []

  amount : any = 0
  mop : any =  "CASH"
  date : any;

  constructor(
    private API : ApiService,
    private growlService : GrowlService
  ) { 
    this.date = moment().format("YYYY-MM-DDTHH:mm")
  }


  ngOnInit() {
    this.getPendingTransactions()
  }

  getPendingTransactions(){
    this.API.post("transactions/get-pending-payments",
    {
      page : this.page,
      rows : this.rows
    },
    ).subscribe(
        (response: any) => {   
          this.pending_payment_list = response.devMessage
          this.pending_payment_total = response.total
        },
        (error: any) => {

        },
    )
  }

  pageChange(event: any){
    this.page = event.page
    this.getPendingTransactions()
  }

  viewDetails(data:any){
    this.view_data = data
    this.modalPayments.show()
  }

  addPayment(){
    this.modalAddPayment.show()
  }
  
  savePayment(){
    if(isNull(this.date) || this.date=="" || this.amount == ""){
      Swal.fire("Oops","Required field(s) Missing!","warning")
    }
    else{
      this.API.post("transactions/add-payment",
        {
          transaction_id  : this.view_data.id,
          remaining_bal : this.view_data.remaining_bal,
          amount : this.amount,
          mop : this.mop,
          date : this.date,
        },
        ).subscribe(
            (response: any) => {   
              if(response.devMessage){
                this.growlService.addSuccess("Payment has been added")
                this.modalAddPayment.hide()
                this.modalPayments.hide()
                this.clearFields()
                this.getPendingTransactions()
              }
  
            },
            (error: any) => {
    
            },
        )
    }
  }

  clearFields(){
    this.amount  = 0
    this.mop  =  "CASH"
    this.date
  }

  checkIfExceed(){
    if(this.view_data.remaining_bal < this.amount){
      this.amount = this.view_data.remaining_bal

      document.getElementById("input_payment_amount").classList.add("is-valid")
    }
    else{
      document.getElementById("input_payment_amount").classList.remove("is-valid")
    }
  }
}
