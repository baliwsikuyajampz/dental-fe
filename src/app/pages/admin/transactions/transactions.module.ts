import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { TransactionsComponent } from "./transactions.component";

import { BsDropdownModule } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { PaginationModule } from 'ngx-bootstrap/pagination';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { SalesComponent } from './sales/sales.component';
import { AddComponent } from './add/add.component';
import { PendingTransactionsComponent } from './pending-transactions/pending-transactions.component';
import { PendingPaymentsComponent } from './pending-payments/pending-payments.component';
import { MomentModule } from 'angular2-moment';

const ROUTE = [
  { 
    path: "", 
    redirectTo : 'sales'
  },
  {
    path : "sales",
    component : SalesComponent
  },
  {
    path : "sales/add",
    component : AddComponent
  },
  {
    path : "pending/list",
    component : PendingTransactionsComponent
  },
  {
    path : "payments/list",
    component : PendingPaymentsComponent
  }
];

@NgModule({
    declarations: [
      TransactionsComponent,
      SalesComponent,
      AddComponent,
      PendingTransactionsComponent,
      PendingPaymentsComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTE),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        PaginationModule.forRoot(),
        TooltipModule.forRoot(),
        NgSelectModule,
        BsDatepickerModule.forRoot(),
        QuillModule,
        MomentModule
    ]
})
export class TransactionsModule { }
