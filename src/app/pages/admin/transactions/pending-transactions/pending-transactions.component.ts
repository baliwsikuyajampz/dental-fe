import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import { GrowlService } from 'ngx-growl';

@Component({
  selector: 'app-pending-transactions',
  templateUrl: './pending-transactions.component.html',
  styleUrls: ['./pending-transactions.component.scss']
})
export class PendingTransactionsComponent implements OnInit {
  @ViewChild("modalReschedule") public modalReschedule: ModalDirective;

  pageTitle : string = "Pending Transactions"
  widgetSubTitle : string = "Manage Pending transactions"

  page : number = 1
  rows : number = 10

  pending_transactions_list = []
  pending_transaction_total = 0

  key  = null
  date : string = null

  constructor(
    private API : ApiService,
    private growlService : GrowlService
  ) { }

  ngOnInit() {
    this.getPendingTransactions()
  }

  getPendingTransactions(){
    this.API.post("transactions/get-pending-transactions",
    {
      page : this.page,
      rows : this.rows
    },
    ).subscribe(
        (response: any) => {   
          this.pending_transactions_list = response.devMessage
          this.pending_transaction_total = response.total
        },
        (error: any) => {

        },
    )
  }

  markAsDone(data:any){
    Swal.fire({
      title: "Are you sure?",
      text: "Mark Transaction as Done??",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then((result) => {
        if(result.value){
          console.log("wew",data)

          this.API.post("transactions/update-pending-transaction",
          {
            id : data.id,
            procedure_id : data.procedure_id,
            transaction_id : data.transaction_id,
            proc_status : data.proc_status
          },
          ).subscribe(
              (response: any) => {   
                if(response.devMessage){
                  this.growlService.addSuccess("Transaction Complete.")
                  this.getPendingTransactions()
                }
              },
              (error: any) => {
      
              },
          )
        }
        
      }
    );
  }

  pageChange(event: any){
    this.page = event.page
    this.getPendingTransactions()
  }

  rescheduleTransaction(data:any){
    this.key = data.id
    this.date = data.covered_date
    this.modalReschedule.show()
  }

  updateSchedule(){
    console.log(this.date)
    this.API.post("transactions/update-schedule",
    {
      key : this.key,
      date : this.date
    },
    ).subscribe(
        (response: any) => {   
          if(response.devMessage){
            this.growlService.addSuccess("Update complete.")
            this.modalReschedule.hide()
            this.getPendingTransactions()
          }
        },
        (error: any) => {

        },
    )
  }
}
