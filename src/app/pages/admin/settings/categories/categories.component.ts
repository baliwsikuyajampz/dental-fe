import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @ViewChild("modalCategories") public modalCategories: ModalDirective;

  pageTitle : string = "Categories"
  widgetSubTitle : string = "Settings for Item Categories"

  page : number = 1;
  rows : number = 10;

  categories_list  : any = []
  categories_count : number = 0

  mode = "add"

  category : string = "";

  key_category : any;

  constructor(
    private API : ApiService
  ) {

  }

  ngOnInit() {
    this.getCategories()
  }

  getCategories(){
    this.API.post("settings/get-categories",
      {
        page : this.page,
        rows : this.rows
      },
    ).subscribe(
        (response: any) => {   
          console.log(response)
          if(response.statusCode==200){
            this.categories_list = response.devMessage
            this.categories_count = response.total
          }
        },
        (error: any) => {

        },
    )
  }

  pageChange(event: any){
    this.page = event.page
    this.getCategories()
  }

  addCategory(){
    this.mode = "add"
    this.modalCategories.show()
  }

  saveCategory(){
    if(this.category == ""){
      Swal.fire("Oops","Required Field(s) Missing","warning")
    }
    else{
      this.API.post("settings/save-category",
        {
          category_name : this.category,
          mode          : this.mode,
          key           : this.key_category
        },
      ).subscribe(
          (response: any) => {   
            console.log(response)
            if(response.statusCode==200){
              Swal.fire("Success",this.mode == 'add' ? "Record has been Saved" : "Record has been Updated","success")
              this.clearFields()
              this.getCategories()
              this.modalCategories.hide()
            }
          },
          (error: any) => {
  
          },
      )
    }
  }

  editCategory(data:any){
    this.mode="edit"
    this.key_category = data.id
    this.category = data.category_name
    this.modalCategories.show()
  }

  clearFields(){
    this.category = ""
    this.mode = "add"
    this.key_category = ""
  }
}
