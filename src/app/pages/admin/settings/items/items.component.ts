import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  @ViewChild("modalItem") public modalItem: ModalDirective;
  @ViewChild('itemAttachment') itemAttachment: any;
  
  pageTitle : string = "Items"
  widgetSubTitle : string = "Settings for Item Information"

  page : number = 1;
  rows : number = 10;

  mode = "add"

  items_list : any = [];
  items_count : number = 0;

  //form
  item_name : string = ""
  item_description : string = ""
  itemFormData = new FormData();
  itemFormDataFlag : boolean = false;


  is_active : boolean = true

  editItemData : any;

  categories_list :any = []
  selected_category : any = null

  constructor(
    private API : ApiService
  ) {

  }

  ngOnInit() {
    this.getItems()
    this.getCategories()
  }

  addItem(){
    this.modalItem.show()
    this.itemFormDataFlag = false
  }

  uploadClickItems(){
    document.getElementById("uploadItemAttachment").click()
  }

  async handleFileUploadItem(event: any) {
    let file = event.target.files[0],
        formData = new FormData();
        this.itemFormData = new FormData()

      this.itemFormData.append('file', file); 
      this.itemFormDataFlag = true
  }

  saveItem(){
    console.log(this.is_active)
    let key = this.editItemData === undefined ? "" : this.editItemData.id

    if(this.item_name == "" || this.selected_category ==null){
      Swal.fire("Oops","Required Field(s) Missing","warning")
    }
    else{
      if(this.itemFormDataFlag==true){
        this.API.post_form_data("uploads/upload-attachment",
        this.itemFormData,
        ).subscribe(
            (response: any) => {   
              if(response.statusCode == 200){
                this.API.post("settings/save-item",
                  {
                    item_name : this.item_name,
                    item_description : this.item_description,
                    attachment : response.devMessage,
                    category_id : this.selected_category.id,
                    mode : this.mode,
                    key  : key,
                    is_active : this.is_active ? "1" : "0"
                  },
                ).subscribe(
                    (response: any) => {   
                      console.log(response)
                      if(response.statusCode==200){
                        this.clearFields()
                        this.getItems()
                        this.modalItem.hide()
                        Swal.fire("Success",this.mode == 'add' ? "Record has been Saved" : "Record has been Updated","success")      
                      }
                    },
                    (error: any) => {
            
                    },
                )
              }
            },
            (error: any) => {
    
            },
        )
      }
      else{
        this.API.post("settings/save-item",
          {
            item_name : this.item_name,
            item_description : this.item_description,
            attachment : "",
            category_id : this.selected_category.id,
            mode : this.mode,
            key  : key,
            is_active : this.is_active ? "1" : "0"
          },
        ).subscribe(
            (response: any) => {   
              console.log(response)
              if(response.statusCode==200){
                this.clearFields()
                this.getItems()
                this.modalItem.hide()
                Swal.fire("Success",this.mode == 'add' ? "Record has been Saved" : "Record has been Updated","success")      
              }
            },
            (error: any) => {
    
            },
        )
      }
    }
  }

  getItems(){
    this.API.post("settings/get-items",
      {
        page : this.page,
        rows : this.rows
      },
    ).subscribe(
        (response: any) => {   
          console.log(response)
          if(response.statusCode==200){
            this.items_list = response.devMessage
            this.items_count = response.total
          }
        },
        (error: any) => {

        },
    )
  }

  clearFields(){
    this.item_name = ""
    this.item_description = ""
    this.itemFormDataFlag = false
    this.itemAttachment.nativeElement.value = '';
  }

  pageChange(event: any){
    this.page = event.page
    this.getItems()
  }

  getCategories(){
    this.API.post("settings/get-categories-list",
      {

      },
    ).subscribe(
        (response: any) => {   
          console.log(response)
          if(response.statusCode==200){
            this.categories_list = response.devMessage
          }
        },
        (error: any) => {

        },
    )
  }

  editItem(data:any){
    this.mode             = "edit"
    this.editItemData     = data
    this.item_name        = data.item_name
    this.item_description = data.item_description
    this.is_active        = data.is_active
    let index = this.categories_list.findIndex((e)=>data.category_id == e.id)
    this.selected_category = this.categories_list[index]

    this.modalItem.show()
  }
}
