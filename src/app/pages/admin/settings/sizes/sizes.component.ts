import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sizes',
  templateUrl: './sizes.component.html',
  styleUrls: ['./sizes.component.scss']
})
export class SizesComponent implements OnInit {
  @ViewChild("modalSize") public modalSize: ModalDirective;

  pageTitle : string = "Sizes"
  widgetSubTitle : string = "Settings for Size Information"

  page : number = 1;
  rows : number = 10;

  sizes_list : any = [];
  sizes_count : number = 0;

  //form
  size_code : string = "";
  size_name : string = ""
  size_key  : any;

  mode = "add"

  constructor(
    private API : ApiService
  ) {

  }

  ngOnInit() {
    this.getSizes()
  }

  getSizes(){
    this.API.post("settings/get-sizes",
      {
        page : this.page,
        rows : this.rows
      },
    ).subscribe(
        (response: any) => {   
          console.log(response)
          if(response.statusCode==200){
            this.sizes_list = response.devMessage
            this.sizes_count = response.total
          }
        },
        (error: any) => {

        },
    )
  }

  pageChange(event: any){
    this.page = event.page
    this.getSizes()
  }

  addSize(){
    this.clearFields()
    this.modalSize.show()
  }

  clearFields(){
    this.mode = "add"
    this.size_key = ""
    this.size_code = ""
    this.size_name = ""
  }

  saveSize(){
    if(this.size_code == "" || this.size_name == ""){
      Swal.fire("Oops","Required Field(s) Missing","warning")
    }
    else{
      this.API.post("settings/save-size",
        {
          size_code : this.size_code,
          size_name : this.size_name,
          mode    : this.mode,
          key     : this.size_key
        },
      ).subscribe(
          (response: any) => {   
            console.log(response)
            if(response.statusCode==200){
              Swal.fire("Success",this.mode == 'add' ? "Record has been Saved" : "Record has been Updated","success")
              this.clearFields()
              this.getSizes()
              this.modalSize.hide()
            }
          },
          (error: any) => {
  
          },
      )
    }
  }

  editSize(data: any){
    this.size_key = data.id
    this.size_code = data.size_code
    this.size_name = data.size_name
    this.mode = "edit"
    this.modalSize.show()
  }

}
