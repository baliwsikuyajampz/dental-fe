import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { TestComponent } from "./test.component";

import { BsDropdownModule } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill'
import { FormTeethComponent } from 'src/app/components/form/form-teeth/form-teeth.component';

const ROUTE = [{ path: "", component: TestComponent }];

@NgModule({
    declarations: [
      TestComponent,
      FormTeethComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTE),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        PaginationModule.forRoot(),
        TooltipModule.forRoot(),
        NgSelectModule,
        QuillModule
    ]
})
export class TestModule { }
