
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  userInfo: any;
  oldPass: any;
  newPass: any;
  confirmPass: any;
  pageTitle : string = "Change Password"

  constructor(
    private API : ApiService
  ) {

   }

  ngOnInit() {
    this.getOnlineUser()
    console.log(this.userInfo)
  }

  getOnlineUser(){
    if(localStorage.getItem("user_info") !== null){
      this.userInfo = JSON.parse(localStorage.getItem("user_info"))
    }
  }

  onChangePassword(){
    
    if(this.oldPass == "" || this.newPass == "" || this.confirmPass == ""){
      Swal.fire("Oops!", "Required fields missing", "warning")
    }
    else if(this.newPass !== this.confirmPass){
      Swal.fire("Oops!","New password didn't match", "warning")
    }
    else{

      
      this.API.post("users/change-password",
        {
        userid: this.userInfo.id,
        oldpass: this.oldPass,
        newpass: this.newPass,
    
        },
        ).subscribe(
            (response: any) => {   
              console.log(response)
              if(response.statusCode==200){
                if(response.devMessage == "pass_mismatch")
                {
                  Swal.fire("Oops!", "Password Mismatch", "warning")
                }else{
                  Swal.fire("Success!","Password Updated","success")
                }
              }
            },
            (error: any) => {
    
            },
      )
    }

    
  }


}
