import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.scss']
})
export class BranchComponent implements OnInit {
  @ViewChild("modalBranch") public modalBranch: ModalDirective;

  pageTitle : string = "Branches"
  widgetSubTitle : string = "Settings for Branch Information"

  page : number = 1;
  rows : number = 10;

  branches_list : any = [];
  branches_count : number = 0;

  mode : string = "add"

  //form
  branch_key  : any;
  branch_code : string = "";
  branch_name : string = "";
  location : string = "";
  address : string = "";
  contact_no : string = "";

  userLoggedInfo: any ;

  constructor(
    private API : ApiService,
    private router : Router
  ) {

  }

  ngOnInit() {
    this.getUserLoggedIn()
    this.getBranches();
    this.getUserLoggedIn();
  }

  getUserLoggedIn(){
    if(localStorage.getItem("user_info")===null){
      this.router.navigate([''])
    }
    else{
      this.userLoggedInfo = JSON.parse(localStorage.getItem("user_info"))
    }
  }

  getBranches(){
    this.API.post("settings/get-branches",
      {
        page : this.page,
        rows : this.rows
      },
    ).subscribe(
        (response: any) => {   
          console.log(response)
          if(response.statusCode==200){
            this.branches_list = response.devMessage
            this.branches_count = response.total
          }
        },
        (error: any) => {

        },
    )
  }

  addBranch(){
    this.clearFields()
    this.modalBranch.show()
  }

  saveBranch(){
    if(this.branch_code == "" || this.branch_name == ""){
      Swal.fire("Oops","Required Field(s) Missing","warning")
    }
    else{
      this.API.post("settings/save-branch",
        {
          branch_code : this.branch_code,
          branch_name : this.branch_name,
          location : this.location,
          contact_no : this.contact_no,
          address : this.address,
          mode    : this.mode,
          key     : this.branch_key,
          created_by : this.userLoggedInfo.id
        },
      ).subscribe(
          (response: any) => {   
            console.log(response)
            if(response.statusCode==200){
              Swal.fire("Success",this.mode == 'add' ? "Record has been Saved" : "Record has been Updated","success")
              this.clearFields()
              this.getBranches()
              this.modalBranch.hide()
            }
          },
          (error: any) => {
  
          },
      )
    }
  }

  clearFields(){
    this.branch_key = ""
    this.branch_code = ""
    this.branch_name = ""
    this.location = ""
    this.contact_no = ""
    this.address = ""
    this.mode = "add"
  }

  pageChange(event: any){
    this.page = event.page
    this.getBranches()
  }

  editBranch(data: any){
    this.branch_key = data.id
    this.branch_code = data.branch_code
    this.branch_name = data.branch_name
    this.location = data.location
    this.contact_no = data.contact_no
    this.address = data.address
    this.mode = "edit"
    this.modalBranch.show()
  }
}
