import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { ListComponent } from "./list.component";

import { BsDropdownModule } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgSelectModule } from '@ng-select/ng-select';


const ROUTE = [{ path: "", component: ListComponent }];

@NgModule({
    declarations: [
      ListComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTE),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        PaginationModule.forRoot(),
        NgSelectModule
    ]
})
export class ListModule { }
