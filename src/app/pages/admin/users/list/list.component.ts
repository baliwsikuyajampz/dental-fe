import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ApiService } from 'src/app/services/api.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @ViewChild("modalUsers") public modalUsers: ModalDirective;

  pageTitle: string = 'User List'
  widgetSubTitle : string = "List of all Users"

  page : number = 1
  rows : number = 10

  users_list : any = [];
  users_count : number = 0;

  branches_lookup : any = [];

  username : string = "";
  fname : string = "";
  mname : string = "";
  lname : string = "";
  key : number;
  email : string = "";

  roles_list : any = []
  role : string = ""
  modeModal : string = ""

  branch_selected : any = null
  role_selected : any = null

  userLoggedInfo : any ;

  constructor(
    private API : ApiService,
    private router : Router
  ) { }

  ngOnInit() {
    this.getUserLoggedIn()
    this.getUserList()
    this.getRoles()
    this.getBranches()
  }

  showModal(){
    this.modeModal = "add"
    this.clearFields()
    this.modalUsers.show()
  }

  getUserLoggedIn(){
    if(localStorage.getItem("user_info")===null){
      this.router.navigate([''])
    }
    else{
      this.userLoggedInfo = JSON.parse(localStorage.getItem("user_info"))
    }
  }

  getUserList(){
    this.API.post("users/get-users",{
      page : this.page,
      rows : this.rows
    }).subscribe(
          (response: any) => {   
            if(response.statusCode == 200){
              this.users_list = response.devMessage
              this.users_count = response.total
            }
          },
          (error: any) => {

          },
    )
  }

  save(){
    if(this.username == "" || this.fname == "" || this.lname == "" || this.role === null || this.branch_selected === null){
      Swal.fire("Oops!","Required field(s) missing.","warning")
    }
    else{
      this.API.post("users/save-user",{
        fname : this.fname,
        mname : this.mname,
        lname : this.lname,
        username : this.username,
        role : this.role_selected.id,
        branch : this.branch_selected.id,
        mode : this.modeModal,
        email : this.email,
        key  : this.key,
        created_by : this.userLoggedInfo.id
      }).subscribe(
            (response: any) => {   
              if(response.statusCode == 200){
                if(response.devMessage == "userExist"){
                  Swal.fire("Oops","Username Already Exists","warning")
                }
                else{
                  if(response.devMessage == true){
                    Swal.fire("Success",this.modeModal == "add" ? "Record Added" : "Record Updated","success")
                    this.getUserList()
                    this.modalUsers.hide()
                  }
                }
              }
            },
            (error: any) => {
  
            },
      )
    }
  }

  editUser(data:any){
    this.modeModal = "edit"
    this.fname = data.fname
    this.mname = data.mname
    this.lname = data.lname
    this.username = data.username
    this.email = data.email
    let roleIndx = this.roles_list.findIndex((e)=>e.id == data.role_id)
    this.role_selected = this.roles_list[roleIndx]

    let branchIndx = this.branches_lookup.findIndex((e)=>e.id == data.branch_id)
    this.branch_selected = this.branches_lookup[branchIndx]

    this.modalUsers.show()
    this.key = data.id
  }

  clearFields(){
    this.fname = "";
    this.mname = "";
    this.lname = "";
    this.username = "";
    this.email = ""
    this.branch_selected = null
    this.role_selected = null
  }

  getRoles(){
    this.API.post("users/get-roles",{

    }).subscribe(
          (response: any) => {   
            if(response.statusCode == 200){
              this.roles_list = response.devMessage
            }
          },
          (error: any) => {

          },
    )
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
      {

      },
    ).subscribe(
        (response: any) => {   
          console.log(response)
          if(response.statusCode==200){
            this.branches_lookup = response.devMessage
          }
        },
        (error: any) => {

        },
    )
  }

  pageChange(event: any){
    this.page = event.page
    this.getUserList()
  }

  setUserAsInactive(data:any){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if(result.value==true){
        this.API.post("users/delete-user",
        {
          id : data.id,
          created_by : this.userLoggedInfo.id
        },
      ).subscribe(
          (response: any) => {   
            if(response.statusCode==200){
              this.getUserList()
              Swal.fire("Success","User Deleted","success")
            }
          },
          (error: any) => {
  
          },
      )
      }
    })
  }
}
