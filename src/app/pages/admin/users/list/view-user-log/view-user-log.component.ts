import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-user-log',
  templateUrl: './view-user-log.component.html',
  styleUrls: ['./view-user-log.component.scss']
})
export class ViewUserLogComponent implements OnInit {
  key : any;

  userInfo : any;

  page : number = 1
  rows : number = 10
  
  auditCount : number = 0 

  constructor(
    private API : ApiService,
    private router : Router,
    private route : ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('id');

    this.getUserByIdLogs()
  }

  getUserByIdLogs(){
    this.API.post("users/get-user-by-id-logs",
      {
        id : this.key,
        page : this.page,
        rows : this.rows
      },
    ).subscribe(
        (response: any) => {   
          console.log(response)
          if(response.statusCode==200){
            this.userInfo = response.devMessage
            this.auditCount = response.total
          }
        },
        (error: any) => {

        },
    )
  }

  pageChange(event: any){
    this.page = event.page
    this.getUserByIdLogs()
  }


}
