import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
  @ViewChild("modalRole") public modalRole: ModalDirective;

  pageTitle : string = "Roles"
  widgetSubTitle : string = "Settings for User Roles"

  mode = "add"

  constructor() { }

  ngOnInit() {
  }

  addRole(){
    this.modalRole.show()
  }
}
