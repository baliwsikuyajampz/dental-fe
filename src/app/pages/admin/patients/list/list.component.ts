import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import {GrowlService} from 'ngx-growl';
import { isNull } from 'util';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild("modalPatient") public modalPatient: ModalDirective;

  pageTitle : string = "Patients"
  widgetSubTitle : string = "Manage Patients"

  patients_list = []
  branches_list = []
  patient_total_count = 0
  key : any = null

  fname : string = ""
  mname : string = ""
  lname : string = ""
  bday : any = null
  gender : string = "Male"
  address : string = ""
  religion : string = ""
  nationality : string = ""
  occupation : string = ""
  office_no : string = ""
  nickname : string = ""
  contact_no : string = ""
  isActive : boolean = true

  guardian_name : string = ""
  guardian_occupation : string = ""

  branch : any  = null

  loggedUserInfo : any = null
  searchKey : string = ""

  page  = 1
  row   = 10
  mode : string = "add"

  constructor(
    private API : ApiService,
    private growlService: GrowlService,
    private common : CommonService
  ) {
      this.loggedUserInfo = this.common.getLoggedInUser()
   }

  ngOnInit() {
    this.getBranches()
    this.getPatientList()
  }

  getPatientList(){
    this.API.post("patients/get",
    {
      page : this.page,
      row  : this.row,
      search : this.searchKey
    },
    ).subscribe(
        (response: any) => {   
          this.patients_list = response.devMessage
          this.patient_total_count = response.total
        },
        (error: any) => {

        },
    )
  }

  addPatient(){
    this.clearFields()
    this.modalPatient.show()
  }

  savePatient(){
    if(this.branch === null){
      Swal.fire("Oops","Please select a branch","warning")
      return
    }
    if(this.fname=="" || this.lname == "" || isNull(this.bday) || this.address == ""){
      Swal.fire("Oops","Required Field(s) Missing","warning")
    }
    else{
      if(this.bday=="Invalid Date"){
        Swal.fire("Oops","Invalid Birthdate","warning")
      }
      else{
        this.API.post("patients/save",
        {
          mode  : this.mode,
          key   : this.key,
          fname : this.fname,
          mname : this.mname,
          lname : this.lname,
          bday : this.bday,
          gender : this.gender,
          address : this.address,
          religion : this.religion,
          nationality : this.nationality,
          occupation : this.occupation,
          office_no : this.office_no,
          created_by : this.loggedUserInfo.id,
          contact_no : this.contact_no,
          is_active : this.isActive,
          guardian_name : this.guardian_name,
          guardian_occupation : this.guardian_occupation,
          branch : this.branch
        },
        ).subscribe(
            (response: any) => {   
              if(response.devMessage == "exists"){
                Swal.fire("Oops","Record Exists","warning")
              }
              else{
                this.modalPatient.hide()
                this.growlService.addSuccess("Record has been saved.")
                this.clearFields()
                this.getPatientList()
              }
            },
            (error: any) => {
    
            },
        )
      }
    }
  }

  pageChange(event: any){
    this.page = event.page
    this.getPatientList()
  }

  editPatient(data:any){
    this.key = data.id
    this.fname = data.fname
    this.mname  = data.mname
    this.lname  = data.lname
    this.bday  = data.birthday_raw
    this.gender  = data.gender
    this.address  = data.address
    this.religion  = data.religion
    this.nationality  = data.nationality
    this.occupation  = data.occupation
    this.office_no  = data.office_no
    this.nickname  = data.nickname
    this.contact_no  = data.contact_no
    this.guardian_name = data.guardian_name
    this.guardian_occupation = data.guardian_occupation
    this.mode = "edit"

    this.modalPatient.show()
  }

  clearFields(){
    this.key = null
    this.fname = ""
    this.mname  = ""
    this.lname  = ""
    this.bday  = ""
    this.gender  = "Male"
    this.address  = ""
    this.religion  = ""
    this.nationality  = ""
    this.occupation  = ""
    this.office_no  = ""
    this.nickname  = ""
    this.contact_no  = ""
    this.mode = "add"
    this.guardian_occupation = ""
    this.guardian_name = ""
    this.branch = null
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {
 
    },
    ).subscribe(
        (response: any) => {   
          this.branches_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  deletePatient(data:any){
    Swal.fire({
      title: "Are you sure?",
      text: "Delete this Record?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then((result) => {
        if(result.value){
          this.API.post("patients/delete-patient",
            {
              key : data.id
            },
            ).subscribe(
                (response: any) => {   
                  if(response.devMessage == "exists"){
                    Swal.fire("Oops","This record cannot be deleted","warning")
                  }
                  else{
                    this.growlService.addSuccess("Record Deleted.")
                    this.getPatientList()
                  }
                },
                (error: any) => {
        
                },
            )
        }
      }
    );
  }

}
