import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { ApiService } from 'src/app/services/api.service';
import { CommonService } from 'src/app/services/common.service';
import { DomSanitizer } from '@angular/platform-browser'
import { GrowlService } from 'ngx-growl';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  key : any = null
  patient_transaction_details = []

  constructor(
    private route : ActivatedRoute,
    private API : ApiService,
    private service: CommonService,
    private sanitizer: DomSanitizer,
    private growlService : GrowlService
  ) { 
    this.service.data$.subscribe(res => this.key = res)
  }

  ngOnInit() {
    this.service.data$.subscribe(res => this.key = res)

    this.getTransactions()
  }

  getTransactions(){
    this.API.post("patients/get-patient-transactions-by-id",
      {
        patient_id : this.key,
      },
      ).subscribe(
          (response: any) => {   
            this.patient_transaction_details = response.devMessage
          },
          (error: any) => {
  
          },
      )
  }

}
