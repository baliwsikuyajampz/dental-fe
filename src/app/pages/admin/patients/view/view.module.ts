import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";

import { BsDropdownModule } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { PaginationModule } from 'ngx-bootstrap/pagination';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill'

import { ViewComponent } from './view.component';

import {AboutComponent} from "./about/about.component";
import {PicturesComponent} from "./pictures/pictures.component";
import {ContactsComponent} from "./contacts/contacts.component";
import {CrystalLightboxModule} from "@crystalui/angular-lightbox";
import { TransactionsComponent } from './transactions/transactions.component';

const ROUTE = [
  { 
    path: "", 
    component: ViewComponent,
    children: [
      {
          path: '',
          redirectTo: 'about',
          pathMatch: 'full'
      },
      {
          path: 'about',
          component: AboutComponent
      },
      {
          path: 'pictures',
          component: PicturesComponent
      },
      {
          path: 'contacts',
          component: ContactsComponent
      },
      {
          path: 'transactions',
          component: TransactionsComponent
      }
    ]
  },
  
];

@NgModule({
    declarations: [
      ViewComponent,
      AboutComponent,
      PicturesComponent,
      ContactsComponent,
      TransactionsComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTE),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        PaginationModule.forRoot(),
        TooltipModule.forRoot(),
        NgSelectModule,
        QuillModule,
        CrystalLightboxModule,
        
    ]
})
export class ViewModule { }
