import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { CommonService } from 'src/app/services/common.service';
import { GrowlService } from 'ngx-growl';

@Component({
  selector: 'app-pictures',
  templateUrl: 'pictures.component.html'
})
export class PicturesComponent implements OnInit {
  key = null
  picturesItems = new Array(24); // Number of pictures currently available is 24
  patient_attachments : any = []

  constructor(
    private route : ActivatedRoute,
    private API : ApiService,
    private service: CommonService,
    private growlService : GrowlService
  ) {
    this.service.data$.subscribe(res => this.key = res)
   }

  ngOnInit() {
    this.getPatientAttachment()
  }

  getPatientAttachment(){
    this.API.post("patients/get_upload_attachment",
      {
        key : this.key
      },
      ).subscribe(
          (response: any) => {   
            this.patient_attachments = response.devMessage
          },
          (error: any) => {
  
          },
      )
  }

}
