import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { RanksComponent } from "./ranks.component";

import { BsDropdownModule } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill'
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


const ROUTE = [{ path: "", component: RanksComponent }];

@NgModule({
    declarations: [
      RanksComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTE),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        PaginationModule.forRoot(),
        TooltipModule.forRoot(),
        NgSelectModule,
        QuillModule,
        AccordionModule.forRoot(),
        BsDatepickerModule.forRoot()
        

    ]
})
export class RanksModule { }
