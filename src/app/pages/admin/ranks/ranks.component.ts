import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import {GrowlService} from 'ngx-growl';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-ranks',
  templateUrl: './ranks.component.html',
  styleUrls: ['./ranks.component.scss']
})
export class RanksComponent implements OnInit {
  @ViewChild("modalRanks") public modalRanks: ModalDirective;

  pageTitle : string = "Ranks"
  widgetSubTitle : string = "Manage Ranks for Dentists"

  key : number = null
  rank_name : string = ""
  procedure : any = []
  isActive : boolean = true

  mode : string = "add"
  procedures_list = []

  page : number = 1
  rows : number = 10

  rank_list = []
  total : number = 0

  constructor(private API : ApiService,private growlService : GrowlService) { }

  ngOnInit() {
    this.getProcedures()
    this.getRanks()
  }

  addRank(){
    this.clearFields()
    this.modalRanks.show()
  }

  getProcedures(){
    this.API.post("lookups/get-procedures-ranks",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.procedures_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  saveRanks(){
    if(this.rank_name == ''){
      Swal.fire("Oops","Required field(s) Missing!","warning")
    }
    else{
      this.API.post("ranks/save",
        {
          mode      : this.mode,
          key       : this.key,
          rank_name : this.rank_name,
          procedure : this.procedure,
          is_active : this.isActive
        },
        ).subscribe(
            (response: any) => {   
              if(response.devMessage){
                let msg = this.mode == "add" ? "Record Saved." : "Record Updated."
                this.growlService.addSuccess(msg)
                this.modalRanks.hide()
                this.clearFields()
                this.getRanks()
              }
            },
            (error: any) => {
    
            },
        )
    }
  }

  clearFields(){
    this.mode = 'add'
    this.rank_name = ""
    this.procedure = []
    this.isActive = true
    this.key = null
  }

  getRanks(){
    this.API.post("ranks/get",
      {
        page : this.page,
        rows : this.rows
      },
      ).subscribe(
          (response: any) => {   
            this.rank_list = response.devMessage
            this.total = response.total
          },
          (error: any) => {
  
          },
      )
  }

  editRank(data:any){
    this.mode = 'edit'
    this.rank_name = data.rank_name
    this.procedure = data.procedure
    this.key = data.id
    this.isActive = data.is_active
    this.modalRanks.show()
  }

  pageChange(event: any){
    this.page = event.page
    this.getRanks()
  }
}
