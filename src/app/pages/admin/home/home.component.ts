import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  pageTitle: string = 'Dashboard';
  pageSubTitle: string = 'Business Overview';

  filter_branch_selected : any = 'all';
  userLoggedInfo : any;

  sales_data : any;

  startDate : any;
  endDate : any;

  constructor(
    private API : ApiService,
    private router : Router
  ) {

  }

  ngOnInit() {
    // this.getCurrDate()
    // this.getUserLoggedIn()
    // this.getSalesChart()
  }

}
