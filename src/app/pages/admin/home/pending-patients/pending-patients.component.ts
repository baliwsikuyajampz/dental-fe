import { Component, OnInit } from '@angular/core';
import { ILineChartOptions, IChartistData } from "chartist";
import { ChartType } from "ng-chartist"
import { ApiService } from 'src/app/services/api.service';


@Component({
  selector: 'app-pending-patients',
  templateUrl: './pending-patients.component.html',
  styleUrls: ['./pending-patients.component.scss']
})
export class PendingPatientsComponent implements OnInit {
  widgetTitle : string = "Pending Patients"
  pending_patient_list = []

  constructor(
    private API : ApiService
  ) {}


  ngOnInit() {
    this.getPendingPatients()
  }

  getPendingPatients(){
    this.API.post("dashboard/get-pending-patients-list",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.pending_patient_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

}
