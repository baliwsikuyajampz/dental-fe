import { Component, OnInit, Input } from '@angular/core';
import {
    IPieChartOptions,
    IChartistData
} from 'chartist';
import { ChartType } from 'ng-chartist';
import * as ctLegends from 'chartist-plugin-legend/chartist-plugin-legend.js';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-branch-pie',
  templateUrl: './branch-pie.component.html',
  styleUrls: ['./branch-pie.component.scss']
})
export class BranchPieComponent implements OnInit {
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];;
  date = new Date();
  monthYear =  this.months[this.date.getMonth()] + ' ' + this.date.getFullYear();

  data: any;
  chartTitle: string = 'Sales of ' + this.monthYear
  chartSubTitle: string = 'Top 5 selling products'

  type: ChartType = 'Pie';


  options: IPieChartOptions = {
    showLabel: false,
    startAngle: 270,
    plugins: [
      ctLegends({
        position: 'bottom'
      })
    ]
  };

  constructor(
    private API : ApiService,
  ) {

  }

  ngOnInit() {
    this.getMonthlySales()
  }

  getMonthlySales(){
    this.API.post("dashboard/get-monthly-sales",
    {

    },
    ).subscribe(
        (response: any) => { 
          this.data = response.devMessage  
        },
        (error: any) => {

        },
    )
  }


}
