import { Component, OnInit } from '@angular/core';
import { ILineChartOptions, IChartistData } from "chartist";
import { ChartType } from "ng-chartist"
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'widget-quick-stats',
  templateUrl: 'quick-stats.component.html',
  styleUrls: ['quick-stats.component.scss']
})
export class QuickStatsComponent implements OnInit {

  daily_sales : any = null
  daily_expenses : any = null
  pending_clients : any = null
  unpaid_amount : any = null
  grand_total : any = null

  // Chart
  type: ChartType = "Bar";
  options: ILineChartOptions = {
    height: "36px",
    width: "65px",
    showPoint: false,
    fullWidth: true,
    chartPadding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    },
    axisX: {
      showGrid: false,
      showLabel: false,
      offset: 0
    },
    axisY: {
      showGrid: false,
      showLabel: false,
      offset: 0
    }
  };

  constructor(
    private API : ApiService
  ) {}

  ngOnInit() {
    this.getDailySales()
    this.getDailyExpenses()
    this.getPendingClients()
    this.getTotalUnpaidAmount()
    this.getGrandTotal()
  }

  getDailySales(){
    this.API.post("dashboard/get-daily-sales",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.daily_sales = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getDailyExpenses(){
    this.API.post("dashboard/get-daily-expenses",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.daily_expenses = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getPendingClients(){
    this.API.post("dashboard/get-pending-clients",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.pending_clients = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getTotalUnpaidAmount(){
    this.API.post("dashboard/get-total-unpaid-amount",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.unpaid_amount = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getGrandTotal(){
    this.API.post("dashboard/get-grand-total",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.grand_total = response.devMessage
        },
        (error: any) => {

        },
    )
  }
}
