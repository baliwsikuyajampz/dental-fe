import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {ChartistModule} from "ng-chartist";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BranchPieComponent } from "./branch-pie/branch-pie.component";
import { PartialPaymentsComponent } from "./partial-payments/partial-payments.component";

import { HomeComponent } from "./home.component";
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { QuickStatsComponent } from "./quick-stats/quick-stats.component";
import { PendingPatientsComponent } from "./pending-patients/pending-patients.component";

const CHARTS_ROUTE = [{path: "", component: HomeComponent}];

@NgModule({
    declarations: [
      HomeComponent,
      BranchPieComponent,
      PartialPaymentsComponent,
      QuickStatsComponent,
      PendingPatientsComponent
    ],
    imports: [
      CommonModule, 
      ChartistModule, 
      RouterModule.forChild(CHARTS_ROUTE),
      NgSelectModule,
      FormsModule,
      ReactiveFormsModule,
      AccordionModule.forRoot(),

    ]
})
export class HomeModule {
}

