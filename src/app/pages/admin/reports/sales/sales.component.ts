import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import {GrowlService} from 'ngx-growl';
import * as moment from 'moment';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

  pageTitle : string = "Sales"
  widgetSubTitle : string = "Sales Report"

  daily_sales_list : any = []
  monthly_sales_list : any = []
  annual_sales_list : any = []

  branches_list : any = []


  dailyDateSelected : any;

  

  constructor(private API : ApiService) { 
    
  }

  ngOnInit() {
    this.getBranches()
    this.dailyDateSelected = moment().format("YYYY-MM-DD")
    this.getDailySales()
    this.getMonthlySales()
    this.getAnnualSales()
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {


    },
    ).subscribe(
        (response: any) => {   
          this.branches_list = response.devMessage
          for (let i=0;i<this.branches_list.length;i++){
            this.declareVar(this.branches_list[i].id)
          }
        },
        (error: any) => {

        },
    )
  }

  declareVar(varname : string){
    this[varname+"__page"] = 1
    this[varname+"__rows"] = 10

    this["monthly__"+varname+"__page"] = 1
    this["monthly__"+varname+"__rows"] = 10

    this["annual__"+varname+"__page"] = 1
    this["annual__"+varname+"__rows"] = 10
  }

  getDailySales(){
    this.API.post("reports/get-daily-sales",
    {
      date : this.dailyDateSelected
    },
    ).subscribe(
        (response: any) => {   
          this.daily_sales_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getMonthlySales(){
    this.API.post("reports/get-montly-sales",
      {
  
      },
      ).subscribe(
          (response: any) => {   
            this.monthly_sales_list = response.devMessage
          },
          (error: any) => {
  
          },
      )
  }

  getAnnualSales(){
    this.API.post("reports/get-annual-sales",
      {
  
      },
      ).subscribe(
          (response: any) => {   
            this.annual_sales_list = response.devMessage
          },
          (error: any) => {
  
          },
      )
  }

  pageChange(branchId,event: any,index : any){
    this[branchId+"__page"] = event.page
    this.getDailySalesDetailsByPageChange(this[branchId+"__page"],branchId,index)
  }

  getDailySalesDetailsByPageChange(page,branchId,index){
    this.API.post("reports/get-daily-sales-details-by-page-change",
      {
        date : this.dailyDateSelected,
        page : page,
        branch : branchId
      },
      ).subscribe(
          (response: any) => {   
            this.daily_sales_list[index].details = response.devMessage
          },
          (error: any) => {
  
          },
      )
  }

  pageChangeMonthly(branchId,event: any,index : any){
    this["monthly__"+branchId+"__page"] = event.page
    this.getMonthlySalesDetailsByPageChange(this["monthly__"+branchId+"__page"],branchId,index)
  }
  
  getMonthlySalesDetailsByPageChange(page,branchId,index){
    this.API.post("reports/get-monthly-sales-details-by-page-change",
      {
        page : page,
        branch : branchId
      },
      ).subscribe(
          (response: any) => {   
            this.monthly_sales_list[index].details = response.devMessage
          },
          (error: any) => {
  
          },
      )
  }

  pageChangeAnnual(branchId,event: any,index : any){
    this["annual__"+branchId+"__page"] = event.page
    this.getAnnualSalesDetailsByPageChange(this["annual__"+branchId+"__page"],branchId,index)
  }

  getAnnualSalesDetailsByPageChange(page,branchId,index){
    this.API.post("reports/get-annual-sales-details-by-page-change",
      {
        page : page,
        branch : branchId
      },
      ).subscribe(
          (response: any) => {   
            this.annual_sales_list[index].details = response.devMessage
          },
          (error: any) => {
  
          },
      )
  }

}
