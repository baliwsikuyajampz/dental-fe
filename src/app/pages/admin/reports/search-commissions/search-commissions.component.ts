import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import {GrowlService} from 'ngx-growl';
import { resetCompiledComponents } from '@angular/core/src/render3/jit/module';

@Component({
  selector: 'app-search-commissions',
  templateUrl: './search-commissions.component.html',
  styleUrls: ['./search-commissions.component.scss']
})
export class SearchCommissionsComponent implements OnInit {
  pageTitle : string = "Search Commissions"
  widgetSubTitle : string = "Filtering for Commissions"

  //dentist
  doctors_list : any = []

  doctors : any = []
  from : any;
  to : any
  pageDoctor : number = 1
  rowsDoctor : number = 10

  dentist_commission_list = []
  dentist_commission_total : number = 0

  assistants_list : any = []
  assistants : any = []

  assistantDateFrom : any;
  assistantDateTo : any

  pageAssist : number = 1
  rowsAssist : number = 10

  assistant_commission_list = []
  assistant_commission_total : number = 0

  secretaries_list : any = []
  secretaries : any = []

  secretaryDateFrom : any;
  secretaryDateTo : any;

  pageSecretary : number = 1
  rowsSecretary : number = 10

  secretary_commission_list = []
  secretary_commission_total = 0

  constructor(private API : ApiService) { 
    this.from = new Date()
    this.to = new Date()
    this.assistantDateFrom = new Date()
    this.assistantDateTo = new Date()
    this.secretaryDateFrom = new Date()
    this.secretaryDateTo = new Date()
  }

  //DENTIST
  ngOnInit() {
    this.getDoctorList()
    this.getAssistantsList()
    this.getSecretariesList()
    this.getDoctorCommissionsSearch()
    this.getAssistantsCommissionsSearch()
    this.getSecretaryCommissionsSearch()
  }

  getDoctorCommissionsSearch(){
    this.API.post("reports/get-commissions-search",
    {
      doctors : this.doctors,
      from : this.from,
      to : this.to,
      page : this.pageDoctor,
      rows : this.rowsDoctor
    },
    ).subscribe(
        (response: any) => {   
          this.dentist_commission_list = response.devMessage
          this.dentist_commission_total = response.total
        },
        (error: any) => {

        },
    )
  }

  getDoctorList(){
    this.API.post("lookups/get-doctors",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.doctors_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  pageChangeDentist(event: any){
    this.pageDoctor = event.page
    this.getDoctorCommissionsSearch()
  }
  //END DENTIST

  //ASSISTANTS
  getAssistantsList(){
    this.API.post("lookups/get-assistants",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.assistants_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getAssistantsCommissionsSearch(){
    this.API.post("reports/get-assistants-commissions-search",
    {
      assistants : this.assistants,
      from : this.assistantDateFrom,
      to : this.assistantDateTo,
      page : this.pageAssist,
      rows : this.rowsAssist
    },
    ).subscribe(
        (response: any) => {   
          this.assistant_commission_list = response.devMessage
          this.assistant_commission_total = response.total
        },
        (error: any) => {

        },
    )
  }

  pageChangeAssistant(event: any){
    this.pageAssist = event.page
    this.getAssistantsCommissionsSearch()
  }

  //END ASSISTANTS

  //SECRETARY

  getSecretariesList(){
    this.API.post("lookups/get-secretaries",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.secretaries_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }
  
  getSecretaryCommissionsSearch(){
    this.API.post("reports/get-secretary-commissions-search",
    {
      secretaries : this.secretaries,
      from : this.secretaryDateFrom,
      to : this.secretaryDateTo,
      page : this.pageSecretary,
      rows : this.rowsSecretary
    },
    ).subscribe(
        (response: any) => {   
          this.secretary_commission_list = response.devMessage
          this.secretary_commission_total = response.total
        },
        (error: any) => {

        },
    )
  }

  pageChangeSecretary(event: any){
    this.pageSecretary = event.page
    this.getSecretaryCommissionsSearch()
  }

  

}
