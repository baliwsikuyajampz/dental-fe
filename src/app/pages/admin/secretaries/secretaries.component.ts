import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import {GrowlService} from 'ngx-growl';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-secretaries',
  templateUrl: './secretaries.component.html',
  styleUrls: ['./secretaries.component.scss']
})
export class SecretariesComponent implements OnInit {
  @ViewChild("modalSecretary") public modalSecretary: ModalDirective;

  loggedUserInfo : any = null
  branch_list = []
  secretary_list = []
  total : number = 0

  branch : any = null
  fname : string = ""
  mname : string = ""
  lname : string = ""
  isActive : boolean = true

  key : number = null
  mode : string = "add"

  page : number = 1
  rows : number = 10
  
  searchKey : string = ""

  pageTitle : string = "Secretaries"
  widgetSubTitle : string = "Manage Secretaries"

  constructor(
    private API : ApiService,
    private growlService : GrowlService,
    private common : CommonService
  ) { 
    this.loggedUserInfo = this.common.getLoggedInUser()
  }

  ngOnInit() {
    this.getBranches()
    this.getSecretary()
  }

  addSecretary(){
    this.modalSecretary.show()
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {


    },
    ).subscribe(
        (response: any) => {   
          this.branch_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  saveSecretary(){
    if(this.fname == "" || this.lname == "" || isNull(this.branch)){
      Swal.fire("Oops","Required field(s) missing!","warning")
    }
    else{
      this.API.post("secretary/save",
      {
        fname : this.fname,
        mname : this.mname,
        lname : this.lname,
        branch: this.branch,
        key   : this.key,
        mode  : this.mode,
        is_active : this.isActive,
        created_by : this.loggedUserInfo.id
      },
      ).subscribe(
          (response: any) => {   
            if(response.devMessage){
              let msg = this.mode == "add" ? "Record Saved" : "Record Updated"
              this.growlService.addSuccess(msg)
              this.modalSecretary.hide()
              this.getSecretary()
              this.clearFields()
            }
          },
          (error: any) => {
  
          },
      )
    }
  }

  getSecretary(){
    this.API.post("secretary/get",
    {
      search : this.searchKey,
      page : this.page,
      rows : this.rows
    },
    ).subscribe(
        (response: any) => {   
          this.secretary_list = response.devMessage
          this.total = response.total
        },
        (error: any) => {

        },
    )
  }

  editSecretary(data:any){
    this.mode = "edit"
    this.key = data.id
    this.fname = data.fname
    this.mname = data.mname
    this.lname = data.lname
    this.branch = data.branch_id
    this.isActive = data.is_active

    this.modalSecretary.show()
  }

  clearFields(){
    this.mode = "add"
    this.key = null
    this.fname = ""
    this.mname = ""
    this.lname = ""
    this.branch = null
    this.isActive = true
  }

  pageChange(event: any){
    this.page = event.page
    this.getSecretary()
  }

}
