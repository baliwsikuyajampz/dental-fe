import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import {GrowlService} from 'ngx-growl';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { isNull } from 'util';
import { CommonService } from 'src/app/services/common.service';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @ViewChild("modalExpenseCategory") public modalExpenseCategory: ModalDirective;

  loggedUserInfo : any = null

  pageTitle : string = "Expenses Category"
  widgetSubTitle : string = "Manage Expenses Category"

  page : number = 1
  rows : number = 10

  mode : string = "add"
  expenseCategoryName : string = ""
  isActive : boolean = true
  key : number = null

  expense_categories_list = []
  expense_categories_total : number = 0

  searchKey : string = ""

  constructor(
    private API : ApiService,
    private growlService: GrowlService,
    private common : CommonService
  ) { 
    this.loggedUserInfo = this.common.getLoggedInUser()
  }

  ngOnInit() {
    this.loadInit()
    
  }

  loadInit(){
    this.clearFields()
    this.getExpenseCategories()
  }
  
  clearFields(){
    this.mode = "add"
    this.expenseCategoryName = ""
    this.isActive = true
    this.key = null
  }

  addExpenseCategory(){
    this.mode = "add"
    this.modalExpenseCategory.show()
  }

  saveExpenseCategory(){
    if(this.expenseCategoryName == ""){
      Swal.fire("Oops","Required Field(s) Missing!","warning")
    }
    else{
      this.API.post("expenses/save-categories",
      {
        expense_category_name : this.expenseCategoryName,
        is_active             : this.isActive,
        mode                  : this.mode,
        key                   : this.key,
        created_by            : this.loggedUserInfo.id
      },
      ).subscribe(
          (response: any) => {   
            this.modalExpenseCategory.hide()
            let msg = this.mode == "add" ? "Record Saved." : "Record Updated."
            this.growlService.addSuccess(msg)
            this.loadInit()
          },
          (error: any) => {
  
          },
      )
    }
  }

  getExpenseCategories(){
    this.API.post("expenses/get-categories-list",
    {
      page : this.page,
      rows : this.rows,
      search : this.searchKey
    },
    ).subscribe(
        (response: any) => {   
          this.expense_categories_list = response.devMessage
          this.expense_categories_total = response.total
        },
        (error: any) => {

        },
    )
  }

  onEdit(data:any){
    this.mode = "edit"
    this.key = data.id
    this.expenseCategoryName = data.expense_category_name
    this.isActive = data.is_active == 1 ? true : false
    this.modalExpenseCategory.show()
  }

  pageChange(event: any){
    this.page = event.page
    this.getExpenseCategories()
  }
}
