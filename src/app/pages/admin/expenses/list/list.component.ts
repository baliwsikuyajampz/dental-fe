import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import {GrowlService} from 'ngx-growl';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { isNull } from 'util';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild("modalExpenses") public modalExpenses: ModalDirective;

  pageTitle : string = "Expenses"
  widgetSubTitle : string = "Manage Expenses"

  loggedUserInfo : any = null

  category_list = [];
  branches_list = [];
  expenses_list = [];

  total : number = 0;

  toggleSwitchStatus : boolean = true;

  mode : string = "add"

  page : number = 1
  rows : number = 10
  key : number = null

  branch = null
  label : string = ""
  amount : number = 0
  category = null
  date : string = null
  is_company_expense : boolean = false

  searchKey : string = ""

  bsConfig: Partial<BsDatepickerConfig> = {
    containerClass: 'date-picker',
  }

  constructor(
    private API : ApiService,
    private growlService: GrowlService,
    private common : CommonService
  ) { 
    this.loggedUserInfo = this.common.getLoggedInUser()
  }

  ngOnInit() {
    this.loadInit()
  }

  loadInit(){
    this.clearFields()
    this.getCategories()
    this.getBranches()
    this.get()
  }

  getCategories(){
    this.API.post("expenses/get-categories",
    {
 
    },
    ).subscribe(
        (response: any) => {   
          this.category_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {
 
    },
    ).subscribe(
        (response: any) => {   
          this.branches_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  get(){
    this.API.post("expenses/get",
    {
      page : this.page,
      rows : this.rows,
      search : this.searchKey
    },
    ).subscribe(
        (response: any) => {   
          this.expenses_list = response.devMessage
          this.total = response.total
        },
        (error: any) => {

        },
    )
  }

  addExpense(){
    this.modalExpenses.show()
  }

  clearFields(){
    this.mode = "add"
    this.branch = null
    this.label = null
    this.amount = 0
    this.category = null
    this.date = null
    this.is_company_expense = false
    this.key = null
  }

  saveExpense(){
    if(isNull(this.branch) || isNull(this.amount) || isNull(this.category) || isNull(this.date)){
      Swal.fire("Oops","Required Field(s) Missing!","warning")
    }
    else if(this.amount <= 0){
      Swal.fire("Oops","Invalid amount value","warning")
    }
    else{
      this.API.post("expenses/save",
      {
        mode               : this.mode,
        key                : this.key,
        branch             : this.branch,
        label              : this.label,
        amount             : this.amount,
        category           : this.category,
        date               : this.date,
        is_company_expense : this.is_company_expense,
        created_by         : this.loggedUserInfo.id
      },
      ).subscribe(
          (response: any) => {   
            let msg = this.mode == "add" ? "Record Saved." : "Record Updated."
            this.growlService.addSuccess(msg)
            this.modalExpenses.hide()
            this.loadInit()
          },
          (error: any) => {
  
          },
      )
    }
  }

  onEdit(data:any){
    this.mode = "edit"
    this.key = data.id
    this.branch = data.branch_id
    this.label = data.expense_name
    this.amount = data.amount
    this.category = data.expense_category_id
    this.date = data.covered_date_raw
    this.is_company_expense = data.is_company_expense == 0 ? false : true

    this.modalExpenses.show()
  }

  pageChange(event: any){
    this.page = event.page
    this.get()
  }

  deleteExpense(data : any){
    
    Swal.fire({
      title: "Are you sure?",
      text: "Delete this Record?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then((result) => {
        if(result.value){
          this.API.post("expenses/delete-record",
            {
              key : data.id
            },
            ).subscribe(
                (response: any) => {   
                  this.growlService.addSuccess("Record Deleted.")
                  this.get()
                },
                (error: any) => {
        
                },
            )
        }
      }
    );
    
  }
}
