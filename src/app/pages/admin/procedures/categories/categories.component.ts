import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import { GrowlService } from 'ngx-growl';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @ViewChild("modalCategory") public modalCategory: ModalDirective;
  loggedUserInfo : any = null
  mode : string = "add"

  key : number = null
  category_name : string = ""
  category_desc : string = ""

  category_list = [];
  category_total : number = 0

  page : number = 1
  rows : number = 10

  searchKey : string = ""

  pageTitle : string = "Procedures Categories"
  widgetSubTitle : string = "Manage Procedures Categories"

  constructor(
    private API : ApiService,
    private growlService: GrowlService,
    private common : CommonService
  ) {
    this.loggedUserInfo = this.common.getLoggedInUser()
   }

  ngOnInit() {
    this.getCategories()
  }

  addCategory(){
    this.clearFields()
    this.modalCategory.show()
  }

  saveCategory(){
    if(this.category_name == ""){
      Swal.fire("Oops","Require field(s) missing!","warning")
    }
    else{
      this.API.post("procedures/category/save",
      {
        mode : this.mode,
        key   : this.key,
        category_name : this.category_name,
        category_desc : this.category_desc,
        created_by : this.loggedUserInfo.id
      },
      ).subscribe(
          (response: any) => {   
            if(response.statusCode == 200){
              this.growlService.addSuccess("Record Saved.")
              this.modalCategory.hide()
              this.getCategories()
              this.clearFields()
            }
          },
          (error: any) => {
  
          },
      )
    }
  }

  getCategories(){
    this.API.post("procedures/category/get",
    {
      page : this.page,
      rows : this.rows,
      search : this.searchKey
    },
    ).subscribe(
        (response: any) => {   
          this.category_list = response.devMessage
          this.category_total = response.total
        },
        (error: any) => {

        },
    )
  }
  
  clearFields(){
    this.mode = "add"
    this.key = null
    this.category_name = ""
    this.category_desc = ""
    this.searchKey = ""
  }

  editCategory(data:any){
    this.mode = "edit"
    this.key = data.id
    this.category_name = data.procedure_name
    this.category_desc = data.procedure_desc
    this.modalCategory.show()
  }

  pageChange(event: any){
    this.page = event.page
    this.getCategories()
  }

}
