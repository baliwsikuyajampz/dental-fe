import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import { GrowlService } from 'ngx-growl';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild("modalProcedures") public modalProcedures: ModalDirective;

  pageTitle : string = "Procedures List"
  widgetSubTitle : string = "Manage Procedures List"

  loggedUserInfo : any = null

  page  = 1
  row   = 10
  mode : string = "add"

  procedure_list = []
  procedure_parents_list = []
  procedure_quantity_req = []
  
  procedure_total = 0

  parent_id : number = null

  commission_type : string = "percentage"
  commission_rate : number = 0.00
  assistants_rate : number = 0.00
  assistants_type : string = "fixed"
  secretary_rate  : number = 0
  secretary_type  : string = "fixed"

  price : number = 0.00
  procedure_desc : string = ""
  procedure_name : string = ""
  key : number;

  isDentistAllowed : boolean = true;
  isAssistantAllowed : boolean = true
  isSecretaryAllowed : boolean = true
  haveQuantity : boolean = false
  quantityComsThreshold : number = 0
  greaterThanQty : number = 0
  lessThanQty : number = 0

  searchKey : string = ""

  constructor(
    private API : ApiService,
    private growlService: GrowlService,
    private common : CommonService
  ) { 
    this.loggedUserInfo = this.common.getLoggedInUser()
  }

  ngOnInit() {
    this.loadInit()
  }

  loadInit(){
    this.getProcedureList()
    this.getProcedureParents()
    this.clearFields()
  }

  addProcedure(){
    this.clearFields()
    this.modalProcedures.show()
  }

  getProcedureList(){
    this.API.post("procedures/get",
    {
      page : this.page,
      row  : this.row,
      search : this.searchKey
    },
    ).subscribe(
        (response: any) => {   
          this.procedure_list = response.devMessage
          this.procedure_total = response.total
        },
        (error: any) => {

        },
    )
  }

  saveProcedure(){
    if( isNull(this.parent_id) || this.procedure_name == ""){
      Swal.fire("Oops","Required Field(s) Missing!","warning")
    }
    else{
      this.API.post("procedures/save",
      {
        mode : this.mode,
        parent_id       : this.parent_id,
        key             : this.key,
        commission_type : this.commission_type,
        commission_rate : this.commission_rate,
        procedure_desc : this.procedure_desc,
        price : this.price,
        procedure_name : this.procedure_name,
        assistants_rate : this.assistants_rate,
        assistants_type : this.assistants_type,
        secretary_rate : this.secretary_rate,
        secretary_type : this.secretary_type,
        isDentistAllowed : this.isDentistAllowed,
        isAssistantAllowed : this.isAssistantAllowed,
        isSecretaryAllowed : this.isSecretaryAllowed,
        haveQuantity : this.haveQuantity,
        greaterThanQty : this.greaterThanQty,
        lessThanQty : this.lessThanQty,
        quantityComsThreshold : this.haveQuantity? this.quantityComsThreshold : 0,
        procedure_quantity_req : this.procedure_quantity_req,
        created_by : this.loggedUserInfo.id
      },
      ).subscribe(
          (response: any) => {   
            if(response.devMessage){
              let msg = this.mode == "add" ? "Record Saved." : "Record Updated."
              this.growlService.addSuccess(msg)
              this.modalProcedures.hide()
              this.loadInit()
            }
            
          },
          (error: any) => {
  
          },
      )
    }
  }

  clearFields(){
    this.mode = "add"
    this.haveQuantity = false
    this.commission_type = "percentage"
    this.commission_rate = 0.00
    this.procedure_desc = ""
    this.price = 0.00
    this.procedure_name = ""
    this.key = null
    this.parent_id = null
    this.assistants_type = "fixed"
    this.assistants_rate = 0.00

    this.isDentistAllowed  = true;
    this.isAssistantAllowed  = true
    this.isSecretaryAllowed  = true
    this.quantityComsThreshold = 0
    this.haveQuantity = false
    this.greaterThanQty=0
    this.lessThanQty =0
    this.procedure_quantity_req = []
  }
  
  pageChange(event: any){
    this.page = event.page
    this.getProcedureList()
  }

  onEdit(data:any){
    this.mode = "edit"
    this.key = data.id
    this.parent_id = data.parent_id
    this.commission_type = data.commission_type
    this.commission_rate = data.commission_rate
    this.procedure_desc = data.procedure_desc
    this.price = data.price
    this.procedure_name = data.procedure_name
    this.assistants_type = data.assistants_type
    this.assistants_rate = data.assistants_rate
    this.secretary_rate = data.secretary_rate
    this.secretary_type = data.secretary_type

    this.isDentistAllowed = data.isDentistAllowed
    this.isAssistantAllowed = data.isAssistantAllowed
    this.isSecretaryAllowed = data.isSecretaryAllowed
    this.quantityComsThreshold = data.quantityComsThreshold
    this.haveQuantity = data.haveQuantity
    this.procedure_quantity_req = data.procedure_quantity_req
    this.modalProcedures.show()
  }

  getProcedureParents(){
    this.API.post("lookups/get-procedure-parents",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.procedure_parents_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  addQtyReq(){
    let tmp = {
      qty : this.procedure_quantity_req.length+1,
      dentist_rate : 0,
      assistant_rate : 0,
      secretary_rate : 0
    }

    this.procedure_quantity_req.push(tmp)
  }

  deleteRowQty(data:any){
    this.procedure_quantity_req.splice(data,1)
  }

  quantityReqChange(){
    if(this.haveQuantity){

    }
    else{
      this.procedure_quantity_req = []
    }
  }
}
