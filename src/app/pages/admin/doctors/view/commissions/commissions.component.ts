import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-commissions',
  templateUrl: './commissions.component.html',
  styleUrls: ['./commissions.component.scss']
})
export class CommissionsComponent implements OnInit {
  key = null
  commission_list = []
  
  constructor(
    private API : ApiService,
    private service: CommonService
    
  ) {
    this.service.doctorKey$.subscribe(res => this.key = res)

   }

  ngOnInit() {
    this.getDoctorCommissionById()
  }

  getDoctorCommissionById(){
    this.API.post("doctors/get-doctor-commission-by-id",
    {
      key : this.key
    },
    ).subscribe(
        (response: any) => {   
          this.commission_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

}
