import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { ApiService } from 'src/app/services/api.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-about',
  templateUrl: 'about.component.html'
})
export class AboutComponent implements OnInit {
  key;
  patient_details;

  aboutInfo: any = {
    name: 'Malinda Hollaway',
    about: 'Pellentesque vitae quam quis tellus dignissim faucibus. Suspendisse mattis felis at faucibus lobortis. Sed sit amet tellus dolor. Fusce quis sollicitudin velit. Praesent gravida ullamcorper lectus et tincidunt. Phasellus lectus quam, porta pharetra feugiat in, auctor eget dolor.',
    aboutMore: 'Vestibulum tincidunt imperdiet egestas. In in nunc vitae elit tincidunt tristique id eu justo. Quisque gravida maximus orci, vulputate pharetra mauris commodo at. Mauris eget fermentum ipsum, quis faucibus neque. Fusce eleifend sapien sit amet convallis rhoncus. Proin commodo lacinia lectus, et tempus turpis.',
    contacts: [
      {
        icon: 'phone',
        value: '308-360-8938'
      },
      {
        icon: 'email',
        value: 'malinda@inbound.plus'
      },
      {
        icon: 'twitter',
        value: '@mallinda-hollaway'
      },
      {
        icon: 'facebook',
        value: 'mallinda-hollaway'
      },
      {
        icon: 'pin',
        value: '5470 Madison Street Severna Park, MD 21146'
      }
    ]
  };

  constructor(
    private route : ActivatedRoute,
    private API : ApiService,
    private service: CommonService
  ) { }

  ngOnInit() {
    this.service.data$.subscribe(res => this.key = res)
    this.getPatientInfo()
  }

  getPatientInfo(){
    this.API.post("patients/get-patient-info-by-id",
    {
      id : this.key
    },
    ).subscribe(
        (response: any) => {   
          this.patient_details = response.devMessage
        },
        (error: any) => {

        },
    )
  }
  

}
