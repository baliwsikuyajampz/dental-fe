import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  key = null;
  doctor_info : any;

  profileInfo: any = {
    name: 'Malinda Hollaway',
    profession: 'Web/UI Developer',
    location: 'Edinburgh, Scotland',
    img: './assets/img/avatar/Blank-Avatar.png',
    summary: 'Cras mattis consectetur purus sit amet fermentum. Maecenas sed diam eget risus varius blandit sit amet non magnae tiam porta sem malesuada magna mollis euismod.',
    contacts: [
        {
            icon: 'phone',
            value: '308-360-8938'
        },
        {
            icon: 'email',
            value: 'malinda@inbound.plus'
        },
        {
            icon: 'twitter',
            value: '@mallinda-hollaway'
        }
    ]
  };

  profileSearch: boolean = false;
  
  constructor(
    private route : ActivatedRoute,
    private API : ApiService,
    private service: CommonService
  ) { 
    this.key = this.route.snapshot.paramMap.get('id');
    this.service.setDentistKey(this.key);  //invoke new Data
  }

  ngOnInit() {
    this.getDoctorDetails()
  }

  getDoctorDetails(){
    this.API.post("doctors/get-doctor-details-by-id",
    {
      id : this.key
    },
    ).subscribe(
        (response: any) => {   
          this.doctor_info = response.devMessage
        },
        (error: any) => {

        },
    )
  }
  

}
