import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import {GrowlService} from 'ngx-growl';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild("modalDoctor") public modalDoctor: ModalDirective;

  pageTitle : string = "Dentist"
  widgetSubTitle : string = "Manage Dentist"

  loggedUserInfo : any = null

  dentist_list = []
  branch_list  = []
  ranks_list  = []

  total_count : number = 0

  searchKey : string = ""

  fname : string = ""
  mname : string = ""
  lname : string = ""
  branch : any = null
  rank : any = null

  key : number = null

  page  = 1
  row   = 10
  mode : string = "add"

  isActive : boolean = true

  constructor(
    private API : ApiService,
    private growlService : GrowlService,
    private common : CommonService
  ) {
    this.loggedUserInfo = this.common.getLoggedInUser()


   }

  ngOnInit() {
    this.getDoctorsList()
    this.getBranches()
    this.getRanks()
  }

  addDoctor(){
    this.clearFields()
    this.modalDoctor.show()
  }

  getDoctorsList(){
    this.API.post("doctors/get",
    {
      page : this.page,
      row  : this.row,
      search : this.searchKey
    },
    ).subscribe(
        (response: any) => {   
          this.dentist_list = response.devMessage
          this.total_count = response.total
        },
        (error: any) => {

        },
    )
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {


    },
    ).subscribe(
        (response: any) => {   
          this.branch_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getRanks(){
    this.API.post("lookups/get-ranks",
    {


    },
    ).subscribe(
        (response: any) => {   
          this.ranks_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  saveDentist(){
    if(isNull(this.branch) || isNull(this.rank) || this.fname == "" || this.lname == ""){
      Swal.fire("Oops","Required Field(s) Missing.","warning")
    }
    else{
      this.API.post("doctors/save",
      {
        mode   : this.mode,
        key    : this.key,
        fname  : this.fname,
        mname  : this.mname,
        lname  : this.lname,
        branch : this.branch,
        rank   : this.rank,
        created_by : this.loggedUserInfo.id,
        is_active : this.isActive
      },
      ).subscribe(
          (response: any) => {   
            this.modalDoctor.hide()
            let msg = this.mode == "add" ? "Record Saved." : "Record Updated"
            this.growlService.addSuccess(msg)
            this.getDoctorsList()
            this.clearFields()
          },
          (error: any) => {
  
          },
      )
    }
    
  }

  clearFields(){
    this.fname = ""
    this.mname = ""
    this.lname = ""
    this.mode = "add"
    this.branch = null
    this.rank = null
    this.key = null
  }

  editDentist(data:any){
    this.key = data.id
    this.mode = "edit"
    this.branch = data.branch_id
    this.rank = data.rank_id
    this.fname = data.fname
    this.mname = data.mname
    this.lname = data.lname
    this.modalDoctor.show()
  }

  pageChange(event: any){
    this.page = event.page
    this.getDoctorsList()
  }
}
