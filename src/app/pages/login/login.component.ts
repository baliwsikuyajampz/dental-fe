import {Component, OnInit} from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import Swal from 'sweetalert2';
import { CommonService } from 'src/app/services/common.service';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

    username : string = "";
    password : string = "";

    loginStats: any = {
        login: true,
        signUp: false,
        forgotPassword: false
    };

    toggleBlock(currentBlock, nextBlock) {
        this.loginStats[currentBlock] = false;
        this.loginStats[nextBlock] = true;
    };

    constructor(
      private API : ApiService,
      private router : Router,
      private common : CommonService
      ) {
    }

    ngOnInit() {
      this.getOnlineUser()
    }

    getOnlineUser(){
      if(localStorage.getItem("user_info") !== null){
        let uInfo = JSON.parse(localStorage.getItem("user_info"))
        console.log("check",uInfo)
        switch(uInfo.role){
          case 1:
            this.router.navigate(['admin'])
            break;
          case 2: 
            this.router.navigate(['head'])
          default :
            break;
        }
      }
    }

    auth(){
      if(this.username == "" || this.password == ""){
        Swal.fire("Oops!","Required field(s) missing.","warning")
      }
      else{
        this.API.post("users/auth",{
          username : this.username,
          password : this.password
        }).subscribe(
              (response: any) => {   
                if(response.statusCode == 200){
                  if(response.devMessage == false){
                    Swal.fire("Oops!","Invalid username/password.","warning")
                  }
                  else{
                    localStorage.setItem("user_info",JSON.stringify(response.devMessage))
                    this.common.setLoggedInUser(response.devMessage);  //invoke new Data

                    switch(response.devMessage.role){
                      case 1:
                        this.router.navigate(['admin'])
                        break;
                      case 2: 
                        this.router.navigate(['head'])
                      default :
                        break;
                    }

                  }
                }
              },
              (error: any) => {
    
              },
        )
      }
    }

}
