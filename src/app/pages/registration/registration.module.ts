import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { RegistrationComponent } from './registration.component';
import { OrthoComponent } from './ortho/ortho.component';
import { FormsModule } from '@angular/forms';

const ROUTE = [
  { path: "", component:  RegistrationComponent},
  { path: "ortho", component:  OrthoComponent}
];

@NgModule({
  declarations: [OrthoComponent,RegistrationComponent],
  imports: [
    RouterModule.forChild(ROUTE),
    CommonModule,
    FormsModule
  ]
})
export class RegistrationModule { }


