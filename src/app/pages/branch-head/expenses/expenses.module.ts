import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { ExpensesComponent } from "./expenses.component";

import { BsDropdownModule } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { PaginationModule } from 'ngx-bootstrap/pagination';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ListComponent } from './list/list.component';

const ROUTE = [
  { 
    path: "", 
    redirectTo : "list"
  },
  {
    path : "list",
    component : ListComponent
  },
];

@NgModule({
    declarations: [
      ExpensesComponent,
      ListComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTE),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        PaginationModule.forRoot(),
        TooltipModule.forRoot(),
        NgSelectModule,
        BsDatepickerModule.forRoot(),
        QuillModule
    ]
})
export class ExpensesModule { }
