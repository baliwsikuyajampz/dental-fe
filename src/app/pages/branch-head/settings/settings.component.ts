import { Component, OnInit } from '@angular/core';
import { sum } from 'chartist';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']

})
export class SettingsComponent implements OnInit {
  num: number;
  str: string;
  arr: Array<any> = [];
  textbox: any;
  textbox1: any;


  constructor(
    private API: ApiService,
  ) {
    
   }

  ngOnInit() {
    this.sampleAPI()
   
  }

  sampleAPI(){
    this.API.post("clients/get-client-list",{
      page: 1,
      rows: 10
    }).subscribe(
          (response: any) => {   
            if (response.statusCode == 200){
              this.arr = response.devMessage
              
            }
          },
          (error: any) => {

          },
    )
  }

  sample(){
    let sum: number = parseInt(this.textbox) + parseInt(this.textbox1)
    
    alert(sum)
    
  }

}

