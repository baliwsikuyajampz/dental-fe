import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { ReportsComponent } from "./reports.component";

import { BsDropdownModule } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { PaginationModule } from 'ngx-bootstrap/pagination';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ProfitAndLostComponent } from './profit-and-lost/profit-and-lost.component';
import { CommissionsComponent } from './commissions/commissions.component';
import { SalesComponent } from './sales/sales.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SearchCommissionsComponent } from './search-commissions/search-commissions.component';

const ROUTE = [
  { 
    path: "", 
    component: ReportsComponent 
  },
  {
    path : "profit-and-loss",
    component : ProfitAndLostComponent
  },
  {
    path : "commissions",
    component : CommissionsComponent
  },
  {
    path : "sales",
    component : SalesComponent
  },
  {
    path : "search-commissions",
    component : SearchCommissionsComponent
  }, 
];

@NgModule({
    declarations: [
      ReportsComponent,
      ProfitAndLostComponent,
      CommissionsComponent,
      SalesComponent,
      SearchCommissionsComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTE),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        PaginationModule.forRoot(),
        TooltipModule.forRoot(),
        NgSelectModule,
        BsDatepickerModule.forRoot(),
        QuillModule,
        TabsModule.forRoot(),

    ]
})
export class ReportsModule { }
