import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import {GrowlService} from 'ngx-growl';

@Component({
  selector: 'app-commissions',
  templateUrl: './commissions.component.html',
  styleUrls: ['./commissions.component.scss']
})
export class CommissionsComponent implements OnInit {
  pageTitle : string = "Commissions"
  widgetSubTitle : string = "Commission Report"

  cutoff_dates = []
  doctors_list = []
  assistant_list = []
  secretary_list = []

  constructor(
    private API : ApiService,
    private growlService : GrowlService
  ) { }

  ngOnInit() {
    this.getCutOffDates()
    this.getDoctorCommissions()
    this.getAssistantsCommissions()
    this.getSecretaryYearCommission()
  }

  getCutOffDates(){
    this.API.post("reports/test",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.cutoff_dates = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getDoctorCommissions(){
    this.API.post("reports/get-commissions",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.doctors_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getAssistantsCommissions(){
    this.API.post("reports/get-assistants-commissions",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.assistant_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getSecretaryYearCommission(){
    this.API.post("reports/get-secretary-commissions",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.secretary_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }
}
