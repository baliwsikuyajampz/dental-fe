import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { GrowlService } from 'ngx-growl';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-profit-and-lost',
  templateUrl: './profit-and-lost.component.html',
  styleUrls: ['./profit-and-lost.component.scss']
})
export class ProfitAndLostComponent implements OnInit {
  pageTitle : string = "Profit and Loss"
  widgetSubTitle : string = ""

  profitAndLoss = []
  expense_categories = [];

  constructor(
    private API : ApiService,
    private growlService: GrowlService
  ) { }

  ngOnInit() {
    this.getProfitAndLoss()
    this.getExpenseCategories()
  }

  getProfitAndLoss(){
    this.API.post("reports/get-profit-and-loss",
    {
      
    },
    ).subscribe(
        (response: any) => {   
          this.profitAndLoss = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getExpenseCategories(){
    this.API.post("lookups/get-expense-categories",
    {
      
    },
    ).subscribe(
        (response: any) => {   
          this.expense_categories = response.devMessage
        },
        (error: any) => {

        },
    )
  }


}
