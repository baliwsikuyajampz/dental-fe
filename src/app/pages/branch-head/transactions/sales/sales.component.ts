import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { isNull } from 'util';
import { GrowlService } from 'ngx-growl';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {
  @ViewChild("modalSales") public modalSales: ModalDirective;
  @ViewChild("modalSearchPatient") public modalSearchPatient: ModalDirective;
  @ViewChild("modalViewSalesDetails") public modalViewSalesDetails: ModalDirective;

  page : number = 1
  rows : number = 10

  patient_list = []
  doctors_list = []
  procedures_list = []
  assistants_list = []
  sales_list = []
  branch_list = []
  secretaries_list = []

  total_sales : number = 0

  patient = {id:null,fullname:""}
  doctor;
  procedure= []
  assistants;
  doctor_fee=0;
  mop = "CASH"
  amount = 0
  branch = null
  date;
  totalProcedurePrice : any = 0.00

  mode = "add"

  //searchPatient Variables
  search_lname : string = ""
  search_fname : string = ""
  search_mname : string = ""

  search_res_list = []

  search_patient  = null
  search_dentist  = null
  search_date  = null
  search_remarks = null

  pageTitle : string = "Sales"
  widgetSubTitle : string = "Manage Sales"

  loggedUserInfo : any;

  viewDetails : null


  constructor(
    private API : ApiService,
    private growlService : GrowlService,
    private common : CommonService
  ) { 
    this.loggedUserInfo = this.common.getLoggedInUser()
    this.branch = this.loggedUserInfo.branch
  }

  ngOnInit() {
    this.getPatientList()
    this.getDoctorList()
    this.getProcedures()
    this.getAssistantsList()
    this.getSalesList()
    this.getBranches()
    this.getSecretariesList()
  }

  addSales(){
    this.modalSales.show()
  }

  getSalesList(opt : string = ''){
    if(opt == 'clear'){
      this.search_patient = null
      this.search_dentist = null
      this.search_date = null
      this.search_remarks = null
    }

    this.API.post("sales/get",
    {
      branch : this.branch,
      rows : this.rows,
      page : this.page,
      search_patient : this.search_patient,
      search_dentist : this.search_dentist,
      search_date : this.search_date,
      search_remarks : this.search_remarks

    },
    ).subscribe(
        (response: any) => {   
          this.sales_list = response.devMessage
          this.total_sales = response.total
        },
        (error: any) => {

        },
    )
  }

  getPatientList(){
    this.API.post("lookups/get-patients",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.patient_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getDoctorList(){
    this.API.post("lookups/get-doctors",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.doctors_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getProcedures(){
    this.API.post("lookups/get-procedures",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.procedures_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getAssistantsList(){
    this.API.post("lookups/get-assistants",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.assistants_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  saveSalesTransaction(){
    if(isNull(this.patient.id)){
      Swal.fire("Oops","Please select a patient","warning")
    }
    else if(isNull(this.branch)){
      Swal.fire("Oops","Please select a branch","warning")
    }
    else if(this.procedure.length==0){
      Swal.fire("Oops","Please select atleast one procedure","warning")
    }
    else{
      this.API.post("sales/save",
      {
        mop           : this.mop,
        branch        : this.branch,
        patient_id    : this.patient.id,
        procedures    : this.procedure,
        total_amount  : this.totalProcedurePrice
      },
      ).subscribe(
          (response: any) => {   
            Swal.fire("Success","Record Saved.","success")
            this.modalSales.hide()
          },
          (error: any) => {
  
          },
      )
    }
  }

  procedureChanged(data){
    this.amount = data.price
    this.staffFee(data)
  }

  staffFee(data){
    this.doctor_fee = (data.commission_rate/100) * this.amount
  }

  getTotalProcedurePrice(){
    console.log(this.procedure)
    this.totalProcedurePrice = 0.00
    for(let i=0;i<this.procedure.length;i++){
      this.totalProcedurePrice = parseFloat(this.totalProcedurePrice) + parseFloat(this.procedure[i].price)
    }
  }

  searchPatient(){
    this.modalSearchPatient.show()
  }

  searchPatientByName(){
    this.API.post("patients/search-patient-by-name",
    {
      lname : this.search_lname,
      fname : this.search_fname,
      mname : this.search_mname,

    },
    ).subscribe(
        (response: any) => {   
          this.search_res_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  selectPatient(data:any){
    this.patient.id = data.id
    this.patient.fullname = data.fullname
    this.modalSearchPatient.hide()
  }

  getBranches(){
    this.API.post("lookups/get-branch-lookup",
    {


    },
    ).subscribe(
        (response: any) => {   
          this.branch_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  getSecretariesList(){
    this.API.post("lookups/get-secretaries",
    {

    },
    ).subscribe(
        (response: any) => {   
          this.secretaries_list = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  voidTransaction(data:any){
    Swal.fire({
      title: "Are you sure?",
      text: "Void Transaction?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then((result) => {
        if(result.value){
          this.API.post("sales/void-transaction",
            {
              id : data.id
            },
            ).subscribe(
                (response: any) => {   
                  if(response.devMessage){
                    this.growlService.addSuccess("Transaction voided.")
                    this.modalViewSalesDetails.hide()
                    this.getSalesList()
                  }
                },
                (error: any) => {
        
                },
            )
        }
      }
    );
  }

    
  pageChange(event: any){
    this.page = event.page
    this.getSalesList()
  }

  viewSalesDetails(data:any){
    this.viewDetails = data
    this.modalViewSalesDetails.show()
  }

}
