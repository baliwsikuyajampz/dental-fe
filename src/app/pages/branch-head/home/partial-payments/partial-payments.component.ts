import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { ApiService } from 'src/app/services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-partial-payments',
  templateUrl: 'partial-payments.component.html'
})
export class PartialPaymentsComponent implements OnInit {
  widgetTitle: string = 'Customer Reservations';
  widgetSubTitle: string = 'List of Customers with reservations';
  todoLists: any[];
  todoListActions: any[];

  partial_payment_list : any = []

  constructor(private service: AppService,private API : ApiService) {
    this.todoLists = service.todoLists;
    this.todoListActions = service.todoListActions;
  }

  ngOnInit() {
  }

  getPartialPaymentList(){
    this.API.post("home/get-partial-payment-list",
      {

      },
    ).subscribe(
        (response: any) => {   
          if(response.statusCode==200){
            this.partial_payment_list = response.devMessage
          }
        },
        (error: any) => {

        },
    )
  }

  markAsDone(data:any){
    Swal.fire({
      title: 'Are you sure?',
      text: "Mark record as done?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if(result.value == true){
        this.API.post("home/mark-as-done",
          {
            id                : data.id,
            remaining_balance : data.remaining_balance
          
          },
        ).subscribe(
            (response: any) => {   
              if(response.statusCode==200){
                Swal.fire("Success","Record marked as done","success")
                this.getPartialPaymentList()
              }
            },
            (error: any) => {
    
            },
        )
      }
    })
  }
}


