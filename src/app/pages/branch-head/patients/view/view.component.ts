import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { CommonService } from 'src/app/services/common.service';
import { GrowlService } from 'ngx-growl';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  key;
  patient_info;

  itemFormData = new FormData();
  itemFormDataFlag : boolean = false;

  profileInfo: any = {
    name: 'Malinda Hollaway',
    profession: 'Web/UI Developer',
    location: 'Edinburgh, Scotland',
    img: './assets/demo/img/contacts/2.jpg',
    summary: 'Cras mattis consectetur purus sit amet fermentum. Maecenas sed diam eget risus varius blandit sit amet non magnae tiam porta sem malesuada magna mollis euismod.',
    contacts: [
        {
            icon: 'phone',
            value: '308-360-8938'
        },
        {
            icon: 'email',
            value: 'malinda@inbound.plus'
        },
        {
            icon: 'twitter',
            value: '@mallinda-hollaway'
        }
    ]
  };

  profileSearch: boolean = false;
  
  constructor(
    private route : ActivatedRoute,
    private API : ApiService,
    private service: CommonService,
    private growlService : GrowlService
  ) { }

  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('id');
    this.service.changeData(this.key);  //invoke new Data
    this.getPatientDetails()
  }

  getPatientDetails(){
    this.API.post("patients/get-patient-by-id",
    {
      id : this.key
    },
    ).subscribe(
        (response: any) => {   
          this.patient_info = response.devMessage
        },
        (error: any) => {

        },
    )
  }

  async handleFileUploadItem(event: any) {
    let file = event.target.files[0],
        formData = new FormData();

      this.itemFormData.append('file', file); 
      this.itemFormData.append('module', 'patients'); 
      this.itemFormData.append('key', this.key); 

      this.API.post_form_data("uploads/upload-attachment",
        this.itemFormData,
        ).subscribe(
            (response: any) => {   
              if(response.statusCode == 200){
                this.API.post("patients/save_upload_attachment",
                  {
                    patient_id : this.key,
                    file_name : response.devMessage
                  },
                  ).subscribe(
                      (response: any) => {   
                        this.itemFormDataFlag = false
                        this.growlService.addSuccess("File Uploaded.")
                      },
                      (error: any) => {
              
                      },
                  )

 
              }
            },
            (error: any) => {
    
            },
        )
  }
  
  async handleProfileFileUploadItem(event: any) {
    let file = event.target.files[0],
        formData = new FormData();

      this.itemFormData.append('file', file); 
      this.itemFormData.append('module', 'patients'); 
      this.itemFormData.append('key', this.key); 

      this.API.post_form_data("uploads/upload-attachment",
        this.itemFormData,
        ).subscribe(
            (response: any) => {   
              if(response.statusCode == 200){
                this.API.post("patients/save_upload_profile",
                  {
                    patient_id : this.key,
                    file_name : response.devMessage
                  },
                  ).subscribe(
                      (response: any) => {   
                        this.itemFormDataFlag = false
                        this.growlService.addSuccess("Profile Image Uploaded.")
                        this.getPatientDetails()
                      },
                      (error: any) => {
              
                      },
                  )

 
              }
            },
            (error: any) => {
    
            },
        )
  }
  
  uploadPatientImg(){
    document.getElementById("uploadItemAttachment").click()
  }

  uploadProfileAttachment(){
    document.getElementById("uploadProfileAttachment").click()
  }

}
